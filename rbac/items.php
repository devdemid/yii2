<?php
return [
    'root' => [
        'type' => 1,
        'description' => 'Разработчик',
        'children' => [
            'editSettings',
            'createSettings',
            'editAccess',
            'changePassword',
            'lockingProfiles',
            'changeProfileGroup',
            'accessProfile',
            'accessSliders',
            'accessSettingForm',
            'readingResultForm',
            'accessNews',
            'accessNewsFeed',
            'accessPage',
            'accessBlock',
            'accessMenu',
            'accessSeo',
        ],
    ],
    'admin' => [
        'type' => 1,
        'description' => 'Администратор',
        'children' => [
            'editAccess',
            'editSettings',
            'accessSliders',
            'client',
        ],
    ],
    'client' => [
        'type' => 1,
        'description' => 'Клиент',
    ],
    'guest' => [
        'type' => 1,
        'description' => 'Гость',
    ],
    'editSettings' => [
        'type' => 2,
        'description' => 'Доступ к настройкам системы',
    ],
    'createSettings' => [
        'type' => 2,
        'description' => 'Создавать настройки',
    ],
    'editAccess' => [
        'type' => 2,
        'description' => 'Доступ к групам пользователей',
    ],
    'changePassword' => [
        'type' => 2,
        'description' => 'Смена пароля учетной записи',
    ],
    'lockingProfiles' => [
        'type' => 2,
        'description' => 'Блокировка учетной записи',
    ],
    'changeProfileGroup' => [
        'type' => 2,
        'description' => 'Изменить группу учетной записи',
    ],
    'accessProfile' => [
        'type' => 2,
        'description' => 'Доступ к профилю учетной записи',
    ],
    'accessSliders' => [
        'type' => 2,
        'description' => 'Доступ к генератору слайдеров',
    ],
    'accessSettingForm' => [
        'type' => 2,
        'description' => 'Доступ к настройкам форм',
    ],
    'readingResultForm' => [
        'type' => 2,
        'description' => 'Доступ к результатам форм',
    ],
    'accessNews' => [
        'type' => 2,
        'description' => 'Доступ к новостям',
    ],
    'accessNewsFeed' => [
        'type' => 2,
        'description' => 'Доступ к новостным лентам',
    ],
    'accessPage' => [
        'type' => 2,
        'description' => 'Доступ к статическим страницам',
    ],
    'accessBlock' => [
        'type' => 2,
        'description' => 'Доступ к управлением блоками',
    ],
    'accessMenu' => [
        'type' => 2,
        'description' => 'Доступ к конструктору меню',
    ],
    'accessSeo' => [
        'type' => 2,
        'description' => 'Доступ к настройкам SEO',
    ],
];
