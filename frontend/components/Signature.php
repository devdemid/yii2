<?php

/**
 * Signature Component
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 *
 */

namespace frontend\components;

use Yii;

class Signature extends \yii\base\Component {

    /**
     * Salt for signing the redirect
     */
    const DEFAULT_SIG_SAILT = '3sdfsdf1dba';

    /**
     * Generating a signature
     * @param  String $signature
     * @return String
     */
    public function generate($string) {
        return md5(
              Yii::$app->request->userAgent
            . Yii::$app->request->userIP
            . $string
            . self::DEFAULT_SIG_SAILT
        );
    }

    /**
     * Signature verification
     * @param  String $signature
     * @param  array  $string
     * @return Bollean
     */
    public function verification($signature, $string) {
        return $signature === $this->generate($string);
    }
}
