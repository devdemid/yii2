<?php

/**
 * Redirect Component
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 *
 * Note:base64: ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=
 */

namespace frontend\components;

use Yii;
use yii\base\Component;
use \yii\validators\UrlValidator;

class Redirect extends Component {

    /**
     * Salt for signing the redirect
     */
    const DEFAULT_SIG_SAILT = '359fc571dba';

    /**
     * Controller name for link processing
     */
    const CONTROLLER_NAME = 'redirect';

    /**
     * Replacement of prohibited characters for url
     */
    const BASE64_NOT_URLSAFE = '+/';
    const BASE64_URLSAFE = '_-';

    /**
     * Separator for lists
     */
    const LIST_SEPARATOR = '$';

    /**
     * Signature Generator
     * @param  [string] $url
     * @return [string]
     */
    public static function generateSignature($url) {
        return hash('crc32b', $url . self::DEFAULT_SIG_SAILT);
    }

    /**
     * Validation of the link and signature
     * @param  [string] $hash
     * @param  [string] $url
     * @return [boolean]
     */
    public static function validate($hash, $url) {
        return (new UrlValidator)->validate($url) && $hash === static::generateSignature($url);
    }

    /**
     * Generating a redirect link
     * @param  [string] $url
     * @return [string]
     */
    public static function generateBase64Url($url) {
        $url = strtr(
            base64_encode(static::generateSignature($url) . self::LIST_SEPARATOR . $url),
            self::BASE64_NOT_URLSAFE,
            self::BASE64_URLSAFE
        );

        return '//' . Yii::$app->request->serverName . '/' . self::CONTROLLER_NAME . '=' . $url;
    }

    /**
     * Receiving data from base64_url
     * @param  [string] $base64_url
     * @return [string]
     */
    public static function getUrlBase64($base64_url) {
        return base64_decode(strtr(
            $base64_url,
            self::BASE64_URLSAFE,
            self::BASE64_NOT_URLSAFE
        ), true);
    }
}