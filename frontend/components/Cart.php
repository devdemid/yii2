<?php

/**
 * Shopping Cart
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0.1
 */

namespace frontend\components;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use common\models\Tours;
use common\models\TourPrices;

class Cart extends \yii\base\Component
{

    /**
     * Impurity for signature
     */
    const DEFAULT_SIG_SAILT = 'S45YER()*&(^&%%';

    /**
     * Parameter name in session
     */
    const SESSION_KEY_NAME = 'cart';

    /**
     * Changes in the shopping cart
     */
    const EVENT_CHANGE_CART = 'cart.change';

    /**
     * Removing items from the shopping cart
     */
    const EVENT_REMOVE_ITEM = 'cart.item.remove';

    /**
     * Adding items to shopping carts
     */
    const EVENT_ADD_ITEM = 'cart.item.add';

    /**
     * Sailt key
     * @var String
     */
    public $key;

    /**
     * Basket of orders
     * @var Array
     */
    private $cart = [];

    /**
     * @inheritdoc
     */
    public function __construct($config = [])
    {
        if (in_array('key', $config)) {
            $this->key = ArrayHelper::getValue($config, 'key', self::DEFAULT_SIG_SAILT);
        }

        // Save the copy in the cookies
        $this->on(self::EVENT_CHANGE_CART, function($event) {
            $cookies = Yii::$app->response->cookies;

            if (is_array($this->cart)) {
                $cookies->add(new \yii\web\Cookie([
                    'name' => self::SESSION_KEY_NAME,
                    'value' => Json::encode($this->cart),
                ]));
            }
        });
    }

    /**
     * Check Signature
     * @param  Integer  $id
     * @param  String $key
     * @return Boolean
     */
    public function checkSignature($id, $key)
    {
        return $this->generateSignature($id) === $key;
    }

    /**
     * Get Signature
     * @param  Integer $id
     * @return String
     */
    public function generateSignature($id)
    {
        return md5($id . $this->key . self::DEFAULT_SIG_SAILT . Yii::$app->session->id);
    }

    /**
     * Add item to cart
     * @param Integer $id
     * @param Array  $options
     * @return  Null|Integer
     */
    public function add($id, $options = [])
    {
        $session = Yii::$app->session;

        if ($session->isActive) {
            if ($session->has(self::SESSION_KEY_NAME)) {
                $this->cart = $session->get(self::SESSION_KEY_NAME);
            }

            if (!is_array($this->cart)) {
                $this->cart = [];
            }

            $this->cart[$id] = $options;
            $session->set(self::SESSION_KEY_NAME, $this->cart);

            // Change event
            $this->trigger(self::EVENT_CHANGE_CART);
            $this->trigger(self::EVENT_ADD_ITEM);
            return true;
        }
        return false;
    }

    /**
     * Remove item to cart
     * @param  [type] $id [description]
     * @return Boolean
     */
    public function remove($id)
    {
        // Load cart
        $this->getRecord();

        if (is_array($this->cart) && array_key_exists($id, $this->cart)) {
            unset($this->cart[$id]);

            $session = Yii::$app->session;
            $session->set(self::SESSION_KEY_NAME, $this->cart);

            // Change event
            $this->trigger(self::EVENT_CHANGE_CART);
            $this->trigger(self::EVENT_REMOVE_ITEM);
            return true;
        }
        return false;
    }

    /**
     * Get the item from the cart
     * @return Array
     */
    public function getRecord()
    {
        if (!count($this->cart)) {
            $session = Yii::$app->session;
            $cookies = Yii::$app->request->cookies;

            if ($session->isActive) {
                if ($session->has(self::SESSION_KEY_NAME)) {
                    $this->cart = $session->get(self::SESSION_KEY_NAME);
                }
            }

            // if (!count($this->cart) && $cookies->has(self::SESSION_KEY_NAME)) {
            //     $cart = Json::decode($cookies->getValue(self::SESSION_KEY_NAME));
            //     if (is_array($cart) && count($cart)) {
            //         $this->cart = $cart;
            //         $session->set(self::SESSION_KEY_NAME, $this->cart);
            //     }
            // }
        }

        return $this->cart;
    }

    /**
     * Number of items in the cart
     * @return Integer
     */
    public function getCount() {
        return count($this->record);
    }

    /**
     * Empty trash
     * @return Boolean
     */
    public function clear() {
        $this->cart = [];

        $session = Yii::$app->session;
        if ($session->has(self::SESSION_KEY_NAME)) {
            $session->set(self::SESSION_KEY_NAME, $this->cart);
        }

        // Change event
        $this->trigger(self::EVENT_CHANGE_CART);
    }

    /**
     * [getLinkCart description]
     * @return String
     */
    public function getLinkCart() {
        return '/cart';
    }

    public function getLinkOrdering() {
        return '/ordering';
    }
}
