<?php

/**
 * Сontroller for convenience
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace frontend\components;

use common\models\ClientSession;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\helpers\ArrayHelper;

class FrontendController extends Controller {

    /**
     * Page description in the head of the page
     * @var string
     */
    public $metaDescription;

    /**
     * Page Keywords in the head of the page
     * @var string
     */
    public $metaKeywords;

    /**
     * Disable indexing
     * @var boolean
     */
    public $noindex = false;

    /**
     * Bread crumbs
     * @var array
     */
    public $breadcrumbs = [];

    /**
     * Bread crumbs manu
     * @var array
     */
    public $actionsLink = [];

    /**
     * Body class
     * @var String
     */
    public $body_class = 'light';

    /**
     * Template wrapper
     */
    const LAYOUT_WRAPPER_NAME = 'wrap';

    /**
     * @inheritdoc
     */
    public function beforeAction($action) {
        Yii::$app->session->open();

        // If the site is off we send on the offline
        if(Yii::$app->options->get('offline')) {
            if(Yii::$app->request->url == '/') {
                $this->layout = self::LAYOUT_WRAPPER_NAME;
                $this->action->actionMethod = 'actionOffline';
            } else $this->redirect('/');
        }

        // Begin Setting Metadata from Settings
        if(is_null($this->view->title)) {
            $this->view->title = Yii::$app->options->get('home_title');
        }

        if(is_null($this->metaDescription)) {
            $this->metaDescription = Yii::$app->options->get('meta_description');
        }

        if(is_null($this->metaKeywords)) {
            $this->metaKeywords = Yii::$app->options->get('meta_keywords');
        }
        // End Setting Metadata from Settings

        return parent::beforeAction($action);
    }

    private $latest_courses = [];

    /**
     * Get latest coin course
     * @param  string $coin
     * @return string
     */
    public function getLatestCourse($coin = null) {

        if(!count($this->latest_courses)) {
            // Get courses
            $courses = file_get_contents('http://m.cryptotrade.lan/ru/api/latest-courses', false);
            if($courses) {
                $this->latest_courses = json_decode($courses, true);
            }
        }

        return ArrayHelper::getValue($this->latest_courses, 'latest_courses.' . strtolower($coin), 'Unkown');
    }

    /**
     * By default, the first stage distribute access
     * @return array
     */
    public function behaviors() {
        return [
            // 'access' => [
            //     'class' => AccessControl::className(),
            //     //'only' => ['index', 'playlist', 'program', 'genres', 'auth'],
            //     'rules' => [
            //         [
            //             'actions' => ['index', 'error', 'offline', 'captcha', 'request'],
            //             'allow' => true,
            //             'roles' => ['?'],
            //         ],
            //         [
            //             'allow' => true,
            //             'roles' => ['@'],
            //         ],
            //     ],
            // ],
        ];
    }

    /**
     * Check controller
     * @param  String $name
     * @return Boolean
     */
    public function isController($name) {
        return Yii::$app->controller->id == $name;
    }
}