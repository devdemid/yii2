<?php

/**
 * HTML Builder
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0.1
 */

namespace frontend\components;

use frontend\components\Redirect;
use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;

class HTMLBuild extends Component {

    /**
     * Expressions for finding short codes
     */
    const PATTERN_SHORTCODE = '#{shortcode="(?<name>[A-z]+)"\s?(?<options>.*?)}#si';

    /**
     * Expressions for finding link codes
     */
    const PATTERN_LINKCODE = '#{linkcode="(?<model>[A-z]+)"\s?id="(?<id>[0-9]+)"}#i';

    /**
     * Expressions for hookcode
     */
    const PATTERN_HOOKCODE = '#{hookcode="(?<name[A-z0-9\-\_]+>)"}#i';

    /**
     * Expressions for arguments
     */
    const PATTERN_ARGUMENTS = '#\s?(?<label>[A-z0-9\_]+)="(?<param>[A-z0-9\-\_\s?]+)"#i';

    /**
     * Expressions for css tag style
     */
    const PATTERN_TAG_STYLE = '#(<style[\s\S]*?>)(.*?)(</style>)#s';

    /**
     * Expressions for attribute style
     */
    const PATTERN_ATTRIBUTE_STYLE = '##s';

    /**
     * Expressions for css tag script
     */
    const PATTERN_TAG_SCRIPT = '#(<script[\s\S]*?>)(.*?)(</script>)#s';

    /**
     * Expressions for html comments
     */
    const PATTERN_HTML_COMMENTS = '#\s*<!--(?!\[if\s).*?-->\s*|(?<!\>)\n+(?=\<[^!])#s';

    /**
     * Expressions for css comments
     */
    const PATTERN_CSS_COMMENTS = '#(/\*.*?\*/)#s';

    /**
     * Expressions for JavaScript comments
     */
    const PATTERN_JAVASCRIPT_COMMENTS = "#(//\s.*?\n|/\*.*?\*/)#s";

    /**
     * Expressions for clean up codes
     */
    const PATTERN_CLEAN_UP = '#{[shortcode|linkcode|hookcode](.*?)}#si';

    /**
     * Expressions for earch for third-party links
     */
    const PATTERN_OTHER_LINKS = '#(<a\shref=")([http|https|ftp|file].*?)(".*?>)#i';

    /**
     * Original content
     * @var [string]
     */
    //private $old_content;

    /**
     * Changed content
     * @var [string]
     */
    private $content;

    /**
     * [__construct description]
     * @param [string] $content
     * @param array  $config
     */
    public function __construct($content, $config = []) {
        // Set variables
        if ($content) {
            //$this->old_content = $content;
            $this->content = $content;
            unset($content);
        }

        parent::__construct($config);
    }

    /**
     * [init description]
     */
    public function init() {
        if (!$this->isTextHtml() || trim($this->content) === "") {
            return false;
        }

        // Processing of short codes
        $this->initShortcode();

        // Modular Link Processing
        $this->initLinkcode();

        // Remove comments
        if (Yii::$app->options->get('remove_comments')) {

            // Remove HTML,CSS,JS comment(s) except IE comment(s)
            $this->content = preg_replace([
                self::PATTERN_HTML_COMMENTS,
                self::PATTERN_CSS_COMMENTS,
                self::PATTERN_JAVASCRIPT_COMMENTS,
            ], ['', '', "\n"], $this->content);
        }

        // Cleaning codes
        $this->content = preg_replace(self::PATTERN_CLEAN_UP, '', $this->content);

        // Merge spaces
        if (Yii::$app->options->get('collapse_whitespace')) {
            $this->content = str_replace(["\t", "\n", "\r", "\n\r", "\x0B", "&nbsp;", "  "], '', $this->content);
        }

        // Finding third-party links and replacing them with a redirect
        if (Yii::$app->options->get('redirect_url')) {
            $this->content = preg_replace_callback(self::PATTERN_OTHER_LINKS, function ($matches) {

                // Check for third-party domain
                $domain = parse_url($matches[2], PHP_URL_HOST);
                if (!is_null($domain) && $domain != Yii::$app->request->serverName) {
                    return $matches[1] . Redirect::generateBase64Url($matches[2]) . $matches[3];
                }

                return $matches[0];

            }, $this->content);
        }

        // Gzip content compression
        if (Yii::$app->options->get('gzip_content')) {
            $this->initGZipContent();
        }

        parent::init();
    }

    /**
     * Browser Support GZip Compression
     * @return boolean
     */
    private function isBrowserSupportGZip() {
        return substr_count(Yii::$app->request->headers->get('accept-encoding'), 'gzip');
    }

    /**
     * Check output text/html
     * @return boolean
     */
    private function isTextHtml() {
        return substr_count(Yii::$app->response->headers->get('content-type'), 'text/html');
    }

    /**
     * Retrieving content
     * @return [string]
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * Support for short module codes
     */
    private function initShortcode() {
        $this->content = preg_replace_callback(self::PATTERN_SHORTCODE, function ($matches) {

            $classShortcode = $matches['name'] . 'Shortcode';
            $pathShortcodes = Yii::getAlias('@frontend/shortcodes/' . $classShortcode . '.php');

            // Check the existence of the widget
            if (file_exists($pathShortcodes)) {
                $shortcode = Yii::createObject([
                    'class' => 'frontend\shortcodes\\' . $classShortcode,
                ]);

                // Get all the parameters for the widget
                $options = [];
                if (preg_match_all(self::PATTERN_ARGUMENTS, $matches['options'], $args, PREG_SET_ORDER)) {
                    foreach ($args as $item) {
                        if($shortcode->canGetProperty($item['label']))
                            $options[$item['label']] = trim($item['param']);
                    }
                }

                unset($args, $matches, $classShortcode, $pathShortcodes);

                return $shortcode::widget($options);

            }

            // Default return value
            return '';

        }, $this->content);
    }

    /**
     * Support for modular dynamic links
     */
    private function initLinkcode() {
        if (preg_match_all(self::PATTERN_LINKCODE, $this->content, $matches, PREG_SET_ORDER)) {
            $modules = [];

            // Group by model for one query
            foreach ($matches as $item) {
                $modules[$item['model']][] = $item['id'];
            }

            // Initializing models to retrieve records
            foreach ($modules as $model_name => $id_records) {
                if (file_exists(Yii::getAlias('@common/models/' . $model_name . '.php'))) {
                    $ActiveRecordModel = Yii::createObject([
                        'class' => '\common\models\\' . $model_name,
                    ]);

                    $modules[$model_name] = $ActiveRecordModel::find()
                        ->select(['id', 'alias', 'published'])
                        ->where(['id' => $id_records])
                        ->andWhere(['published' => $ActiveRecordModel::PUBLISHED_ON])
                        ->indexBy('id')
                        ->all();

                    unset($ActiveRecordModel, $model_name);
                }
            }

            // Replacement linkcode for links
            $this->content = preg_replace_callback(self::PATTERN_LINKCODE, function ($matches) use ($modules) {
                if (ArrayHelper::keyExists($matches['model'], $modules, false)) {
                    // Get model recored
                    $model = ArrayHelper::getValue($modules, $matches['model'] . '.' . $matches['id'], null);

                    if (!is_null($model) && is_object($model)) {
                        return $model->link;
                    }

                }

                // Default return value
                return '';

            }, $this->content);

            unset($matches, $modules);
        }
    }

    /**
     * Support for hooks
     */
    private function initHookcode() {}

    /**
     * Gzip content compression
     * @return [boolean]
     */
    private function initGZipContent() {
        if ($this->isBrowserSupportGZip()) {
            $this->content = gzencode($this->content);

            // Add GZip headers
            //Yii::$app->response->headers->add('Content-encoding', 'gzip');
            //Yii::$app->response->headers->add('Vary', 'accept-encoding');
            //Yii::$app->response->headers->add('Content-length', mb_strlen($this->content));
            //yii\web\Response::sendHeaders().
            header('Content-encoding: gzip');
            header('Vary: accept-encoding');
            //header('Content-length: ' . mb_strlen($this->content));
            return true;
        }

        return false;
    }

    private function initCssOptimize($input) {
        return $input;
    }

    private function initJavaScriptOptimize($input) {
        return $input;
    }




    /**
     * Combine all JavaScript scripts into one file
     * @return string
     */
    private function combineJavaScript() {}

    /**
     * Combine all CSS styles into one file
     * @return string
     */
    private function combineCss() {}

    /**
     * Minimize JavaScript
     * @return string
     */
    private function minifyJavaScript() {}

    /**
     * Minimize CSS
     * @return string
     */
    private function minifyCss() {}

    /**
     * Delayed image uploading
     * @return string
     */
    private function lazyloadImages() {}

    /**
     * Optimize CSS in style attributes
     * @return string
     */
    private function rewriteStyleAttributesWithUrl() {}

    /**
     * Delete extra quotes
     * @return string
     */
    private function removeQuotes() {}
}