<?php

/**
 * Connecting the NC files of the system
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace frontend\assets;

use yii\web\AssetBundle;

class NcAsset extends AssetBundle {

    public $sourcePath = '@frontend/nc-asset/';

    public $publishOptions = [
        'forceCopy' => false,
    ];

    public $css = [
        'css/nc-styles.css'
    ];

    public $js = [
        'js/nc.js',
        'js/nc-common.js',
        'js/nc-shop.js?dev'
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];

    public function init() {
        parent::init();

        foreach ($this->js as $i => $item) {
            $this->js[$i] = str_replace('?dev' , (YII_DEV ? '?dev=' . date('U') : ''), $item);
        }

        foreach ($this->css as $i => $item) {
            $this->css[$i] = str_replace('?dev' , (YII_DEV ? '?dev=' . date('U') : ''), $item);
        }
    }
}
