<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class ltIEAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $jsOptions = [
        'condition' => 'lt IE 9'
    ];

    public $js = [
        '/js/html5shiv.min.js',
        '/js/respond.min.js'
    ];
}