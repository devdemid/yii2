<?php

/**
 * Connecting template files
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace frontend\assets;

use yii\web\AssetBundle;


class FrontendAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';

    // public $jsOptions = [
    //     'position' => \yii\web\View::POS_HEAD
    // ];

    public $cssOptions = [
        'noscript' => false
    ];

    public $css = [
        '/css/font-awesome.min.css?dev',
        '/css/bootstrap.min.css?dev',
        '/css/magnific-popup.css?dev',
        '/css/select2.min.css?dev',
        '/css/style.css?dev',
        '/css/skins/blue.css?dev',
        '/css/styleswitcher.css?dev',
        '/css/customize.css?dev'
    ];

    public $js = [
        '/js/modernizr.js?dev',
        'js/bootstrap.min.js?dev',
        '/js/select2.min.js?dev',
        '/js/jquery.magnific-popup.min.js?dev',
        '/js/custom.js?dev',
        '/js/styleswitcher.js?dev',
        '/js/common.js?dev'
    ];

    public $depends = [
        //'frontend\assets\ltIEAsset', // Fix IE 9
        //'frontend\assets\GoogleFonts',
        //'yii\web\YiiAsset',
        //'yii\web\JqueryAsset',
        //'yii\bootstrap\BootstrapAsset',
        'frontend\assets\NcAsset'
    ];

    public function init() {
        parent::init();

        foreach ($this->js as $i => $item) {
            $this->js[$i] = str_replace('?dev' , (YII_DEV ? '?dev=' . date('U') : ''), $item);
        }

        foreach ($this->css as $i => $item) {
            $this->css[$i] = str_replace('?dev' , (YII_DEV ? '?dev=' . date('U') : ''), $item);
        }
    }
}
