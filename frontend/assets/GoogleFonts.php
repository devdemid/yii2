<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class GoogleFonts extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'https://fonts.googleapis.com/css?family=Gochi+Hand|Lato:300,400|Montserrat:400,400i,700,700i'
    ];
}