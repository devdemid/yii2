/**
 * Add product to cart
 * @param String sig
 * @param Integer id
 * @param Object el
 */
function addToCart(sig, id, el) {
    $(el).addClass('btn_processing');
    $.get('/cart/add', {sig: sig, id, id}, function(Response) {
        $(el).removeClass('btn_processing');
        if(Response.body) {
            $('#header-cart').html(Response.body);
        }
        if(Response.modal) {
            var $modal = $('#base-modal');
                $modal.find('.modal-content').html(Response.modal);
                $modal.modal();
        }
    });
    return false;
}

/**
 * Remove item from cart
 * @param String sig
 * @param Integer id
 * @param Object el
 */
function removeItemCart(sig, id, el) {
    $.get('/cart/remove', {sig: sig, id, id}, function(Response) {
        if(Response.count > 0) {
            $('[data-cart-id='+ id +']').remove();
            $('.-cart-count').text(Response.count);
        } else {
            $('#base-modal').modal('hide');
        }
        if(Response.body) {
            $('#header-cart').html(Response.body);
        }
    });
    return false;
}

/**
 * Preliminary booking tour
 * @param  Integer id
 * @param  Object el
 * @return boolean
 */
function tourBooking(id, name) {
    var $reservation = $('#reservation');
    $reservation.find('#formmodel-id-2').val(id);
    $reservation.find('#formmodel-tour-2').val(name);
    $reservation.modal();
    return false;
}

/**
 * Total order value
 * @return String
 */
function totalAmount() {
    var total = 0;
    $('[data-product]').each(function() {
        var $this = $(this);
        var adults = parseInt($('input[data-attr="adults"]', $this).val()) || 0;
        var children = parseInt($('input[data-attr="children"]', $this).val()) || 0;
        var adultsPrice = parseInt($('input[data-adults-price]:checked', $this).data('adults-price')) || 0;
        var childrenPrice = parseInt($('input[data-children-price]:checked', $this).data('children-price')) || 0;
        total = total + (adults*adultsPrice) + (children*childrenPrice);
        $('#total-amount').text(total.toFixed(2) + '₽');
    });
    return false;
}

$().ready(function() {
    if($('[data-product]').length) {
        totalAmount();
        $('[data-product] .icheck').on('ifChecked ifUnchecked', function (event) {
            $(event.target).change();
            console.log('change');
        });
    }
});