'use strict';

;(function($, document, undefined) {

    // Faq list
    $('div[id^=question-]').click(function(Event) {
        var $this = $(this),
            $name = $('> .faq-name', $this),
            $content = $this.next('> .faq-content', $this);

        if(!$this.hasClass('open')) {
            $this.addClass('open');
        } else {
            $this.removeClass('open');
        }
    });

})(jQuery, document);