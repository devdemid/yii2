'use strict';

/**
 * Updates images of captcha
 * @param  {object} img
 * @param  {string} controller
 * @return {boolean}
 */
function captchaRefresh(img, controller) {
    $.get('/' + controller + '/captcha', {refresh: 1}, function(Response) {
        $('#' + img.id).attr('src', Response.url);
    }, 'json');
    return false;
}

/**
 * Processing ajax request
 * @param  {object} form
 * @param  {string} return_id
 * @param  {Function} callback
 * @return {boolean}
 */
function requestForm(form, return_id, processing, callback) {
    var $form = $(form);

    // Default class
    if($.type(processing) == 'undefined') {
        processing = 'ajax-proccesing';
    }

    $.ajax({
        cache: false,
        contentType: false,
        data: new FormData(form) || $form.serialize(),
        dataType: 'json',
        processData:false,
        type: $form.attr('method'),
        url: $form.attr('action'),
        beforeSend: function(xhr, opts) {
            $(return_id).addClass(processing);
            $('body').addClass(processing); // Custom
        },
        success: function(Response) {
            $(return_id).html(Response.body).removeClass(processing);
            $('body').removeClass(processing); // Custom
            if($.isFunction(callback)) {
                callback.call(this, Response);
            }

            // Reload mask
            // https://github.com/igorescobar/jQuery-Mask-Plugin
            if($.isFunction($.fn.mask)) {
                $('input[data-mask]', $form).each(function() {
                    var $this = $(this);
                    $this.mask($this.data('mask'));
                });
            }
        }
    });

    return false;
}

/**
 * [formResultModal description]
 * @param  {string} message
 * @return boolean
 */
function formResultModal(title, message) {
    var $modal = $('#form-modal');

    $('.modal-title', $modal).html(title);
    $('.modal-body', $modal).html(message);
    $modal.modal('show');

    //alert(message);
    return false;
}

/**
 * [ajaxPagination description]
 * @param  String   url
 * @param  Object   data
 * @param  String   return_id
 * @param  String   pagination_id
 * @param  String   processing
 * @param  Function callback
 * @return Boolean
 */
function ajaxPagination(url, return_id, _this, processing, callback) {
    var $this = $(_this);

    // Default class
    if($.type(processing) == 'undefined') {
        processing = 'ajax-proccesing';
    }

    $.ajax({
        cache: false,
        contentType: false,
        data: {
            'page': $this.attr('data-next-page')
        },
        dataType: 'json',
        type: 'GET',
        url: url,
        beforeSend: function(xhr, opts) {
            $(return_id).addClass(processing);
            $('body').addClass(processing); // Custom
        },
        success: function(Response) {
            if(Response.nextPage == false) {
                $this.addClass('hidden');
            } else {
                $this.attr('data-next-page', Response.nextPage);
            }

            $(return_id).append(Response.body).removeClass(processing);
            $('body').removeClass(processing); // Custom
            if($.isFunction(callback)) {
                callback.call(this, Response);
            }
        }
    });

    return false;
}