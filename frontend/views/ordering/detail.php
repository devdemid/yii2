<section id="hero_2">
    <div class="intro_title animated fadeInDown">
        <h1>Оформленная покупка</h1>
        <p>Резюме совершенной покупки #ID <?= $model->id ?></p>
    </div>
</section>

<main>
    <div class="container margin_60">
        <div class="row">
            <div class="col-md-12 add_bottom_15">
                <? if(count($model->orderData)): ?>
                <? foreach($model->orderData as $data): ?>
                    <div class="form_title">
                        <h3><strong><i class="icon-tag-1"></i></strong><?= $data->name ?></h3>
                        <p>Номер заказа: #ID<?= $model->id ?></p>
                    </div>
                    <div class="step">
                        <table class="table confirm">
                            <thead>
                                <tr>
                                    <th colspan="2"><?= $data->name ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><strong>Дата начала</strong></td>
                                    <td><?= date($data::OUT_DATE_FORMAT, $data->start_date_timestamp) ?></td>
                                </tr>
                                <tr>
                                  <td><strong>Дата окончания</strong></td>
                                  <td><?= date($data::OUT_DATE_FORMAT, $data->end_date_timestamp) ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Имя оформителя</strong></td>
                                    <td><?= $model->user->nameTogether ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Кол-во взрослых</strong></td>
                                    <td><?= $data->adults ?></td>
                                </tr>
                                <? if($data->children): ?>
                                <tr>
                                    <td><strong>Кол-во детей</strong></td>
                                    <td><?= $data->children ?></td>
                                </tr>
                                <? endif; ?>
                                <tr>
                                    <td><strong>Способ оплаты</strong></td>
                                    <td><?= $model::getPaymentName($model->payment_type) ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Статус</strong></td>
                                    <td><?= $model::getStatusName($model->status) ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                <? endforeach; ?>
                <? endif; ?>
                <br>
                <div class="text-right">
                    <h4>Стоимость заказа: <?= $model->totalPriceFormat ?></h4>
                </div>
            </div>
            <!--End col-md-8 -->
        </div>
        <!--End row -->
    </div>
    <!--End container -->
</main>
<!-- End main -->
