<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\TourPrices;
?>
<section id="hero_2">
    <div class="intro_title animated fadeInDown">
        <h1>Оформление</h1>
        <div class="bs-wizard">
            <div class="col-xs-4 bs-wizard-step complete">
                <div class="text-center bs-wizard-stepnum">Корзина</div>
                <div class="progress">
                    <div class="progress-bar"></div>
                </div>
                <a href="/cart" class="bs-wizard-dot"></a>
            </div>
            <div class="col-xs-4 bs-wizard-step active">
                <div class="text-center bs-wizard-stepnum">Оформление</div>
                <div class="progress">
                    <div class="progress-bar"></div>
                </div>
                <a href="/ordering" class="bs-wizard-dot"></a>
            </div>
            <div class="col-xs-4 bs-wizard-step disabled">
                <div class="text-center bs-wizard-stepnum">Готово!</div>
                <div class="progress">
                    <div class="progress-bar"></div>
                </div>
                <a href="/confirmation" class="bs-wizard-dot"></a>
            </div>
        </div>
        <!-- End bs-wizard -->
    </div>
    <!-- End intro-title -->
</section>
<!-- End Section hero_2 -->
<main>
    <div class="container margin_60">
        <div class="row">
            <div class="col-md-12 add_bottom_15">
            <? if(Yii::$app->user->isGuest): ?>
                <div class="text-center">
                    <h3>Для оформления заказа Вам необходимо авторизоваться в личном кабинете<br> или зарегистрироваться.</h3>
                    <br>
                    <a class="btn_1 mt-15" href="<?= Yii::$app->user->url->signin ?>">
                         Войти в личный кабинет
                    </a>
                    <a class="btn_1_outline mt-15" href="<?= Yii::$app->user->url->signup ?>">
                        Регистрация
                    </a>
                </div>
            <? else: ?>

                <? $form = ActiveForm::begin(['id' => 'change-password-form']); ?>
                <!--Begin tour information step -->
                <div class="form_title">
                    <h3><strong>1</strong>Туры онлайн</h3>
                    <p>Информация для оформления тура онлайн</p>
                </div>
                <div class="step">
                    <table class="table table-top table-striped cart-list add_bottom_30 cart-order">
                        <thead>
                            <th class="text-left" width="30%">Тур онлайн</th>
                            <th class="text-center" width="10%">Взрослые</th>
                            <th class="text-center" width="10%">Дети</th>
                            <th class="text-left" width="50%">Выберите стоимость</th>
                        </thead>
                        <tbody>
                        <? foreach($products as $id => $item): ?>
                            <tr data-product="<?= $item->id ?>">
                                <td>
                                    <div class="mt-13">
                                        <strong><?= $item->name ?></strong>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <div class="numbers-row m-center">
                                        <?= Html::activeInput('text', $order_data[$item->id], '['. $item->id .']adults', [
                                            'class' => 'qty2 form-control',
                                            'data-attr' => 'adults',
                                            'onchange' => "return totalAmount()"
                                        ]) ?>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <div class="numbers-row m-center">
                                        <?= Html::activeInput('text', $order_data[$item->id], '['. $item->id .']children', [
                                            'class' => 'qty2 form-control',
                                            'data-attr' => 'children',
                                            'onchange' => "return totalAmount()"
                                        ]) ?>
                                    </div>
                                </td>
                                <td>
                                    <? foreach ($item->prices as $i => $price): ?>
                                    <div class="cart-order-price row">
                                        <div class="col-sm-1">
                                            <?= Html::activeInput('radio', $order_data[$item->id], '['. $item->id .']price_id', [
                                                'class' => 'icheck',
                                                'value' => $price->id,
                                                'id' => 'price_' . $price->id,
                                                'checked ' => ($order_data[$item->id]->price_id == $price->id || !$i ? '' : false),
                                                'data-adults-price' => $price->getPrice(false),
                                                'data-children-price' => $price->children_price,
                                                'onchange' => "return totalAmount()"
                                            ]) ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <label for="price_<?= $price->id ?>">
                                                <strong><?= $price->price ?></strong><br>
                                                <small>
                                                    <?= date($price::OUT_DATE_FORMAT, $price->start_date_timestamp) ?>
                                                    -
                                                    <?= date($price::OUT_DATE_FORMAT, $price->end_date_timestamp) ?>
                                                </small>
                                            </label>
                                        </div>
                                        <div class="col-sm-7">
                                            <small class="cart-order-price__descp"><?= $price->short_description ?></small>
                                        </div>
                                    </div>
                                    <? endforeach; ?>
                                </td>
                            </tr>
                        <? endforeach; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="4" class="text-right">
                                    <h5><strong>Итого к оплате: <span id="total-amount" class="ml-5"><?= $order->totalPriceFormat ?></span></strong></h5>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                    <? if(count($order_data)): ?>
                        <? foreach($order_data as $item): ?>
                            <? if(count($item->errors)): ?>
                                <?= Html::errorSummary($item, ['class' => 'order-errors']); ?>
                            <? endif;?>
                        <? endforeach; ?>
                    <? endif; ?>
                </div>
                <!--End tour information step -->

                <div class="form_title">
                    <h3><strong>2</strong>Согласие с условиями сайта</h3>
                    <p>Политика конфиденциальности и условия обработки персональных данных Клиента.</p>
                </div>
                <div class="step">
                    <div class="row">
                        <div class="col-sm-6 mt-20">
                            <div class="form-group">
                                <label class="mr-5">
                                    <?= $form->field($order, 'agreement', [
                                        'template' => '<label class="mr-5">{input}</label>'. $order->getAttributeLabel('agreement'),
                                    ])->label(false)->checkbox(['class' => 'icheck'], false) ?>
                                </label>
                                <a href="/agreement.html" target="_blank" class="link-green">
                                    условия и общую политику.
                                </a>
                            </div>
                            <? if(count($order->errors)): ?>
                                <?= Html::errorSummary($order, ['class' => 'order-errors']); ?>
                            <? endif; ?>
                        </div>
                        <div class="col-sm-6 text-right">
                            <a class="btn_1_outline mr-5" href="/cart"><i class="icon-right"></i> Вернуться в корзину</a>
                            <button type="submit" class="btn_1 green mt-20">
                                Оплатить заказ <i class="icon-left"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <? ActiveForm::end(); ?>
        <? endif; ?>
        </div>
        <!--End row -->
    </div>
    <!--End container -->
</main>
<!-- End main -->