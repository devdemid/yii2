<?php

use yii\helpers\Html;
use yii\widgets\LinkPager;
use \common\models\DataGroup;
use \common\models\TourPrices;

$cat_models = DataGroup::getGroupModel(DataGroup::GROUP_TOURS_CAT_ID);
$type_models = DataGroup::getGroupModel(DataGroup::GROUP_TOURS_TYPE_ID);
$restriction_models = DataGroup::getGroupModel(DataGroup::GROUP_TOURS_RESTRICTION_ID);

?>

<section class="parallax-window" data-parallax="scroll" data-image-src="/img/header_bg.jpg" data-natural-width="1400" data-natural-height="470">
    <div class="parallax-content-1">
        <div class="animated fadeInDown">
            <h1>Туры в Грузию</h1>
            <p>Предлагаем широкий выбор туров в Грузию на любой вкус!</p>
        </div>
    </div>
</section>
<!-- End section -->

<main>
    <div class="collapse" id="collapseMap">
        <div id="map" class="map"></div>
    </div>
    <!-- End Map -->
    <div class="container margin_60">
        <div class="row">
            <aside class="col-lg-3 col-md-3">
                <?= $this->context->renderPartial('aside-filters', ['filters' => $filters])?>
                <div class="box_style_2">
                    <i class="icon_set_1_icon-57"></i>
                    <?= \frontend\shortcodes\BlockShortcode::widget(['id' => 2]) ?>
                </div>
            </aside>
            <!--End aside -->
            <div class="col-lg-9 col-md-8">
                <? if(count($tours)): ?>
                    <div id="tools">
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-6">
                                <div class="styled-select-filters">
                                    <?= Html::dropDownList('sort', $ordering->sort, $ordering::getList(), [
                                        'prompt' => 'Сортировать по:',
                                        'class' => 'url-filter'
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End tools -->
                    <div class="row">
                    <? foreach($tours as $i => $item): ?>
                        <div class="col-md-6 col-sm-6 wow zoomIn" data-wow-delay="0.1s">
                            <div class="tour_container">
                                <?/*<div class="ribbon_3 popular">
                                    <span>Популярный</span>
                                </div>*/?>
                                <? if($item->selling == $item::SELLING_ON): ?>
                                    <div class="ribbon_3">
                                        <span>Тур онлайн</span>
                                    </div>
                                <? endif; ?>
                                <div class="img_container">
                                    <a href="<?= $item->link ?>">
                                        <? if($item->image): ?>
                                        <img src="<?= $item->getImage('800x533') ?>" width="800" height="533" class="img-responsive" alt="<?= Html::encode($item->name) ?>">
                                        <? endif ?>
                                        <div class="short_info">
                                            <? if($item->catModel !== null): ?>
                                                <i class="<?= $item->catModel->icon_css ?>"></i>
                                                <span><?= $item->catModel->name ?></span>
                                            <? endif; ?>
                                            <span class="price">
                                                <?= $item->defaultPrice ?>
                                            </span>
                                        </div>
                                    </a>
                                </div>
                                <div class="tour_title">
                                    <h3><?= $item->name ?></h3>
                                    <span class="tour_views" title="Всего просмотров">
                                        <i class="icon-eye-7"></i>
                                        <span>(<?= $item->views ?>)</span>
                                    </span><!-- tour_views -->
                                    <?= \frontend\widgets\RatingWidget::widget([
                                        'id' => $item->id,
                                        'module' => Yii::$app->controller->id,
                                        'rating' => $item->rating,
                                        'votes' => $item->rating_votes,
                                        'rate'  => $item->getRate(),
                                    ]) ?>
                                    <!-- end rating -->
                                </div>
                                <div class="text-center tour_container__buy">
                                    <? if($item->selling && Yii::$app->options->get('enable_payment')): ?>
                                    <button onclick="return addToCart('<?= Yii::$app->cart->generateSignature($item->id) ?>', <?= $item->id ?>, this);" class="btn_1">
                                        <i class="icon-basket-1 mr-5"></i>
                                        <span>Купить онлайн</span>
                                    </button>
                                    <? else: ?>
                                    <button onclick="return tourBooking(<?= $item->id ?>, '<?= Html::encode($item->name) ?>');" class="btn_1">
                                        <i class="icon-basket-1 mr-5"></i>
                                        <span>Забронировать тур</span>
                                    </button>
                                    <? endif; ?>
                                </div>
                            </div>
                            <!-- End box tour -->
                        </div>
                        <!-- End col-md-6 -->
                    <? if($i % 2 == true): ?></div><div class="row"><? endif ?>
                    <? endforeach ?>
                    </div>
                <? else: ?>
                    <div class="row">
                        <div class="col-xs-12 text-center error-block">
                            <h2>Не удалось найти туры</h2>
                        </div>
                    </div>
                <!-- End row -->
                <? endif; ?>
                <? if($pages->pageCount > 1): ?>
                <hr>
                <div class="text-center">
                    <?= LinkPager::widget([
                        'pagination' => $pages,
                        'options'    => [
                            'class' => 'pagination',
                            'prevPageLabel' => 'Предыдущая',
                            'nextPageLabel' => 'Следующая',
                        ]
                    ]); ?>
                </div>
                <!-- end pagination-->
                <? endif ?>
            </div>
            <!-- End col lg 9 -->
        </div>
        <!-- End row -->
    </div>
    <!-- End container -->
</main>
<!-- End main -->
<?= \frontend\widgets\ReservationWidget::widget() ?>