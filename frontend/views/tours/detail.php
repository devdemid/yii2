<?php

use yii\helpers\Html;

?>
<section class="parallax-window" data-parallax="scroll" data-image-src="/img/single_tour_bg_1.jpg" data-natural-width="1400" data-natural-height="470">
    <div class="parallax-content-2">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-8">
                    <h1><?= Html::encode($model->name) ?></h1>
                    <? if($model->catModel !== null): ?>
                        <i class="<?= $model->catModel->icon_css ?>"></i>
                        <span class="mr-5"><?= $model->catModel->name ?></span>
                    <? endif; ?>
                    <?= \frontend\widgets\RatingWidget::widget([
                        'id' => $model->id,
                        'module' => Yii::$app->controller->id,
                        'rating' => $model->rating,
                        'votes' => $model->rating_votes,
                        'rate'  => $model->getRate(),
                    ]) ?>
                </div>
                <? if($model->defaultPrice): ?>
                <div class="col-md-4 col-sm-4">
                    <div id="price_single_main">
                           цена от <span><?= $model->defaultPrice ?></span>
                    </div>
                </div>
                <? endif ?>
            </div>
        </div>
    </div>
</section>
<!-- End section -->

<main>
    <div class="container margin_60">
        <div class="row">
            <div class="col-md-8" id="single_tour_desc">

                <? if(count($model->catList)): ?>
                <div class="row">
                    <div class="col-md-3">
                        <h3>Категории</h3>
                    </div>
                    <div class="col-md-9 mt-5">
                        <? foreach($model->catList as $item): ?>
                            <a href="<?= $item->link?>" class="btn-cat--2">
                                <i class="<?= $item->icon_css ?>"></i>
                                <span><?= $item->name ?></span>
                            </a>
                        <? endforeach; ?>
                    </div>
                </div>
                <hr>
                <? endif; ?>

                <!-- Begin description -->
                <div class="row">
                    <div class="col-md-3">
                        <h3>Описание</h3>
                    </div>
                    <div class="col-md-9">
                        <?= $model->description ?>
                    </div>
                </div>

                <!-- End description -->
                <? if(count($model->programs)): ?>
                <hr>
                    <!-- Begin programs -->
                    <div class="row">
                        <div class="col-md-3">
                            <h3>Программа</h3>
                        </div>
                        <div class="col-md-9">
                            <div class="panel-group" id="accordion">
                            <? foreach($model->programs as $i => $item): ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#program-<?= $item->id ?>" aria-expanded="<?= !$i ? 'true' : 'false' ?>">
                                               <?= Html::encode($item->name) ?>
                                                <i class="indicator pull-right icon-<?= !$i ? 'minus' : 'plus' ?>"></i>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="program-<?= $item->id ?>" class="panel-collapse collapse <?= !$i ? 'in' : false ?>">
                                        <div class="panel-body">
                                            <?= $item->description ?>
                                        </div>
                                    </div>
                                </div>
                            <? endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <!-- End programs -->
                <? endif; ?>
                <? if(count($model->prices)): ?>
                <hr>
                <!-- Begin prices -->
                <div class="row">
                    <div class="col-md-3">
                        <h3>Стоимость</h3>
                    </div>
                    <div class="col-md-9">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <tbody>
                                <? foreach($model->filterPrices as $item): ?>
                                    <tr>
                                        <td width="25%">
                                            <div><strong><?= $item->price ?></strong></div>
                                            <small>
                                                <?= date($item::OUT_DATE_FORMAT, $item->start_date_timestamp) ?>
                                                -
                                                <?= date($item::OUT_DATE_FORMAT, $item->end_date_timestamp) ?>
                                            </small>
                                        </td>
                                        <td>
                                            <?= $item->short_description ?>
                                        </td>
                                    </tr>
                                <? endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <br>
                        <div class="row">
                            <? if($model->included_in_price): ?>
                            <div class="col-md-6 col-sm-6">
                                <h4>В стоимость входит:</h4>
                                <ul class="list_ok">
                                <? foreach(preg_split($model::PATTERN_PREG_SPLIT, $model->included_in_price) as $item): ?>
                                    <li><?= Html::encode($item) ?></li>
                                <? endforeach;?>
                                </ul>
                            </div>
                            <? endif; ?>
                            <? if($model->not_included_in_price): ?>
                            <div class="col-md-6 col-sm-6">
                                <h4>В стоимость не входит:</h4>
                                <ul>
                                <? foreach(preg_split($model::PATTERN_PREG_SPLIT, $model->not_included_in_price) as $item): ?>
                                    <li><?= Html::encode($item) ?></li>
                                <? endforeach;?>
                                </ul>
                            </div>
                            <? endif ?>
                        </div>
                    </div>
                </div>
                <!-- End prices -->
                <? endif; ?>
            </div>
            <aside class="col-md-4" id="sidebar">
                <!-- Begin buy tour -->
                <div class="theiaStickySidebar">
                    <div class="box_style_1 expose" id="booking_box">
                        <h3 class="inner">- Купить тур -</h3>
                        <? if($model->selling && Yii::$app->options->get('enable_payment')): ?>
                        <button onclick="return addToCart('<?= Yii::$app->cart->generateSignature($model->id) ?>', <?= $model->id ?>, this);" class="btn_full">Купить онлайн</button>
                        <a class="btn_full_outline" href="<?= Yii::$app->cart->linkOrdering ?>">Оформить заказ</a>
                        <? else: ?>
                        <button onclick="return tourBooking(<?= $model->id ?>, '<?= Html::encode($model->name) ?>');" class="btn_full">Забронировать тур</button>
                        <? endif; ?>
                    </div>
                </div>
                <!-- End buy tour -->
            </aside>
        </div>
        <!-- Begin discount banner -->
        <div class="container margin_top_60 ml--15">
            <?= \frontend\widgets\SubscribeWidget::widget() ?>
        </div>
        <!-- End discount banner -->

        <?= \frontend\widgets\RecommendedToursWidget::widget([
            'ignore_id' => $model->id
        ]) ?>
    </div>
</main>
<!-- End main -->
<?= \frontend\widgets\ReservationWidget::widget() ?>