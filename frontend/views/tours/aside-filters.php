<?php

use yii\helpers\Html;
use \common\models\DataGroup;
use \common\models\TourPrices;
use yii\helpers\ArrayHelper;

// Get Data group
$cat_models         = DataGroup::getGroupModel(DataGroup::GROUP_TOURS_CAT_ID);
$type_models        = DataGroup::getGroupModel(DataGroup::GROUP_TOURS_TYPE_ID);
$restriction_models = DataGroup::getGroupModel(DataGroup::GROUP_TOURS_RESTRICTION_ID);

// Defaukt icon
$default_imagesrc = '/img/icons_search/all_tours.png';

?>
<!-- Begin filters -->
<?= Html::beginForm('/' . Yii::$app->controller->id, 'get') ?>
<div id="filters_col">
    <a data-toggle="collapse" href="#collapseFilters" aria-expanded="false" aria-controls="collapseFilters" id="filters_col_bt">
        <i class="icon_set_1_icon-65"></i>
        <span>Фильтры</span>
    </a>
    <div class="collapse" id="collapseFilters">

        <!-- Begin cat -->
        <div class="form-group">
            <h6>Категория тура</h6>
            <?= Html::dropDownList('Filters[cat_id]', $filters->cat_id, ArrayHelper::map($cat_models, 'id', 'name'), [
                'class' => 'ddslick',
                'prompt' => [
                    'text' => 'Все туры',
                    'options' => [
                        'data-fonticon' => 'icon_set_1_icon-51'
                    ]
                ],
                'options' => ArrayHelper::map($cat_models, 'id', 'fonticon')
            ]); ?>
            <?= Html::error($filters, 'cat_id', ['class' => 'filter-error']) ?>
        </div>
        <!-- End cat -->

        <!-- Begin type -->
        <div class="filter_type">
            <h6>Тип</h6>
            <?= Html::dropDownList('Filters[type_id]', $filters->type_id, ArrayHelper::map($type_models, 'id', 'name'), [
                'class' => 'ddslick',
                'prompt' => [
                    'text' => 'Все типы',
                    'options' => [
                        'data-fonticon' => 'icon_set_1_icon-51'
                    ]
                ],
                'options' => ArrayHelper::map($type_models, 'id', 'fonticon')
            ]); ?>
            <?= Html::error($filters, 'type_id', ['class' => 'filter-error']) ?>
        </div>
        <!-- End type-->

        <!-- Begin restriction -->
        <div class="filter_type">
            <h6>Ограничения</h6>
            <?= Html::dropDownList('Filters[restriction_id]', $filters->restriction_id, ArrayHelper::map($restriction_models, 'id', 'name'), [
                'class' => 'ddslick',
                'prompt' => [
                    'text' => 'Без ограничений',
                    'options' => [
                        'data-fonticon' => 'icon_set_1_icon-51'
                    ]
                ],
                'options' => ArrayHelper::map($restriction_models, 'id', 'fonticon')
            ]); ?>
            <?= Html::error($filters, 'restriction_id', ['class' => 'filter-error']) ?>
        </div>
        <!-- End restriction -->

        <div class="filter_type">
            <h6>Цена</h6>
            <?= Html::input('text', 'Filters[range]', null, [
                'data-range'   => 'aside-filter',
                'data-min'     => $filters->minPrice,
                'data-max'     => $filters->maxPrice,
                'data-postfix' => TourPrices::DEFAULT_CURRENCY_NAME,
                'data-step'    => 500,
                'data-from'    => (is_array($filters->range) ? $filters->range[0] : $filters->minPrice),
                'data-to'      => (is_array($filters->range) ? $filters->range[1] : $filters->maxPrice)
            ]) ?>
            <?= Html::error($filters, 'range', ['class' => 'filter-error']) ?>
        </div>
        <hr>
        <div class="form-group">
            <h6><i class="icon-calendar-7"></i> Период с</h6>
            <?= Html::input('text', 'Filters[start_date]', $filters->start_date, [
                'class' => 'date-pick form-control',
                'data-date-format' => 'yyyy-mm-dd',
                'data-date-start-date' => '<?= $filters->start_date ?>',
                'data-storage' => 'start_date',
            ]) ?>
            <?= Html::error($filters, 'start_date', ['class' => 'filter-error']) ?>
        </div>
        <div class="form-group">
            <h6><i class="icon-calendar-7"></i> По</h6>
            <?= Html::input('text', 'Filters[end_date]', $filters->end_date, [
                'class' => 'date-pick form-control',
                'data-date-format' => 'yyyy-mm-dd',
                'data-date-start-date' => '<?= $filters->end_date ?>',
                'data-storage' => 'end_date',
            ]) ?>
            <?= Html::error($filters, 'end_date', ['class' => 'filter-error']) ?>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="form-group">
                    <label>Взрослые</label>
                    <div class="numbers-row">
                        <?= Html::input('text', 'Filters[adults]', $filters->adults, [
                            'class' => 'qty2 form-control',
                            'data-storage' => 'adults',
                        ]) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="form-group">
                    <label>Дети</label>
                    <div class="numbers-row">
                        <?= Html::input('text', 'Filters[children]', $filters->children, [
                            'class' => 'qty2 form-control',
                            'data-storage' => 'children',
                        ]) ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <?= Html::error($filters, 'adults', ['class' => 'filter-error']) ?>
                <?= Html::error($filters, 'children', ['class' => 'filter-error']) ?>
            </div>
        </div>
    </div>
    <!--End collapse -->
</div>
<!--End filters col-->
<p>
    <button type="submit" class="btn_full">Подобрать туры</button>
    <a href="/tours" class="btn_full_outline" onclick="return confirm('Вы уверены что хотите сбросить все фильтры?')">Сбросить фильтры</a>
</p>
<?= Html::endForm() ?>
<!-- End filters -->