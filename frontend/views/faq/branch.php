<? if(!is_null($model) && count($model->answers)): ?>
<h2 class="text-center title-head"><?= $model->name ?></h2>
<br>
<!-- Panel Group Starts -->
<div class="panel-group" id="accordion">
    <? foreach($model->answers as $i => $item): ?>
    <!-- Panel Starts -->
    <div class="panel panel-default">
        <!-- Panel Heading Starts -->
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse-<?= $item->id ?>">
                    <?= $item->question ?>
                </a>
            </h4>
        </div>
        <!-- Panel Heading Ends -->
        <!-- Panel Content Starts -->
        <div id="collapse-<?= $item->id ?>" class="panel-collapse collapse <?= !$i ? 'in' : false ?>">
            <div class="panel-body"><?= $item->answer ?></div>
        </div>
        <!-- Panel Content Starts -->
    </div>
    <!-- Panel Ends -->
    <? endforeach; ?>
</div>
<!-- Panel Group Ends -->
<? endif; ?>