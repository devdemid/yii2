<? if($model): ?>
<section class="parallax-window" data-parallax="scroll" data-image-src="/img/header_bg.jpg" data-natural-width="1400" data-natural-height="470">
    <div class="parallax-content-1">
        <div class="animated fadeInDown">
        <h1><?= $model->question ?></h1>
        <p>Для туристов</p>
        </div>
    </div>
</section><!-- End Section -->

<main>
    <div class="container margin_60">
        <?= $model->answer ?>
    </div>
</main>
<? endif; ?>
