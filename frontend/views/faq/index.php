<!-- Banner Area Starts -->
<section class="banner-area">
    <div class="banner-overlay">
        <div class="banner-text text-center">
            <div class="container">
                <!-- Section Title Starts -->
                <div class="row text-center">
                    <div class="col-xs-12">
                        <!-- Title Starts -->
                        <h2 class="title-head"><?= $model->name ?></h2>
                        <!-- Title Ends -->
                    </div>
                </div>
                <!-- Section Title Ends -->
            </div>
        </div>
    </div>
</section>
<!-- Banner Area Ends -->

<!-- Section FAQ Starts -->
<section class="faq">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-8">
                <!-- Panel Group Starts -->
                <div class="panel-group" id="accordion">
                    <? foreach($model->answers as $i => $item): ?>
                    <!-- Panel Starts -->
                    <div class="panel panel-default">
                        <!-- Panel Heading Starts -->
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse-<?= $item->id ?>"><?= $item->question ?></h4>
                        </div>
                        <!-- Panel Heading Ends -->
                        <!-- Panel Content Starts -->
                        <div id="collapse-<?= $item->id ?>" class="panel-collapse collapse <?= !$i ? 'in' : false ?>">
                            <div class="panel-body"><?= $item->answer ?></div>
                        </div>
                        <!-- Panel Content Starts -->
                    </div>
                    <!-- Panel Ends -->
                    <? endforeach; ?>
                </div>
                <!-- Panel Group Ends -->
            </div>
            <!-- Sidebar Starts -->
            <div class="sidebar col-xs-12 col-md-4">
                <!-- Latest Posts Widget Ends -->
                <div class="widget recent-posts">
                    <h3 class="widget-title">Последние новости</h3>
                    <ul class="unstyled clearfix">
                    <!-- Recent Post Widget Starts -->
                    <li>
                        <div class="posts-thumb pull-left">
                            <a href="blog-post.html">
                                <img alt="img" src="images/blog/blog-post-small-1.jpg">
                            </a>
                        </div>
                        <div class="post-info">
                            <h4 class="entry-title">
                                <a href="blog-post.html">Risks & Rewards Of Investing In Bitcoin</a>
                            </h4>
                            <p class="post-meta">
                                <span class="post-date"><i class="fa fa-clock-o"></i> January 19, 2017</span>
                            </p>
                        </div>
                        <div class="clearfix"></div>
                    </li>
                    <!-- Recent Post Widget Ends -->
                    <!-- Recent Post Widget Starts -->
                    <li>
                        <div class="posts-thumb pull-left">
                            <a href="blog-post-light.html">
                                <img alt="img" src="images/blog/blog-post-small-2.jpg">
                            </a>
                        </div>
                        <div class="post-info">
                            <h4 class="entry-title">
                                <a href="blog-post-light.html">Cryptocurrency - Who Are Involved With It?</a>
                            </h4>
                            <p class="post-meta">
                                <span class="post-date"> August 03, 2017</span>
                            </p>
                        </div>
                        <div class="clearfix"></div>
                    </li>
                    <!-- Recent Post Widget Ends -->
                    <!-- Recent Post Widget Starts -->
                    <li>
                        <div class="posts-thumb pull-left">
                            <a href="blog-post-light.html">
                                <img alt="img" src="images/blog/blog-post-small-3.jpg">
                            </a>
                        </div>
                        <div class="post-info">
                            <h4 class="entry-title">
                                <a href="blog-post-light.html">How Cryptocurrency Begun and Its Impact</a>
                            </h4>
                            <p class="post-meta">
                                <span class="post-date"> March 27, 2017</span>
                            </p>
                        </div>
                        <div class="clearfix"></div>
                    </li>
                    <!-- Recent Post Widget Ends -->
                    </ul>
                </div>
                <!-- Latest Posts Widget Ends -->
            </div>
        <!-- Sidebar Ends -->
        </div>
    </div>
</section>
<!-- Section FAQ Ends -->
<?= \frontend\shortcodes\BlockShortcode::widget(['id' => 1, 'view' => 'block-reg']) ?>