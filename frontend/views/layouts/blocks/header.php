<!-- Header Starts -->
        <header class="header">
            <div class="container">
                <div class="row">
                    <!-- Logo Starts -->
                    <div class="main-logo col-xs-12 col-md-3 col-md-2 col-lg-2 hidden-xs">
                        <a href="/" title="<?= Yii::$app->options->get('home_title') ?>">
                            <img class="img-responsive" src="/images/logo.png?v1" alt="<?= Yii::$app->options->get('home_title') ?>">
                        </a>
                    </div>
                    <!-- Logo Ends -->
                    <!-- Statistics Starts -->
                    <div class="col-md-6 col-lg-6">
                        <ul class="unstyled bitcoin-stats text-center">
                            <? foreach(['btc', 'eth', 'xrp', 'bch', 'ltc'] as $coin): ?>
                            <li>
                                <h6>
                                    <?= $this->context->getLatestCourse($coin) ?> USD
                                </h6>
                                <span>
                                    1 <?= strtoupper($coin) ?>
                                </span>
                            </li>
                            <? endforeach ?>
                        </ul>
                    </div>
                    <!-- Statistics Ends -->
                    <!-- User Sign In/Sign Up Starts -->
                    <div class="col-md-4 col-lg-4">
                        <ul class="unstyled user">
                            <li class="sign-in">
                                <a href="https://my.cryptotrade.trade/en/signin" class="btn btn-primary">
                                    <i class="fa fa-user"></i> Войти
                                </a>
                            </li>
                            <li class="sign-up">
                                <a href="https://my.cryptotrade.trade/en/signup" class="btn btn-primary">
                                    <i class="fa fa-user-plus"></i> Регистрация
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- User Sign In/Sign Up Ends -->
                </div>
            </div>
            <!-- Navigation Menu Starts -->
            <nav class="site-navigation navigation" id="site-navigation">
                <div class="container">
                    <div class="site-nav-inner">
                        <!-- Logo For ONLY Mobile display Starts -->
                        <a class="logo-mobile" href="index.html">
                            <img class="img-responsive" src="/images/logo.png" alt="">
                        </a>
                        <!-- Logo For ONLY Mobile display Ends -->
                        <!-- Toggle Icon for Mobile Starts -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Переключить навигацию</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <!-- Toggle Icon for Mobile Ends -->
                        <div class="collapse navbar-collapse navbar-responsive-collapse">
                            <!-- Main Menu Starts -->
                            <?= \frontend\shortcodes\MenuShortcode::widget([
                                'id' => 1,
                                'options' => [
                                    'class' => 'nav navbar-nav'
                                ]
                            ]) ?>
                            <?/*<ul class="nav navbar-nav">
                                <!-- Search Icon Starts -->
                                <li class="search"><button class="fa fa-search"></button></li>
                                <!-- Search Icon Ends -->
                            </ul>*/?>
                            <!-- Main Menu Ends -->
                        </div>
                    </div>
                </div>
                <!-- Search Input Starts -->
                <div class="site-search">
                    <div class="container">
                        <input type="text" placeholder="Введите свое ключевое слово и нажмите Enter..">
                        <span class="close">×</span>
                    </div>
                </div>
                <!-- Search Input Ends -->
            </nav>
            <!-- Navigation Menu Ends -->
        </header>
        <!-- Header Ends -->