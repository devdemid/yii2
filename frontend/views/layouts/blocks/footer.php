<!-- Footer Starts -->
<footer class="footer">
    <!-- Footer Top Area Starts -->
    <div class="top-footer">
        <div class="container">
            <div class="row">
                <!-- Footer Widget Starts -->
                <div class="col-sm-4 col-md-2">
                    <h4>Навигация</h4>
                    <div class="menu">
                        <?= \frontend\shortcodes\MenuShortcode::widget(['id' => 1]) ?>
                    </div>
                </div>
                <!-- Footer Widget Ends -->
                <!-- Footer Widget Starts -->
                <div class="col-sm-4 col-md-2">
                    <h4>Помощь</h4>
                    <div class="menu">
                        <?= \frontend\shortcodes\MenuShortcode::widget(['id' => 3]) ?>
                    </div>
                </div>
                <!-- Footer Widget Ends -->
                <?= \frontend\shortcodes\BlockShortcode::widget(['id' => 2, 'view' => 'block-footer-contact']) ?>
                <!-- Footer Widget Starts -->
                <div class="col-sm-12 col-md-5">
                    <!-- Facts Starts -->
                    <div class="facts-footer">
                        <div>
                            <h5>$198.76B</h5>
                            <span>Рыночная капитализация</span>
                        </div>
                        <div>
                            <h5>243K</h5>
                            <span>Ежедневные транзакции</span>
                        </div>
                        <div>
                            <h5>369K</h5>
                            <span>Активные аккаунты</span>
                        </div>
                        <div>
                            <h5>127</h5>
                            <span>Поддерживаемых стран</span>
                        </div>
                    </div>
                    <!-- Facts Ends -->
                    <hr>
                    <!-- Supported Payment Cards Logo Starts -->
                    <div class="payment-logos">
                        <h4 class="payment-title">Поддерживаемые способы выплат</h4>
                        <img src="/images/icons/payment/american-express.png" alt="American express">
                        <img src="/images/icons/payment/mastercard.png" alt="MasterCard">
                        <img src="/images/icons/payment/visa.png" alt="Visa">
                        <img src="/images/icons/payment/paypal.png" alt="Paypal">
                        <img class="last" src="/images/icons/payment/maestro.png" alt="Maestro">
                    </div>
                    <!-- Supported Payment Cards Logo Ends -->
                </div>
                <!-- Footer Widget Ends -->
            </div>
        </div>
    </div>
    <!-- Footer Top Area Ends -->
    <!-- Footer Bottom Area Starts -->
    <div class="bottom-footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Copyright Text Starts -->
                    <p class="text-center"><?= Yii::$app->options->get('copyright') ?></p>
                    <!-- Copyright Text Ends -->
                </div>
            </div>
        </div>
    </div>
    <!-- Footer Bottom Area Ends -->
</footer>
<!-- Footer Ends -->
<!-- Back To Top Starts  -->
<a href="#" id="back-to-top" class="back-to-top fa fa-arrow-up"></a>
<!-- Back To Top Ends  -->