<?php

use yii\helpers\Html;

?>
<? if($model): ?>
<section class="parallax-window" data-parallax="scroll" data-image-src="/img/bg_blog.jpg" data-natural-width="1400" data-natural-height="470">
    <div class="parallax-content-1">
        <div class="animated fadeInDown">
            <h1><?= $model->name ?></h1>
            <p><?= date('d M Y', $model->date_timestamp) ?></p>
        </div>
    </div>
</section>
<!-- End section -->

<main>
    <div class="container margin_60">
        <div class="row">
            <div class="col-md-9">
                <div class="box_style_1">
                    <div class="post nopadding">
                        <div class="clearfix">
                            <h3 class="mt-0"><?= $model->name ?></h3>
                        </div>
                        <hr>
                        <? if($model->image): ?>
                            <img src="<?= $model->getImage('900x375') ?>" alt="<?= Html::encode($model->name) ?>" class="img-responsive">
                        <? endif; ?>
                        <div class="post_info clearfix">
                            <div class="post-left">
                                <ul>
                                    <? if($newsFeed->is_date_published): ?>
                                    <li>
                                        <i class="icon-calendar-empty"></i>
                                        <span><?= date('d M Y', $model->date_timestamp) ?></span>
                                    </li>
                                    <? endif; ?>
                                    <? if($newsFeed->is_count_views): ?>
                                    <li>
                                        <i class="icon-eye-7"></i>
                                        <?= $model->views ?>
                                    </li>
                                    <? endif; ?>
                                    <? if(!is_null($model->cat)): ?>
                                    <li>
                                        <i class="icon-inbox-alt"></i>
                                        <a href="<?= $model->cat->link ?>">
                                            <?= $model->cat->name ?>
                                        </a>
                                    </li>
                                    <? endif ?>
                                </ul>
                            </div>
                            <? if($newsFeed->enable_comments && !$model->enable_comments): ?>
                            <div class="post-right">
                                <i class="icon-comment"></i>
                                <?= $model->countComments ?>
                            </div>
                            <? endif; ?>
                        </div>
                        <? if($model->full_description): ?>
                        <div><?= $model->full_description ?></div>
                        <? endif; ?>
                    </div>
                </div>

                <?= \frontend\widgets\CommentWidget::widget([
                    'id' => $model->id,
                    'module' => 'news',
                    'enable' => $newsFeed->enable_comments && !$model->enable_comments
                ]) ?>

            </div>
            <aside class="col-md-3 add_bottom_30">
                <? if(count($newsFeed->categories)): ?>
                <div class="widget cat_blog" id="cat_blog">
                   <h4>Категории</h4>
                   <ul>
                      <li>
                           <a href="<?= $newsFeed->link ?>">
                                Все записи
                           </a>
                      </li>
                      <? foreach($newsFeed->categories as $item): ?>
                      <li <? if(!is_null($model->cat) && $model->cat->alias == $item->alias): ?>class="active"<? endif; ?>>
                           <a href="<?= $item->link ?>">
                               <?= $item->name ?>
                           </a>
                      </li>
                      <? endforeach; ?>
                   </ul>
                </div>
                <!-- End widget -->
                <? endif; ?>
                <? if(count($newsFeed->getNews(6))): ?>
                <br>
                <br>
                <div class="widget">
                    <h4>Популярное</h4>
                    <ul class="recent_post">
                        <? foreach($newsFeed->getNews(6) as $item): ?>
                        <li>
                            <i class="icon-calendar-empty"></i>
                            <?= date('d M Y', $item->date_timestamp) ?>
                            <div>
                                <a href="<?= $item->link ?>">
                                    <?= $item->name ?>
                                </a>
                            </div>
                        </li>
                        <? endforeach ?>
                    </ul>
                </div>
                <!-- End widget -->
                <? endif ?>
            </aside>
        </div>
    </div>
</main>
<? endif; ?>