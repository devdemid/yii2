<?php

use yii\helpers\Html;
use yii\widgets\LinkPager;

?>
<!-- Banner Area Starts -->
<section class="banner-area">
    <div class="banner-overlay">
        <div class="banner-text text-center">
            <div class="container">
                <!-- Section Title Starts -->
                <div class="row text-center">
                    <div class="col-xs-12">
                        <? if($newsFeed->name): ?>
                        <!-- Title Starts -->
                        <h2 class="title-head"><?= $newsFeed->name ?></h2>
                        <!-- Title Ends -->
                        <? endif; ?>
                    </div>
                </div>
                <!-- Section Title Ends -->
            </div>
        </div>
    </div>
</section>
<!-- Banner Area Ends -->
<!-- Section Content Starts -->
<section class="container blog-page">
    <div class="row">
        <div class="content col-xs-12 col-md-12">
            <? if(count($models)): ?>
                <? foreach($models as $i => $item): ?>
                <!-- Article Starts -->
                <article class="col-md-6">
                    <a href="<?= $item->link ?>"><h4><?= Html::encode($item->name) ?></h4></a>
                    <!-- Figure Starts -->
                    <figure>
                        <a href="<?= $item->link ?>">
                            <img class="img-responsive" src="<?= $item->getImage('750x365') ?>" alt="<?= Html::encode($item->name) ?>">
                        </a>
                    </figure>
                    <!-- Figure Ends -->
                    <br>
                    <!-- Excerpt Starts -->
                    <div class="excerpt"><?= $item->short_description ?></div>
                    <!-- Excerpt Ends -->
                    <a href="<?= $item->link ?>" class="btn btn-primary btn-readmore">
                        Читать далее...
                    </a>
                    <!-- Meta Starts -->
                    <div class="meta">
                        <? if($newsFeed->is_date_published): ?>
                        <span>
                            <i class="fa fa-calendar"></i> <?= date('d M Y', $item->date_timestamp) ?>
                        </span>
                        <? endif; ?>
                        <? if($newsFeed->enable_comments && !$item->enable_comments): ?>
                        <span>
                            <i class="fa fa-commenting"></i>
                            <a href="<?= $item->link ?>"><?= $item->countComments ?></a>
                        </span>
                        <? endif; ?>
                    </div>
                    <!-- Meta Ends -->
                </article>
                <!-- Article Ends -->
                <? endforeach; ?>
            <? else: ?>
            <div class="text-center error-block">
                <h2>В этом разделе нет записей!</h2>
            </div>
            <? endif; ?>
            <nav class="col-xs-12 text-center" aria-label="Page navigation">
              <ul class="pagination">
                <li>
                  <a href="#" aria-label="Previous">
                    <span aria-hidden="true"><i class="fa fa-angle-double-left"></i></span>
                  </a>
                </li>
                <li><a href="#">1</a></li>
                <li class="active"><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li>
                  <a href="#" aria-label="Next">
                    <span aria-hidden="true"><i class="fa fa-angle-double-right"></i></span>
                  </a>
                </li>
              </ul>
            </nav>
        </div>
    </div>
</section>
<!-- Section Content Ends -->

<?= \frontend\shortcodes\BlockShortcode::widget(['id' => 1, 'view' => 'block-reg']) ?>