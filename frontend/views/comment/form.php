<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<h4>Оставить комментарий</h4>
<? if(Yii::$app->options->get('is_guest_comment')): ?>
<? $form = ActiveForm::begin([
    'action' => '/comment/form',
    'id' => 'comment-form',
    'options' => [
        'onsubmit' => "return requestForm(this, '#comment-wrapper-form');",
    ]
]); ?>
    <? if(Yii::$app->user->isGuest): ?>
    <?= $form->field($model, 'name',  [
        'template' => '{input}{error}',
        'inputOptions' => [
            'class'        => 'form-control',
            'autocomplete' => 'off',
            'placeholder'  => $model->getAttributeLabelPlaceholder('name'),
        ],
        'options' => [
            'class' => 'form-group'
        ]
    ]) ?>

    <?= $form->field($model, 'email',  [
        'template' => '{input}{error}',
        'inputOptions' => [
            'class'        => 'form-control',
            'autocomplete' => 'off',
            'placeholder'  => $model->getAttributeLabelPlaceholder('email'),
        ],
        'options' => [
            'class' => 'form-group'
        ]
    ]) ?>
    <? endif ?>

    <?= $form->field($model, 'content',  [
        'template' => '{input}{error}',
        'inputOptions' => [
            'class'        => 'form-control style_2',
            'rows'         => 8,
            'autocomplete' => 'off',
            'placeholder'  => $model->getAttributeLabelPlaceholder('content'),
        ],
        'options' => [
            'class' => 'form-group'
        ]
    ])->textarea() ?>

    <? if(Yii::$app->user->isGuest): ?>
    <?= $form->field($model, 'captcha', [
        'template' => '{label}{input}{error}{hint}',
        'inputOptions' => [
            'class' => 'form-control',
        ],
        'options' => [
            'class' => 'form-group row form-captcha'
        ]
    ])->widget(\yii\captcha\Captcha::className(), [
        'template' => '<div class="col-sm-3 mt-15">{image}</div><div class="col-sm-9">{input}</div>',
        'captchaAction' => 'comment/captcha',
        'imageOptions' => [
            'class' => 'form-captcha-image',
            'id' => Html::getInputId($model, 'captcha'),
            'onclick' => 'captchaRefresh(this, \'comment\'); return false;',
            'title' => 'Обновить изображение?',
        ],
        'options' => [
            'class' => 'form-control form-input',
            'placeholder' => $model->getAttributeLabelPlaceholder('captcha')
        ]
    ])->hint(false)->label(
        $model->getAttributeLabel('captcha'), [
            'class' => 'col-sm-12',
            'for' => Html::getInputId($model, 'captcha')
        ]
    ) ?>
    <? endif ?>

    <?= $form->field($model, 'module')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'item_id')->hiddenInput()->label(false) ?>
    <br>
    <? if($model->is_success): ?>
    <div class="text-center">
        Спасибо! Ваш комментарий добавлен успешно.
    </div>
    <? endif; ?>
    <div class="form-group">
        <input type="reset" class="btn_1" value="Очистить" />
        <input type="submit" class="btn_1" value="Отправить" />
    </div>
<? ActiveForm::end(); ?>
<? else: ?>
<div class="box_style_2">
    Для того чтобы оставить комментарий <a href="<?= Yii::$app->user->url->signin ?>">войдите</a> или <a href="<?= Yii::$app->user->url->signup ?>">зарегистрируйтесь</a>.
</div>
<? endif;?>