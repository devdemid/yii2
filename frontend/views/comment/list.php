<?php
use yii\helpers\Html;
?>
<? foreach($comments as $item): ?>
    <li id="comment-<?= $item->id ?>">
        <? if($item->user_id && !is_null($item->user) && $item->user->pathLogo): ?>
        <div class="avatar">
            <img src="<?= $item->user->pathLogo ?>" alt="<?= Html::encode($item->user->nameTogether) ?>" class="img-circle">
        </div>
        <? else: ?>
        <div class="avatar avatar-default">
            <?= $item->symbolName ?>
        </div>
        <? endif; ?>
        <div class="comment_right clearfix">
            <div class="comment_info">
                <? if($item->user_id): ?>
                    <a href="#">Adam White</a>
                <? else: ?>
                    <strong><?= $item->name ?></strong>
                <? endif; ?>
                <span>|</span> <a href="#comment-<?= $item->id ?>"><?= date('d M Y H:s', $item->date_timestamp) ?></a>
                <?/*<span>|</span><a href="#">Ответить</a>*/?>
            </div>
            <div>
                <?= $item->content ?>
            </div>
        </div>
    </li>
<? endforeach; ?>