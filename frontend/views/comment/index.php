<? if(count($comments)): ?>
    <h4>
        <?= Yii::$app->i18n->format(
            '{n, plural, =0{# Комментариев} one{# Комментарий} few{# Комментариев} many{# Комментарий} other{# Комментариев}}',
            ['n' => $pages->totalCount],
            Yii::$app->language
        ) ?>
    </h4>
    <div id="comments">
        <ol>
            <?= \Yii::$app->controller->renderPartial('//comment/list', ['comments' => $comments], false, true); ?>
        </ol>
    </div>
    <!-- End Comments -->
    <? if($pages->pageCount > 1): ?>
    <div class="text-center">
        <a href="#" data-next-page="1" onclick="return ajaxPagination('<?= $ajax_pagination_url ?>', '#comments', this);" class="btn-default">Показать еще...</a><br>
    </div>
    <? endif ?>
<? endif; ?>

<div id="comment-wrapper-form">
    <?= \Yii::$app->controller->renderPartial('//comment/form', ['model' => $model], false, true); ?>
</div>