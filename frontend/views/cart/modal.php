<div class="modal-header">
    <h5 class="modal-title">Ваша корзина (<?= Yii::$app->cart->count ?>)</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть" title="Закрыть">
        <i class="icon-cancel-2" aria-hidden="true"></i>
    </button>
</div>
<div class="modal-body">
    <? if(count($models)): ?>
    <table class="table cart-list add_bottom_30">
        <thead>
            <tr>
                <th width="70%">Туры онлайн</th>
                <th class="text-center">Стоимость от</th>
                <th class="text-center">Действия</th>
            </tr>
        </thead>
        <tbody>
        <? foreach($models as $item): ?>
            <tr data-cart-id="<?= $item->id ?>">
                <td>
                    <? if($item->image): ?>
                    <div class="thumb_cart" style="background-image: url('<?= $item->getImage('100x100') ?>')"></div>
                    <? else: ?>
                    <div class="thumb_cart"></div>
                    <? endif; ?>
                    <span class="item_cart">
                        <a href="<?= $item->link ?>" class="item_cart-name">
                            <?= $item->name ?>
                        </a>
                        <? if($item->catModel !== null): ?>
                        <div class="item_cart-cat">
                            <i class="<?= $item->catModel->icon_css ?>"></i>
                            <span><?= $item->catModel->name ?></span>
                        </div>
                        <? endif; ?>
                    </span>
                </td>
                <td class="text-center">
                    <strong><?= $item->defaultPrice ?></strong>
                </td>
                <td class="options text-center">
                    <a href="#" onclick="return removeItemCart('<?= Yii::$app->cart->generateSignature($item->id) ?>', <?= $item->id ?>, this);" title="Удалить из корзины"><i class=" icon-trash"></i></a>
                    <?/*<a href="#" onclick="return false" title="Обновить"><i class="icon-ccw-2"></i></a>*/?>
                </td>
            </tr>
        <? endforeach; ?>
        </tbody>
    </table>
    <? else: ?>
    <div class="text-center">
        Ваша корзина пустая
    </div>
    <? endif; ?>
</div>
<div class="modal-footer">
    <a href="#" data-dismiss="modal" class="btn_1 outline mr-5 btn_xs-block">
        <i class="icon-right"></i>
        Вернуться в каталог
    </a>
    <a href="<?= Yii::$app->cart->linkOrdering ?>" class="btn_1 btn_xs-block">
        Оформить заказ
        <i class="icon-left"></i>
    </a>
</div>