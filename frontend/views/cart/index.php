<?php
use yii\helpers\Html;
?>
<section id="hero_2">
    <div class="intro_title animated fadeInDown">
        <h1>Корзина</h1>
        <div class="bs-wizard">
            <div class="col-xs-4 bs-wizard-step active">
                <div class="text-center bs-wizard-stepnum">Корзина</div>
                <div class="progress">
                    <div class="progress-bar"></div>
                </div>
                <a href="cart.html" class="bs-wizard-dot"></a>
            </div>
            <div class="col-xs-4 bs-wizard-step disabled">
                <div class="text-center bs-wizard-stepnum">Оформление</div>
                <div class="progress">
                    <div class="progress-bar"></div>
                </div>
                <a href="ordering.html" class="bs-wizard-dot"></a>
            </div>
            <div class="col-xs-4 bs-wizard-step disabled">
                <div class="text-center bs-wizard-stepnum">Готово!</div>
                <div class="progress">
                    <div class="progress-bar"></div>
                </div>
                <a href="confirmation.html" class="bs-wizard-dot"></a>
            </div>
        </div>
        <!-- End bs-wizard -->
    </div>
    <!-- End intro-title -->
</section>
<!-- End Section hero_2 -->
<main class="white_bg">
    <div class="container margin_60">
        <div class="row">
            <div class="col-md-12">
            <? if(count($models)): ?>
                <table class="table table-striped cart-list add_bottom_30">
                    <thead>
                        <tr>
                            <th width="70%">Туры онлайн</th>
                            <th>Стоимость от</th>
                            <th class="text-center">Действия</th>
                        </tr>
                    </thead>
                    <tbody>
                    <? foreach($models as $item): ?>
                        <tr data-cart-id="<?= $item->id ?>">
                            <td>
                                <? if($item->image): ?>
                                <div class="thumb_cart" style="background-image: url('<?= $item->getImage('100x100') ?>')"></div>
                                <? else: ?>
                                <div class="thumb_cart"></div>
                                <? endif; ?>
                                <span class="item_cart">
                                    <a href="<?= $item->link ?>" class="item_cart-name">
                                        <?= $item->name ?>
                                    </a>
                                    <? if($item->catModel !== null): ?>
                                    <div class="item_cart-cat">
                                        <i class="<?= $item->catModel->icon_css ?>"></i>
                                        <span><?= $item->catModel->name ?></span>
                                    </div>
                                    <? endif; ?>
                                </span>
                            </td>
                            <td>
                                <strong><?= $item->defaultPrice ?></strong>
                            </td>
                            <td class="options text-center">
                                <a href="#" onclick="return removeItemCart('<?= Yii::$app->cart->generateSignature($item->id) ?>', <?= $item->id ?>, this);" title="Удалить из корзины"><i class=" icon-trash"></i></a>
                                <?/*<a href="#" onclick="return false" title="Обновить"><i class="icon-ccw-2"></i></a>*/?>
                            </td>
                        </tr>
                    <? endforeach; ?>
                    </tbody>
                </table>
                <div class="row">
                    <div class="col-sm-6">
                        <a class="btn_1_outline mr-5" href="/tours?sort=online">
                            <i class="icon-right"></i> Вернуться в каталог
                        </a>
                    </div>
                    <div class="col-sm-6 text-right">
                        <a class="btn_1" href="<?= Yii::$app->cart->linkOrdering ?>">
                            Оформить заказ
                            <i class="icon-left"></i>
                        </a>
                    </div>
                </div>
            <? else: ?>
            <div class="text-center">
                <h4>Ваша корзина пустая, Заполните корзину онлайн турами,<br> чтобы оформить заказ.</h4>
                <a class="btn_1_outline mt-15" href="/tours?sort=online">
                    <i class="icon-right"></i> Перейти в каталог туров
                </a>
            </div>
            <? endif; ?>
            </div>
            <?/*<!-- End col-md-8 -->
            <aside class="col-md-4">
                <div class="box_style_1">
                    <h3 class="inner">- Резюме -</h3>
                    <table class="table table_summary">
                        <tbody>
                            <tr>
                                <td>
                                    Взрослые
                                </td>
                                <td class="text-right">
                                    2
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Дети
                                </td>
                                <td class="text-right">
                                    0
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Джип-тур
                                </td>
                                <td class="text-right">
                                    $34
                                </td>
                            </tr>
                            <tr class="total">
                                <td>
                                    Итого
                                </td>
                                <td class="text-right">
                                    $104
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <a class="btn_full" href="ordering.html">Оформить</a>
                    <a class="btn_full_outline" href="/tours?sort=online"><i class="icon-right"></i> Вернуться к поиску</a>
                </div>
            </aside>
            <!-- End aside -->*/?>
        </div>
        <!--End row -->
    </div>
    <!--End container -->
</main>
<!-- End main -->