<?php
/**
 * Body for form generation
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

use common\models\Form;
use common\models\FormField;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

?>

<?/* The result form is displayed above the form */?>
<? if($form_model->status && $model->success_position == Form::SUCCESS_POSITION_OVER_FORM): ?>
    <div class="form-success form-success-top">
        <?= $model->success_text ?>
    </div>
<? endif; ?>

<? if(Yii::$app->request->isAjax) {
    echo Html::hiddenInput(\Yii::$app->request->csrfParam, \Yii::$app->request->csrfToken, []);
}?>

<? foreach($model->fields as $item): ?>

    <? if(ArrayHelper::isIn($item->type, [
        FormField::TYPE_EMAIL,
        FormField::TYPE_INTEGER,
        FormField::TYPE_PHONE,
        FormField::TYPE_STRING,
    ])): ?>
        <?= $form->field($form_model, $item->attribute, [
            'template' => '{label}{input}{error}{hint}',
            'inputOptions' => ArrayHelper::merge([
                'class' => 'form-control form-input',
                'placeholder' => $form_model->getAttributePlaceholder($item->attribute, $item->placeholder),
                'id' => Html::getInputId($form_model, $item->attribute) . '-' . $model->id
            ], ($item->input_mask ? ['data-mask' => $item->input_mask] : [])),
            'options' => [
                'class' => 'form-group'
            ]
        ])->hint($item->hint)->label(
            $form_model->getAttributeLabelRequired($item->attribute, $item->placeholder), [
                'class' => 'form-label',
                'for' => Html::getInputId($form_model, $item->attribute) . '-' . $model->id
            ]
        ) ?>
    <? endif ?>

    <? if($item->type == FormField::TYPE_FILE): ?>
        <?= $form->field($form_model, $item->attribute, [
            'template' => '{label}{input}{error}{hint}',
            'inputOptions' => [
                'class' => 'form-control form-file',
                'id' => Html::getInputId($form_model, $item->attribute) . '-' . $model->id
            ],
            'options' => [
                'class' => 'form-group'
            ]
        ])->fileInput()->hint($item->hint)->label(
            $form_model->getAttributeLabelRequired($item->attribute), [
                'class' => 'form-label',
                'for' => Html::getInputId($form_model, $item->attribute) . '-' . $model->id
            ]
        ) ?>
    <? endif ?>

    <? if($item->type == FormField::TYPE_CHECKBOX): ?>
        <?= $form->field($form_model, $item->attribute, [
            'template' => '<label class="form-checkbox">{input}'. $item->label .'</label>{error}{hint}',
            'options' => [
                'class' => 'form-group',
            ]
        ])->checkbox([
            'id' => Html::getInputId($form_model, $item->attribute) . '-' . $model->id,
            'labelOptions' => [
                //'class' => 'form-label',
                'for' => Html::getInputId($form_model, $item->attribute) . '-' . $model->id
            ],
        ], false)->hint($item->hint) ?>
    <? endif ?>

    <? if($item->type == FormField::TYPE_RADIO_GROUP): ?>
        <?= $form->field($form_model, $item->attribute, [
            'radioTemplate' => '{label}{input}{error}{hint}',
            'options' => [
                'class' => 'form-group form-radio',
                'id' => Html::getInputId($form_model, $item->attribute) . '-' . $model->id
            ]
        ])->inline()->radioList($item->listExplode)->hint($item->hint)->label(
            $form_model->getAttributeLabelRequired($item->attribute, $item->placeholder), [
                //'class' => 'form-label',
                'for' => Html::getInputId($form_model, $item->attribute) . '-' . $model->id
            ]
        ) ?>
    <? endif ?>

    <? if($item->type == FormField::TYPE_SELECT): ?>
        <?= $form->field($form_model, $item->attribute, [
            'template' => '{label}{input}{error}{hint}',
            'inputOptions' => [
                'class' => 'form-control form-select',
                'prompt' => '-- Выберите --',
                'id' => Html::getInputId($form_model, $item->attribute) . '-' . $model->id,
            ],
            'options' => [
                'class' => 'form-group'
            ]
        ])->dropDownList($item->listExplode)->hint($item->hint)->label(
            $form_model->getAttributeLabelRequired($item->attribute, $item->placeholder), [
                'class' => 'form-label',
                'for' => Html::getInputId($form_model, $item->attribute) . '-' . $model->id
            ]
        ) ?>
    <? endif ?>

    <? if($item->type == FormField::TYPE_TEXTAREA): ?>
        <?= $form->field($form_model, $item->attribute, [
            'template' => '{label}{input}{error}{hint}',
            'inputOptions' => [
                'class' => 'form-control form-textarea',
                'placeholder' => $form_model->getAttributePlaceholder($item->attribute, $item->placeholder),
                'id' => Html::getInputId($form_model, $item->attribute) . '-' . $model->id
            ],
            'options' => [
                'class' => 'form-group'
            ]
        ])->textarea()->hint($item->hint)->label(
            $form_model->getAttributeLabelRequired($item->attribute, $item->placeholder), [
                'class' => 'form-label',
                'for' => Html::getInputId($form_model, $item->attribute) . '-' . $model->id
            ]
        ) ?>
    <? endif ?>

<? endforeach; ?>

<? if($model->captcha): ?>
    <?= $form->field($form_model, 'captcha', [
            'template' => '{label}{input}{error}{hint}',
            'inputOptions' => [
                'class' => 'form-control',
            ],
            'options' => [
                'class' => 'form-group row form-captcha'
            ]
        ])->widget(\yii\captcha\Captcha::className(), [
            'template' => '<div class="col-sm-3">{image}</div><div class="col-sm-9">{input}</div>',
            'captchaAction' => 'form/captcha',
            'imageOptions' => [
                'class' => 'form-captcha-image',
                'id' => Html::getInputId($form_model, 'captcha') . '-' . $model->id,
                'onclick' => 'captchaRefresh(this, \'form\'); return false;',
                'title' => 'Обновить изображение?',
            ],
            'options' => [
                'class' => 'form-control form-input',
                'placeholder' => $form_model->getAttributeLabelRequired('captcha')
            ]
        ])->hint(false)->label(
            $form_model->getAttributeLabelRequired('captcha'), [
                'class' => 'col-sm-12',
                'for' => Html::getInputId($form_model, 'captcha') . '-' . $model->id
            ]
        ) ?>
<? endif ?>

<div class="form-buttons">
    <button type="submit" class="btn btn-default form-btn-send">
        <span>
            <?= ($model->btn_name) ? $model->btn_name : Form::DEFAULT_BTN_NAME ?>
        </span>
    </button>
    <? if($model->btn_reset): ?>
        <button type="reset" class="btn btn-default form-btn-reset">
            <span>Сбросить</span>
        </button>
    <? endif ?>
</div>

<?/* The result form is displayed below the form */?>
<? if($form_model->status && $model->success_position == Form::SUCCESS_POSITION_UNDER_FORM): ?>
    <div class="form-success form-success-bottom">
        <?= $model->success_text ?>
    </div>
<? endif; ?>

<?/* The result of the form is displayed in the modal window */?>
<? if($form_model->status && $model->success_position == Form::SUCCESS_POSITION_MODAL): ?>
    <script>
        // The result of the form is displayed in the modal window
        if(typeof formResultModal === 'function') {
            formResultModal('<?= $model->name ?>', '<?= $model->success_text ?>');
        }
    </script>
<? endif; ?>