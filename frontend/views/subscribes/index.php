<div class="banner colored add_bottom_30">
    <div class="row" id="subscribes-form">
        <?= \Yii::$app->controller->renderPartial('//subscribes/form', [
            'model' => $model
        ]) ?>
    </div>
</div>