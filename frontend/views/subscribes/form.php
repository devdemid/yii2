<?php

use yii\bootstrap\ActiveForm;

?>
<? if(!$model->is_success): ?>
<div class="col-sm-4">
    <h4>Получить <span>подарок</span></h4>
    <p>Наши подписчики, получают подарки<br> при заказе тура!</p>
</div>
<div class="col-sm-8 subscribes-widget">
    <? $form = ActiveForm::begin([
        'action' => '/subscribes/new',
        'id' => 'subscribes-form',
        'options' => [
            'onsubmit' => "return requestForm(this, '#subscribes-form');",
            'class' => 'form-inline'
        ]
    ]); ?>
    <?= $form->field($model, 'email', [
        'template' => '<div>{input}<button type="submit" class="btn_1 white pull-right">Получить подарок!</button></div>{error}',
        'inputOptions' => [
            'class' => 'form-control',
            'placeholder' => $model->getAttributeLabel('email') . ':'
        ],
        'options' => [
            'id' => 'subscribes-wrapper-form'
        ]
    ]) ?>
    <? ActiveForm::end(); ?>
</div>
<? else: ?>
<div class="col-sm-12 text-center subscribes-widget-success">
    <?= $model->is_success ?>
</div>
<? endif; ?>