<? if($model->is_wrapper): ?>
<!-- Begin page section -->
<section>
    <?= $model->full_description ?>
</section>
<!--  End page section -->
<? else: ?>
<!-- Banner Area Starts -->
<section class="banner-area">
    <div class="banner-overlay">
        <div class="banner-text text-center">
            <div class="container">
                <!-- Section Title Starts -->
                <div class="row text-center">
                    <div class="col-xs-12">
                        <!-- Title Starts -->
                        <h2 class="title-head"><?= $model->name ?></h2>
                        <!-- Title Ends -->
                    </div>
                </div>
                <!-- Section Title Ends -->
            </div>
        </div>
    </div>
</section>
<!-- Banner Area Ends -->
<!-- Begin page section -->
<section>
    <div class="container">
        <div>
            <?= $model->full_description ?>
        </div>
    </div>
</section>
<!--  End page section -->

<?= \frontend\shortcodes\BlockShortcode::widget(['id' => 1, 'view' => 'block-reg']) ?>
<? endif; ?>