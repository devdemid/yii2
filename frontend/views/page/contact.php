<? if($model->is_wrapper): ?>
<!-- Begin page section -->
<section>
    <?= $model->full_description ?>
</section>
<!--  End page section -->
<? else: ?>
<!-- Banner Area Starts -->
<section class="banner-area">
    <div class="banner-overlay">
        <div class="banner-text text-center">
            <div class="container">
                <!-- Section Title Starts -->
                <div class="row text-center">
                    <div class="col-xs-12">
                        <!-- Title Starts -->
                        <h2 class="title-head"><?= $model->name ?></h2>
                        <!-- Title Ends -->
                    </div>
                </div>
                <!-- Section Title Ends -->
            </div>
        </div>
    </div>
</section>
<!-- Banner Area Ends -->
<!-- Contact Section Starts -->
<section class="contact">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-8 contact-form">
                <?= $model->full_description ?>
                <?= \frontend\shortcodes\FormShortcode::widget([
                    'id' => 1, 'title' => false, 'form_class' => 'contact-form'
                ]) ?>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="widget">
                    <div class="contact-page-info">
                        <?= \frontend\shortcodes\BlockShortcode::widget([
                            'id' => 5,
                            'view' => 'contact-info',
                            'icon' => 'fa-home'
                        ]) ?>
                        <?= \frontend\shortcodes\BlockShortcode::widget([
                            'id' => 6,
                            'view' => 'contact-info',
                            'icon' => 'fa-phone'
                        ]) ?>
                        <?= \frontend\shortcodes\BlockShortcode::widget([
                            'id' => 7,
                            'view' => 'contact-info',
                            'icon' => 'fa-envelope'
                        ]) ?>
                        <!-- Social Media Icons Starts -->
                        <div class="contact-info-box">
                            <i class="fa fa-share-alt big-icon"></i>
                            <div class="contact-info-box-content">
                                <h4>Социальные профили</h4>
                                <div class="social-contact">
                                    <ul>
                                        <? if(Yii::$app->options->get('facebook')): ?>
                                        <li class="facebook">
                                            <a href="<?= Yii::$app->options->get('facebook') ?>" target="_blank">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                        </li>
                                        <? endif ?>
                                        <? if(Yii::$app->options->get('twitter')): ?>
                                        <li class="twitter">
                                            <a href="<?= Yii::$app->options->get('twitter') ?>" target="_blank">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                        </li>
                                        <? endif ?>
                                        <? if(Yii::$app->options->get('googleplus')): ?>
                                        <li class="google-plus">
                                            <a href="<?= Yii::$app->options->get('googleplus') ?>" target="_blank">
                                                <i class="fa fa-google-plus"></i>
                                            </a>
                                        </li>
                                        <? endif ?>
                                        <? if(Yii::$app->options->get('linkedin')): ?>
                                        <li class="linkedin">
                                            <a href="<?= Yii::$app->options->get('linkedin') ?>" target="_blank">
                                                <i class="fa fa-linkedin"></i>
                                            </a>
                                        </li>
                                        <? endif ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- Social Media Icons Starts -->
                    </div>
                </div>
                <!-- Contact Widget Ends -->
            </div>
        </div>
    </div>
</section>
<!-- Contact Section Ends -->

<?= \frontend\shortcodes\BlockShortcode::widget(['id' => 1, 'view' => 'block-reg']) ?>
<? endif; ?>