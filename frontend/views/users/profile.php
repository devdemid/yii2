<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Json;

?>
<section class="parallax-window" data-parallax="scroll" data-image-src="/img/account_top.jpg" data-natural-width="1400" data-natural-height="470">
    <div class="parallax-content-1">
        <div class="animated fadeInDown">
            <h1>Личный кабинет</h1>
            <p>Информация личного кабинета</p>
        </div>
    </div>
</section>
<!-- End section -->
<main>
    <div class="margin_60 container">
        <div class="tabs profile-tabs">
            <nav>
                <ul>
                    <li class="tab-current">
                        <a href="#profile" onclick="return false;" class="icon-user-5"><span>Информация</span></a>
                    </li>
                    <li>
                        <a href="<?= Yii::$app->user->url->orders ?>" class="icon-cart"><span>Мои заказы</span></a>
                    </li>
                    <li>
                        <a href="<?= Yii::$app->user->url->settings ?>" class="icon-cog-3"><span>Настройки</span></a>
                    </li>
                </ul>
            </nav>
            <div class="content">
                <section class="content-current">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <h4>Информация о пользователе</h4>
                            <ul id="profile_summary">
                                <li>Имя <span><?= $model->first_name ?></span></li>
                                <li>Фамилия <span><?= $model->second_name ?></span></li>
                                <li>Email <span><?= $model->email ?></span></li>
                                <? if($model->phone): ?>
                                <li>Номер телефона <span><?= $model->phone ?></span></li>
                                <? endif; ?>
                                <? if($model->country): ?>
                                <li>Страна <span><?= $model->country ?></span></li>
                                <? endif; ?>
                                <? if($model->city): ?>
                                <li>Город <span><?= $model->city ?></span></li>
                                <? endif; ?>
                                <? if($model->address): ?>
                                <li>Адрес <span><?= $model->address ?></span></li>
                                <? endif; ?>
                                <? if($model->index): ?>
                                <li>Индекс<span><?= $model->index ?></span></li>
                                <? endif; ?>
                            </ul>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <? if($model->pathLogo): ?>
                            <img src="<?= $model->pathLogo ?>" alt="<?= Html::encode($model->nameTogether) ?>" class="img-responsive styled profile_pic" id="user-avatar">
                            <? else: ?>
                            <img src="/img/noimage.jpg" alt="<?= Html::encode($model->nameTogether) ?>" class="img-responsive styled profile_pic" id="user-avatar">
                            <? endif ?>
                        </div>
                    </div>
                    <!-- End row -->
                    <div class="divider"></div>
                    <? $form = ActiveForm::begin(['id' => 'profile-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Изменить данные</h4>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <?= $form->field($model, 'first_name',  [
                                        'template' => '{label}{input}{error}',
                                        'inputOptions' => [
                                            'class'        => 'form-control',
                                            'autocomplete' => 'off',
                                            'placeholder'  => 'Ваше имя',
                                        ],
                                        'options' => [
                                            'class' => 'form-group'
                                        ]
                                    ])->label(
                                        $model->getAttributeLabelRequired('first_name')
                                    ) ?>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <?= $form->field($model, 'second_name',  [
                                        'template' => '{label}{input}{error}',
                                        'inputOptions' => [
                                            'class'        => 'form-control',
                                            'autocomplete' => 'off',
                                            'placeholder'  => 'Ваша фамилия',
                                        ],
                                        'options' => [
                                            'class' => 'form-group'
                                        ]
                                    ])->label(
                                        $model->getAttributeLabelRequired('second_name')
                                    ) ?>
                                </div>
                            </div>
                        </div>
                        <!-- End row -->
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="form-group">
                                    <?= $form->field($model, 'phone',  [
                                        'template' => '{label}{input}{error}',
                                        'inputOptions' => [
                                            'class'        => 'form-control',
                                            'autocomplete' => 'off',
                                            'placeholder'  => 'Ваш номер телефона',
                                            'data-mask' => '+0 (000) 00 00 000'
                                        ],
                                        'options' => [
                                            'class' => 'form-group'
                                        ]
                                    ])->label(
                                        $model->getAttributeLabelRequired('phone')
                                    ) ?>
                                </div>
                            </div>
                        </div>
                        <!-- End row -->
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Изменить адрес</h4>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <?= $form->field($model, 'country',  [
                                        'template' => '{label}{input}{error}',
                                        'inputOptions' => [
                                            'class'        => 'form-control',
                                            'autocomplete' => 'off',
                                            'placeholder'  => 'Ваша страна',
                                        ],
                                        'options' => [
                                            'class' => 'form-group'
                                        ]
                                    ])->label(
                                        $model->getAttributeLabelRequired('country')
                                    ) ?>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <?= $form->field($model, 'city',  [
                                        'template' => '{label}{input}{error}',
                                        'inputOptions' => [
                                            'class'        => 'form-control',
                                            'autocomplete' => 'off',
                                            'placeholder'  => 'Ваш город',
                                        ],
                                        'options' => [
                                            'class' => 'form-group'
                                        ]
                                    ])->label(
                                        $model->getAttributeLabelRequired('city')
                                    ) ?>
                                </div>
                            </div>
                        </div>
                        <!-- End row -->
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <?= $form->field($model, 'address',  [
                                        'template' => '{label}{input}{error}',
                                        'inputOptions' => [
                                            'class'        => 'form-control',
                                            'autocomplete' => 'off',
                                            'placeholder'  => 'Ваш адрес',
                                        ],
                                        'options' => [
                                            'class' => 'form-group'
                                        ]
                                    ])->label(
                                        $model->getAttributeLabelRequired('address')
                                    ) ?>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <?= $form->field($model, 'index',  [
                                        'template' => '{label}{input}{error}',
                                        'inputOptions' => [
                                            'class'        => 'form-control',
                                            'autocomplete' => 'off',
                                            'placeholder'  => 'Ваш индекс',
                                        ],
                                        'options' => [
                                            'class' => 'form-group'
                                        ]
                                    ])->label(
                                        $model->getAttributeLabelRequired('index')
                                    ) ?>
                                </div>
                            </div>
                        </div>
                        <!-- End row -->
                        <hr>
                        <h4>Загрузка фотографии</h4>
                        <div>
                            <div class="upload-drop-zone dropzone needsclick" id="drop-zone" data-dropzone-url="/users/avatar" data-dropzone-params='<?= Json::encode([Yii::$app->request->csrfParam => Yii::$app->request->csrfToken, 'Files[item_id]' => Yii::$app->user->identity->id, 'Files[module]' => 'User', 'Files[type]' => 1, 'Files[sizes]' => Yii::$app->user->identity->image_sizes[0]]) ?>' data-dropzone-param-name="Files[uploadedFile]" data-dropzone-returnid="#user-avatar">
                                <span class="dz-message needsclick">Перетащите файл сюда или нажмите, чтобы загрузить.</span>
                            </div>
                            <hr>
                            <button type="submit" class="btn_1 green">Обновить информацию</button>
                        </div>
                    <? ActiveForm::end(); ?>
                </section>
            </div>
        </div>
    </div>
</main>