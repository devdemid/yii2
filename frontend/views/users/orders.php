<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
?>
<section class="parallax-window" data-parallax="scroll" data-image-src="/img/account_top.jpg" data-natural-width="1400" data-natural-height="470">
    <div class="parallax-content-1">
        <div class="animated fadeInDown">
            <h1>Личный кабинет</h1>
            <p>История заказов</p>
        </div>
    </div>
</section>
<!-- End section -->
<main>
    <div class="margin_60 container">
        <div class="tabs profile-tabs">
            <nav>
                <ul>
                    <li>
                        <a href="<?= Yii::$app->user->url->profile ?>" class="icon-user-5"><span>Информация</span></a>
                    </li>
                    <li class="tab-current">
                        <a href="#orders" onclick="return false;" class="icon-cart"><span>Мои заказы</span></a>
                    </li>
                    <li>
                        <a href="<?= Yii::$app->user->url->settings ?>" class="icon-cog-3"><span>Настройки</span></a>
                    </li>
                </ul>
            </nav>
            <div class="content">
                <section class="content-current">
                <? if(count($models)): ?>
                    <div id="tools">
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-6">
                                <div class="styled-select-filters">
                                    <?= Html::dropDownList('sort', $filter_sort, $filters, [
                                        'prompt' => 'Сортировать по дате',
                                        'id' => 'sort_date',
                                        'class' => 'url-filter'
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <? foreach($models as $item): ?>
                    <div class="strip_booking">
                        <div class="row">
                            <div class="col-md-2 col-sm-2">
                                <div class="date">
                                    <span class="month">Номер заказа</span>
                                    <span class="day">
                                        <strong>#ID <?= $item->id ?></strong>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-3 <? if(count($item->orderData) > 1): ?> first-order-data<? endif; ?>">
                            <? if(count($item->orderData)): ?>
                                <? foreach($item->orderData as $i =>  $data): ?>
                                <h3>
                                    <?= $data->name ?>
                                    <span>
                                        Взрослые: <b><?= $data-> adults ?></b> чел.
                                        <? if($data->children): ?> / Дети: <b><?= $data->children ?></b> чел.<? endif; ?>
                                    </span>
                                </h3>
                                <? endforeach ?>
                            <? endif; ?>
                            </div>
                            <div class="col-md-2 col-sm-3 text-center">
                                <div class="mt-20">Стоимость заказа:</div>
                                <strong><?= $item->totalPriceFormat ?></strong>
                            </div>
                            <div class="col-md-2 col-sm-3">
                                <ul class="info_booking">
                                    <li>
                                        <strong>Заказ создан</strong>
                                        <?= date($item::OUT_DATE_FORMAT, $item->create_timestamp) ?>
                                    </li>
                                    <li>
                                        <strong>Заказ оплачен</strong>
                                        <? if($item->status === $item::STATUS_SUCCESS): ?>
                                        <?= date($item::OUT_DATE_FORMAT, $item->paid_timestamp) ?>
                                        <? else: ?>
                                        <?= $item::getStatusName($item->status) ?>
                                        <? endif; ?>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <div class="booking_buttons">
                                    <? if($item->status !== $item::STATUS_SUCCESS): ?>
                                    <a href="<?= $item->linkPay ?>" class="btn_2">Оплатить заказ</a>
                                    <a href="<?= $item->linkCancel ?>" class="btn_4">Отменить заказ</a>
                                    <? else: ?>
                                    <a href="<?= $item->linkDetail ?>" class="btn_3">Детально</a>
                                    <? endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <? endforeach; ?>
                <? else: ?>
                    <div class="text-center">
                        <h3>Нет записей для отображения!</h3>
                    </div>
                <? endif; ?>
                </section>
                <? if($pages->getPageCount() > 1): ?>
                <div class="text-center">
                    <?= LinkPager::widget([
                        'pagination' => $pages,
                        'options'    => [
                            'class' => 'pagination'
                        ]
                    ]); ?>
                    <!-- end pagination-->
                </div>
                <? endif; ?>
            </div>
        </div>
    </div>
</main>