<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<header class="not-sticky">
    <div id="top_line">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-9 col-xs-9">
                    <i class="icon-phone"></i><strong>+995 595-73-33-11</strong>
                    <span id="opening"><i class="icon-phone"></i><strong>+995 595-71-33-11</strong></span>
                    <span id="opening"><i class="icon-mail-1"></i><?= Yii::$app->options->get('primary_email') ?></span>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <ul id="top_links">
                        <li id="social_top">
                            <a href="#0"><i class="icon-facebook"></i></a>
                            <a href="#0"><i class="icon-twitter"></i></a>
                            <a href="#0"><i class="icon-google"></i></a>
                            <a href="#0"><i class="icon-instagramm"></i></a>
                            <a href="#0"><i class="icon-vimeo"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- End row -->
        </div>
        <!-- End container-->
    </div>
    <!-- End top line-->
</header>
<main class="full-page">
    <section id="hero" class="login">
        <div class="container">
            <div class="row mb-30">
                <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                    <div id="login">
                        <div class="text-center">
                            <a href="/">
                                <img src="/img/logo_sticky_2x.png" alt="<?= Yii::$app->options->get('home_title') ?>" data-retina="true" class="img-responsive">
                            </a>
                        </div>
                        <hr>
                        <div class="text-center">
                            <strong>Забыли пароль?</strong>
                            <p><small>Мы вышлем вам инструкции по электронной почте</small></p>
                        </div>
                        <hr>
                        <form action="" method="post">
                            <div class="form-group">
                                <label for="">Ваш email:</label>
                                <input type="text" class="form-control" placeholder="email@example.com">
                            </div>
                            <button type="submit" class="btn_full">Сброс пароля</button>
                            <a href="<?= Yii::$app->user->url->signin ?>" class="btn_full_outline">Войти</a>
                        </form>
                    </div>
                    <div class="text-center">
                        <?= Yii::$app->options->get('copyright') ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main><!-- End main -->