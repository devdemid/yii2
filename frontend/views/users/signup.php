<?/*= Yii::$app->user->url->signup */?>
<?/*= Yii::$app->user->url->reset_password */?>
<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<header class="not-sticky">
    <div id="top_line">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-9 col-xs-9">
                    <i class="icon-phone"></i><strong>+995 595-73-33-11</strong>
                    <span id="opening"><i class="icon-phone"></i><strong>+995 595-71-33-11</strong></span>
                    <span id="opening"><i class="icon-mail-1"></i><?= Yii::$app->options->get('primary_email') ?></span>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <ul id="top_links">
                        <li id="social_top">
                            <a href="#0"><i class="icon-facebook"></i></a>
                            <a href="#0"><i class="icon-twitter"></i></a>
                            <a href="#0"><i class="icon-google"></i></a>
                            <a href="#0"><i class="icon-instagramm"></i></a>
                            <a href="#0"><i class="icon-vimeo"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- End row -->
        </div>
        <!-- End container-->
    </div>
    <!-- End top line-->
</header>
<main>
    <section id="hero" class="login">
        <div class="container">
            <div class="row mb-30">
                <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
                    <div id="login">
                        <div class="text-center">
                            <a href="/" class="d-inline-block">
                                <img src="/img/logo_sticky.png" alt="<?= Yii::$app->options->get('home_title') ?>" data-retina="true" class="img-responsive">
                            </a>
                        </div>
                        <hr>
                        <? $form = ActiveForm::begin(['id' => 'register-form']); ?>
                            <div class="row">
                                <div class="col-sm-6">
                                    <?= $form->field($model, 'first_name',  [
                                        'template' => '{input}{error}',
                                        'inputOptions' => [
                                            'class'        => 'form-control',
                                            'autocomplete' => 'off',
                                            'placeholder'  => $model->getAttributeLabel('first_name'),
                                        ],
                                        'options' => [
                                            'class' => 'form-group'
                                        ]
                                    ])->label(false)->hint(false) ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= $form->field($model, 'second_name',  [
                                        'template' => '{input}{error}',
                                        'inputOptions' => [
                                            'class'        => 'form-control',
                                            'autocomplete' => 'off',
                                            'placeholder'  => $model->getAttributeLabel('second_name'),
                                        ],
                                        'options' => [
                                            'class' => 'form-group'
                                        ]
                                    ])->label(false)->hint(false) ?>
                                </div>
                            </div>
                            <?= $form->field($model, 'phone',  [
                                'template' => '{input}{error}',
                                'inputOptions' => [
                                    'class'        => 'form-control',
                                    'data-mask'    => '+0 (000) 00 00 000',
                                    'autocomplete' => 'off',
                                    'placeholder'  => $model->getAttributeLabel('phone'),
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->label(false)->hint(false) ?>
                            <hr>
                            <div class="row">
                                <div class="col-sm-6">
                                    <?= $form->field($model, 'email',  [
                                        'template' => '{input}{error}',
                                        'inputOptions' => [
                                            'class'        => 'form-control',
                                            'autocomplete' => 'off',
                                            'placeholder'  => $model->getAttributeLabel('email'),
                                        ],
                                        'options' => [
                                            'class' => 'form-group'
                                        ]
                                    ])->label(false)->hint(false) ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= $form->field($model, 'email_repeat',  [
                                        'template' => '{input}{error}',
                                        'inputOptions' => [
                                            'class'        => 'form-control',
                                            'autocomplete' => 'off',
                                            'placeholder'  => $model->getAttributeLabel('email_repeat'),
                                        ],
                                        'options' => [
                                            'class' => 'form-group'
                                        ]
                                    ])->label(false)->hint(false) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <?= $form->field($model, 'password',  [
                                        'template' => '{input}{error}',
                                        'inputOptions' => [
                                            'class'        => 'form-control',
                                            'autocomplete' => 'off',
                                            'placeholder'  => $model->getAttributeLabel('password'),
                                        ],
                                        'options' => [
                                            'class' => 'form-group'
                                        ]
                                    ])->label(false)->hint(false) ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= $form->field($model, 'password_repeat',  [
                                        'template' => '{input}{error}',
                                        'inputOptions' => [
                                            'class'        => 'form-control',
                                            'autocomplete' => 'off',
                                            'placeholder'  => $model->getAttributeLabel('password_repeat'),
                                        ],
                                        'options' => [
                                            'class' => 'form-group'
                                        ]
                                    ])->label(false)->hint(false) ?>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-6">
                                    <a href="<?= Yii::$app->user->url->signin ?>" class="btn_full_outline">Войти</a>
                                </div>
                                <div class="col-sm-6">
                                    <button type="submit" class="btn_full">Регистрация</button>
                                </div>
                            </div>
                        <? ActiveForm::end(); ?>
                    </div>
                    <div class="text-center">
                        <?= Yii::$app->options->get('copyright') ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main><!-- End main -->

