<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<section class="parallax-window" data-parallax="scroll" data-image-src="/img/account_top.jpg" data-natural-width="1400" data-natural-height="470">
    <div class="parallax-content-1">
        <div class="animated fadeInDown">
            <h1>Личный кабинет</h1>
            <p>Настройки личного кабинета</p>
        </div>
    </div>
</section>
<!-- End section -->
<main>
    <div class="margin_60 container">
        <div class="tabs profile-tabs">
            <nav>
                <ul>
                    <li>
                        <a href="<?= Yii::$app->user->url->profile ?>" class="icon-user-5"><span>Информация</span></a>
                    </li>
                    <li>
                        <a href="<?= Yii::$app->user->url->orders ?>" class="icon-cart"><span>Мои заказы</span></a>
                    </li>
                    <li class="tab-current">
                        <a href="#settings" onclick="return false;" class="icon-cog-3"><span>Настройки</span></a>
                    </li>
                </ul>
            </nav>
            <div class="content">
                <section class="content-current">
                    <div class="row">
                            <div class="col-md-6 col-sm-6 add_bottom_30">
                                <? $form = ActiveForm::begin(['id' => 'change-password-form']); ?>
                                    <h4>Изменить пароль</h4>
                                    <?= $form->field($change_password, 'old_password',  [
                                        'template' => '{label}{input}{error}',
                                        'inputOptions' => [
                                            'class'        => 'form-control',
                                            'autocomplete' => 'off',
                                        ],
                                        'options' => [
                                            'class' => 'form-group'
                                        ]
                                    ])->label(
                                        $change_password->getAttributeLabelRequired('old_password')
                                    )->passwordInput() ?>
                                    <?= $form->field($change_password, 'new_password',  [
                                        'template' => '{label}{input}{error}',
                                        'inputOptions' => [
                                            'class'        => 'form-control',
                                            'autocomplete' => 'off',
                                        ],
                                        'options' => [
                                            'class' => 'form-group'
                                        ]
                                    ])->label(
                                        $change_password->getAttributeLabelRequired('new_password')
                                    ) ?>
                                    <?= $form->field($change_password, 'password_repeat',  [
                                        'template' => '{label}{input}{error}',
                                        'inputOptions' => [
                                            'class'        => 'form-control',
                                            'autocomplete' => 'off',
                                        ],
                                        'options' => [
                                            'class' => 'form-group'
                                        ]
                                    ])->label(
                                        $change_password->getAttributeLabelRequired('password_repeat')
                                    )->passwordInput() ?>
                                    <button type="submit" class="btn_1 green">Изменить пароль</button>
                                <? ActiveForm::end(); ?>
                            </div>
                            <div class="col-md-6 col-sm-6 add_bottom_30">
                                <? $form = ActiveForm::begin(['id' => 'change-email-form']); ?>
                                    <h4>Изменить Email</h4>
                                    <?= $form->field($change_email, 'old_email',  [
                                        'template' => '{label}{input}{error}',
                                        'inputOptions' => [
                                            'class'        => 'form-control',
                                            'autocomplete' => 'off',
                                        ],
                                        'options' => [
                                            'class' => 'form-group'
                                        ]
                                    ])->label(
                                        $change_email->getAttributeLabelRequired('old_email')
                                    ) ?>
                                    <?= $form->field($change_email, 'new_email',  [
                                        'template' => '{label}{input}{error}',
                                        'inputOptions' => [
                                            'class'        => 'form-control',
                                            'autocomplete' => 'off',
                                        ],
                                        'options' => [
                                            'class' => 'form-group'
                                        ]
                                    ])->label(
                                        $change_email->getAttributeLabelRequired('new_email')
                                    ) ?>
                                    <?= $form->field($change_email, 'email_repeat',  [
                                        'template' => '{label}{input}{error}',
                                        'inputOptions' => [
                                            'class'        => 'form-control',
                                            'autocomplete' => 'off',
                                        ],
                                        'options' => [
                                            'class' => 'form-group'
                                        ]
                                    ])->label(
                                        $change_email->getAttributeLabelRequired('email_repeat')
                                    ) ?>
                                    <button type="submit" class="btn_1 green">Изменить Email</button>
                                <? ActiveForm::end(); ?>
                            </div>
                        </div>
                        <!-- End row -->
                        <hr>
                        <br>
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <h4>Настройки уведомлений</h4>
                                <? $form = ActiveForm::begin(['id' => 'change-notification-form']); ?>
                                    <table class="table table-striped options_cart">
                                        <tbody>
                                            <tr>
                                                <td style="width:10%"><i class="icon_set_1_icon-33"></i></td>
                                                <td style="width:60%">
                                                    <?= $change_notification->getAttributeLabelRequired('notification_new_tours') ?>
                                                </td>
                                                <td style="width:35%">
                                                    <?=  $form->field($change_notification, 'notification_new_tours')->checkbox([
                                                        'template' => '<label class="switch-light switch-ios pull-right">{input}<span><span>Нет</span><span>Да</span></span><a></a></label>'
                                                    ]) ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><i class="icon_set_1_icon-81"></i></td>
                                                <td>
                                                    <?= $change_notification->getAttributeLabelRequired('notification_comp_news') ?>
                                                </td>
                                                <td>
                                                    <?=  $form->field($change_notification, 'notification_comp_news')->checkbox([
                                                        'template' => '<label class="switch-light switch-ios pull-right">{input}<span><span>Нет</span><span>Да</span></span><a></a></label>'
                                                    ]) ?>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <button type="submit" class="btn_1 green">Обновить настройки уведомлений</button>
                                <? ActiveForm::end(); ?>
                            </div>
                        </div>
                        <!-- End row -->
                </section>
            </div>
        </div>
    </div>
</main>