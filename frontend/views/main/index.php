<?= \frontend\shortcodes\SliderShortcode::widget(['id' => 1]) ?>
<?= \frontend\shortcodes\BlockShortcode::widget(['id' => 4, 'view' => 'features-row']) ?>
<?= \frontend\shortcodes\BlockShortcode::widget(['id' => 9, 'view' => 'about-us']) ?>
<?= \frontend\shortcodes\BlockShortcode::widget(['id' => 12, 'view' => 'crypto-system']) ?>
<?= \frontend\shortcodes\BlockShortcode::widget(['id' => 11, 'view' => 'advantages-cryptotrade']) ?>
<?= \frontend\shortcodes\BlockShortcode::widget(['id' => 13, 'view' => 'block-investor']) ?>
<?= \frontend\shortcodes\NewsFeedShortcode::widget(['id' => 2]) ?>

<!-- Begin section-faq -->
<section id="section-faq" class="features-row">
    <div class="container">
        <?= \frontend\shortcodes\FaqShortcode::widget(['id' => 1]) ?>
    </div>
</section>
<!--End section-faq -->

<!-- Begin present-block -->
<div class="text-center present-block">
    <div>
        <i class="fa fa-file-pdf-o"></i>
        <a href="<?= Yii::$app->options->get('present_link') ?>" target="_blank">Узнать больше о CryptoTrade (pdf)</a>
    </div>
</div>
<!-- End present-block -->

<?= \frontend\shortcodes\NewsFeedShortcode::widget(['id' => 1]) ?>
<?= \frontend\shortcodes\BlockShortcode::widget(['id' => 3, 'view' => 'block-btc']) ?>
<?/*<!-- Quote and Chart Section Starts -->
<section class="image-block2">
    <div class="container">
        <div class="row">
            <!-- Chart Starts -->
            <div class="col-md-12 bg-grey-chart">
                <div class="chart-widget dark-chart chart-1">
                    <div>
                        <div class="btcwdgt-chart" data-bw-theme="light"></div>
                    </div>
                </div>
            </div>
            <!-- Chart Ends -->
        </div>
    </div>
</section>
<!-- Quote and Chart Section Ends -->*/?>

<!-- Contact Section Starts -->
<section class="contact" id="section-contact">
    <div class="container">
        <?/* <h2 class="title-head">Контакты</h2> */?>
        <div class="row">
            <div class="col-xs-12 col-md-8 col-md-offset-2 contact-form">
                <div class="text-center">
                    <?= \frontend\shortcodes\BlockShortcode::widget(['id' => 8]) ?>
                </div>
                <?= \frontend\shortcodes\FormShortcode::widget([
                    'id' => 1, 'title' => false, 'form_class' => 'contact-form'
                ]) ?>
            </div>
            <?/*<div class="col-xs-12 col-md-4">
                <div class="widget">
                    <div class="contact-page-info">
                        <?= \frontend\shortcodes\BlockShortcode::widget([
                            'id' => 5,
                            'view' => 'contact-info',
                            'icon' => 'fa-home'
                        ]) ?>
                        <?= \frontend\shortcodes\BlockShortcode::widget([
                            'id' => 6,
                            'view' => 'contact-info',
                            'icon' => 'fa-phone'
                        ]) ?>
                        <?= \frontend\shortcodes\BlockShortcode::widget([
                            'id' => 7,
                            'view' => 'contact-info',
                            'icon' => 'fa-envelope'
                        ]) ?>
                        <!-- Social Media Icons Starts -->
                        <div class="contact-info-box">
                            <i class="fa fa-share-alt big-icon"></i>
                            <div class="contact-info-box-content">
                                <h4>Социальные профили</h4>
                                <div class="social-contact">
                                    <ul>
                                        <? if(Yii::$app->options->get('facebook')): ?>
                                        <li class="facebook">
                                            <a href="<?= Yii::$app->options->get('facebook') ?>" target="_blank">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                        </li>
                                        <? endif ?>
                                        <? if(Yii::$app->options->get('twitter')): ?>
                                        <li class="twitter">
                                            <a href="<?= Yii::$app->options->get('twitter') ?>" target="_blank">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                        </li>
                                        <? endif ?>
                                        <? if(Yii::$app->options->get('googleplus')): ?>
                                        <li class="google-plus">
                                            <a href="<?= Yii::$app->options->get('googleplus') ?>" target="_blank">
                                                <i class="fa fa-google-plus"></i>
                                            </a>
                                        </li>
                                        <? endif ?>
                                        <? if(Yii::$app->options->get('linkedin')): ?>
                                        <li class="linkedin">
                                            <a href="<?= Yii::$app->options->get('linkedin') ?>" target="_blank">
                                                <i class="fa fa-linkedin"></i>
                                            </a>
                                        </li>
                                        <? endif ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- Social Media Icons Starts -->
                    </div>
                </div>
                <!-- Contact Widget Ends -->
            </div>*/?>
        </div>
    </div>
</section>
<!-- Contact Section Ends -->

<?= \frontend\shortcodes\BlockShortcode::widget(['id' => 1, 'view' => 'block-reg']) ?>