<?php
    use yii\helpers\Html;
    use Yii;
?>

<!-- Begin container-fluid -->
<div class="container-fluid error">
    <div>
        <div class="text-center">
            <!-- Logo Starts -->
            <a class="logo" href="" title="<?= Yii::$app->options->get('home_title') ?>">
                <img class="img-responsive" src="/images/logo.png" alt="<?= Yii::$app->options->get('home_title') ?>">
            </a>
            <!-- Logo Ends -->
            <!-- Error 404 Content Starts -->
            <div class="big-404"><?= $statusCode ?></div>
            <h3><?= $name ?></h3>
            <p><?= nl2br(Html::encode($message)) ?></p>
            <a href="/" class="btn btn-primary">
                Главная страница
            </a>
            <!-- Error 404 Content Starts -->
        </div>
    </div>
</div>
<!-- End container-fluid -->