<style>
    html, body {
        background: #f1f1f1;
    }
</style>
<div class="container text-center">
    <div style="padding-top: 15%">
        <?= $offline_message ?>
    </div>
    <small><?= $copyright ?></small>
</div><!-- /.container -->
