<div class="modal" id="reservation">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Забронировать тур</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= \frontend\shortcodes\FormShortcode::widget(['id' => 2, 'title' => false]) ?>
            </div>
        </div>
    </div>
</div>