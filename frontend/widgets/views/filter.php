<?php

use yii\helpers\Html;
use \common\models\DataGroup;
use \common\models\TourPrices;
use yii\helpers\ArrayHelper;

// Get Data group
$cat_models         = DataGroup::getGroupModel(DataGroup::GROUP_TOURS_CAT_ID);
$type_models        = DataGroup::getGroupModel(DataGroup::GROUP_TOURS_TYPE_ID);
$restriction_models = DataGroup::getGroupModel(DataGroup::GROUP_TOURS_RESTRICTION_ID);

// Defaukt icon
$default_imagesrc = '/img/icons_search/all_tours.png';

?>
<section id="search_container">
    <div id="search">
        <div class="tab-content">
            <?= Html::beginForm('/tours', 'get') ?>
                <div class="tab-pane active" id="tours">
                    <h3>Подобрать тур в Грузии</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Поиск</label>
                                <?= Html::input('text', 'q', null, [
                                    'class' => 'form-control',
                                    'placeholder' => "Например: 'Отдых в Тбилиси' или 'Мцхета'..."
                                ]) ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="filter_type">
                                <?= Html::input('text', 'Filters[range]', null, [
                                    'data-range'   => 'head-range',
                                    'data-min'     => $filters->minPrice,
                                    'data-max'     => $filters->maxPrice,
                                    'data-postfix' => TourPrices::DEFAULT_CURRENCY_NAME,
                                    'data-step'    => 500,
                                    'data-from'    => ($filters->range[0] ? $filters->range[0] : $filters->minPrice),
                                    'data-to'      => ($filters->range[1] ? $filters->range[1] : $filters->maxPrice)
                                ]) ?>
                            </div>
                        </div>
                    </div>
                    <!-- End row -->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Категория тура</label>
                                <?= Html::dropDownList('Filters[cat_id]', $filters->cat_id, ArrayHelper::map($cat_models, 'id', 'name'), [
                                    'class' => 'ddslick',
                                    'prompt' => [
                                        'text' => 'Все туры',
                                        'options' => [
                                            'data-imagesrc' => $default_imagesrc
                                        ]
                                    ],
                                    'options' => ArrayHelper::map($cat_models, 'id', 'imagesrc')
                                ]); ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Тип</label>
                                <?= Html::dropDownList('Filters[type_id]', $filters->type_id, ArrayHelper::map($type_models, 'id', 'name'), [
                                    'class' => 'ddslick',
                                    'prompt' => [
                                        'text' => 'Все типы',
                                        'options' => [
                                            'data-imagesrc' => $default_imagesrc
                                        ]
                                    ],
                                    'options' => ArrayHelper::map($type_models, 'id', 'imagesrc')
                                ]); ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Ограничения</label>
                                <?= Html::dropDownList('Filters[restriction_id]', $filters->restriction_id, ArrayHelper::map($restriction_models, 'id', 'name'), [
                                    'class' => 'ddslick',
                                    'prompt' => [
                                        'text' => 'Без ограничений',
                                        'options' => [
                                            'data-imagesrc' => $default_imagesrc
                                        ]
                                    ],
                                    'options' => ArrayHelper::map($restriction_models, 'id', 'imagesrc')
                                ]); ?>
                            </div>
                        </div>
                    </div>
                    <!-- End row -->
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label><i class="icon-calendar-7"></i> С</label>
                                <?= Html::input('text', 'Filters[start_date]', $filters->start_date, [
                                    'class' => 'date-pick form-control',
                                    'data-date-format' => 'yyyy-mm-dd',
                                    'data-date-start-date' => '<?= $filters->start_date ?>',
                                    'data-storage' => 'start_date',
                                ]) ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label><i class="icon-calendar-7"></i> По</label>
                                <?= Html::input('text', 'Filters[end_date]', $filters->end_date, [
                                    'class' => 'date-pick form-control',
                                    'data-date-format' => 'yyyy-mm-dd',
                                    'data-date-start-date' => '<?= $filters->end_date ?>',
                                    'data-storage' => 'end_date',
                                ]) ?>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label>Взрослые</label>
                                <div class="numbers-row">
                                    <?= Html::input('text', 'Filters[adults]', $filters->adults, [
                                        'class' => 'qty2 form-control',
                                        'data-storage' => 'adults',
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label>Дети</label>
                                <div class="numbers-row">
                                    <?= Html::input('text', 'Filters[children]', $filters->children, [
                                        'class' => 'qty2 form-control ',
                                        'data-storage' => 'children',
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End row -->
                    <hr>
                    <button class="btn_1 green"><i class="icon-search"></i>Подобрать</button>
                </div>
                <!-- End rab -->
            <?= Html::endForm() ?>
        </div>
    </div>
</section>
<!-- End hero -->