<?php

use yii\helpers\Html;
use \common\models\DataGroup;
use \common\models\TourPrices;
use yii\helpers\ArrayHelper;

// Get Data group
$cat_models         = DataGroup::getGroupModel(DataGroup::GROUP_TOURS_CAT_ID);
$type_models        = DataGroup::getGroupModel(DataGroup::GROUP_TOURS_TYPE_ID);
$restriction_models = DataGroup::getGroupModel(DataGroup::GROUP_TOURS_RESTRICTION_ID);

// Defaukt icon
$default_imagesrc = '/img/icons_search/all_tours.png';

?>
<section class="main-search-container">
    <? if(count($slides)): ?>
    <div id="home-slider" class="home-slider">
    <? foreach($slides as $item): ?>
        <div style="background-image: url(<?= $item->pathImage ?>)">
            <div class="home-slider-text animated fadeInDown"><?= $item->getFieldValue('text') ?></div>
        </div>
    <? endforeach;?>
    </div>
    <? else: ?>
    <? endif; ?>
    <div class="main-search-container-wrap">
        <?= Html::beginForm('/tours', 'get', ['class' => 'main-search-container-form']) ?>
            <h3>Подобрать тур в Грузии</h3>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label><i class="icon-calendar-7"></i> С</label>
                        <?= Html::input('text', 'Filters[start_date]', $filters->start_date, [
                            'class' => 'date-pick form-control',
                            'data-date-format' => 'yyyy-mm-dd',
                            'data-date-start-date' => '<?= $filters->start_date ?>',
                            'data-storage' => 'start_date',
                        ]) ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label><i class="icon-calendar-7"></i> По</label>
                        <?= Html::input('text', 'Filters[end_date]', $filters->end_date, [
                            'class' => 'date-pick form-control',
                            'data-date-format' => 'yyyy-mm-dd',
                            'data-date-start-date' => '<?= $filters->end_date ?>',
                            'data-storage' => 'end_date',
                        ]) ?>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <div class="form-group">
                        <label>Поиск</label>
                        <?= Html::input('text', 'q', null, [
                            'class' => 'form-control',
                            'placeholder' => "Например: 'Отдых в Тбилиси' или 'Мцхета'..."
                        ]) ?>
                    </div>
                </div>
            </div>
            <!-- End row -->
            <div class="text-right">
                <button class="btn_1 green"><i class="icon-search"></i>Подобрать тур</button>
            </div>
        <?= Html::endForm() ?>
    </div>
</section>
<?/*<section id="search_container">
    <div id="search" class="min-search-filter">
        <div class="text-center parallax-content-1 parallax-content-1-head">
            <div class="replace-me replace-me--header">
                {shortcode="Block" id="4"}
            </div>
        </div>
        <div class="tab-content  container">
            <?= Html::beginForm('/tours', 'get') ?>
                <div class="tab-pane active" id="tours">
                    <h3>Подобрать тур в Грузии</h3>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label><i class="icon-calendar-7"></i> С</label>
                                <?= Html::input('text', 'Filters[start_date]', $filters->start_date, [
                                    'class' => 'date-pick form-control',
                                    'data-date-format' => 'yyyy-mm-dd',
                                    'data-date-start-date' => '<?= $filters->start_date ?>',
                                    'data-storage' => 'start_date',
                                ]) ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label><i class="icon-calendar-7"></i> По</label>
                                <?= Html::input('text', 'Filters[end_date]', $filters->end_date, [
                                    'class' => 'date-pick form-control',
                                    'data-date-format' => 'yyyy-mm-dd',
                                    'data-date-start-date' => '<?= $filters->end_date ?>',
                                    'data-storage' => 'end_date',
                                ]) ?>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Поиск</label>
                                <?= Html::input('text', 'q', null, [
                                    'class' => 'form-control',
                                    'placeholder' => "Например: 'Отдых в Тбилиси' или 'Мцхета'..."
                                ]) ?>
                            </div>
                        </div>
                    </div>
                    <!-- End row -->
                    <div class="text-right">
                        <button class="btn_1 green"><i class="icon-search"></i>Подобрать тур</button>
                    </div>
                </div>
                <!-- End rab -->
            <?= Html::endForm() ?>
        </div>
    </div>
</section>
<!-- End hero -->*/?>