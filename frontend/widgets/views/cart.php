<?php
use yii\helpers\Html;
?>
<a href="#" class="dropdown-toggle" data-toggle="dropdown">
    <i class="icon-basket-1"></i> Корзина (<span class="-cart-count"><?= count($models) ?></span>)
</a>
<ul class="dropdown-menu" id="cart_items">
<? if(count($models)): ?>
    <? foreach($models as $item): ?>
    <li data-cart-id="<?= $item->id ?>">
        <? if($item->image): ?>
        <div class="image" style="background-image: url('<?= $item->getImage('100x100') ?>')"></div>
        <? else: ?>
        <div class="image"></div>
        <? endif; ?>
        <strong>
            <a href="<?= $item->link ?>"><?= $item->name ?></a>
            Стоимость от <?= $item->defaultPrice ?>
        </strong>
        <a href="#" class="action" onclick="return removeItemCart('<?= Yii::$app->cart->generateSignature($item->id) ?>', <?= $item->id ?>, this);" title="Удалить">
            <i class="icon-trash"></i>
        </a>
    </li>
    <? endforeach; ?>
    <li>
        <a href="<?= Yii::$app->cart->linkCart ?>" class="button_drop btn_1 outline">Посмотреть</a>
        <a href="<?= Yii::$app->cart->linkOrdering ?>" class="button_drop btn_1 outline">Оформить</a>
    </li>
<? else: ?>
    <li class="text-center">Ваша корзина пустая</li>
<? endif; ?>
</ul>