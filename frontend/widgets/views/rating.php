<span class="rating" id="rating-<?= $this->context->id ?>">
    <?= \Yii::$app->controller->renderPartial('//' . $this->context->module . '/rating', [
        'id' => $this->context->id,
        'module' => $this->context->module,
        'votes' => $this->context->votes,
        'rate' => $this->context->rate,
    ])?>
</span>