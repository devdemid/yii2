<?php
use yii\helpers\Html;
?>
<!-- Begin Recommended Tours -->
<? if(count($models)): ?>
<div class="main_title">
    <h2>Рекомендуемые <span>Туры</span></h2>
    <?/*<p>Quisque at tortor a libero posuere laoreet vitae sed arcu. Curabitur consequat.</p>*/?>
</div>
<div class="row">
    <? foreach($models as $item): ?>
    <div class="col-md-4 col-sm-6 wow zoomIn" data-wow-delay="0.1s">
        <div class="tour_container">
            <? if($item->selling == $item::SELLING_ON): ?>
                <div class="ribbon_3">
                    <span>Тур онлайн</span>
                </div>
            <? endif; ?>
            <div class="img_container">
                <a href="<?= $item->link ?>">
                    <? if($item->image): ?>
                    <img src="<?= $item->getImage('800x533') ?>" width="800" height="533" class="img-responsive" alt="<?= Html::encode($item->name) ?>">
                    <? endif ?>
                    <div class="short_info">
                        <? if($item->catModel !== null): ?>
                            <i class="<?= $item->catModel->icon_css ?>"></i>
                            <span><?= $item->catModel->name ?></span>
                        <? endif; ?>
                        <span class="price">
                            <?= $item->defaultPrice ?>
                        </span>
                    </div>
                </a>
            </div>
            <div class="tour_title">
                <h3><?= $item->name ?></h3>
                <span class="tour_views" title="Всего просмотров">
                    <i class="icon-eye-7"></i>
                    <span>(<?= $item->views ?>)</span>
                </span><!-- tour_views -->
                <?= \frontend\widgets\RatingWidget::widget([
                    'id' => $item->id,
                    'module' => 'tours',
                    'rating' => $item->rating,
                    'votes' => $item->rating_votes,
                    'rate'  => $item->getRate(),
                ]) ?>
                <!-- end rating -->
            </div>
        </div>
    </div>
    <? endforeach; ?>
</div>
<?endif; ?>
<!-- End Recommended Tours -->