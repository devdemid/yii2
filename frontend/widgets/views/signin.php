<? if(Yii::$app->user->isGuest): ?>
<a href="<?= Yii::$app->user->url->signin ?>">
    <i class=" icon-user-1"></i>Войти
</a>
<?/*<div class="dropdown-menu">
    <div class="form-group">
        <input type="text" class="form-control" id="inputUsernameEmail" placeholder="Email">
    </div>
    <div class="form-group">
        <input type="password" class="form-control" id="inputPassword" placeholder="Пароль">
    </div>
    <a id="forgot_pw" href="<?= Yii::$app->user->url->reset_password ?>">Забыли пароль?</a>
    <button type="submit" class="button_drop">
        Войти
    </button>
    <a href="<?= Yii::$app->user->url->signup ?>" class="button_drop outline">Регистрация</a>
</div>*/?>
<? else: ?>
<a href="<?= Yii::$app->user->url->profile ?>" class="dropdown-toggle" data-toggle="dropdown">
    <i class="icon-user-1"></i> Профиль
</a>
<div class="dropdown-menu">
    <ul class="user-menu">
        <li>
            <i class="icon-user-5 mr-5"></i>
            <a href="<?= Yii::$app->user->url->profile ?>">Личный кабинет</a>
        </li>
        <li>
            <i class="icon-cart mr-5"></i>
            <a href="<?= Yii::$app->user->url->orders ?>">Мои заказы</a>
        </li>
        <li>
            <i class="icon-cog-3 mr-5"></i>
            <a href="<?= Yii::$app->user->url->settings ?>">Настройки</a>
        </li>
        <li>
            <i class="icon-export-1 mr-5"></i>
            <a href="<?= Yii::$app->user->url->logout ?>">Выйти</a>
        </li>
    </ul>
</div>
<? endif; ?>