<?php

/**
 * Buy tour
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace frontend\widgets;

use yii\base\Widget;

class BuyTourWidget extends Widget
{
    public $id;

    public function run() {
        return $this->render('buy-tour');
    }
}