<?php

/**
 * Output of comments
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace frontend\widgets;

use frontend\models\FormComment;
use Yii;
use yii\base\Widget;
use common\models\Comment;
use yii\data\Pagination;

class CommentWidget extends Widget
{
    /**
     * Module name
     * @var string
     */
    public $module;

    /**
     * Unique record id
     * @var integer
     */
    public $id;

    /**
     * Enable comments
     * @var integer
     */
    public $enable = 1;

    /**
     *
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Output of comments
     */
    public function run()
    {
        if ($this->enable) {
            $model = new FormComment;

            if (Yii::$app->user->isGuest) {
                $model->setScenario($model::SCENARIO_GUEST);
            }

            $model->module = $this->module;
            $model->item_id = $this->id;

            $_where = [
                'item_id' => $this->id,
                'module'  => $this->module,
                'published' => Comment::PUBLISHED_ON
            ];

            // Only comments that have been moderated
            if(Yii::$app->options->get('only_moderation')) {
                $_where['moderation'] = Comment::MODERATION_ON;
            }

            // Total comments
            $total = Comment::find()
                ->where($_where)
                ->orderBy([Comment::ORDER_BY_SORTING => SORT_DESC])
                ->count();

            $pages = new Pagination([
                'totalCount' => $total,
                'pageSize' => Yii::$app->options->get('page_limit_comments'),
            ]);

            // Comments
            $comments = Comment::find()
                ->where($_where)
                ->orderBy([Comment::ORDER_BY_SORTING => SORT_DESC])
                ->offset($pages->offset)
                ->limit($pages->limit)
                ->all();

            return \Yii::$app->controller->renderPartial('//comment/index', [
                'model' => $model,
                'comments' => $comments,
                'pages' => $pages,
                'ajax_pagination_url' => '/comment/list/' . $this->module . '/' . $this->id
            ], false, true);
        } else {
            return '';
        }
    }
}
