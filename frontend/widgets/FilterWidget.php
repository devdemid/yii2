<?php

/**
 * Tour filters
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace frontend\widgets;

use yii\base\Widget;
use frontend\models\Filters;

class FilterWidget extends Widget
{
    public function run() {
        return $this->render('filter', [
            'filters' => new Filters
        ]);
    }
}