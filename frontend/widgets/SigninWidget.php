<?php

/**
 * Output of comments
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace frontend\widgets;

use yii\base\Widget;

class SigninWidget extends Widget
{
    /**
     *
     */
    public function init() {
        parent::init();
    }

    /**
     *
     */
    public function run() {
        return $this->render('signin');
    }
}