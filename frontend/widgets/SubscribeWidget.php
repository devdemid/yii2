<?php

/**
 * Discount Banner
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace frontend\widgets;

use common\models\Subscribes;

class SubscribeWidget extends \yii\base\Widget
{
    public function run() {
        if(\Yii::$app->options->get('is_nable_subscription')) {
            $cookies = \Yii::$app->request->cookies;

            if($cookies->has(Subscribes::COOKIE_KEY_NAME)) {
                return false;
            }

            return \Yii::$app->controller->renderPartial('//subscribes/index', [
                'model' => new Subscribes
            ]);
        }
    }
}