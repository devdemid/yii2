<?php

/**
 * Recommended Tours
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace frontend\widgets;

use common\models\Tours;

class RecommendedToursWidget extends \yii\base\Widget
{
    /**
     * Ignore tour
     * @var Integer
     */
    public $ignore_id;

    /**
     * Maximum number of displayed tours
     * @var integer
     */
    public $limit = 3;

    public function run() {
        return $this->render('recommended-tours', [
            'models' => Tours::find()
                ->where(['published' => Tours::PUBLISHED_ON])
                ->andFilterWhere(['AND', ['<>', 'id', $this->ignore_id]])
                ->limit($this->limit)
                ->orderBy(['views' => SORT_DESC])
                ->all()
        ]);
    }
}