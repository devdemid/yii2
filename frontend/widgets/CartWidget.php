<?php

/**
 * Main cart
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace frontend\widgets;

use Yii;
use common\models\Tours;

class CartWidget extends \yii\base\Widget
{
    public function run() {
        $cart_record = Yii::$app->cart->record;
        $models = [];

        if(is_array($cart_record) && count($cart_record)) {
            $models = Tours::findAll(array_keys($cart_record));
        }

        return $this->render('cart', ['models' => $models]);
    }
}