<?php

/**
 * Tour filters
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace frontend\widgets;

use yii\base\Widget;
use frontend\models\Filters;
use common\models\Slide;

class MinFilterWidget extends Widget
{
    public function run() {
        return $this->render('min-filter', [
            'filters' => new Filters,
            'slides' => Slide::find()->where([
                'published' => Slide::PUBLISHED_ON,
                'item_id' => 1
            ])->all()
        ]);
    }
}