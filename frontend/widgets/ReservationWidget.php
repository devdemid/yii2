<?php

/**
 * Tour booking
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace frontend\widgets;

use common\models\Tours;

class ReservationWidget extends \yii\base\Widget
{
    public function run() {
        return $this->render('reservation');
    }
}