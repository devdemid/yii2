<?php

/**
 * Rating
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace frontend\widgets;

use yii\base\Widget;

class RatingWidget extends Widget
{
    public $id;
    public $module;
    public $rating = 0;
    public $rate = 0;
    public $votes = 0;

    public function run() {
        return $this->render('rating');
    }
}