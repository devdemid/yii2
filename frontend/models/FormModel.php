<?php

/**
 * Validating Form Designer Fields
 *
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 *
 */

namespace frontend\models;

use common\models\Files;
use common\models\FormField;
use common\models\FormResult;
use Yii;
use yii\base\DynamicModel;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use \yii\web\UploadedFile;

class FormModel extends DynamicModel {

    /**
     * Space for saving files
     */
    const ATTACHMENTS_DIR = '/form/';

    /**
     * List of escaped characters.
     */
    const JSON_REGEXP_CHARLIST = '+":;.*^$#?';

    /**
     * Verify phone number (RUSSIA)
     *
     * ======================================================
     * ^\+?[78][-\(]?\d{3}\)?-?\d{3}-?\d{2}-?\d{2}$
     *
     * +7(903)888-88-88
     * 8(999)99-999-99
     * +380(67)777-7-777
     * 001-541-754-3010
     * +1-541-754-3010
     * 19-49-89-636-48018
     * +233 205599853
     * ======================================================
     */
    const PATTERN_PHONE = '/^((8|\+7)[\-\s]?)?(\(?\d{3}\)?[\-\s]?)?[\d\-\s]{7,10}$/i';

    /**
     * Field for the result of captcha
     * @var string
     */
    public $captcha;

    /**
     * Status of successfully received data
     * @var boolean
     */
    public $status = false;

    /**
     * Attribule labels list
     * @var array
     */
    private $attributeLabels = [];

    /**
     * Field types
     * @var array
     */
    private $types = [];

    /**
     * For upload files attributes
     * @var array
     */
    public $fileAttributes = [];

    /**
     * Data lists for attributes
     * @var array
     */
    private $attributeLists = [];

    /**
     * Form identifier
     * @var integer
     */
    private $form_id;

    /**
     * Entry identifier in the results database
     * @var integer
     */
    public $variable_id;

    /**
     * Set label for attributes
     */
    public function setAttributeLabels($labels) {
        $this->attributeLabels = $labels;
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels() {
        return ArrayHelper::merge($this->attributeLabels, [
            'captcha' => 'Введите код',
        ]);
    }

    /**
     * Loading the Field Model
     * @param  common\models\Form $form
     * @return common\models\FormModel;
     */
    public static function getModel($form) {
        $rules = [
            'uniques' => []
        ];
        $_types = [];
        $_attributeLists = [];

        // if(!is_array($form->fields))
        //     throw new \yii\web\NotFoundHttpException();

        // Rule stacking
        foreach ($form->fields as $item) {
            $rules['attributes'][] = $item->attribute;
            $rules['labels'][$item->attribute] = $item->label;

            if ($item->required && $item->type !== FormField::TYPE_FILE) {
                $rules['required'][] = $item->attribute;
            }

            if ($item->unique) {
                $rules['uniques'][] = $item->attribute;
            }

            if ($item->type == FormField::TYPE_FILE) {
                $rules['types'][$item->type][] = $item;
            } else {
                $rules['types'][$item->type][] = $item->attribute;
            }

            // Collect lists
            if (ArrayHelper::isIn($item->type, [
                FormField::TYPE_SELECT,
                FormField::TYPE_RADIO_GROUP,
            ])) {
                $_attributeLists[$item->attribute] = $item->listExplode;
            }

            // Select Field Types
            $_types[$item->attribute] = $item->type;
        }

        // An array is not needed as an object
        $rules = (object) $rules;

        $self = new FormModel($rules->attributes);

        $self->types = $_types; // List of field types
        $self->attributeLists = $_attributeLists; // Lists for saving results

        // Set attr labels name
        if (is_array($rules->labels) && count($rules->labels)) {
            $self->setAttributeLabels($rules->labels);
        }

        // Set rule for required
        if (is_array($rules->required) && count($rules->required)) {
            $self->addRule($rules->required, 'required');
        }

        // Set rule for email
        if (array_key_exists(FormField::TYPE_EMAIL, $rules->types) && count($rules->types[FormField::TYPE_EMAIL])) {
            $self->addRule($rules->types[FormField::TYPE_EMAIL], 'email');
        }

        // Set rule for integer
        if (array_key_exists(FormField::TYPE_PHONE, $rules->types) && count($rules->types[FormField::TYPE_PHONE])) {
            $self->addRule($rules->types[FormField::TYPE_PHONE], 'match', [
                'pattern' => self::PATTERN_PHONE,
                'message' => '"{attribute}" номер телефона указан в неверном формате.',
            ]);
        }

        // Set rule for type integer
        if (array_key_exists(FormField::TYPE_INTEGER, $rules->types) && count($rules->types[FormField::TYPE_INTEGER])) {
            $self->addRule($rules->types[FormField::TYPE_INTEGER], 'integer');
        }

        // Set rule for integer key list
        if (array_key_exists('list', $rules->types) && count($rules->types['list'])) {
            $self->addRule($rules->types['list'], 'integer');
        }

        // Apply a garbage filter rule
        if (is_array($rules->attributes) && count($rules->attributes)) {
            $self->addRule($rules->attributes, 'filter', ['filter' => function ($value) {
                return \yii\helpers\Html::encode($value);
                //return \yii\helpers\HtmlPurifier::process($value);
            }]);
        }

        // Set rule captcha
        if ($form->captcha) {
            $self->addRule('captcha', 'captcha', [
                'captchaAction' => 'form/captcha',
                'caseSensitive' => false,
            ]);
        }

        // Set rule for files
        if (array_key_exists(FormField::TYPE_FILE, $rules->types) && count($rules->types[FormField::TYPE_FILE])) {
            foreach ($rules->types[FormField::TYPE_FILE] as $item) {
                $self->addRule($item->attribute, 'file', [
                    'skipOnEmpty' => true,
                    'extensions' => $item['extension'],
                    'maxSize' => 1024 * 1024 * $item['size'], // In the settings, the size is set in megabytes
                    'maxFiles' => 1,
                ]);

                // Select a list of attributes for uploading files
                $self->fileAttributes[] = $item->attribute;
            }
        }

        // Check for uniqueness of data
        if (is_array($rules->uniques) && count($rules->uniques)) {
            foreach ($rules->uniques as $item) {
                $self->addRule($item, 'checkUniqueJsonValue');
            }
        }

        // Remember Form ID
        $self->form_id = $form->id;

        unset($rules, $form, $_types, $_attributeLists);

        return $self;
    }

    /**
     * Check for uniqueness value
     * @param  string $attribute
     * @param  string $params
     * @param  object $validator
     */
    public function checkUniqueJsonValue($attribute, $params, $validator) {
        if ($this->$attribute) {
            // Checking the value by mask
            $JSON_REGEXP = '^.*"attribute":"' . $attribute;
            $JSON_REGEXP .= '".*"value":"' . addcslashes($this->$attribute, self::JSON_REGEXP_CHARLIST);
            $JSON_REGEXP .= '".*';

            // Checking the value in the database
            $duplicate_entries = FormResult::find()
                ->where(['item_id' => $this->form_id])
                ->andWhere(['REGEXP', 'json_result', $JSON_REGEXP])
                ->count();

            if ($duplicate_entries) {
                $this->addError($attribute, 'это значение уже используется');
            }
        }
    }

    /**
     * Cleaning fields
     * @param  array $names
     */
    public function unsetAttributes($names = null) {
        if ($names === null) {
            $names = $this->attributes();
        }

        foreach (array_merge($names, ['captcha']) as $name) {
            $this->$name = null;
        }
    }

    /**
     * Adding a prefix if the attribute is required
     * @param  string $attribute
     * @param  boolean $placeholder
     * @return string
     */
    public function getAttributeLabelRequired($attribute, $placeholder = false) {
        if ($placeholder) {
            return false;
        }

        $label = $this->getAttributeLabel($attribute) . ':';

        if ($this->isAttributeRequired($attribute)) {
            $label .= ' *';
        }
        return $label;
    }

    /**
     * Description within the field
     * @param  string  $attribute
     * @param  boolean $placeholder
     * @return string
     */
    public function getAttributePlaceholder($attribute, $placeholder = false) {
        if ($placeholder) {
            return $this->getAttributeLabelRequired($attribute);
        }

        return false;
    }

    /**
     * Create a data format for saving to the database
     * @return array
     */
    public function getFormResults() {
        $results = [];
        foreach ($this->variables as $attribute => $value) {
            $results[] = [
                "attribute" => $attribute,
                'label' => ArrayHelper::getValue($this->attributeLabels, $attribute, 'Unknown'),
                'type' => ArrayHelper::getValue($this->types, $attribute, 'Unknown'),
                'value' => $value,
            ];
        }
        return $results;
    }

    /**
     * List of variables containing the result for the message
     * @param  boolean $fileLink
     * @return array
     */
    public function getVariables($fileLink = false) {
        $variables = [];
        foreach ($this->attributes as $attribute => $value) {

            // Link to attached file
            if ($fileLink && $value && ArrayHelper::isIn($attribute, $this->fileAttributes)) {
                $value = Yii::getAlias('@files' . self::ATTACHMENTS_DIR . $this->form_id . '/' . $value);
            }

            $type = ArrayHelper::getValue($this->types, $attribute, false);

            // If this list of values select the result
            if ($type && ArrayHelper::isIn($type, [
                FormField::TYPE_SELECT,
                FormField::TYPE_RADIO_GROUP,
            ])) {
                $value = ArrayHelper::getValue($this->attributeLists, $attribute . '.' . $value, 'Unknown');
            }

            // State of the checkboxes
            if ($type && $type == FormField::TYPE_CHECKBOX) {
                $value = ArrayHelper::getValue(FormField::getCheckboxStates(), $value, 'Unknown');
            }

            $variables[$attribute] = $value;
        }

        // Get the record ID in the results database
        if(!is_null($this->variable_id)) {
            $variables['id'] = $this->variable_id;
        }

        return $variables;
    }

    /**
     * Uploading form files
     * @param integer $id
     * @return boolean
     */
    public function uploadAttachments() {
        foreach ($this->fileAttributes as $attribute) {
            $UploadedFile = UploadedFile::getInstance($this, $attribute);
            if (!is_null($UploadedFile)) {
                $fileName = uniqid() . '.' . $UploadedFile->extension;
                $fullPatch = Yii::getAlias('@upload' . self::ATTACHMENTS_DIR . $this->form_id . '/');

                // Create destination directory
                FileHelper::createDirectory($fullPatch, Files::CHMOD);
                $UploadedFile->saveAs($fullPatch . $fileName);
                unset($UploadedFile);

                $this->$attribute = $fileName;
            }
        }

        return true;
    }

    /**
     * [getFieldEmail description]
     * @param  array $fields
     * @return boolean || string
     */
    public function getFieldEmail($fields) {
        if (is_array($fields)) {
            foreach ($fields as $model) {
                if($model->type == FormField::TYPE_EMAIL && ArrayHelper::getValue($this->attributes, $model->attribute, false)) {
                    return ArrayHelper::getValue($this->attributes, $model->attribute);
                }
            }
        }
        return false;
    }
}