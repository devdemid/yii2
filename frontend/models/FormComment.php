<?php

/**
 * Form for receiving comments
 *
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 *
 */

namespace frontend\models;

use common\ext\base\Model;
use common\models\Comment;
use Yii;

class FormComment extends Model
{
    /**
     * Script for the guest
     */
    const SCENARIO_GUEST = 'guest';

    /**
     * Author's name
     * @var String
     */
    public $name;

    /**
     * Email of the author
     * @var String
     */
    public $email;

    /**
     * Binding to the module
     * @var String
     */
    public $module;

    /**
     * Comment content
     * @var String
     */
    public $content;

    /**
     * Captcha verify code
     * @var Integer
     */
    public $captcha;

    /**
     * Unique record id
     * @var Integer
     */
    public $item_id;

    /**
     * Guest details
     * @var String
     */
    private $data;

    /**
     * Messages on successful completion of the form
     * @var String
     */
    public $is_success = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id', 'module', 'content'], 'required'],
            ['email', 'email'],
            [['name', 'email', 'content', 'module'], 'trim'],
            [['name', 'email'], 'string', 'max' => '255'],
            ['module', 'string', 'max' => '100'],
            ['content', 'string', 'max' => '65535'],
            ['item_id', 'integer'],

            // Filers
            [['name', 'content'], 'filter', 'filter' => function ($value) {
                return \yii\helpers\Html::encode($value);
            }],

            // Guest
            [['name', 'email'], 'required', 'on' => self::SCENARIO_GUEST],
            [
                'captcha',
                'captcha',
                'captchaAction' => 'comment/captcha',
                'caseSensitive' => false,
                'on' => self::SCENARIO_GUEST,
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'content' => 'Комментарий',
            'email' => 'Ваш E-Mail',
            'name' => 'Ваше Имя',
            'captcha' => 'Код из картинки',
        ];
    }

    /**
     * Saving a comment in the database
     * @return Boolean
     */
    public function save()
    {
        $model = new Comment;
        $model->attributes = [
            'content' => $this->content,
            'item_id' => $this->item_id,
            'user_id' => Yii::$app->user->identity->id,
            'module' => $this->module
        ];

        // Guest info
        if(Yii::$app->user->identity->id === null) {
            $model->name = $this->name;
            $model->email = $this->email;
        }

        return $model->save();
    }
}
