<?php

/*
 * Customer email changes
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace frontend\models;

use common\models\User;

class ChangeEmail extends \common\ext\base\Model
{

    /**
     * Form Variables
     */
    public $old_email;
    public $new_email;
    public $email_repeat;

    /**
     * User object
     * @var null|\common\models\User
     */
    private $_user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['old_email', 'new_email', 'email_repeat'], 'required'],
            [['old_email', 'new_email', 'email_repeat'], 'email'],
            ['new_email', 'string', 'min' => 6, 'max' => 100],
            ['email_repeat', 'compare', 'compareAttribute' => 'new_email'],
            [['old_email', 'new_email', 'email_repeat'], 'filter', 'filter' => 'trim'],
            ['new_email', 'checkEmail'],
        ];
    }

    /**
     * Checking the email address
     */
    public function checkEmail($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (!is_null($this->user) && !$this->user->validateEmail($this->old_email)) {
                $this->addError('old_email', 'Неверный старый пароль');
            }

            if ($this->old_email === $this->new_email) {
                $this->addError('new_email', 'Новый email не может совпадать со старым email');
            }
        }
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'old_email' => 'Старый email',
            'new_email' => 'Новый email',
            'email_repeat' => 'Повторите новый email',
        ];
    }

    /**
     * Update the data
     * @return Boolean
     */
    public function update()
    {
        if ($this->validate()) {
            $this->user->setEmail($this->new_email);
            return $this->user->update();
        }
        return false;
    }

    /**
     * Get user object
     * @return \common\models\User
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findOne(\Yii::$app->user->id);
        }
        return $this->_user;
    }
}
