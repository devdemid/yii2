<?php

/**
 * Tours filter
 *
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 *
 */

namespace frontend\models;

use common\models\TourPrices;
use Yii;

class Filters extends \common\ext\base\Model
{

    /**
     * Date format in the database
     */
    const PROP_DATE_FORMAT = 'Y-m-d';

    /**
     * Separator for range
     */
    const RANGE_SEPARATOR = ';';

    /**
     * Default end date
     */
    const DEFAULT_END_DATE = '+10 months';

    /**
     * Category of the tour
     * @var Integer
     */
    public $cat_id;

    /**
     * Type of tour
     * @var Integer
     */
    public $type_id;

    /**
     * Tour Restrictions
     * @var Integer
     */
    public $restriction_id;

    /**
     * Price range
     * @var String
     */
    public $range;

    /**
     * Start date
     * @var String
     */
    public $start_date;

    /**
     * Final date
     * @var String
     */
    public $end_date;

    /**
     * Number of adults
     * @var Integer
     */
    public $adults = 1;

    /**
     * Amount of children
     * @var Integer
     */
    public $children = 0;

    /**
     * Sort tours
     * @var String
     */
    public $order;

    /**
     * Search for text
     * @var String
     */
    public $text;

    public function init() {
        if($this->start_date === null)
            $this->start_date = date(self::PROP_DATE_FORMAT);

        if($this->end_date === null)
            $this->end_date = date(self::PROP_DATE_FORMAT, strtotime(self::DEFAULT_END_DATE));

        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['range', 'start_date', 'end_date'], 'trim'],
            [['cat_id', 'type_id', 'restriction_id', 'adults', 'children'], 'integer'],
            [['start_date', 'end_date'], 'date', 'format' => 'php:' . self::PROP_DATE_FORMAT],

            // Set default value
            [['cat_id', 'type_id', 'restriction_id'], function ($attribute) {
                if(!$this->$attribute)
                    $this->$attribute = null;
            }],

            // Check range prices
            ['range', function ($attribute) {
                $this->range = explode(self::RANGE_SEPARATOR, $this->range);
                if(is_array($this->range) && count($this->range) == 2) {
                    $this->range = array_map('intval', $this->range);
                } else {
                    $this->addError($attribute, 'Неверный формат цены');
                }
            }],

            // Filter search text
            ['text', 'filter', 'filter' => function ($value) {
                return \yii\helpers\Html::encode($value);
            }],

            // Check date interval
            ['start_date', function($attribute) {
                $start_date  = strtotime('+1 second', strtotime($this->start_date));
                if($start_date < strtotime(date(self::PROP_DATE_FORMAT))) {
                    $this->addError($attribute, 'Дата не может быть меньшей от текущей даты');
                }
            }],

            ['end_date', function($attribute) {
                if(strtotime($this->end_date) < strtotime($this->start_date)) {
                    $this->addError($attribute, 'Дата не может быть меньшой от начальной даты.');
                }
            }],

            // Control of quantity
            [['adults', 'children'], 'integer', 'max' => 100],

            // Default
            ['adults', 'default', 'value' => 1],
            ['children', 'default', 'value' => 0],

            // Defaukt dates
            ['start_date', 'default', 'value' => date(self::PROP_DATE_FORMAT)],
            [
                'end_date',
                'default',
                'value' => date(self::PROP_DATE_FORMAT, strtotime(self::DEFAULT_END_DATE))
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'cat_id' => 'Категория тура',
            'type_id' => 'Тип',
            'restriction_id' => 'Ограничения',
            'range' => 'Цена',
            'start_date' => 'Начальная дата',
            'end_date' => 'Конечная дата',
            'adults' => 'Взрослые',
            'children' => 'Дети'
        ];
    }

    /**
     * The minimum amount of the price
     * @return Integer
     */
    public function getMinPrice() {
        $model = TourPrices::find()
            ->where(['published' => TourPrices::PUBLISHED_ON])
            ->orderBy(['base_price' => SORT_ASC])
            ->one();
        return (int) $model->base_price;
    }

    /**
     * Maximum price amount
     * @return Integer
     */
    public function getMaxPrice() {
        $model = TourPrices::find()
            ->where(['published' => TourPrices::PUBLISHED_ON])
            ->orderBy(['base_price' => SORT_DESC])
            ->one();
        return (int) $model->base_price;
    }
}