<?php

/*
 * Notification settings
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace frontend\models;

use common\models\User;

class ChangeNotification extends \common\ext\base\Model
{
    /**
     * Notifications
     */
    public $notification_new_tours;
    public $notification_comp_news;

    /**
     * User object
     * @var null|\common\models\User
     */
    private $_user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notification_new_tours', 'notification_comp_news'], 'integer'],
            [['notification_new_tours', 'notification_comp_news'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'notification_new_tours' => 'Новые туры',
            'notification_comp_news' => 'Новости компании',
        ];
    }

    /**
     * Update the data
     * @return Boolean
     */
    public function update()
    {
        if ($this->validate()) {
            $this->user->notification_new_tours = $this->notification_new_tours;
            $this->user->notification_comp_news = $this->notification_comp_news;
            return $this->user->update();
        }
        return false;
    }

    /**
     * Get user object
     * @return \common\models\User
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findOne(\Yii::$app->user->id);
        }
        return $this->_user;
    }
}
