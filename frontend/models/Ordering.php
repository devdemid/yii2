<?php

/**
 * Tours ordering
 *
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 *
 */

namespace frontend\models;

class Ordering extends \common\ext\base\Model
{
    /**
     * By popularity
     */
    const ORDERING_BY_TOP = 'top';

    /**
     * By views
     */
    const ORDERING_BY_VIEWS = 'views';

    /**
     * From cheap to expensive
     */
    const ORDERING_BY_PRICE_ASC = 'price-asc';

    /**
     * From expensive to cheap
     */
    const ORDERING_BY_PRICE_DESC = 'price-desc';

    /**
     * Online
     */
    const ORDERING_BY_SELLING = 'online';

    /**
     * Sorting type
     * @var String
     */
    public $sort;

    /**
     * List of all sorting filters
     * @return Array
     */
    public static function getList()
    {
        return [
            self::ORDERING_BY_TOP => 'по популярности',
            self::ORDERING_BY_VIEWS => 'по просмотрам',
            self::ORDERING_BY_PRICE_DESC => 'от дорогих к дешёвым',
            self::ORDERING_BY_PRICE_ASC => 'от дешёвых к дорогим',
            self::ORDERING_BY_SELLING => 'Туры онлайн',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                'sort', 'in', 'range' => [
                    self::ORDERING_BY_TOP,
                    self::ORDERING_BY_PRICE_DESC,
                    self::ORDERING_BY_PRICE_ASC,
                    self::ORDERING_BY_VIEWS,
                    self::ORDERING_BY_SELLING,
                ],
            ],
        ];
    }
}
