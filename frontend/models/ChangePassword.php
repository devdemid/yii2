<?php

/*
 * Client password changes
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace frontend\models;

use common\models\User;

class ChangePassword extends \common\ext\base\Model
{

    /**
     * Form Variables
     */
    public $old_password;
    public $new_password;
    public $password_repeat;

    /**
     * User object
     * @var null|\common\models\User
     */
    private $_user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['old_password', 'new_password', 'password_repeat'], 'required'],
            ['new_password', 'string', 'min' => 6, 'max' => 100],
            ['password_repeat', 'compare', 'compareAttribute' => 'new_password'],
            [['old_password', 'new_password', 'password_repeat'], 'filter', 'filter' => 'trim'],
            ['new_password', 'checkPassword'],
        ];
    }

    /**
     * Checking the old password
     */
    public function checkPassword()
    {
        if (!$this->hasErrors()) {
            if (!is_null($this->user) && !$this->user->validatePassword($this->old_password)) {
                $this->addError('old_password', 'Неверный старый пароль');
            }
            if ($this->old_password === $this->new_password) {
                $this->addError('new_password', 'Новый пароль не может совпадать со старым паролем');
            }
        }
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'old_password' => 'Старый пароль',
            'new_password' => 'Новый пароль',
            'password_repeat' => 'Повторите новый пароль',
        ];
    }

    /**
     * Update the data
     * @return Boolean
     */
    public function update()
    {
        if ($this->validate()) {
            $this->user->setPassword($this->new_password);
            return $this->user->update();
        }
        return false;
    }

    /**
     * Get user object
     * @return \common\models\User
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findOne(\Yii::$app->user->id);
        }
        return $this->_user;
    }
}
