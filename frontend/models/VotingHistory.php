<?php

/**
 * Voting history
 *
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 *
 */

namespace frontend\models;

/**
 * Database structure
 *
 * @property int(20)      $id                - Unique identificator
 * @property int(11)      $item_id           - text
 * @property varchar(50)  $module            - text
 * @property tinyint(2)   $value             - text
 * @property varchar(100) $ip                - text
 * @property varchar(32)  $signature         - text
 * @property int(11)      $created_timestamp - text
 */
class VotingHistory extends \common\ext\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%voting_history}}';
    }


    /**
     * @inheritdoc
     * @return array
     */
    public function rules() {
        return [
            [['item_id', 'module', 'signature'], 'required'],
            [['item_id', 'value'], 'integer'],
            ['signature', 'safe'],
            ['ip', 'default', 'value' => \Yii::$app->request->userIP],
            ['created_timestamp', 'default', 'value' => date('U')],
        ];
    }
}