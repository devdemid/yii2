<?php
/*
 * Finding data in the model
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */
namespace frontend\models;

use yii\base\Model;

class Search extends Model {

    /**
     * The default text
     */
    const DEFAULT_BLANK = '';

    /**
     * The text you want to search
     * @var string
     */
    public $q;

    /**
     * @inheritdoc
     * @return array
     */
    public function rules() {
        return [
            ['q', 'filter', 'filter' => 'trim'],
            ['q', 'string'],
            ['q', 'filter', 'filter' => function ($value) {
                return \yii\helpers\Html::encode($value);
            }]
        ];
    }

    /**
     * Apply text search
     * @param  string $text
     * @return string
     */
    public function apply($text) {
        $this->q = $text;

        if (!$this->validate()) {
            $this->q = self::DEFAULT_BLANK;
        }
        return $this->q;
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels() {
        return [
            'q' => 'Поиск',
        ];
    }
}