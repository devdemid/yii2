<?php

// Set russian locale
@setlocale(LC_ALL, 'ru_RU.UTF-8');

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_DEV') or define('YII_DEV', true);
defined('YII_ENV') or define('YII_ENV', 'dev');


require(__DIR__ . '/../../vendor/autoload.php');
require(__DIR__ . '/../../vendor/yiisoft/yii2/Yii.php');
require(__DIR__ . '/../../common/config/bootstrap.php');
require(__DIR__ . '/../config/bootstrap.php');

$config = yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../../common/config/main.php'),
    require(__DIR__ . '/../config/main.php')
);

if(YII_DEV) {
    $config = $config = yii\helpers\ArrayHelper::merge($config,
        require(__DIR__ . '/../../common/config/main-local.php'),
        require(__DIR__ . '/../config/main-local.php')
    );
}

$application = new yii\web\Application($config);

$application->on(yii\web\Application::EVENT_BEFORE_REQUEST, function(yii\base\Event $event){

    $event->sender->response->on(yii\web\Response::EVENT_BEFORE_SEND, function($e){
        //ob_start("ob_gzhandler");
        ob_start();
    });

    $event->sender->response->on(yii\web\Response::EVENT_AFTER_SEND, function($e){
        //ob_end_flush();

        $ob_get_contents = ob_get_contents();
        ob_end_clean();

        $html_build = new frontend\components\HTMLBuild($ob_get_contents);
        echo $html_build->getContent();
    });
});

$application->run();
