;(function($) {
    $('a[href^="#"]:not([data-parent])').click(function(event) {
        event.preventDefault();
        event.stopPropagation();

        var $this = $(this),
            //id = ($this.attr('href')).match(/#([a-z\-\_]+)/)[1],
            $target = $($this.attr('href'));

        if($target.length) {
            $('html,body').stop().animate({
                scrollTop: $target.offset().top - 70
            }, 500, 'swing');
        }
    });
})(jQuery);