<!-- Contact Info Box Starts -->
<div class="contact-info-box">
    <i class="fa <?= $this->context->icon ?> big-icon"></i>
    <div class="contact-info-box-content">
        <?= $model->content ?>
    </div>
</div>
<!-- Contact Info Box Ends -->