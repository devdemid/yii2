<!-- Call To Action Section Starts -->
<section class="call-action-all">
    <div class="call-action-all-overlay">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Call To Action Text Starts -->
                    <div class="action-text">
                        <?= $model->content ?>
                    </div>
                    <!-- Call To Action Text Ends -->
                    <!-- Call To Action Button Starts -->
                    <p class="action-btn text-center">
                        <?/*<a class="btn btn-primary" href="<?= Yii::$app->options->get('present_link') ?>" target="_blank">
                            Скачать презентацию
                        </a>*/?>
                        <a class="btn btn-primary" href="https://my.cryptotrade.trade/en/signup">
                            Регистрация
                        </a>
                    </p>
                    <!-- Call To Action Button Ends -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Call To Action Section Ends -->