<? if(count($model)): ?>
<!-- Slider Starts -->
<div id="main-slide" class="carousel slide carousel-fade" data-ride="carousel">
    <!-- Indicators Starts -->
    <ol class="carousel-indicators visible-lg visible-md">
        <li data-target="#main-slide" data-slide-to="0" class="active"></li>
        <li data-target="#main-slide" data-slide-to="1"></li>
        <li data-target="#main-slide" data-slide-to="2"></li>
    </ol>
    <!-- Indicators Ends -->
    <!-- Carousel Inner Starts -->
    <div class="carousel-inner">
        <? foreach($model as $i => $item): ?>
        <!-- Carousel Item Starts -->
        <div class="item <?= !$i ? 'active' : false ?> bg-parallax" style="background-image: url(<?= $item->pathImage ?>) ">
            <div class="slider-content">
                <div class="container">
                    <div class="slider-text text-center">
                        <? if($item->hasFileld('text')): ?>
                        <h3 class="slide-title">
                            <?= $item->getFieldValue('text') ?>
                        </h3>
                        <? endif ?>
                        <? if($item->url): ?>
                        <p>
                            <a href="<?= $item->url ?>" class="slider btn btn-primary">
                                <?= $item->getFieldValue('button_text', 'Подробнее...') ?>
                            </a>
                        </p>
                        <? endif ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- Carousel Item Ends -->
        <? endforeach; ?>
    </div>
    <!-- Carousel Inner Ends -->
    <!-- Carousel Controlers Starts -->
    <a class="left carousel-control" href="#main-slide" data-slide="prev">
        <span><i class="fa fa-angle-left"></i></span>
    </a>
    <a class="right carousel-control" href="#main-slide" data-slide="next">
        <span><i class="fa fa-angle-right"></i></span>
    </a>
    <!-- Carousel Controlers Ends -->
</div>
<!-- Slider Ends -->
<? endif; ?>