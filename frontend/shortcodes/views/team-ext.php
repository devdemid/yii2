<?

use yii\helpers\Html;
use yii\helpers\StringHelper;

?>
<? if(count($model->getNews($model->page_size))): ?>
<!-- Team Section Starts -->
<section class="team">
    <div class="container">
        <!-- Section Title Starts -->
        <div class="row text-center">
            <h2 class="title-head">Наша <span>команда</span></h2>
            <? if($model->content): ?>
            <div class="title-head-subtitle">
                <?= $model->content ?>
            </div>
            <? endif ?>
        </div>
        <!-- Section Title Ends -->
        <!-- Team Members Starts -->
        <div class="row team-content team-members">
            <? foreach($model->getNews(4) as $item): ?>
            <!-- Team Member Starts -->
            <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                <div class="team-member">
                    <!-- Team Member Picture Starts -->
                    <img src="<?= $item->getImage('570x713') ?>" class="img-responsive" alt="<?= Html::encode($item->name) ?>">
                    <!-- Team Member Picture Ends -->
                    <!-- Team Member Details Starts -->
                    <div class="team-member-caption social-icons">
                        <h4><?= Html::encode($item->name) ?></h4>
                        <? if($item->hasFileld('position')): ?>
                            <p><?= $item->getFieldValue('position') ?></p>
                        <? endif ?>
                        <ul class="list list-inline social">
                            <? if($item->hasFileld('facebook')): ?>
                            <li>
                                <a href="<?= $item->getFieldValue('facebook', '#facebook') ?>" class="fa fa-facebook"></a>
                            </li>
                            <? endif ?>
                            <? if($item->hasFileld('twitter')): ?>
                            <li>
                                <a href="<?= $item->getFieldValue('twitter', '#twitter') ?>" class="fa fa-twitter"></a>
                            </li>
                            <? endif ?>
                            <? if($item->hasFileld('googleplus')): ?>
                            <li>
                                <a href="<?= $item->getFieldValue('googleplus', '#google-plus') ?>" class="fa fa-google-plus"></a>
                            </li>
                            <? endif ?>
                        </ul>
                    </div>
                    <!-- Team Member Details Ends -->
                </div>
            </div>
            <!-- Team Member Ends -->
            <? endforeach; ?>
        </div>
        <!-- Team Members Ends -->
    </div>
</section>
<!-- Team Section Ends -->
<? endif; ?>