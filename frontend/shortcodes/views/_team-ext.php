<?

use yii\helpers\Html;
use yii\helpers\StringHelper;

?>
<? if(count($model->getNews(4))): ?>
<div class="container margin_60">
    <div class="main_title">
        <h2>БЛОГ <span>Green</span> IBERIA</h2>
        <? if($model->content) echo $model->content ?>
    </div>
    <br>
    <div class="row">
        <? foreach($model->getNews(4) as $item): ?>
        <div class="col-md-3 col-sm-6 text-center">
            <p>
                <a href="<?= $item->link ?>">
                    <img src="<?= $item->getImage('800x450') ?>" alt="<?= Html::encode($item->name) ?>" class="img-responsive">
                </a>
            </p>
            <div class="news-text">
                <h4><?= $item->name ?></h4>
                <div>
                    <?= StringHelper::truncate($item->short_description, 120, '...') ?>
                </div>
            </div>
            <p>
                <a href="<?= $item->link ?>" class="btn_1 outline">Подробнее</a>
            </p>
        </div>
        <? endforeach; ?>
    </div>
    <hr>
    <p class="text-center nopadding">
        <a href="<?= $model->link ?>" class="btn_1 medium">
            <i class="icon-eye-7"></i> Все посты
        </a>
    </p>
</div>
<!-- End row -->
<? endif; ?>