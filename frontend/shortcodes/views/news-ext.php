<?

use yii\helpers\Html;
use yii\helpers\StringHelper;

?>
<? if(count($model->getNews(100))): ?>
<!-- Section News Starts -->
<section class="services">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="title-head text-center">Новости <span>CryptoTrade</span></h2>
                <div class="gap-20"></div>
                <div class="gap-20"></div>
            </div>
        <? foreach($model->getNews(100) as $i => $item): ?>
            <!-- Service Box Starts -->
            <div class="col-md-6 service-box">
                <div>
                    <div class="service-box-content">
                        <h3><?= $item->name ?></h3>
                        <?= $item->short_description ?>
                    </div>
                </div>
            </div>
            <!-- Service Box Ends -->
        <? if($i % 2 == true): ?>
        </div>
        <div class="row">
        <? endif ?>
        <? endforeach; ?>
        </div>
    </div>
</section>
<!-- Section News Ends -->
<? endif; ?>