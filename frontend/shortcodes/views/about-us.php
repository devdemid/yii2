<!-- About Section Starts -->
<section class="about-us" id="section-crypto-arbitration">
    <div class="container">
        <!-- Section Title Starts -->
        <div class="row text-center">
            <h2 class="title-head">Крипто <span>Арбитаж</span></h2>
            <div class="title-head-subtitle">
                <p>СТАБИЛЬНЫЙ ДОХОД НА РЫНКЕ КРИПТОВАЛЮТ</p>
            </div>
        </div>
        <!-- Section Title Ends -->
        <!-- Section Content Starts -->
        <div class="row about-content">
            <!-- Image Starts -->
            <div class="col-sm-12 col-md-5 col-lg-6 text-center">
                <img class="img-responsive img-about-us" src="/images/about-us.png?v=2" alt="about us">
            </div>
            <!-- Image Ends -->
            <!-- Content Starts -->
            <div class="col-sm-12 col-md-7 col-lg-6">
                <?= $model->content ?>
            </div>
            <!-- Content Ends -->
        </div>
        <!-- Section Content Ends -->
    </div>
</section>
<!-- About Section Ends -->