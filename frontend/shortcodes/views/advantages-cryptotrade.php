<!-- Features and Video Section Starts -->
<section id="block-advantages-cryptotrade">
    <div class="container">
        <div class="row text-center">
            <h2 class="title-head">ПРЕИМУЩЕСТВА <span>CRYPTOTRADE</span></h2>
            <div class="title-head-subtitle">
                <p>ИНСТИТУЦИОНАЛЬНЫЙ ПОДХОД К ИНВЕСТИЦИЯМ </p>
            </div>
        </div>
        <div class="row">
            <?= $model->content ?>
            <?/*<!-- Features Starts -->
            <div class="col-md-12">
                <div class="gap-20"></div>
                <div class="row">
                    <!-- Feature Starts -->
                    <div class="col-sm-4 col-md-4 col-xs-12">
                        <div class="feature text-center">
                            <span class="feature-icon">
                                <img src="/images/icons/blue/strong-security.png?v=1" alt="strong security"/>
                            </span>
                            <h3 class="feature-title">Минимальный риск</h3>
                            <p>Доходность не зависит от<br> направления движения рынка</p>
                        </div>
                    </div>
                    <!-- Feature Ends -->
                    <div class="gap-20-mobile"></div>
                    <!-- Feature Starts -->
                    <div class="col-sm-4 col-md-4 col-xs-12">
                        <div class="feature text-center">
                            <span class="feature-icon">
                                <img src="/images/icons/blue/world-coverage.png" alt="world coverage"/>
                            </span>
                            <h3 class="feature-title">Высокая надежность</h3>
                            <p>Проверенный временем<br> арбитражный алгоритм</p>
                        </div>
                    </div>
                    <!-- Feature Ends -->
                    <!-- Feature Starts -->
                    <div class="col-sm-4 col-md-4 col-xs-12">
                        <div class="feature text-center">
                            <span class="feature-icon">
                                <img src="/images/icons/blue/payment-options.png" alt="payment options"/>
                            </span>
                            <h3 class="feature-title">Команда профи</h3>
                            <p>Программисты, HFT трейдеры, аналитики,<br> риск менеджеры и др. </p>
                        </div>
                    </div>
                    <!-- Feature Ends -->
                </div>
                <div class="gap-20"></div>
                <div class="row">
                    <div class="gap-20-mobile"></div>
                    <!-- Feature Starts -->
                    <div class="col-sm-4 col-md-4 col-xs-12">
                        <div class="feature text-center">
                            <span class="feature-icon">
                                <img src="/images/icons/blue/mobile-app.png" alt="mobile app"/>
                            </span>
                            <h3 class="feature-title">Java / Python</h3>
                            <p>Современные технологии<br> разработки</p>
                        </div>
                    </div>
                    <!-- Feature Ends -->
                    <!-- Feature Starts -->
                    <div class="col-sm-4 col-md-4 col-xs-12">
                        <div class="feature text-center">
                            <span class="feature-icon">
                                <img src="/images/icons/blue/cost-efficiency.png" alt="cost efficiency"/>
                            </span>
                            <h3 class="feature-title">Скоростное API</h3>
                            <p>Прямое подключение<br> к серверам криптобирж</p>
                        </div>
                    </div>
                    <!-- Feature Ends -->
                    <div class="gap-20-mobile"></div>
                    <!-- Feature Starts -->
                    <div class="col-sm-4 col-md-4 col-xs-12">
                        <div class="feature text-center">
                            <span class="feature-icon">
                                <img src="/images/icons/blue/high-liquidity.png" alt="high liquidity"/>
                            </span>
                            <h3 class="feature-title">Не зависит от $</h3>
                            <p>Баланс стратегии<br> привязан к <b>BTC</b></p>
                        </div>
                    </div>
                    <!-- Feature Ends -->
                </div>
            </div>
            <!-- Features Ends -->*/?>
        </div>
    </div>
</section>
<!-- Features and Video Section Ends -->