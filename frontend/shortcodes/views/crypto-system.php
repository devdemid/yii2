<!-- Features and Video Section Starts -->
<section class="image-block">
    <div class="container-fluid">
        <div class="row">
            <!-- Features Starts -->
            <div class="col-md-8 ts-padding img-block-left">
                <?= $model->content ?>
                <?/*<h2>О системе CryptoTrade</h2>
                <p><b>CryptoTrade</b> - это криптоарбитражная платформа, реализованная профессионалами финансовой индустрии. Мы не "изобретали велосипед",а применили работающие арбитражные технологии на рынки криптовалют.
                </p>
                <p>В основе системы - эксклюзивный, проверенный временем, арбитражный программный комплекс. В 2007-2011 годах с его помощью успешно осуществлялась высокочастотная (HFT) торговля на RTS (до 3-6% дневного объема торгов) и мультивалютный арбитраж между Micex и LSE.
                </p>
                <p>В сочетании с глубокими знаниями криптоиндустрии (на уровне разработки собственной криптовалюты), мы создали <b>CryptoTrade</b> - уникальную стратегию, открывающую инвесторам прямой доступ к институциональным инструментам криптоарбитража.
                </p>
                <p><b>CryptoTrade</b>  - это готовый крипто-финансовый продукт, который генерирует стабильный, фиатно-независимый Cash Flow, обеспечивая долгосрочный прирост вложенных криптоактивов.
                </p>*/?>
            </div>
            <!-- Features Ends -->
            <!-- Video Starts -->
            <div class="col-md-4 ts-padding bg-image-1">
                <?/*<div>
                    <div class="text-center">
                        <a class="button-video mfp-youtube" href="https://www.youtube.com/watch?v=RERSnzGjgL4"></a>
                    </div>
                </div>*/?>
            </div>
            <!-- Video Ends -->
        </div>
    </div>
</section>
<!-- Features and Video Section Ends -->