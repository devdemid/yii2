<ul class="dropdown-menu" role="menu">
    <? foreach($children as $item): ?>
        <li>
            <a href="<?= $item->url ?>"<?= ($item->target_blank)? ' target="_blank"' : false ?><?= ($item->url == '#' ? ' onclick="return false;"' : false)?>>
                <?= $item->name ?>
            </a>
            <? if(count($item->children)) echo Yii::$app->controller->renderPartial('@frontend/shortcodes/views/menu-child', [
                'children' => $item->children
            ]); ?>
        </li>
    <? endforeach; ?>
</ul>