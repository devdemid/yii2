<!-- Begin news -->
<section class="features-row" id="section-investor">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="title-head text-center">КАК СТАТЬ <span>ИНВЕСТОРОМ</span></h2>
                <p class="message text-center">НАЧАТЬ ЗАРАБАТЫВАТЬ НА КРИПТО-АРБИТРАЖЕ</p>
                <div class="gap-20"></div>
                <div class="gap-20"></div>
            </div>
            <?= $model->content ?>
            <?/*<div class="col-sm-4">
                <div class="feature text-center">
                    <span class="feature-icon">
                        <img src="/images/icons/blue/mobile-app.png?v=1" alt="strong security">
                    </span>
                    <h3 class="feature-title">Зарегистрируйтесь на сайте</h3>
                    <p>После регистрации с вами свяжется наш<br> менеджер для уточнения всех деталей<br> работы на платформе <span>CryptoTrade</span></p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="feature text-center">
                    <span class="feature-icon">
                        <img src="/images/icons/blue/payment-options.png?v=1" alt="strong security">
                    </span>
                    <h3 class="feature-title">Переведите криптоактив</h3>
                    <p>Система работает только с криптовалютами,<br> все расчеты ведутся в базовой валюте - BTC. </p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="feature text-center">
                    <span class="feature-icon">
                        <img src="/images/icons/blue/high-liquidity.png?v=1" alt="strong security">
                    </span>
                    <h3 class="feature-title">Отслеживайте результаты в ЛК</h3>
                    <p>Многофункциональный личный кабинет<br> поможет оставаться в курсе изменений вашего баланса</p>
                </div>
            </div>*/?>
        </div>
        <br><br>
        <div class="text-center mt-5">
            <a href="https://my.cryptotrade.trade/en/signup" class="btn btn-primary">
                Регистрация
            </a>
        </div>
    </div>
</section>
<!-- End news -->