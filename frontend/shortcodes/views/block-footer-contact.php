<!-- Footer Widget Starts -->
<div class="col-sm-4 col-md-3">
    <h4><?= $model->name ?></h4>
    <div class="contacts">
        <?= $model->content ?>
    </div>
    <?/*<!-- Social Media Profiles Starts -->
    <div class="social-footer">
        <ul>
            <? if(Yii::$app->options->get('facebook')): ?>
            <li>
                <a href="<?= Yii::$app->options->get('facebook') ?>" target="_blank">
                    <i class="fa fa-facebook"></i>
                </a>
            </li>
            <? endif ?>
            <? if(Yii::$app->options->get('twitter')): ?>
            <li>
                <a href="<?= Yii::$app->options->get('twitter') ?>" target="_blank">
                    <i class="fa fa-twitter"></i>
                </a>
            </li>
            <? endif ?>
            <? if(Yii::$app->options->get('googleplus')): ?>
            <li>
                <a href="<?= Yii::$app->options->get('googleplus') ?>" target="_blank">
                    <i class="fa fa-google-plus"></i>
                </a>
            </li>
            <? endif ?>
            <? if(Yii::$app->options->get('linkedin')): ?>
            <li>
                <a href="<?= Yii::$app->options->get('linkedin') ?>" target="_blank">
                    <i class="fa fa-linkedin"></i>
                </a>
            </li>
            <? endif ?>
        </ul>
    </div>
    <!-- Social Media Profiles Ends -->*/?>
</div>
<!-- Footer Widget Ends -->