<?
/**
 * Dynamic shape output
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

use yii\bootstrap\ActiveForm;
use common\models\Form;

?>
<? if($model): ?>
    <? $form = ActiveForm::begin([
        'id' => 'form-' . $model->uniqueKey,
        'action' => '/form/request/' . $model->uniqueKey,
        'options' => [
            'role'    => 'form',
            'class' => $this->context->form_class,
            'onsubmit' => "return requestForm(this, '#form-" . $model->uniqueKey . "');",
            'enctype' => 'multipart/form-data',
        ]
    ]); ?>
    <? if($model->name && $this->context->title): ?>
        <h5 class="form-title"><?= $model->name ?></h5>
    <? endif ?>
    <div id="form-body-<?= $model->uniqueKey ?>">
        <?= \Yii::$app->controller->renderPartial('@frontend/views/form/request', [
            'form' => $form,
            'model' => $model,
            'form_model' => $form_model
        ])?>
    </div>
    <? ActiveForm::end(); ?>
<? endif; ?>