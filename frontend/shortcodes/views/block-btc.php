<!-- Quote and Chart Section Starts -->
<section class="image-block2">
    <div class="container-fluid">
        <div class="row">
            <!-- Quote Starts -->
            <div class="col-md-4 img-block-quote bg-image-2">
                <blockquote>
                    <?= $model->content ?>
                </blockquote>
            </div>
            <!-- Quote Ends -->
            <!-- Chart Starts -->
            <div class="col-md-8 bg-grey-chart">
                <div class="chart-widget dark-chart chart-1">
                    <div>
                        <div class="btcwdgt-chart" data-bw-theme="light"></div>
                    </div>
                </div>
            </div>
            <!-- Chart Ends -->
        </div>
    </div>
</section>
<!-- Quote and Chart Section Ends -->