<? if(count($model)): ?>
<ul <?= $root_attributes ?>>
<? foreach($model as $item): ?>
    <? if(!$item->level): ?>
        <? if($this->context->active_url == $item->url): ?>
        <li class="<?= $item->css_class ?> active">
        <? else: ?>
        <li<?= count($item->children) ? ' class="dropdown"' : false ?>>
        <? endif; ?>
            <a href="<?= $item->url ?>"<?= ($item->target_blank)? ' target="_blank"' : false ?><?= ($item->url == '#' ? ' onclick="return false;"' : false)?>>
                <?= $item->name ?>
            </a>
            <? if(count($item->children)) echo Yii::$app->controller->renderPartial('@frontend/shortcodes/views/menu-child', [
                'children' => $item->children
            ]); ?>
        </li>
    <? endif; ?>
<? endforeach; ?>
</ul>
<? endif; ?>