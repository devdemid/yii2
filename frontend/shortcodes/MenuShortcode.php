<?php

/**
 * The short code handler for menu
 *
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 *
 */

namespace frontend\shortcodes;

use common\models\Menu;
use Yii;
use yii\base\Widget;

class MenuShortcode extends Widget {

    /**
     * Form identifier
     * @var integer
     */
    public $id;

    /**
     * CSS Class root list
     * @var string
     */
    public $root_class;

    /**
     * Navigation Block Parameters
     * @var array
     */
    public $options = [];

    /**
     * Identifier root list
     * @var string
     */
    public $root_id;

    /**
     * Active link
     * @var string
     */
    public $active_url;

    public function run() {
        $menu = Menu::findOne([
            'id' => (int) $this->id,
            'published' => Menu::PUBLISHED_ON,
        ]);

        if (is_null($menu)) {
            return false;
        }

        // Active link default
        if (is_null($this->active_url)) {
            $this->active_url = '/' . Yii::$app->request->pathInfo;
        }

        $root_attributes = '';
        foreach ($this->options as $key => $value) {
            $root_attributes .= $key . '="' . $value . '" ';
        }

        return $this->render('menu', [
            'model' => $menu->tree,
            'root_attributes' => $root_attributes
        ]);
    }
}