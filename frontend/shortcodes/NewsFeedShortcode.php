<?php

/**
 * Displaying news on a page
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace frontend\shortcodes;

use Yii;
use yii\base\Widget;
use common\models\NewsFeed;

class NewsFeedShortcode extends Widget {

    /**
     * Block identifier
     * @var integer
     */
    public $id;

    /**
     * Number of records
     * @var integer
     */
    public $limit;

    /**
     * Template for widget
     * @var [string]
     */
    public $view;

    public function run() {
        $model = NewsFeed::findOne([
            'id' => $this->id,
            'published' => NewsFeed::PUBLISHED_ON
        ]);

        if(!is_null($model)) {
            return $this->render(($model->view ? $model->viewsExt : 'news-feed'), [
                'model' => $model
            ]);
        }
    }
}