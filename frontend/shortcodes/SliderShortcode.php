<?php

/**
 * Displaying blocks on a page
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace frontend\shortcodes;

use Yii;
use yii\base\Widget;
use common\models\Slide;

class SliderShortcode extends Widget {

    /**
     * Slider identifier
     * @var integer
     */
    public $id;

    /**
     * Template for block output
     * @var string
     */
    public $view = 'slider';

    public function run() {
        $model = Slide::find()
            ->where(['published' => Slide::PUBLISHED_ON, 'item_id' => $this->id])
            ->all();

        if(!is_null($model))
            return $this->render($this->view, ['model' => $model]);
    }
}