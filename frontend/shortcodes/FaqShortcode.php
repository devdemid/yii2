<?php

/**
 * The output of the faq block
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace frontend\shortcodes;

use Yii;
use yii\base\Widget;
use common\models\Faq;

class FaqShortcode extends Widget {

    /**
     * Faq identifier
     * @var integer
     */
    public $id;

    public function run() {
        $model = Faq::findOne(['published' => Faq::PUBLISHED_ON, 'id' => (int)$this->id]);

        if(!is_null($model)) {
            return Yii::$app->controller->renderPartial('@frontend/views/faq/branch', [
                'model' => $model
            ]);
        }
    }
}