<?php

/**
 * Displaying blocks on a page
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace frontend\shortcodes;

use Yii;
use yii\base\Widget;
use common\models\Block;

class BlockShortcode extends Widget {

    /**
     * Block identifier
     * @var integer
     */
    public $id;

    /**
     * Template for block output
     * @var string
     */
    public $view = 'block';

    /**
     * Icon css class
     * @var string
     */
    public $icon;

    public function run() {
        $model = Block::findOne([
            'published' => Block::PUBLISHED_ON,
            'id' => $this->id
        ]);

        if(!is_null($model))
            return $this->render($this->view, ['model' => $model]);
    }
}