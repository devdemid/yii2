<?php

/**
 * The short code handler for forms
 *
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 *
 */

namespace frontend\shortcodes;

use yii\base\Widget;
use common\models\Form;
use \frontend\models\FormModel;

class FormShortcode extends Widget {

    /**
     * Form identifier
     * @var integer
     */
    public $id;

    /**
     * CSS Class for form
     * @var stting
     */
    public $form_class;

    /**
     * Disabling the form name
     * @var boolean
     */
    public $title = true;

    public function run() {
        $model = Form::find()
            ->select(['id', 'published', 'name', 'captcha', 'btn_name', 'btn_reset'])
            ->where([
                'id' => (int)$this->id,
                'published' => Form::PUBLISHED_ON,
            ])
            ->one();

        if(is_null($model)) {
            return false;
        }

        return $this->render('form', [
            'model' => $model,
            'form_model' => FormModel::getModel($model)
        ]);
    }
}