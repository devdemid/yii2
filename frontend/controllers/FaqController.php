<?php

/**
 * FAQ - Frequently Asked Questions
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace frontend\controllers;

use common\models\Faq;
use frontend\components\FrontendController;

class FaqController extends FrontendController
{

    /**
     * List of branch responses
     * @param  [string] $alias
     * @param  [integer] $answer_id
     * @return [string]
     */
    public function actionIndex($alias, $answer_id = null, $answer_alias = null)
    {
        $model = Faq::findOne([
            'alias' => $alias,
            'published' => Faq::PUBLISHED_ON,
        ]);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        // Set Name and Metadata
        $this->view->title = $model->name;
        $this->metaDescription = $model->meta_description;
        $this->metaKeywords = $model->meta_keywords;
        $this->noindex = $model->noindex;

        if ($answer_id === null && count($model->answers) > 0) {
            $answer_id = $model->answers[0]->id;
        }

        return $this->render('index', ['model' => $model, 'answer_id' => $answer_id]);
    }

    /**
     * Reply page
     * @param  [string] $faq_alias
     * @param  [alias] $item_id
     * @param  [alias] $id
     * @return [string]
     */
    // public function actionDetail($faq_alias, $item_id, $id) {
    //     $model = FaqAnswers::find()
    //         ->where([
    //             'id' => (int) $id,
    //             'item_id' => (int) $item_id,
    //             'published' => FaqAnswers::PUBLISHED_ON
    //         ])
    //         ->one();

    //     if (is_null($model) && $model->enable_page == Faq::ENABLE_PAGE) {
    //         throw new \yii\web\NotFoundHttpException();
    //     }

    //     return $this->render('detail', ['model' => $model]);
    // }
}
