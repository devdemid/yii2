<?php

/**
 * Subscription management
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace frontend\controllers;

use common\models\Subscribes;
use Yii;
use yii\web\Response;

class SubscribesController extends \frontend\components\FrontendController
{

    /**
     * Container for ajax replies
     * @var array
     */
    public $ajaxResponse = [
        'body' => false,
    ];

    /**
     * New subscriber
     * @return String
     */
    public function actionNew()
    {
        if (!Yii::$app->request->isAjax) {
            die('Access denied');
        }

        if(Yii::$app->options->get('is_nable_subscription')) {
            $model = new Subscribes;

            if (Yii::$app->request->isPost) {
                if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                    $model->save();
                    $model->sendConfirm();

                    // Save the data to the cookies to control the output of the block
                    Yii::$app->response->cookies->add(new \yii\web\Cookie([
                        'name' => Subscribes::COOKIE_KEY_NAME,
                        'value' => true,
                    ]));

                    $model->unsetAttributes();
                    $model->is_success = 'Спасибо! Вам необходимо подтвердить адрес для получения скидки!';
                }
            }

            $this->ajaxResponse['body'] = $this->renderPartial('form', [
                'model' => $model,
            ]);
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $this->ajaxResponse;
    }

    /**
     * Confirm email address
     * @param  String $hash
     * @return String
     */
    public function actionConfirm($hash)
    {
        $model = Subscribes::findBySql('
            SELECT *
            FROM '. Subscribes::getTableSchema()->fullName .'
            WHERE MD5(CONCAT(`status`,`hash_salt`)) = :hash AND status = :status;',
            [
                ':hash' => $hash,
                ':status' => Subscribes::STATUS_NEW
            ]
        )->one();

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $model->status = Subscribes::STATUS_CONFIRMED;
        $model->update();

        if (!headers_sent()) {
            header('Refresh:5; url=/');
        }

        // Save the data to the cookies to control the output of the block
        Yii::$app->response->cookies->add(new \yii\web\Cookie([
            'name' => Subscribes::COOKIE_KEY_NAME,
            'value' => $model->hash,
        ]));

        $this->layout = self::LAYOUT_WRAPPER_NAME;
        return $this->render('confirm', ['model' => $model]);
    }

    /**
     * Unsubscribe
     * @param  String $hash
     * @return String
     */
    public function actionUnsubscribe($hash)
    {
        $model = Subscribes::findBySql('
            SELECT *
            FROM '. Subscribes::getTableSchema()->fullName .'
            WHERE MD5(CONCAT(`status`,`hash_salt`)) = :hash AND status = :status;',
            [
                ':hash' => $hash,
                ':status' => Subscribes::STATUS_CONFIRMED
            ]
        )->one();

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $model->status = Subscribes::STATUS_DISABLED;
        $model->update();
        $model->clearData();

        if (!headers_sent()) {
            header('Refresh:5; url=/');
        }

        $this->layout = self::LAYOUT_WRAPPER_NAME;
        return $this->render('unsubscribe', ['model' => $model]);
    }
}
