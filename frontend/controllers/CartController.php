<?php

/**
 * Cart products
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace frontend\controllers;

use common\models\Tours;
use frontend\widgets\CartWidget;
use Yii;
use yii\web\Response;

/**
 * Site controller
 */
class CartController extends \frontend\components\FrontendController
{

    /**
     * Container for ajax replies
     * @var array
     */
    public $ajaxResponse = [
        'body' => false,
    ];

    /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            // Checking the signature for requests to the cart
            if (in_array($action->id, ['add', 'remove'])) {
                 if (!Yii::$app->request->isAjax) {
                    die('Access denied');
                }

                Yii::$app->response->format = Response::FORMAT_JSON;

                $id = Yii::$app->request->get('id', false);
                $sig = Yii::$app->request->get('sig', false);

                if ($id && $sig) {
                    if (!Yii::$app->cart->checkSignature($id, $sig)) {
                        $this->ajaxResponse['error'] = 'Ошибка! Неверная подпись.';
                    }
                } else {
                    $this->ajaxResponse['error'] = 'Access denied';
                }
            }

            return true;
        }
        return false;
    }

    /**
     * [actionIndex description]
     * @return String
     */
    public function actionIndex() {
        $cart = Yii::$app->cart->getRecord();
        $models = [];

        if(is_array($cart) && count($cart)) {
            $models = Tours::findAll(array_keys($cart));
        }
        return $this->render('index', ['models' => $models]);
    }

    /**
     * Add item to cart
     * @return String
     */
    public function actionAdd()
    {
        if (!array_key_exists('error', $this->ajaxResponse)) {
            $model = Tours::findOne((int) Yii::$app->request->get('id', 0));

            if (!is_null($model) && Yii::$app->cart->add($model->id, [])) {
                $this->ajaxResponse['count'] = Yii::$app->cart->count;
                if(Yii::$app->cart->count) {
                    $this->ajaxResponse['body'] = CartWidget::widget();

                    $cart_record = Yii::$app->cart->record;
                    $models = [];

                    if(is_array($cart_record) && count($cart_record)) {
                        $models = Tours::findAll(array_keys($cart_record));
                    }

                    $this->ajaxResponse['modal'] = $this->renderPartial('modal', [
                        'models' => $models
                    ]);
                }
            } else {
                $this->ajaxResponse['error'] = 'Не удалось найти запись в `DB`';
            }
        }

        return $this->ajaxResponse;
    }

    /**
     * Remove item from cart
     * @return [type] [description]
     */
    public function actionRemove()
    {
        if (!array_key_exists('error', $this->ajaxResponse)) {
            $id = (int) Yii::$app->request->get('id', 0);
            if($id && Yii::$app->cart->remove($id)) {
                $this->ajaxResponse['count'] = Yii::$app->cart->count;
                if(!$this->ajaxResponse['count']) {
                    $this->ajaxResponse['body'] = CartWidget::widget();
                }
            } else {
                $this->ajaxResponse['error'] = 'Не удалось удалить запись!';
            }
        }
        return $this->ajaxResponse;
    }
}
