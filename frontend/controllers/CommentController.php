<?php

/**
 * Receiving and processing incoming comments
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace frontend\controllers;

use common\models\Comment;
use frontend\components\FrontendController;
use frontend\models\FormComment;
use Yii;
use yii\data\Pagination;
use yii\web\Response;

/**
 * Site controller
 */
class CommentController extends FrontendController
{

    /**
     * Container for ajax replies
     * @var array
     */
    public $ajaxResponse = [
        'body' => false,
    ];

    /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if ($action->id != 'captcha' && !Yii::$app->request->isAjax) {
                die('Access denied');
            }
            return true;
        }
        return false;
    }

    /**
     * Data handler from the form
     * @return JSON String
     */
    public function actionForm()
    {
        $model = new FormComment;

        if (Yii::$app->user->isGuest) {
            $model->setScenario($model::SCENARIO_GUEST);
        }

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {
                $model->unsetAttributes();
                $model->is_success = true;
            }
        }

        $this->ajaxResponse['body'] = $this->renderPartial('form', [
            'model' => $model,
        ]);

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $this->ajaxResponse;
    }

    /**
     * Paginate comments output
     * @return JSON String
     */
    public function actionList($id, $module)
    {

        $_where = [
            'item_id' => (int) $id,
            'module' => $module,
            'published' => Comment::PUBLISHED_ON
        ];

        // Only comments that have been moderated
        if(Yii::$app->options->get('only_moderation')) {
            $_where['moderation'] = Comment::MODERATION_ON;
        }

        $total = Comment::find()
            ->where($_where)
            ->orderBy([Comment::ORDER_BY_SORTING => SORT_DESC])
            ->count();

        $pages = new Pagination([
            'totalCount' => $total,
            'pageSize' => Yii::$app->options->get('page_limit_comments'),
        ]);

        $next_page = (int) Yii::$app->request->get('page', 1);
        $pages->page = $next_page;

        // Comments
        $comments = Comment::find()
            ->where($_where)
            ->orderBy([Comment::ORDER_BY_SORTING => SORT_DESC])
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        $this->ajaxResponse['offset'] = $pages->page;

        // Next page
        $this->ajaxResponse['nextPage'] = ($next_page++ >= $pages->pageCount-1) ? 0 : $next_page;

        // List comments
        $this->ajaxResponse['body'] = $this->renderPartial('list', [
            'comments' => $comments,
        ]);

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $this->ajaxResponse;
    }

    /**
     * @inheritdoc
     * @link http://www.yiiframework.com/doc-2.0/yii-captcha-captchaaction.html
     * @return array
     */
    public function actions()
    {
        return [
            'captcha' => [
                'backColor' => 0xFFFFFF,
                'class' => 'common\ext\captcha\CaptchaAction',
                'fontFile' => '@yii/captcha/SpicyRice.ttf',
                'foreColor' => 0x576371,
                'height' => 40,
                'offset' => 4,
                'transparent' => false,
                'width' => 160,
            ],
        ];
    }
}
