<?php

/**
 * Sitemap controller
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace frontend\controllers;

use frontend\components\FrontendController;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use Yii;

class SitemapController extends FrontendController {

    /**
     * Site map for search robots
     * @return string
     */
    public function actionIndex() {
        Yii::$app->response->format = Response::FORMAT_XML;
        return $this->renderPartial('index');
    }
}