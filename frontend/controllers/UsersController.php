<?php

/**
 * User Accounts
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace frontend\controllers;

use common\models\Files;
use common\models\SigninForm;
use common\models\SignupForm;
use common\models\User;
use frontend\models\ChangeEmail;
use frontend\models\ChangeNotification;
use frontend\models\ChangePassword;
use common\models\Order;
use Yii;
use yii\web\Response;
use yii\data\Pagination;

class UsersController extends \frontend\components\FrontendController
{

    public $defaultPageSize = 10; // Number of entries per page

    /**
     * Profile info
     * @return String
     */
    public function actionProfile()
    {
        if(Yii::$app->user->isGuest) {
            return $this->redirect(Yii::$app->user->url->signin);
        }

        $model = User::findOne(Yii::$app->user->identity->id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            Yii::$app->session->setFlash('success', 'Данные профиля успешно изменены!');
            return $this->redirect(Yii::$app->request->url);
        }

        return $this->render('profile', ['model' => $model]);
    }

    /**
     * Settings account
     * @return String
     */
    public function actionSettings()
    {
        if(Yii::$app->user->isGuest) {
            return $this->redirect(Yii::$app->user->url->signin);
        }

        $model = User::findOne(Yii::$app->user->identity->id);
        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        // Password Updates
        $change_password = new ChangePassword;
        if ($change_password->load(Yii::$app->request->post()) && $change_password->update()) {
            Yii::$app->session->setFlash('success', 'Пароль успешно изменен!');
            return $this->redirect(Yii::$app->request->url);
        }

        // Email Updates
        $change_email = new ChangeEmail;
        if ($change_email->load(Yii::$app->request->post()) && $change_email->update()) {
            Yii::$app->session->setFlash('success', 'Email успешно изменен!');
            return $this->redirect(Yii::$app->request->url);
        }

        // Notification settings
        $change_notification = new ChangeNotification;
        $change_notification->setAttributes([
            'notification_new_tours' => $model->notification_new_tours,
            'notification_comp_news' => $model->notification_comp_news,
        ]);
        unset($model);

        if ($change_notification->load(Yii::$app->request->post()) && $change_notification->update()) {
            Yii::$app->session->setFlash('success', 'Настроки уведомлений успешно обновлены!');
            return $this->redirect(Yii::$app->request->url);
        }

        return $this->render('settings', [
            'change_password' => $change_password,
            'change_email' => $change_email,
            'change_notification' => $change_notification,
        ]);
    }

    /**
     * My orders
     * @return String
     */
    public function actionOrders($sort = null)
    {

        if(Yii::$app->user->isGuest) {
            return $this->redirect(Yii::$app->user->url->signin);
        }

        // Filters
        $filters = [
            'asc' => 'Сначала старые',
            'desc' => 'Сначала новые',
        ];

        // Order by filter
        $order_by = ['id' => SORT_DESC];
        if(isset($_GET['sort']) && $sort !== null) {
            if($sort == 'desc') {
                $order_by = [Order::ORDER_BY_SORTING => SORT_DESC];
            }
            if($sort == 'asc') {
                $order_by = [Order::ORDER_BY_SORTING => SORT_ASC];
            }
        }

        $total = Order::find()
            ->where(['user_id' => Yii::$app->user->identity->id])
            ->andWhere('status != :status', [':status' => Order::STATUS_CANCEL])
            ->orderBy($order_by)
            ->count();

        $pages = new Pagination([
            'totalCount' => $total,
            'pageSize' => $this->defaultPageSize,
        ]);

        $models = Order::find()
            ->where(['user_id' => Yii::$app->user->identity->id])
            ->andWhere('status != :status', [':status' => Order::STATUS_CANCEL])
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy($order_by)
            ->all();

        return $this->render('orders', [
            'models' => $models,
            'pages' => $pages,
            'filters' => $filters,
            'filter_sort' => $sort
        ]);
    }

    /**
     * Authorization
     * @return String
     */
    public function actionSignin()
    {
        $this->layout = self::LAYOUT_WRAPPER_NAME;
        $model = new SigninForm;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->login();
            return $this->redirect(Yii::$app->user->url->profile);
        }

        return $this->render('signin', ['model' => $model]);
    }

    /**
     * Registration
     * @return String
     */
    public function actionSignup()
    {
        $this->layout = self::LAYOUT_WRAPPER_NAME;
        $model = new SignupForm;

        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            $model->login();
            return $this->redirect(Yii::$app->user->url->profile);
        }

        return $this->render('signup', ['model' => $model]);
    }

    /**
     * Restore password
     * @return String
     */
    public function actionPasswd()
    {
        return $this->render('passwd');
    }

    /**
     * Loading User avatar
     * @return String
     */
    public function actionAvatar()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost && !Yii::$app->user->isGuest) {

            $image = new Files(['scenario' => Files::SCENARIO_IMAGE]);
            $user = Yii::$app->user->identity;

            if ($image->load(Yii::$app->request->post())) {
                $image->setRules(Yii::$app->params['upload'][Files::SCENARIO_IMAGE]);

                foreach ($user->files as $item) {
                    $item->delete();
                }

                if ($image->upload()) {
                    return [
                        'link' => $image->getImageLink($user->image_sizes[0]),
                        'item_id' => $image->item_id,
                        'size' => $image->filesize,
                        'id' => $image->id
                    ];
                }
            } else {
                return ['errors' => $image->errors];
            }
        } else {
            return ['access' => 'Access denied'];
        }

        return [];
    }

    /**
     * Exit
     * @return Object
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }
}
