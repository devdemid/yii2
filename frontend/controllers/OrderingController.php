<?php

/**
 * Ordering controller
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace frontend\controllers;

use common\models\Order;
use common\models\OrderData;
use common\models\TourPrices;
use common\models\Tours;
use frontend\components\FrontendController;
use Yii;
use yii\helpers\ArrayHelper;

class OrderingController extends FrontendController
{
    /**
     * Ordering
     * @return String
     */
    public function actionIndex()
    {
        $cart = Yii::$app->cart->getRecord();
        $products = [];

        // Receiving data by goods
        if (is_array($cart) && count($cart)) {
            $products = Tours::find()
                ->where(['IN', 'id', array_keys($cart)])
                ->indexBy('id')
                ->all();
        }

        // If we do not send goods to the cart
        if (!count($products)) {
            return $this->redirect('/cart');
        }

        $order = new Order;
        $order_data = [];

        // Adding data to the order
        foreach ($products as $item) {
            $model = new OrderData;

            if (Yii::$app->request->isPost && isset($_POST['OrderData'])) {
                $model->setAttributes(Yii::$app->request->post('OrderData')[$item->id]);
            } else {
                $model->adults = 1;
                $model->children = 0;
            }

            $order_data[$item->id] = $model;
            unset($model);
        }

        // Save order and send for payment
        if (Yii::$app->request->isPost && isset($_POST['Order'])) {
            if ($order->load(Yii::$app->request->post()) && $order->validate()) {
                $valid = true;

                // Validation of order data
                foreach ($order_data as $model) {
                    $valid = $valid && $model->validate();
                }

                // Save an order
                if ($valid) {
                    $order->payment_type = $order::PAYMENT_TYPE_ROBOKASSA;
                    $order->save(true);

                    foreach ($order_data as $item_id => $model) {
                        $tour_price = TourPrices::findOne($model->price_id);
                        $model->setAttributes([
                            'item_id' => $order->id,
                            'name' => ArrayHelper::getValue($products[$item_id], 'name', ''),
                            'price' => $tour_price->getPrice(false),
                            'children_price' => $tour_price->children_price,
                            'start_date' => $tour_price->start_date,
                            'end_date' => $tour_price->end_date,
                        ]);
                        $model->save(true);
                    }

                    // get merchant
                    $merchant = Yii::$app->get('robokassa');

                    // Recent Status Updates
                    $order->updateAttributes(['status' => $order::STATUS_PENDING]);

                    // Clearing the order basket
                    Yii::$app->cart->clear();

                    // We will send for payment
                    return $merchant->payment($order->totalPrice, $order->id, 'Оплата заказа #ID' . $order->id, null, Yii::$app->user->identity->email);
                }
            }
        }

        return $this->render('index', [
            'products' => $products,
            'order' => $order,
            'order_data' => $order_data,
        ]);
    }

    /**
     * Payment of tour
     * @param  String $hash
     * @return String
     */
    public function actionPay($hash) {

        if(Yii::$app->user->isGuest) {
            throw new \yii\web\NotFoundHttpException();
        }

        $user = Yii::$app->user->identity;
        $model = Order::find()
            ->where("MD5(CONCAT(`id`, `create_timestamp`)) = '" . $hash . "'")
            ->andWhere(['user_id' => $user->id])
            ->andWhere('status != :status', [':status' => Order::STATUS_CANCEL])
            ->one();

        if(is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        // get merchant
        $merchant = Yii::$app->get('robokassa');
        $model->updateAttributes(['status' => $model::STATUS_PENDING]);
        return $merchant->payment($model->totalPrice, $model->id, 'Оплата заказа #ID' . $model->id, null, $user->email);
    }

    /**
     * Cancellations
     * @param String $hash
     */
    public function actionCancel($hash) {
        if(Yii::$app->user->isGuest) {
            throw new \yii\web\NotFoundHttpException();
        }

        $user = Yii::$app->user->identity;
        $model = Order::find()
            ->where("MD5(CONCAT(`id`, `create_timestamp`)) = '" . $hash . "'")
            ->andWhere(['user_id' => $user->id])
            ->andWhere('status != :status', [':status' => Order::STATUS_SUCCESS])
            ->one();

        if(is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $model->updateAttributes(['status' => Order::STATUS_CANCEL]);
        Yii::$app->session->setFlash('success', 'Отмена заказа выполнена успешно!');
        return $this->redirect(Yii::$app->user->url->orders);
    }

    /**
     * Payment Information
     * @param  String $hash
     * @return String
     */
    public function actionDetail($hash) {
        if(Yii::$app->user->isGuest) {
            throw new \yii\web\NotFoundHttpException();
        }

        $model = Order::find()
            ->where("MD5(CONCAT(`id`, `create_timestamp`)) = '" . $hash . "'")
            ->andWhere(['status' => Order::STATUS_SUCCESS])
            ->one();

        if(is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        // Set Name and Metadata
        $this->view->title = 'Заказа #ID ' . $model->id;
        return $this->render('detail', ['model' => $model]);
    }
}
