<?php

/**
 * Handling Form Design Data
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace frontend\controllers;

use common\models\Form;
use common\models\FormResult;
use frontend\components\FrontendController;
use frontend\models\FormModel;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\helpers\Json;
use yii\web\Response;

class FormController extends FrontendController {

    /**
     * Container for ajax replies
     * @var array
     */
    public $ajaxResponse = [
        'body' => false,
    ];

    /**
     * Processing of dynamic forms
     * @param  string $unique
     * @return array
     */
    public function actionRequest($unique) {
        if (!Yii::$app->request->isAjax) {
            die('Access denied!');
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = Form::find()
            ->select([
                'id', 'published', 'name', 'captcha', 'btn_name', 'btn_reset', 'success_position', 'success_text',
                'email', 'format_mail', 'from_view', 'to_view', 'is_reply_message', 'return_type', 'run_method',
                'is_save_result', 'is_add_attachments', 'is_to_message',
            ])
            ->where("MD5(CONCAT(`id`, `name`)) = '" . $unique . "'")
            ->andWhere([
                'published' => Form::PUBLISHED_ON,
            ])
            ->one();

        if (is_null($model)) {
            return $ajaxResponse['errors'] = Yii::t('app', 'There is no such form!');
        }

        $form_model = FormModel::getModel($model);

        if ($form_model->load(Yii::$app->request->post()) && $form_model->validate()) {
            // If there are files upload
            $form_model->uploadAttachments($model->id);

            /**
             * Begin run method
             * \common\components\FormRunMethod run static method
             */
            if ($model->return_type == Form::RETURN_TYPE_RUN_METHOD && $model->run_method) {
                $FormRunMethod = new \common\components\FormRunMethod;

                if (method_exists($FormRunMethod, $model->run_method) /* Has method */) {
                    $return_run_method = forward_static_call(
                        [$FormRunMethod, $model->run_method],
                        $form_model->attributes
                    );

                    if (is_string($return_run_method)) {
                        $model->success_text = $return_run_method;
                    }
                }
            }
            // End run method

            // Begin Saving data to form results
            if ($model->is_save_result) {
                $form_result_model = new FormResult;
                $form_result_model->setAttributes([
                    'item_id' => $model->id,
                    'json_result' => Json::encode($form_model->formResults),
                    'to_email' => $model->email,
                ]);
                $form_result_model->save();

                // Get the record ID in the results database
                $form_model->variable_id = $form_result_model->id;
                unset($form_result_model);
            }
            // End Saving data to form results

            // Apply variables to messages
            $model->setVariables($form_model->getVariables(true));

            // Default Sender email
            if (!Yii::$app->options->hasParamValue('system_email')) {
                $from_system_email = Form::DEFAULT_EMAIL_USER . '@' . Yii::$app->request->serverName;
            } else {
                $from_system_email = Yii::$app->options->get('system_email');
            }

            // Begin send messages
            if ($model->is_to_message == Form::IS_TO_MESSAGE_ON) {
                $messages = [];
                $fullPatch = Yii::getAlias('@upload' . FormModel::ATTACHMENTS_DIR . $model->id . '/');

                foreach ($model->emailList as $index => $email) {
                    // Send messages to email inboxes
                    if ($model->format_mail == Form::FORMAT_MESSAGE_TEXT) {
                        $mailer = Yii::$app->mailer->compose('form-text', [
                            'content' => $model->to_view,
                        ]);
                    } else {
                        $mailer = Yii::$app->mailer->compose('form-html', [
                            'content' => $model->to_view,
                        ]);
                    }

                    $mailer->setSubject($model->name)
                        ->setFrom($from_system_email)
                        ->setTo($email);

                    // Attaching files to a message
                    if ($model->is_add_attachments == Form::IS_ADD_ATTACHMENTS_ON) {
                        foreach ($form_model->fileAttributes as $item) {
                            if ($form_model->$item && file_exists($fullPatch . $form_model->$item)) {
                                $mailer->attach($fullPatch . $form_model->$item);
                            }
                        }
                    }

                    $messages[$index] = $mailer;
                }

                $form_model->status = (boolean) Yii::$app->mailer->sendMultiple($messages);
                unset($fullPatch, $messages, $mailer);
            } else {
                $form_model->status = true;
            }
            // End send messages

            // Begin Send reply messages
            if ($form_model->status && $model->is_reply_message == Form::IS_REPLY_MESSAGE_ON) {
                $email = $form_model->getFieldEmail($model->fields);

                if ($email) {
                    // Send messages to email inboxes
                    if ($model->format_mail == Form::FORMAT_MESSAGE_TEXT) {
                        $mailer = Yii::$app->mailer->compose('form-text', [
                            'content' => $model->from_view,
                        ]);
                    } else {
                        $mailer = Yii::$app->mailer->compose('form-html', [
                            'content' => $model->from_view,
                        ]);
                    }

                    $mailer->setSubject($model->name)
                        ->setFrom($from_system_email)
                        ->setTo($email)
                        ->send();
                }
            }
            // End Send reply messages

            unset($email, $mailer);
            $form_model->unsetAttributes();
        }

        // else {
        //     $this->ajaxResponse['errors'] = $form_model->getErrors();
        // }

        $this->ajaxResponse['body'] = $this->renderPartial('request', [
            'form' => new ActiveForm,
            'model' => $model,
            'form_model' => $form_model,
        ]);

        return $this->ajaxResponse;
    }

    /**
     * @inheritdoc
     * @link http://www.yiiframework.com/doc-2.0/yii-captcha-captchaaction.html
     * @return array
     */
    public function actions() {
        return [
            'captcha' => [
                'backColor' => 0xFFFFFF,
                'class' => 'common\ext\captcha\CaptchaAction',
                'fontFile' => '@yii/captcha/SpicyRice.ttf',
                'foreColor' => 0x576371,
                'height' => 40,
                'offset' => 4,
                'transparent' => false,
                'width' => 160,
            ],
        ];
    }
}