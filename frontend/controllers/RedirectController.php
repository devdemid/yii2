<?php

/**
 * Redirect controller
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace frontend\controllers;

use frontend\components\FrontendController;
use frontend\components\Redirect;
use Yii;

class RedirectController extends FrontendController {

    /**
     * Redirect to other sites
     * @param  string $url_base64
     */
    public function actionIndex($url_base64) {
        $redirect = explode(Redirect::LIST_SEPARATOR, Redirect::getUrlBase64($url_base64));

        if (is_array($redirect) && count($redirect) == 2) {
            if (Redirect::validate($redirect[0], $redirect[1])) {
                $this->redirect($redirect[1]);
            } else {
                throw new \yii\web\NotFoundHttpException("Неверная ссылка перенаправления!");
            }

        } else {
            throw new \yii\web\NotFoundHttpException("Неверный формат данных!");
        }

    }
}