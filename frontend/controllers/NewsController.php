<?php

/**
 * News Content Output
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace frontend\controllers;

use frontend\components\FrontendController;
use Yii;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use \common\models\News;
use \common\models\NewsFeed;
use \common\models\NewsCat;

/**
 * Site controller
 */
class NewsController extends FrontendController {

    public $defaultPageSize = 8; // Number of entries per page

    /**
     * All news feed
     * @param  string $alias
     * @param  string $cat_alias
     * @return string
     */
    public function actionIndex($alias, $cat_alias = null, $tag_group_alias = null, $tag = null) {

        $this->body_class = 'light blog grid-no-sidebar';

        $newsFeed = NewsFeed::findOne([
            'alias' => $alias,
            'published' => NewsFeed::PUBLISHED_ON
        ]);

        if (is_null($newsFeed)) {
            throw new \yii\web\NotFoundHttpException();
        }

        // Set Name and Metadata
        $this->view->title = $newsFeed->name;
        $this->metaDescription = $newsFeed->meta_description;
        $this->metaKeywords = $newsFeed->meta_keywords;


        $_where = [
            'item_id' => $newsFeed->id,
            'published' => News::PUBLISHED_ON,
        ];

        // Begin fix filters
        $form_filters = [];
        $selected = [
            'tags'   => [],
            'stars'  => [],
            'cat_id' => (int) Yii::$app->request->get('cat_id'),
            'q'      => (new \frontend\models\Search)->apply(Yii::$app->request->get('q', null))
        ];
        if(isset($_GET['tags']) || isset($_GET['stars'])) {
            $form_filters[] = 'OR';
        }
        if(isset($_GET['tags'])) {
            foreach ($_GET['tags'] as $tag) {
                $selected['tags'][] = md5($tag);
                $form_filters[] = ['like', 'tags', $tag];
            }
        }
        if(isset($_GET['stars'])) {
            foreach ($_GET['stars'] as $tag) {
                $selected['stars'][] = md5($tag);
                $form_filters[] = ['like', 'fields', $tag];
            }
        }
        if($selected['cat_id']) {
            $_where['cat_id'] = $selected['cat_id'];
        }
        // End fix filters

        // Tags
        $url_filter = [];
        if($tag_group_alias !== null && $tag !== null) {
            $url_filter = ['AND', ['like', 'tags', $tag]];
        }

        // Category model
        if(!is_null($cat_alias)) {
            $cat_model = NewsCat::find()
                ->select('id')
                ->where([
                    'alias' => $cat_alias,
                    'item_id' => $newsFeed->id,
                    'published' => NewsCat::PUBLISHED_ON
                ])
                ->one();

            if(!is_null($cat_model)) {
                $_where['cat_id'] = $cat_model->id;
            }
        }

        $total = News::find()
            ->where($_where)
            ->andFilterWhere($url_filter)
            ->andFilterWhere($form_filters)
            ->count();

        $pages = new Pagination([
            'totalCount' => $total,
            'pageSize' => $newsFeed->page_size ? $newsFeed->page_size : $this->defaultPageSize,
        ]);

        $pages->pageSizeParam = false;

        $models = News::find()
            ->where($_where)
            ->andFilterWhere($url_filter)
            ->andFilterWhere($form_filters)
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy([
                News::ORDER_BY_FIXED => SORT_DESC,
                News::ORDER_BY_SORTING => SORT_ASC
            ])
            ->all();

        $view_name = $newsFeed->view_index ? $newsFeed->view_index : 'index';
        return $this->render($view_name, [
            'models' => $models,
            'newsFeed' => $newsFeed,
            'pages' => $pages,
            'total' => $total,
            'cat_alias' => $cat_alias,
            'selected' => $selected,
        ]);
    }

    /**
     * A detailed description of the news
     * @param  string  $alias_feed
     * @param  integer $id
     * @param  string  $alias
     * @return string
     */
    public function actionDetail($alias_feed, $id, $alias) {
        $newsFeed = NewsFeed::findOne(['alias' => $alias_feed, 'published' => NewsFeed::PUBLISHED_ON]);
        $model = News::findOne(['id' => $id, 'published' => News::PUBLISHED_ON]);

        if (is_null($newsFeed) || is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        // Begin Update record views
        $session = Yii::$app->session;
        if ($session->isActive && $newsFeed->is_count_views) {

            if($session->has(Yii::$app->controller->id)) {
                $views = $session->get(Yii::$app->controller->id);
            } else {
                $views = [];
            }

            if (!ArrayHelper::isIn($model->id, $views)) {
                $views[] = $model->id;
                $session->set(Yii::$app->controller->id, $views);
                $model->updateCounters(['views' => 1]);
            }

            unset($views, $session);
        }
        // End Update record views

        // Set Name and Metadata
        $this->view->title = $model->name;
        $this->metaDescription = $model->meta_description;
        $this->metaKeywords = $model->meta_keywords;
        $this->noindex = $model->noindex;

        //var_dump($alias_feed, $id, $alias); exit;
        $view_name = $newsFeed->view_detail ? $newsFeed->view_detail : 'detail';
        return $this->render($view_name, ['newsFeed' => $newsFeed, 'model' => $model]);
    }
}