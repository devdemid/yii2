<?php
/**
 * Static Content Output
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace frontend\controllers;

use frontend\components\FrontendController;
use yii\helpers\ArrayHelper;
use Yii;
use common\models\Page;

/**
 * Site controller
 */
class PageController extends FrontendController {

    public function actionIndex($alias) {
        $model = Page::find()
            ->where(['published' => Page::PUBLISHED_ON])
            ->andWhere(['alias' => $alias])
            ->one();

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        // Wrap layer connections
        if($model->is_wrapper)
            $this->layout = self::LAYOUT_WRAPPER_NAME;

        // Set Name and Metadata
        $this->view->title = $model->name;
        $this->metaDescription = $model->meta_description;
        $this->metaKeywords = $model->meta_keywords;
        $this->noindex = $model->noindex;

        // Verify and connect a custom template
        if($model->view && file_exists(__DIR__ . '/../views/page/' . $model->view . '.php')) {
            $_view = $model->view;
        } else {
            $_view = 'index';
        }

        return $this->render($_view, ['model' => $model]);
    }
}