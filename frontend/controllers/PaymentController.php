<?php

/**
 * Payment processing
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace frontend\controllers;
use common\models\Order;
use Yii;

class PaymentController extends \frontend\components\FrontendController {

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'result' => [
                'class' => '\robokassa\ResultAction',
                'callback' => [$this, 'resultCallback'],
            ],
            'success' => [
                'class' => '\robokassa\SuccessAction',
                'callback' => [$this, 'successCallback'],
            ],
            'fail' => [
                'class' => '\robokassa\FailAction',
                'callback' => [$this, 'failCallback'],
            ],
        ];
    }

    /**
     * Callback.
     * @param \robokassa\Merchant $merchant merchant.
     * @param integer $nInvId invoice ID.
     * @param float $nOutSum sum.
     * @param array $shp user attributes.
     */
    public function successCallback($merchant, $nInvId, $nOutSum, $shp)
    {
        $this->loadModel($nInvId)->updateAttributes([
            'paid_timestamp' => date('U')
        ]);
        Yii::$app->session->setFlash('success', 'Оплата выполнена успешно!');
        return $this->redirect('/users/orders');
    }

    public function resultCallback($merchant, $nInvId, $nOutSum, $shp)
    {
        $this->loadModel($nInvId)->updateAttributes(['status' => Order::STATUS_SUCCESS]);
        return $this->redirect('/users/orders');
    }

    public function failCallback($merchant, $nInvId, $nOutSum, $shp)
    {
        $model = $this->loadModel($nInvId);
        if ($model->status == Order::STATUS_PENDING) {
            $model->updateAttributes(['status' => Order::STATUS_FAIL]);
            Yii::$app->session->setFlash('error', 'Ошибка при оплате заказа!');
        } else {
            Yii::$app->session->setFlash('error', 'Статус не изменился!');
        }
        return $this->redirect('/users/orders');
    }

    /**
     * @param integer $id
     * @return Invoice
     * @throws \yii\web\BadRequestHttpException
     */
    protected function loadModel($id) {
        $model = Order::findOne($id);
        if ($model === null) {
            throw new BadRequestHttpException;
        }
        return $model;
    }
}