<?php
/**
 * Processing search queries
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace frontend\controllers;

use frontend\components\FrontendController;
use Yii;

/**
 * Site controller
 */
class SearchController extends FrontendController {
    public function actionIndex() {
        return $this->render('index');
    }
}