<?php

/**
 * Tours
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace frontend\controllers;

use common\models\TourPrices;
use common\models\Tours;
use frontend\components\FrontendController;
use frontend\models\Filters;
use frontend\models\Ordering;
use frontend\models\VotingHistory;
use Yii;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use common\models\DataGroup;

/**
 * Site controller
 */
class ToursController extends FrontendController
{
    public $pageSize = 8; // Number of entries per page

    /**
     * Tours
     * @return String
     */
    public function actionIndex($cat_alias = null)
    {

        // All filters
        $filters = new Filters;

        if($cat_alias !== null && !isset($_GET['Filters'])) {
            $cat_model = DataGroup::findOne(['alias' => $cat_alias]);

            if($cat_model !== null) {
                $_GET['Filters']['cat_id'] = $cat_model->id;
                unset($cat_model);
            }
        }

        // Set Name and Metadata
        $this->view->title = 'Туры в Грузию';

        $sql_params = [
            ':published' => Tours::PUBLISHED_ON,
            ':start_date' => date(TourPrices::PROP_DATE_FORMAT),
        ];

        // Filters
        $where_filter = ''; // Primary filter
        $where_filter_prices = ''; // Filter for price and date table

        if (isset($_GET['Filters'])) {
            $filters->attributes = Yii::$app->request->get('Filters');
            if ($filters->validate()) {

                // Filter categories
                if ($filters->cat_id) {
                    $where_filter .= 'AND (`t`.`cat_id` REGEXP :cat_id) ';
                    $sql_params[':cat_id'] = '\"' . $filters->cat_id . '\"';
                }

                // Filter restrictions
                if ($filters->restriction_id) {
                    $where_filter .= 'AND (`t`.`restrictions` REGEXP :restrictions) ';
                    $sql_params[':restrictions'] = '\"' . $filters->restriction_id . '\"';
                }

                // Filter types
                if ($filters->type_id) {
                    $where_filter .= 'AND (`t`.`type_id` = :type_id) ';
                    $sql_params[':type_id'] = $filters->type_id;
                }

                // Date range filter
                if ($filters->start_date && $filters->end_date || is_array($filters->range) && count($filters->range) == 2) {
                    $where_filter .= 'AND EXISTS(
                        SELECT 1
                        FROM `' . TourPrices::getTableSchema()->fullName . '`
                        WHERE `item_id` = `t`.`id`';

                    // Date range filter
                    if ($filters->start_date && $filters->end_date) {
                        $where_filter_prices .= 'AND (`start_date` >= :start_date AND `end_date` <= :end_date) ';
                        $sql_params[':start_date'] = $filters->start_date;
                        $sql_params[':end_date'] = $filters->end_date;
                    }

                    // Filter by price
                    if (is_array($filters->range) && count($filters->range) == 2) {
                        $where_filter_prices .= 'AND (`base_price` >= :start_price AND `base_price` <= :end_price) ';
                        $sql_params[':start_price'] = $filters->range[0];
                        $sql_params[':end_price'] = $filters->range[1];
                    }

                    $where_filter .= $where_filter_prices . ') ';
                }
            }
        }

        // Begin Query SQL -----------------------------------------------------------------
        $sql = '
            SELECT `t`.*,
                (
                    SELECT
                        MIN(base_price)
                    FROM `' . TourPrices::getTableSchema()->fullName . '`
                    WHERE `item_id` = `t`.`id`
                      AND `published` = :published
                      AND `start_date` >= :start_date ' . $where_filter_prices . '
                ) AS `default_price`
            FROM `' . Tours::getTableSchema()->fullName . '` AS t
            WHERE (`t`.`published`= :published) ' . $where_filter;

        // Search LIKE text
        if(isset($_GET['q'])) {
            $q_search = (new \frontend\models\Search)->apply(Yii::$app->request->get('q', null));

            if($q_search !== null) {
                $sql .= 'AND `t`.`name` LIKE \'%' . $q_search . '%\' ';
            }
        }

        // Sorting tours
        $ordering = new Ordering;
        if (isset($_GET['sort'])) {
            $ordering->sort = Yii::$app->request->get('sort', false);
            if ($ordering->validate()) {
                switch ($ordering->sort) {
                    case Ordering::ORDERING_BY_TOP:
                        $sql .= 'ORDER BY `t`.`rating` DESC ';
                        break;
                    case Ordering::ORDERING_BY_VIEWS:
                        $sql .= 'ORDER BY `t`.`views` DESC ';
                        break;
                    case Ordering::ORDERING_BY_PRICE_DESC:
                        $sql .= 'ORDER BY `default_price` DESC ';
                        break;
                    case Ordering::ORDERING_BY_PRICE_ASC:
                        $sql .= 'ORDER BY `default_price` ASC ';
                        break;
                    case Ordering::ORDERING_BY_SELLING:
                        $sql .= 'AND (`t`.`selling` = '. Tours::SELLING_ON .') ';
                        break;
                }
            }
        } else {
            // Default sorting
            $sql .= 'ORDER BY `t`.`fixed` DESC ';
            $ordering->sort = null;
        }
        // And Query SQL -------------------------------------------------------------------

        unset($where_filter, $where_filter_prices);

        $pages = new Pagination([
            'totalCount' => Tours::findBySql($sql, $sql_params)->count(),
            'pageSize' => $this->pageSize,
        ]);

        // Paginate
        $sql .= 'LIMIT ' . $pages->limit . ' OFFSET ' . $pages->offset;

        //var_dump($sql); exit;

        return $this->render('index', [
            'tours' => Tours::findBySql($sql, $sql_params)->all(),
            'filters' => $filters,
            'pages' => $pages,
            'ordering' => $ordering,
        ]);
    }

    /**
     * Detailed information about the tour
     * @param  Integer $id
     * @param  String $alias
     * @return String
     */
    public function actionDetail($id, $alias)
    {
        $model = Tours::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        // Begin Update record views
        $session = Yii::$app->session;
        if ($session->has(Yii::$app->controller->id)) {
            $views = $session->get(Yii::$app->controller->id);
        } else {
            $views = [];
        }

        if (!ArrayHelper::isIn($model->id, $views)) {
            $views[] = $model->id;
            $session->set(Yii::$app->controller->id, $views);
            $model->updateCounters(['views' => 1]);
        }

        unset($views, $session);
        // End Update record views

        // Set Name and Metadata
        $this->view->title = $model->name;
        $this->metaDescription = $model->meta_description;
        $this->metaKeywords = $model->meta_keywords;
        $this->noindex = $model->noindex;

        return $this->render('detail', ['model' => $model]);
    }

    /**
     * Rating of the rating
     * @param  Integer $id
     * @param  Integer $rating
     * @param  String $sig
     * @return JSONString
     */
    public function actionRating($id, $rating, $sig)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (!Yii::$app->request->isAjax) {
            return ['error' => 'Access denied!'];
        }

        $model = Tours::findOne((int) $id);

        if (is_null($model)) {
            return ['error' => 'Не удалось найти запись в базе данных!'];
        }

        $_signature = \Yii::$app->signature->generate(Yii::$app->controller->id . $model->id);

        // Signature verification
        if ($_signature !== $sig) {
            return ['error' => 'Неверная подпись!'];
        }

        if (VotingHistory::findOne(['signature' => $_signature])) {
            return [
                'id' => 'rating-' . $model->id,
                'success' => 'Вы уже поставили оценку!',
            ];
        }

        // Update rating
        $model->updateCounters([
            'rating' => (int) $rating,
            'rating_votes' => 1,
        ]);

        // Save history
        $voting_history = new VotingHistory;
        $voting_history->attributes = [
            'item_id' => $model->id,
            'module' => Yii::$app->controller->id,
            'value' => (int) $rating,
            'signature' => $_signature,
        ];
        $voting_history->save();

        return [
            'id' => 'rating-' . $model->id,
            'rate' => $model->getRate(),
            'success' => 'Спасибо! Ваша оценка принята.',
            'body' => trim($this->renderPartial('rating', [
                'id' => $model->id,
                'votes' => $model->rating_votes,
                'module' => Yii::$app->controller->id,
                'rate' => $model->getRate(),
            ])),
        ];
    }
}
