<?php

/**
 * Home page
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace frontend\controllers;

use frontend\components\FrontendController;
use yii\helpers\ArrayHelper;
use common\models\DataGroup;
use common\models\Tours;
use Yii;

/**
 * Site controller
 */
class MainController extends FrontendController {

    /**
     * Home page
     * @return string
     */
    public function actionIndex() {
        return $this->render('index', [
            'cats_model' => DataGroup::getGroupModel(DataGroup::GROUP_TOURS_CAT_ID),
            'tours' => Tours::find()
                ->where([
                    'published' => Tours::PUBLISHED_ON,
                    //'selling' => Tours::SELLING_ON
                ])
                ->orderBy('fixed DESC')
                ->limit(9)
                ->all()
        ]);
    }

    /**
     * The list of available channels
     * @return JSON String
     */
    public function actionError() {
        $exception = Yii::$app->errorHandler->exception;

        if ($exception !== null) {
            $this->layout = self::LAYOUT_WRAPPER_NAME;
            $this->body_class = 'light error-page';

            return $this->render('error', [
                'exception' => $exception,
                'statusCode' => $exception->statusCode,
                'name' => $exception->getName(),
                'message' => $exception->getMessage(),
                'copyright' => Yii::$app->options->get('copyright')
            ]);
        }
    }

    /**
     * Pages shutdown resource
     * @return array
     */
    public function actionOffline() {
        $this->view->title = 'Сайт выключен';

        return $this->render('offline', [
            'copyright' => Yii::$app->options->get('copyright'),
            'offline_message' => Yii::$app->options->get('offline_message')
        ]);
    }
}