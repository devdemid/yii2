<?php
return [
    'adminEmail' => 'admin@example.com',
    'upload' => [

        // Default image settings
        'image' => [
            'maxFiles' => 1,
            'maxSize' => 1024 * 1024 * 5, // Default maximum 5Mb
            'extensions' => 'jpg, jpeg, png, gif',
        ],

        // Default file settings
        'file' => [
            'maxFiles' => 1,
            'maxSize' => 1024 * 1024 * 5, // Default maximum 5Mb
            'extensions' => 'rar, zip, pdf, docs, mp4, mp3, jpg, jpeg, png, gif',
        ],
    ],
];
