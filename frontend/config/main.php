<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/params.php')
);

if(YII_DEV) {
    $params = array_merge(
        require(__DIR__ . '/../../common/config/params-local.php'),
        require(__DIR__ . '/params-local.php')
    );
}

return [
    'id' => 'Yii2 (NC)',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    //'on beforeSend' => '',
    //'on afterSend' => '',
    'components' => [

        'request' => [
            'csrfParam' => '_csrf-frontend',
            'cookieValidationKey' => 'yii_Y06sdfsdc4545kfDFGisjdfPp6ksiNJ',
        ],

        'user' => [
            'class' => 'common\ext\web\User',
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => false,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
            'loginUrl' => '/users/signin',
            'urls' => [
                'logout' => '/users/logout',
                'orders' => '/users/orders',
                'profile' => '/users/profile',
                'reset_password' => '/users/passwd',
                'signin' => '/users/signin',
                'signup' => '/users/signup',
                'settings' => '/users/settings',
            ]
        ],

        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'nc-f',
        ],

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'main/error',
        ],

        'assetManager' => [
            'appendTimestamp' => true,
            'linkAssets' => false,
        ],

        'signature' => [
            'class' => '\frontend\components\Signature'
        ],

        'cart' => [
            'class' => '\frontend\components\Cart',
            'key' => 'shop_key_123456789'
        ],

        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'rules' => [

                // Home
                'GET /' => 'main/index',

                // Tours
                'GET /tours' => 'tours/index',
                'GET /tours/cat/<cat_alias:[0-9A-z\_\-]+>' => 'tours/index',
                'GET /tours/<id:\d+>-<alias:[0-9A-z\_\-]+>.html' => 'tours/detail',

                // Pages
                'GET /<alias:[0-9A-z\_\-]+>.html' => 'page/index',

                // Faq
                'GET /faq/branch/<alias:[0-9A-z\_\-]+>' => 'faq/index',
                'GET /faq/branch/<alias:[0-9A-z\_\-]+>/<answer_id:\d+>-<answer_alias:[0-9A-z\_\-]+>.html' => 'faq/index',
                //'/faq/<faq_alias:[0-9A-z\_\-]+>-<item_id:\d+>/<id:\d+>.html' => 'faq/detail',

                // Forms
                'POST /form/request/<unique:[a-z0-9]{32}>' => 'form/request',

                // News feed
                //'<controller:news>/<alias:[0-9A-z\_\-]+>' => '<controller>/index',
                //'GET /news/<alias:[0-9A-z\_\-]+>-<id:\d+>' => 'news/index',
                'GET /news/feed/<alias:[0-9A-z\_\-]+>' => 'news/index',
                'GET /news/feed/<alias:[0-9A-z\_\-]+>/<cat_alias:[0-9A-z\_\-]+>' => 'news/index',
                'GET /news/feed/<alias:[0-9A-z\_\-]+>/<tag_group_alias:[0-9A-z\_\-]+>/<tag:[0-9А-яA-z\_\-\*\s\(\)\[\]]+>' => 'news/index',
                'GET /news/feed/<alias_feed:[0-9A-z\_\-]+>/<id:\d+>-<alias:[0-9A-z\_\-]+>.html' => 'news/detail',

                // Comments
                '/comment/<module:[0-9A-z]+>' => 'comment/index',
                '/comment/list/<module:[0-9A-z]+>/<id:\d+>' => 'comment/list',

                // Search
                //'/search' => 'search/index',

                // Payments
                'GET /payment/robokassa/result' => 'payment/result',
                'GET /payment/robokassa/success' => 'payment/success',
                'GET /payment/robokassa/fail' => 'payment/fail',

                'GET /ordering/detail/<hash:[a-z0-9]{32}>' => 'ordering/detail',
                'GET /ordering/pay/<hash:[a-z0-9]{32}>' => 'ordering/pay',
                'GET /ordering/cancel/<hash:[a-z0-9]{32}>' => 'ordering/cancel',

                // Users
                'POST /users/avatar' => 'users/avatar',

                // Redirect to other sites
                'GET /redirect=<url_base64:[0-9A-z\_\-\=]+>' => 'redirect/index',

                // Sitemap
                 'GET /sitemap.xml' => 'sitemap/index',

                 // Subscribes
                 'GET /subscribes/confirm/<hash:[a-z0-9]{32}>' => 'subscribes/confirm',
                 'GET /subscribes/unsubscribe/<hash:[a-z0-9]{32}>' => 'subscribes/unsubscribe',

                // Default
                '<controller:\w+>' => '<controller>/index',
                //'<controller:\w+>/<action:\w+>' => '<controller>/<action>'

            ],
        ],
    ],
    'params' => $params,
];
