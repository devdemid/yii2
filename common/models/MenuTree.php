<?php
/**
 * Form Generator
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace common\models;

use common\ext\db\ActiveRecord;

/**
 * Database structure
 *
 * @property int(11)      $id               - Unique identificator
 * @property int(11)      $parent_id        - text
 * @property int(11)      $level            - text
 * @property varchar(255) $name             - text
 * @property varchar(255) $url              - text
 * @property varchar(100) $css_class        - text
 * @property int(11)      $sorting          - text
 * @property tinyint(1)   $target_blank     - text
 * @property tinyint(1)   $published        - text
 */
class MenuTree extends ActiveRecord {

    /**
     * Active recording
     */
    const PUBLISHED_ON = 1;

    /**
     * Inactive recording
     */
    const PUBLISHED_OFF = 0;

    /**
     * Pattern for the css class
     */
    const PATTERN_CSS_CLASS = '/([А-яA-z0-9\s\-]+)/i';

    /**
     * Pattern for the url
     */
    const PATTERN_URL = '/(.*?)/i';

    /**
     * Root of the menu
     */
    const ROOT_PARENT_ID = 0;

    /**
     * Default level
     */
    const DEFAULT_LEVEL = 0;

    /**
     * Sort channels field
     */
    const ORDER_BY_SORTING = 'sorting';

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%menu_tree}}';
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules() {
        return [
            [['name', 'url'], 'required'],
            [['parent_id', 'level', 'sorting', 'target_blank', 'published'], 'integer'],
            [
                'css_class',
                'match',
                'pattern' => self::PATTERN_CSS_CLASS,
                'message' => '"{attribute}" неверное имя, разрешено только [A-z0-9\_]',
            ],

            ['parent_id', 'default', 'value' => self::ROOT_PARENT_ID],
            ['level', 'default', 'value' => self::DEFAULT_LEVEL],

            ['css_class', 'string', 'max' => '100'],
            [['name', 'url'], 'string', 'max' => '255']
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels() {
        return [
            'parent_id' => 'Идентификатор родителя',
            'name' => 'Название атрибута',
            'url' => 'Ссылка',
            'css_class' => 'CSS Класс',
            'sorting' => 'Позиция',
            'target_blank' => 'Новое окно',
            'published' => 'Публикация ссылки'
        ];
    }

    /**
     * Parent element menu
     * @return common\models\MenuTree
     */
    public function getParent() {
        return $this->hasOne(self::className(), [
            'id' => 'parent_id',
            'item_id' => 'item_id'
        ]);
    }

    public function getChildren() {
        return $this->hasMany(self::className(), [
            'parent_id' => 'id'
        ]);
    }

    /**
     * [getList description]
     * @return [array] common\models\MenuTree
     */
    // public function getList() {
    //     return $this->hasMany(self::className(), [
    //         'item_id' => 3
    //     ]);
    // }

    /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if($this->parent_id && !is_null($this->parent)) {
            $this->level = ++$this->parent->level;
        }

        // If the new record is set to the default position
        if ($this->isNewRecord) {
            $this->sorting = self::find()->where(['item_id' => $this->item_id])->count() + 1;
        }

        return true;
    }
}