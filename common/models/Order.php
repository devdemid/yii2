<?php

/**
 * Sales Orders
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Database structure
 *
 * @property int(11)      $id               - Unique identificator
 */
class Order extends \common\ext\db\ActiveRecord
{
    /**
     * Order statuses
     */
    const STATUS_ACCEPTED = 0;
    const STATUS_PENDING = 1;
    const STATUS_SUCCESS = 2;
    const STATUS_FAIL = 3;
    const STATUS_CANCEL = 4;

    /**
     * Payments
     */
    const PAYMENT_TYPE_ROBOKASSA = 1;

    /**
     * Date output format
     */
    const OUT_DATE_FORMAT = 'd.m.Y H:i';

    /**
     * Sorting
     */
    const ORDER_BY_SORTING = 'create_timestamp';

    /**
     * Agreement
     * @var Integer
     */
    public $agreement;

    /**
     * List of statuses
     * @return Array
     */

    public static function getStatusesList()
    {
        return [
            self::STATUS_ACCEPTED => 'Новый заказ',
            self::STATUS_SUCCESS => 'Оплата выполнена',
            self::STATUS_PENDING => 'Ожидание оплаты',
            self::STATUS_FAIL => 'Ошибка при оплате',
            self::STATUS_CANCEL => 'Отмена заказа',
        ];
    }

    /**
     * List of statuses color
     * @return Array
     */
    public static function getStatusesColor()
    {
        return [
            self::STATUS_ACCEPTED => 'bg-slate-300',
            self::STATUS_SUCCESS => 'bg-success-400',
            self::STATUS_PENDING => 'bg-grey-400',
            self::STATUS_FAIL => 'bg-danger',
            self::STATUS_CANCEL => 'bg-danger',
        ];
    }

    /**
     * List of payments
     * @return Array
     */
    public static function getPaymentList()
    {
        return [
            self::PAYMENT_TYPE_ROBOKASSA => 'RoboKassa',
        ];
    }

    /**
     * Status Name
     * @param  Integer $status
     * @return String
     */
    public static function getStatusName($status)
    {
        return ArrayHelper::getValue(static::getStatusesList(), $status, 'Unknown');
    }

    /**
     * Payment Name
     * @param  Integer $payment_type
     * @return String
     */
    public static function getPaymentName($payment_type)
    {
        return ArrayHelper::getValue(static::getPaymentList(), $payment_type, 'Unknown');
    }

    /**
     * Status color
     * @param  Integer $status
     * @return String
     */
    public static function getStatusColor($status)
    {
        return ArrayHelper::getValue(static::getStatusesColor(), $status, 'Unknown');
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%orders}}';
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return [
            ['agreement', 'required', 'on' => 'insert'],
            ['agreement', function ($attribute, $params, $validator) {
                if ($this->$attribute == false) {
                    $this->addError($attribute, 'Вы не приняли условия сайта.');
                }
            }],
            [['user_id', 'create_timestamp', 'paid_timestamp', 'status', 'agreement', 'payment_type'], 'integer'],
            ['cart', 'filter', 'filter' => 'trim'],
            ['cart', 'safe'],
            ['paid_timestamp', 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'agreement' => 'Я принимаю',
            'cart' => 'Корзина',
            'create_timestamp' => 'Время создания заказа',
            'paid_timestamp' => 'Время оплаты заказа',
            'payment_type' => 'Метод оплаты',
            'status' => 'Статус заказа',
            'user_id' => 'Идентификатор пользователя',
        ];
    }

    /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($this->getIsNewRecord()) {
            $this->user_id = Yii::$app->user->identity->id;
            $this->cart = Json::encode(array_keys(Yii::$app->cart->getRecord()));
            $this->create_timestamp = date('U');
            $this->status = self::STATUS_ACCEPTED;

            // Send message for administrator
        }

        return true;
    }

    /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }

        // Deleting order data
        OrderData::deleteAll(['item_id' => $this->id]);

        return true;
    }

    /**
     * Order data
     * @return Array
     */
    public function getOrderData()
    {
        return $this->hasMany(OrderData::className(), ['item_id' => 'id']);
    }

    /**
     * User model
     * @return \common\models\User
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * The total amount of the order
     * @return String
     */
    public function getTotalPrice()
    {
        $orderData = $this->orderData;
        $total_price = 0;

        // Total order amount calculation
        if (is_array($orderData)) {
            foreach ($orderData as $item) {
                $total_price = $total_price + ($item->adults * $item->price) + ($item->children * $item->children_price);
            }
        }

        return $total_price;
    }

    /**
     * Counting the total order amount by bp formatting
     * @return String
     */
    public function getTotalPriceFormat()
    {
        return number_format($this->totalPrice, 2, '.', ' ') . ' ' . \common\models\TourPrices::DEFAULT_CURRENCY_NAME;
    }

    /**
     * link to the page
     * @return string
     */
    public function getLinkDetail()
    {
        return '/ordering/detail/' . md5($this->id . $this->create_timestamp);
    }

    public function getLinkPay()
    {
        return '/ordering/pay/' . md5($this->id . $this->create_timestamp);
    }

    public function getLinkCancel()
    {
        return '/ordering/cancel/' . md5($this->id . $this->create_timestamp);
    }
}
