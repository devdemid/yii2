<?php

/**
 * E-MAIL newsletter subscription
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * Database structure
 *
 * @property int(11)       $id                 - Unique identificator
 * @property varchar(255)  $email              - text
 * @property varchar(255)  $name               - text
 * @property varchar(10)   $hash_salt          - text
 * @property int(15)       $create_timestamp   - text
 * @property tinyint(1)    $status             - text
 *
 */
class Subscribes extends \common\ext\db\ActiveRecord
{
    /**
     * New subscriber
     */
    const STATUS_NEW = 0;

    /**
     * Confirmed e-mail address
     */
    const STATUS_CONFIRMED = 1;

    /**
     * Disconnected from mailing
     */
    const STATUS_DISABLED = 2;

    /**
     * Number of characters in salt
     */
    const SAILT_COUNT = 10;

    /**
     * The key name for storing the subscription flag
     */
    const COOKIE_KEY_NAME = 'subscribes';

    /**
     * Messages on successful completion of the form
     * @var String
     */
    public $is_success = false;

    /**
     * Status of subscriber
     * @return Array
     */
    public static function getStatuses()
    {
        return [
            self::STATUS_NEW => 'Новый',
            self::STATUS_CONFIRMED => 'Подтвержденный',
            self::STATUS_DISABLED => 'Отключенный',
        ];
    }

    /**
     * Subscriber state name
     * @param  Integer $type
     * @return String
     */
    public static function getStatusName($type)
    {
        return ArrayHelper::getValue(static::getStatuses(), $type, 'Unknown');
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%subscribes}}';
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['name', 'email'], 'string', 'max' => '255'],
            ['email', 'email'],
            [
                'email',
                'unique',
                'targetClass' => self::className(),
                'message' => 'Ваш E-mail уже зарегистрирован!',
            ],
            [['create_timestamp', 'status'], 'integer'],

            // Default
            ['name', 'default', 'value' => ''],
            ['status', 'default', 'value' => self::STATUS_NEW],
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'create_timestamp' => 'Дата подписки',
            'email' => 'Введите ваш e-mail',
            'name' => 'Ваше имя',
            'status' => 'Статус подписчика',
            'hash_salt' => 'Соль для хеша',
        ];
    }

    /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($this->getIsNewRecord()) {
            $this->create_timestamp = date('U');
            $this->hash_salt = Yii::$app->security->generateRandomString(self::SAILT_COUNT);
        }

        return true;
    }

    /**
     * Sending a message to a subscriber
     * @param  String $text
     * @return Bollean
     */
    public function sendConfirm($text = null)
    {
        $mailer = Yii::$app->mailer
            ->compose('subscribe-confirm-html', ['confirm_link' => $this->confirmLink])
            ->setSubject('Скидка на туры!')
            ->setFrom(Yii::$app->options->get('system_email'))
            ->setTo($this->email);

        return Yii::$app->mailer->send($mailer);
    }

    /**
     * Check there is a discount on the hash
     * @param  String  $hash
     * @return Boolean
     */
    public static function isDiscount($hash = null)
    {
        // Checking the key in cookies
        if ($hash === null) {
            $cookies = Yii::$app->request->cookies;

            if (($cookie = $cookies->get(self::COOKIE_KEY_NAME)) !== null) {
                $hash = is_bool($cookie->value) ? null : $cookie->value;
            }
        }

        // Checking the flag in a session or in a database
        if ($hash !== null) {
            $session = Yii::$app->session;

            if ($session->has(self::COOKIE_KEY_NAME)) {
                return $session->get(self::COOKIE_KEY_NAME);
            } else {
                $return = (boolean) self::findBySql('
                    SELECT *
                    FROM ' . self::getTableSchema()->fullName . '
                    WHERE MD5(CONCAT(`create_timestamp`,`email`)) = :hash AND status = :status
                    LIMIT 1',
                    [
                        ':hash' => $hash,
                        ':status' => self::STATUS_CONFIRMED,
                    ]
                )->count();

                // Save the result to the session
                if ($return) {
                    $session->set(self::COOKIE_KEY_NAME, $return);
                }

                return $return;
            }
        }

        return false;
    }

    /**
     * Clear all subscription labels
     */
    public static function clearData() {
        // If there are entries in the session, then we delete
        $session = Yii::$app->session;
        if ($session->has(self::COOKIE_KEY_NAME)) {
            $session->remove(self::COOKIE_KEY_NAME);
        }

        // If there are entries in the cookies then delete
        $cookies = Yii::$app->request->cookies;
        if ($cookies->has(self::COOKIE_KEY_NAME)) {
            $cookies->set(self::COOKIE_KEY_NAME, true);
        }
    }

    /**
     * The key for getting a discount
     * @return String
     */
    public function getHash()
    {
        return md5($this->create_timestamp . $this->email);
    }

    /**
     * Disconnect link from mailing list
     * @return String
     */
    public function getUnsubscribeLink()
    {
        return '//' . Yii::$app->request->serverName . '/subscribes/unsubscribe/' .
        md5(self::STATUS_CONFIRMED . $this->hash_salt);
    }

    /**
     * Link for confirmation email
     * @return String
     */
    public function getConfirmLink()
    {
        return '//' . Yii::$app->request->serverName . '/subscribes/confirm/' .
        md5(self::STATUS_NEW . $this->hash_salt);
    }
}
