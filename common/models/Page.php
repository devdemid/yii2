<?php
/**
 * Static pages
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace common\models;

use common\ext\db\ActiveRecord;
use common\ext\validators\TranslitFilter;
use \yii\helpers\StringHelper;

/**
 * Database structure
 *
 * @property int(11)      $id                 - Unique identificator
 * @property varchar(255) $name               - text
 * @property varchar(255) $small_description  - text
 * @property varchar(255) $alias              - text
 * @property text         $full_description   - text
 * @property varchar(200) $meta_description   - text
 * @property varchar(250) $meta_keywords      - text
 * @property varchar(100) $view               - text
 * @property tinyint(1)   $is_wrapper         - text
 * @property tinyint(1)   $noindex            - text
 * @property tinyint(1)   $published          - text
 */
class Page extends ActiveRecord {

    /**
     * Active recording
     */
    const PUBLISHED_ON = 1;

    /**
     * Inactive recording
     */
    const PUBLISHED_OFF = 0;

    /**
     * Enable indexing
     */
    const NOINDEX_ON = 1;

    /**
     * Disable indexing
     */
    const NOINDEX_OFF = 0;

    /**
     * Enable template
     */
    const IS_WRAPPER_ON = 1;

    /**
     * Disable tamplate
     */
    const IS_WRAPPER_OFF = 0;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%page}}';
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules() {
        return [
            [['name', 'full_description'], 'required'],
            [
                'alias',
                'unique',
                'targetClass' => self::className(),
                'message' => '"{attribute}" уже используеться в другой странице',
            ],
            [['name', 'small_description', 'alias'], 'string', 'max' => '255'],
            ['view', 'string', 'max' => '100'],
            ['meta_description', 'string', 'max' => '200'],
            ['meta_keywords', 'string', 'max' => '250'],
            ['full_description', 'string', 'max' => '65535'],
            [['published', 'noindex', 'is_wrapper'], 'integer'],
            [['name', 'alias', 'meta_description', 'meta_keywords', 'small_description'], 'trim'],
            ['alias', TranslitFilter::className(), 'translitAttribute' => 'name'],

            [['meta_description', 'meta_keywords'], 'filter', 'filter' => function ($value) {
                return \yii\helpers\Html::encode($value);
            }]
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels() {
        return [
            'alias' => 'Алиас',
            'full_description' => 'Полное описание',
            'is_wrapper' => 'Включить wrapper',
            'meta_description' => 'Описание (Description)',
            'meta_keywords' => 'Ключевые слова (Keywords)',
            'name' => 'Название страницы',
            'noindex' => 'Запретить индексацию',
            'published' => 'Публикация страницы',
            'small_description' => 'Короткое описания страницы',
            'view' => 'Шаблон страницы',
        ];
    }

    /**
     * Link Reference Code
     * @return string
     */
    public function getLinkcode() {
        return '{linkcode="' . StringHelper::basename(self::className()) . '" id="' . $this->id . '"}';
    }

    /**
     * link to the page
     * @return string
     */
    public function getLink() {
        return '/' . $this->alias . '.html';
    }
}