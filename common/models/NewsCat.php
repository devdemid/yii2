<?php
/**
 * Module for news categories
 *
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace common\models;

use common\ext\db\ActiveRecord;
use common\ext\validators\TranslitFilter;
use Yii;

/**
 * Database structure
 *
 * @property int(11)      $id                 - Unique identificator
 * @property int(11)      $item_id            - text
 * @property varchar(255) $cat_id             = text
 * @property varchar(255) $name               - text
 * @property varchar(255) $alias              - text
 * @property text         $short_description  - text
 * @property text         $full_description   - text
 * @property varchar(200) $meta_description   - text
 * @property varchar(250) $meta_keywords      - text
 * @property varchar(19)  $date               - text
 * @property int(11)      $sorting            - text
 * @property tinyint(1)   $published          - text
 * @property int(11)      $views              - text
 * @property int(11)      $fixed              - text
 * @property int(11)      $noindex            - text
 */
class NewsCat extends ActiveRecord
{

    /**
     * Active recording
     */
    const PUBLISHED_ON = 1;

    /**
     * Inactive recording
     */
    const PUBLISHED_OFF = 0;

    /**
     * Sort news category
     */
    const ORDER_BY_SORTING = 'sorting';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news_cat}}';
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['item_id', 'sorting', 'published'], 'integer'],
            [
                'alias',
                'unique',
                'targetClass' => self::className(),
                'message' => '"{attribute}" уже используеться в другой категории',
            ],
            [['name', 'alias'], 'string', 'max' => '255'],
            ['meta_description', 'string', 'max' => '200'],
            ['meta_keywords', 'string', 'max' => '250'],
            [['name', 'alias', 'meta_description', 'meta_keywords'], 'trim'],
            ['alias', TranslitFilter::className(), 'translitAttribute' => 'name'],
            [['meta_description', 'meta_keywords'], 'filter', 'filter' => function ($value) {
                return \yii\helpers\Html::encode($value);
            }],

            // Default value
            [['item_id', 'sorting'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'alias' => 'Алиас',
            'meta_description' => 'Описание (Description)',
            'meta_keywords' => 'Ключевые слова (Keywords)',
            'name' => 'Название категории',
            'published' => 'Публикация категории',
            'sorting' => 'Позиция в списке',
        ];
    }

    /**
     * Get news feed module
     * @return common\models\NewsFeed
     */
    public function getFeedModel()
    {
        return $this->hasOne(NewsFeed::className(), ['id' => 'item_id']);
    }

    /**
     * link to the page
     * @return string
     */
    public function getLink()
    {
        return $this->feedModel->link . '/' . $this->alias;
    }
}
