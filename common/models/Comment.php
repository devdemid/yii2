<?php
/**
 * Comments
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace common\models;

use common\ext\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Database structure
 *
 * @property int(11)      $id                 - Unique identificator
 * @property int(11)      $item_id            - text
 * @property int(11)      $user_id            - text
 * @property varchar(100) $module             - text
 * @property text         $content            - text
 * @property text         $data               - text
 * @property varchar(19)  $date               - text
 * @property tinyint(1)   $moderation         - text
 * @property tinyint(1)   $published          - text
 */
class Comment extends ActiveRecord
{
    /**
     * Active recording
     */
    const PUBLISHED_ON = 1;

    /**
     * Inactive recording
     */
    const PUBLISHED_OFF = 0;

    /**
     * Active moderation
     */
    const MODERATION_ON = 1;

    /**
     * Inactive moderation
     */
    const MODERATION_OFF = 0;

    /**
     * Date format in the database
     */
    const PROP_DATE_FORMAT = 'Y-m-d H:i:s';

    /**
     * Date output format
     */
    const OUT_DATE_FORMAT = 'm.d.Y H:i:s';

    /**
     * Sort channels field
     */
    const ORDER_BY_SORTING = 'date';

    /**
     * Date in UNIX TIMESTAMP format
     * @var integer
     */
    public $date_timestamp;

    /**
     * Information from the guest
     * @var Array
     */
    private $json_data;

    /**
     * Guest name
     * @var String
     */
    public $name;

    /**
     * Guest email
     * @var String
     */
    public $email;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%comments}}';
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules() {
        return [
            [['module', 'item_id'], 'required'],
            ['module', 'string', 'max' => '255'],
            [['content', 'data'], 'string', 'max' => '65535'],
            [['content', 'data'], 'trim'],
            ['date', 'default', 'value' => date(self::PROP_DATE_FORMAT)],
            ['data', 'safe'],
            ['email', 'email'],

            // Filers
            ['name', 'filter', 'filter' => function ($value) {
                return \yii\helpers\Html::encode($value);
            }],

            [['item_id', 'user_id', 'moderation', 'published'], 'integer'],
            [['user_id', 'moderation'], 'default', 'value' => 0],
            ['published', 'default', 'value' => self::PUBLISHED_ON]
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels() {
        return [
            'content' => 'Комментарий',
            'email' => 'E-Mail гостя',
            'item_id' => 'Идентификатор записи',
            'moderation' => 'Модерация',
            'module' => 'Модуль',
            'name' => 'Имя гостя',
            'published' => 'Публикация',
            'user_id' => 'Идентификатор пользователя',
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterFind() {
        $this->date_timestamp = strtotime($this->date);

        if ($this->data) {
            $this->json_data = Json::decode($this->data);
            $this->name = $this->json_data['name'];
            $this->email = $this->json_data['email'];
        }
    }

    /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($this->name || $this->email) {
            $this->data = Json::encode([
                'name' => $this->name,
                'email' => $this->email
            ]);
        }

        return true;
    }

    /**
     * The first character of the name string
     * @return String
     */
    public function getSymbolName() {
        if($this->data) {
            return mb_substr($this->name, 0, 1);
        }
        return '-';
    }

    /**
     * Get user object
     * @return common\models\User;
     */
    public function getUser() {
        return $this->hasOne(User::classname(), ['id' => 'user_id']);
    }
}
