<?php
/**
 * FAQ - Questions and answers
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace common\models;

use common\ext\db\ActiveRecord;
use common\ext\validators\TranslitFilter;

/**
 * Database structure
 *
 * @property int(11)      $id          - Unique identificator
 * @property int(11)      $item_id     - text
 * @property varchar(255) $question    - text
 * @property varchar(255) $alias       - text
 * @property text         $answer      - text
 * @property int(11)      $sorting     - text
 * @property tinyint(1)   $published   - text
 */
class FaqAnswers extends ActiveRecord
{

    /**
     * Active recording
     */
    const PUBLISHED_ON = 1;

    /**
     * Inactive recording
     */
    const PUBLISHED_OFF = 0;

    /**
     * Sorting questions
     */
    const ORDER_BY_SORTING = 'sorting';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%faq_answers}}';
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return [
            ['question', 'required'],
            ['question', 'string', 'min' => '1', 'max' => '255'],
            ['answer', 'string', 'max' => '65535'],
            ['alias', TranslitFilter::className(), 'translitAttribute' => 'question'],
            [['question', 'answer'], 'filter', 'filter' => 'trim'],
            [['published', 'item_id'], 'integer'],
            ['sorting', 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'answer' => 'Ответ на вопрос',
            'published' => 'Публикация вопроса',
            'question' => 'Вопрос',
        ];
    }

    /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($this->isNewRecord) {
            $this->sorting = self::find()->where(['item_id' => $this->id])->count();
        }

        return true;
    }

    /**
     * Hash for identifier
     * @return [string]
     */
    public function getHashLink()
    {
        return hash('crc32b', $this->item_id . $this->id);
    }

    /**
     * Answer branch model
     * @return common\models\Faq;
     */
    public function getFaq()
    {
        return $this->hasOne(Faq::classname(), ['id' => 'item_id']);
    }

    /**
     * Link to answer page
     * @return string
     */
    public function getLink()
    {
        return $this->faq->link . '/' . $this->id . '-' . $this->alias .'.html';
    }
}
