<?php
/**
 * Module for generating news feeds
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace common\models;

use backend\models\Field;
use common\ext\db\ActiveRecord;
use common\ext\validators\TranslitFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use \yii\helpers\StringHelper;

/**
 * Database structure
 *
 * @property int(11)      $id                - Unique identificator
 * @property varchar(255) $name              - text
 * @property varchar(255) $alias             - text
 * @property text         $content           - text
 * @property varchar(200) $meta_description  - text
 * @property varchar(250) $meta_keywords     - text
 * @property text         $field_options     - text
 * @property varchar(255) $img_sizes         - text
 * @property varchar(100) $view              - text
 * @property varchar(100) $view_index        - text
 * @property varchar(100) $view_detail       - text
 * @property tinyint(1)   $img_enable        - text
 * @property tinyint(1)   $is_count_views    - text
 * @property tinyint(1)   $is_date_published - text
 * @property tinyint(1)   $enable_comments   - text
 * @property tinyint(3)   $page_size         - text
 * @property tinyint(1)   $published         - text
 */
class NewsFeed extends ActiveRecord
{

    /**
     * Active recording
     */
    const PUBLISHED_ON = 1;

    /**
     * Inactive recording
     */
    const PUBLISHED_OFF = 0;

    /**
     * Active images
     */
    const IMAGE_ON = 1;

    /**
     * Inactive images
     */
    const IMAGE_OFF = 0;

    /**
     * Limit for the default widget
     */
    const DEFAULT_SHORTCODE_LIMIT = 10;

    /**
     * Default image size
     */
    const DEFAULT_SIZE = '100x100';

    /**
     * Default page size
     */
    const DEFAULT_PAGE_SIZE = 5;

    /**
     * Checking the template name
     */
    const VIEW_PATTERN = '/^([a-z0-9\-\_]{1,20})$/i';

    /**
     * Template file prefix
     */
    const VIEW_PREFIX = '-ext';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news_feed}}';
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            [
                'alias',
                'unique',
                'targetClass' => self::className(),
                'message' => '"{attribute}" уже используеться в другой ленте',
            ],
            [['name', 'alias', 'img_sizes'], 'string', 'max' => '255'],
            ['meta_description', 'string', 'max' => '200'],
            ['meta_keywords', 'string', 'max' => '250'],
            [['content', 'field_options'], 'string', 'max' => '65535'],
            [['img_enable', 'is_count_views', 'is_date_published', 'enable_comments', 'published'], 'integer'],
            ['alias', TranslitFilter::className(), 'translitAttribute' => 'name'],
            [['field_options'], 'safe'],
            [['name', 'alias', 'meta_description', 'meta_keywords', 'view', 'view_index', 'view_detail', 'img_sizes'], 'trim'],
            [
                'img_sizes',
                'match',
                'pattern' => Files::SIZES_PATTERN,
                'message' => '{attribute} - Неверный формат данных',
            ],
            [
                'view',
                'match',
                'pattern' => self::VIEW_PATTERN,
                'message' => '{attribute} - Недопустимые символы для имени шаблона',
            ],

            // Removing unnecessary delimiters
            ['img_sizes', function ($attribute, $params, $validator) {
                $this->$attribute = trim($this->$attribute, Files::SIZES_LIST_SEPARATOR);
            }],

            [['meta_description', 'meta_keywords'], 'filter', 'filter' => function ($value) {
                return \yii\helpers\Html::encode($value);
            }],

            // Checking the existence of a template
            // ['view', function ($attribute, $params, $validator) {
            //     $viewFullPatch  = Yii::getAlias('@frontend/views/' . Yii::$app->controller->id .'/');
            //     $viewFullPatch .= $this->$attribute . self::VIEW_PREFIX . '.php';

            //     if (!file_exists($viewFullPatch))
            //         $this->addError($attribute, "Не удалось найти шаблон, необходимо создать файл\n " . $viewFullPatch);
            // }],

            // Page size
            ['page_size', 'integer', 'min' => 1, 'max' => 127],
            ['page_size', 'default', 'value' => self::DEFAULT_PAGE_SIZE],

            [['is_count_views', 'is_date_published', 'enable_comments'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'alias' => 'Алиас',
            'content' => 'Краткое описание ленты',
            'enable_comments' => 'Комментарии',
            'field_options' => 'Дополнительные поля',
            'img_enable' => 'Включить изображения',
            'img_sizes' => 'Размеры изображений',
            'is_count_views' => 'Счетчик просмотров',
            'is_date_published' => 'Дата публикации',
            'meta_description' => 'Описание (Description)',
            'meta_keywords' => 'Ключевые слова (Keywords)',
            'name' => 'Название новостной ленты',
            'page_size' => 'Размер страницы',
            'published' => 'Публикация новостной ленты',
            'view' => 'Шаблон для {shortcode}',
            'view_detail' => 'Шаблон страницы',
            'view_index' => 'Шаблон списка записей',
        ];
    }

    /**
     * Number of news in the stream
     * @return integer
     */
    public function getCountNews()
    {
        return $this->hasMany(News::className(), ['item_id' => 'id'])->count();
    }

    /**
     * All news from this tape
     * @param  integer $limit
     * @return array
     */
    public function getNews($limit = 3)
    {
        return $this->hasMany(News::className(), ['item_id' => 'id'])
            ->where(['published' => News::PUBLISHED_ON])
            ->orderBy([
                News::ORDER_BY_FIXED => SORT_DESC,
                News::ORDER_BY_SORTING => SORT_ASC,
            ])
            ->limit($limit)
            ->all();
    }

    /**
     * Number of groups in this news line
     * @return Integer
     */
    public function getCountTagGroup() {
        return $this->hasMany(TagGroup::className(), [
                'item_id' => 'id'
            ])
            ->where(['module' => StringHelper::basename(self::className())])
            ->count();
    }

    /**
     * List of group tags for this entry
     * @return Array
     */
    public function getTagGroup() {
        return $this->hasMany(TagGroup::className(), [
                'item_id' => 'id'
            ])
            ->where(['module' => StringHelper::basename(self::className())])
            ->indexBy('id')
            ->all();
    }

    /**
     * Number of additional fields
     * @return integer
     */
    public function getFieldCount()
    {
        return count(Json::decode($this->field_options));
    }

    /**
     * News Feed categories
     * @return Array
     */
    public function getCategories()
    {
        return $this->hasMany(NewsCat::className(), ['item_id' => 'id'])
            ->where(['published' => NewsCat::PUBLISHED_ON])
            ->orderBy([
                NewsCat::ORDER_BY_SORTING => SORT_ASC,
            ])
            ->all();
    }

    /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        // When creating a new record, the settings field must always be an empty array
        if ($this->getIsNewRecord()) {
            $this->field_options = Field::DEFAULT_NEW_VALUE;
        }

        return true;
    }

    /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }

        // Delete all news from this feed
        News::deleteAll(['item_id' => $this->id]);

        // Delete all tag groups tied to the news feed
        TagGroup::deleteAll([
            'item_id' => $this->id,
            'module' => StringHelper::basename(self::className())
        ]);

        return true;
    }

    /**
     * Additional template name
     * @return string
     */
    public function getViewsExt()
    {
        return $this->view . self::VIEW_PREFIX;
    }

    /**
     * An array of the right size for images
     * @return array
     */
    public function getImgSizes()
    {
        if ($this->img_sizes) {
            $sizes = explode(Files::SIZES_LIST_SEPARATOR, $this->img_sizes);
        } else {
            $sizes = [];
        }

        return array_unique(ArrayHelper::merge($sizes, [self::DEFAULT_SIZE]));
    }

    /**
     * Image size in line format (Advanced)
     * @return string
     */
    public function getImgSizeString()
    {
        return implode(Files::SIZES_LIST_SEPARATOR, $this->imgSizes);
    }

    /**
     * Check if additional fields are available
     * @return boolean
     */
    public function getIsFields()
    {
        return $this->field_options == Field::DEFAULT_NEW_VALUE ? false : true;
    }

    /**
     * Fields array
     * @return Array
     */
    public function getFieldOptionsList() {
        if($this->isFields) {
            $field_options = [];
            $_field_options = Json::decode($this->field_options);
            foreach ($_field_options as $item) {
                if($item['type'] === Field::TYPE_SELECT) {
                    $field_options[$item['param']] = @explode(Field::LIST_SEPARATOR, $item['list']);
                }
            }
            return $field_options;
        }
        return [];
    }

    /**
     * News Feed shortcode
     * @return string
     */
    public function getShortcode()
    {
        $shortcode = '{shortcode="' . StringHelper::basename(self::className());
        $shortcode .= '" id="' . $this->id . '" limit="' . self::DEFAULT_SHORTCODE_LIMIT;

        // Block Template
        if ($this->view) {
            $shortcode .= '" view="' . $this->viewsExt;
        }

        $shortcode .= '"}';

        return $shortcode;
    }

    /**
     * Link Reference Code
     * @return string
     */
    public function getLinkcode()
    {
        return '{linkcode="' . StringHelper::basename(self::className()) . '" id="' . $this->id . '"}';
    }

    /**
     * link to the page
     * @return string
     */
    public function getLink()
    {
        return '/news/feed/' . $this->alias;
    }
}
