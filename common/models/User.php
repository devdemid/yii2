<?php
/**
 * User account data model
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use common\ext\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;
use \yii\helpers\StringHelper;

/**
 * User model
 *
 * @property integer      $id                     - Unique identificator
 * @property varchar      $username               - text
 * @property varchar      $first_name             - text
 * @property varchar      $second_name            - text
 * @property integer      $sex                    - text
 * @property varchar      $auth_key               - text
 * @property varchar      $password_hash          - text
 * @property varchar      $password_reset_token   - text
 * @property varchar      $email                  - text
 * @property smallint     $status                 - text
 * @property varchar      $group                  - text
 * @property varchar(100) $country                - text
 * @property varchar(100) $city                   - text
 * @property varchar(255) $address                - text
 * @property int(10)      $index                  - text
 * @property tinyint(1)   $notification_new_tours - text
 * @property tinyint(1)   $notification_comp_news - text
 * @property varchar      $last_auth_ip           - text
 * @property integer      $created_timestamp      - text
 * @property integer      $updated_timestamp      - text
 * @property integer      $auth_timestamp         - text
 */
class User extends ActiveRecord implements IdentityInterface {

    /**
     * Name group in which user
     * @var string
     */
    public $groupName;

    /**
     * Name together
     * @var ыекштп
     */
    public $nameTogether;

    // public $create_timestamp;
    // public $updated_timestamp;

    /*
    * User statuses
    */
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_BLOCKED = 2;
    const STATUS_WAIT = 3;

    /**
     * Affiliation to sex
     */
    const SEX_NOT_SPECIFIED = 0;
    const SEX_MENS = 1;
    const SEX_WOMEN = 2;

    const DEFAULT_USER_GROUP = 'client';

    public $image_sizes = [
        '300x300'
    ];

    /**
     * Verify phone number (RUSSIA)
     *
     * ======================================================
     * ^\+?[78][-\(]?\d{3}\)?-?\d{3}-?\d{2}-?\d{2}$
     *
     * +7(903)888-88-88
     * 8(999)99-999-99
     * +380(67)777-7-777
     * 001-541-754-3010
     * +1-541-754-3010
     * 19-49-89-636-48018
     * +233 205599853
     * ======================================================
     */
    const PATTERN_PHONE = '/^((8|\+7)[\-\s]?)?(\(?\d{3}\)?[\-\s]?)?[\d\-\s]{7,10}$/i';

    /**
     * Calling events when the model is initialized
     */
    public function init() {
        parent::init();
        Yii::$app->user->on(Yii\web\User::EVENT_AFTER_LOGIN, [$this, 'accountStatistics']);
        Yii::$app->user->on(Yii\web\User::EVENT_BEFORE_LOGOUT, [$this, 'accountStatistics']);
    }

    /**
     * Updating data in the model database
     */
    public function accountStatistics() {
        $this->auth_timestamp = date('U');
        $this->last_auth_ip = Yii::$app->request->getUserIP();
        $this->save(false);
    }

    /**
     * List status
     * @return array
     */
    public static function getStatusesList() {
        return [
            self::STATUS_DELETED => 'Удалено',
            self::STATUS_ACTIVE => 'Активный',
            self::STATUS_BLOCKED => 'Заблокирован',
            self::STATUS_WAIT => 'Не активирован'
        ];
    }

    /**
     * Colour status
     * @return array
     */
    public static function getStatusesLabel() {
        return [
            self::STATUS_DELETED => 'label-default',
            self::STATUS_ACTIVE => 'label-success',
            self::STATUS_BLOCKED => 'label-danger',
            self::STATUS_WAIT => 'label-info'
        ];
    }

    /**
     * Sex list
     * @return array
     */
    public static function getSexList() {
        return [
            self::SEX_NOT_SPECIFIED => 'Не указан',
            self::SEX_MENS => 'Мужской',
            self::SEX_WOMEN => 'Женский'
        ];
    }

    /**
     * Status name
     * @return string
     */
    public function getStatusName() {
        return self::getStatusesList()[$this->status];
    }

    /**
     * Status label color
     * @return string
     */
    public function getStatusLabel() {
        return self::getStatusesLabel()[$this->status];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%users}}';
    }

    /**
     * @inheritdoc
     */
    // public function behaviors() {
    //     return [
    //         [
    //             'class' => \yii\behaviors\TimestampBehavior::className(),
    //             'createdAtAttribute' => 'create_timestamp',
    //             'updatedAtAttribute' => 'updated_timestamp',
    //             'value' => function() {
    //                 return date('U');
    //             },
    //         ],
    //     ];
    // }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['username', 'first_name', 'second_name', 'country', 'city', 'address'], 'string', 'length' => [3, 20]],
            [['email', 'first_name', 'second_name'], 'required'],
            [
                ['username', 'email'],
                'unique',
                'targetClass' => self::className(),
                'message' => 'Имя или Email "{attribute}" уже зарегистрировано'
            ],
            ['sex', 'in', 'range' => [
                self::SEX_NOT_SPECIFIED,
                self::SEX_MENS,
                self::SEX_WOMEN
            ]],
            [
                'phone',
                'match',
                'pattern' => self::PATTERN_PHONE,
                'message' => '"{attribute}" неверный номер',
            ],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'integer', 'min' => self::STATUS_DELETED, 'max' => self::STATUS_WAIT],
            [['created_timestamp', 'updated_timestamp', 'auth_timestamp', 'index', 'notification_new_tours', 'notification_comp_news'], 'integer'],
            [['username', 'email', 'first_name', 'second_name'], 'filter', 'filter' => 'trim'],
            ['last_auth_ip', 'ip'],
            ['group', 'default', 'value' => self::DEFAULT_USER_GROUP],
            [['country', 'city', 'address'], 'filter', 'filter' => function ($value) {
                return \yii\helpers\Html::encode($value);
            }],

            ['index', 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels() {
        return [
            'username' => 'Логин',
            'first_name' => 'Имя',
            'second_name' => 'Фамилия',
            'sex' => 'Пол',
            'email' => 'E-Mail',
            'phone' => 'Номер телефона',
            'status' => 'Статус аккаунта',
            'group' => 'Группа',
            'last_auth_ip' => 'Последный IP авторизации',
            'created_timestamp' => 'Дата регистрации',
            'updated_timestamp' => 'Дата редактирования',
            'auth_timestamp' => 'Последний визит',
            'country' => 'Страна',
            'city' => 'Город',
            'address' => 'Адрес',
            'index' => 'Индекс',
            'notification_new_tours' => 'Уведомления о новых турах',
            'notification_comp_news' => 'Новости  компании',
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterFind() {
        // Define user group
        $rbacRoles = Yii::$app->getAuthManager()->getRoles();
        $this->groupName = ArrayHelper::getValue($rbacRoles, $this->group . '.description');

        // Name together
        $this->nameTogether = $this->first_name . ' ' . $this->second_name;

        if(!$this->index) {
            $this->index = '';
        }

        parent::afterFind();
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if($this->isNewRecord) {
                $this->generatePasswordResetToken();
                $this->generateAuthKey();
                $this->created_timestamp = date('U');
            }

            $this->updated_timestamp = date('U');
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id) {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username) {
        return static::findOne(['username' => $username]); // , 'status' => self::STATUS_ACTIVE
    }

    /**
     * Finds user by email
     *
     * @param string $username
     * @return static|null
     */
    public static function findByEmail($email) {
        return static::findOne(['email' => $email]); // , 'status' => self::STATUS_ACTIVE
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token) {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token) {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password) {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Validates email
     *
     * @param string $email email to validate
     * @return boolean if email provided is valid for current user
     */
    public function validateEmail($email) {
        return $this->email === $email;
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password) {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Set email the model
     *
     * @param string $email
     */
    public function setEmail($email) {
        $this->email = $email;
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey() {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken() {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken() {
        $this->password_reset_token = null;
    }

    /**
     * List of all groups
     * @return array
     */
    public function getGroupList() {
        return ArrayHelper::map(Yii::$app->getAuthManager()->getRoles(), 'name', 'description');
    }

    /**
     * Default Images
     * @return [string]
     */
    public function getPathLogo() {
        if (count($this->files)) {
            return $this->files[0]->getImageSize('300x300');
        }
        return '';
    }

    /**
     * All related files
     * @return array
     */
    protected function getFiles() {
        return $this->hasMany(Files::className(), ['item_id' => 'id'])->andOnCondition([
            'module' => StringHelper::basename(self::className()),
        ])->select(['id', 'item_id', 'file', 'module', 'sizes']);
    }

    /**
     * Prewiev Widget files
     * @return array
     */
    public function getPrewiewsWidget() {
        $files = [];
        foreach ($this->files as $item) {
            $files[] = $item->getPrewiews();
        }
        return $files;
    }

    /**
     * Full path to the application directory
     * @return string
     */
    public function getFullPatch() {
        return Yii::getAlias('@upload') . '/' . mb_strtolower(StringHelper::basename(self::className())) . '/';
    }

    /**
     * The first character of the first_name string
     * @return String
     */
    public function getSymbolName() {
        return mb_substr($this->first_name, 0, 1);
    }

    /**
     * link to the page
     * @return string
     */
    public function getProfileLink()
    {
        return '/users/profile/' . $this->id;
    }

    // Резервные коды для двухэтапной авторизации
    //
    // const CHR_RESERVE_CODES = '0123456789abcdefghijklmnopqrstuvwxyz';
    //
    // public function generateReserveCodes($count = 15, $length = 8) {
    //     for($i = 1; $i <= $count; $i++) {
    //         $code = substr(str_shuffle(str_repeat(self::CHR_RESERVE_CODES, $length)), 0, $length);
    //         $reserve_codes[$code] = md5(md5($code, true));
    //     }
    //     $this->reserve_codes = '';
    //     var_dump($reserve_codes); exit;
    // }
}
