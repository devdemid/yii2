<?php

/**
 * Tour Prices
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace common\models;

use common\ext\db\ActiveRecord;

/**
 * Database structure
 *
 * @property int(11)       $id                 - Unique identificator
 * @property int(11)       $item_id            - text
 * @property decimal(10,2) $base_price         - text
 * @property decimal(10,2) $discount_price     - text
 * @property decimal(10,2) $children_price     - text
 * @property date          $start_date         - text
 * @property date          $end_date           - text
 * @property text          $short_description  - text
 * @property tinyint(1)    $published          - text
 *
 */
class TourPrices extends ActiveRecord
{

    /**
     * Active recording
     */
    const PUBLISHED_ON = 1;

    /**
     * Inactive recording
     */
    const PUBLISHED_OFF = 0;

    /**
     * Date format in the database
     */
    const PROP_DATE_FORMAT = 'Y-m-d';

    /**
     * Date output format
     */
    const OUT_DATE_FORMAT = 'd.m.Y';

    /**
     * Default currency name
     */
    const DEFAULT_CURRENCY_NAME = ' ₽'; // &#8381 | ₽ | руб.

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tour_prices}}';
    }

    /**
     * Dates in UNIX TIMESTAMP
     * @var Integer
     */
    public $start_date_timestamp;
    public $end_date_timestamp;

    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return [
            [
                ['item_id', 'base_price', 'discount_price', 'start_date', 'end_date'], 'required'
            ],
            [['item_id', 'published'], 'integer'],
            [['base_price', 'discount_price'], 'number'],
            [['start_date', 'end_date'], 'date', 'format' => 'php:' . self::PROP_DATE_FORMAT],
            ['short_description', 'safe'],

            // Default prices
            [['base_price', 'discount_price', 'children_price'], 'default', 'value' => '0.00'],

            // Check date interval
            ['start_date', function($attribute) {
                $start_date  = strtotime('+1 second', strtotime($this->start_date));
                if($start_date < strtotime(date('Y-m-d'))) {
                    $this->addError($attribute, 'Дата не может быть меньшей от текущей даты');
                }
            }],

            ['end_date', function($attribute) {
                if(strtotime($this->end_date) < strtotime($this->start_date)) {
                    $this->addError($attribute, 'Дата не может быть меньшой от начальной даты.');
                }
            }],
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'base_price' => 'Цена',
            'discount_price' => 'Цена со скидкой',
            'end_date' => 'Окончание',
            'item_id' => 'Идентификатор тура',
            'short_description' => 'Краткое описание',
            'start_date' => 'Начало',
            'published' => 'Публикация',
            'children_price' => 'Цена для ребенка',
        ];
    }

    /**
     * [getPrice description]
     * @return String
     */
    public function getPrice($format = true) {
        if(!Subscribes::isDiscount()) {
            $price = $this->base_price;
        } else {
            $price = $this->discount_price;
        }

        if(!$format) {
            return $price;
        }

        return number_format($price, 0, '.', ' ') . ' ' . self::DEFAULT_CURRENCY_NAME;
    }

    public function getDiscountPrice() {
        return number_format($this->discount_price, 0, '.', ' ') . ' ' . self::DEFAULT_CURRENCY_NAME;
    }

    public function getChildrenPrice() {
        return number_format($this->children_price, 0, '.', ' ') . ' ' . self::DEFAULT_CURRENCY_NAME;
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        if ($this->start_date) {
            $this->start_date_timestamp = strtotime($this->start_date);
        }

        if ($this->end_date) {
            $this->end_date_timestamp = strtotime($this->end_date);
        }

        parent::afterFind();
    }

    /**
     * Tour model
     * @return common\models\Tours;
     */
    public function getTour()
    {
        return $this->hasOne(Tours::className(), ['id' => 'item_id']);
    }
}
