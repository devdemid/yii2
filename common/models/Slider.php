<?php
/**
 * Slider Designer
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace common\models;

use backend\models\Field;
use common\ext\db\ActiveRecord;
use common\models\Slide;
use \yii\helpers\Json;
use \yii\helpers\StringHelper;

/**
 * Database structure
 *
 * @property int(11)      $id        - Unique identificator
 * @property varchar(100) $name      - text
 * @property text         $field_options    - text
 * @property int(5)       $width     - text
 * @property int(5)       $height    - text
 * @property tinyint(1)   $autoplay  - text
 * @property tinyint(1)   $published - text
 */
class Slider extends ActiveRecord {

    /**
     * Active recording
     */
    const PUBLISHED_ON = 1;

    /**
     * Inactive recording
     */
    const PUBLISHED_OFF = 0;

    /**
     * Image preview size
     */
    const IMAGE_SMALL_SIZE = '135x70';

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%slider}}';
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules() {
        return [
            [['name', 'width', 'height'], 'required'],
            [['width', 'height', 'autoplay', 'published'], 'integer'],
            ['field_options', 'string', 'max' => '65535'],
            ['field_options', 'safe'],
            ['name', 'trim']
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels() {
        return [
            'name' => 'Название слайдера',
            'autoplay' => 'Автовоспроизведение',
            'width' => 'Ширина',
            'height' => 'Высота',
            'published' => 'Публикация слайдера'
        ];
    }

    /**
     * Number of additional fields
     * @return integer
     */
    public function getFieldCount() {
        return count(Json::decode($this->field_options));
    }

    /**
     * Slider size
     * @return string
     */
    public function getSize() {
        return @implode('x', [$this->width, $this->height]);
    }

    /**
     * Slider widget code
     * @return string
     */
    public function getShortcode() {
        return '{shortcode="' . StringHelper::basename(self::className()) . '" id="' . $this->id . '"}';
    }

    /**
     * Slides
     * @return integer
     */
    public function getCountSlides() {
        return $this->hasMany(Slide::className(), ['item_id' => 'id'])->count();
    }

    /**
     * Slider settings
     * @return Json:string
     */
    public function getSettings() {
        return Json::encode([
            'autoplay' => ($this->autoplay ? 'true' : 'false'),
            'size' => $this->size,
        ]);
    }

    /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        // When creating a new record, the settings field must always be an empty array
        if ($this->getIsNewRecord()) {
            $this->field_options = Field::DEFAULT_NEW_VALUE;
        }

        return true;
    }

    /**
     * Delete slides from the database
     * @return [boolean]
     */
    public function beforeDelete() {
        if (parent::beforeDelete()) {
            // Remove slide
            //Slide::deleteAll(['item_id' => $this->id]);
            foreach (Slide::find()->where(['item_id' => $this->id])->all() as $model) {
                $model->delete();
            }
            return true;
        }
        return false;
    }

    /**
     * Check if additional fields are available
     * @return boolean
     */
    public function getIsFields() {
        return $this->field_options == Field::DEFAULT_NEW_VALUE ? false : true;
    }
}