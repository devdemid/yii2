<?php
/**
 * Module for generating news
 *
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 *
 * @note Example of obtaining data: $model->fieldValue('name')
 */

namespace common\models;

use backend\models\Field;
use common\ext\db\ActiveRecord;
use common\ext\validators\TranslitFilter;
use common\models\Files;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use \yii\helpers\StringHelper;

/**
 * Database structure
 *
 * @property int(11)      $id                 - Unique identificator
 * @property int(11)      $item_id            - text
 * @property int(11)      $cat_id             = text
 * @property varchar(255) $name               - text
 * @property varchar(255) $alias              - text
 * @property text         $short_description  - text
 * @property text         $full_description   - text
 * @property varchar(200) $meta_description   - text
 * @property varchar(250) $meta_keywords      - text
 * @property varchar(19)  $date               - text
 * @property mediumtext   $fields             - Dynamic data for fields in JSON String format
 * @property text         $tags               - text
 * @property int(11)      $sorting            - text
 * @property tinyint(1)   $published          - text
 * @property int(11)      $views              - text
 * @property int(11)      $fixed              - text
 * @property int(11)      $noindex            - text
 * @property tinyint(1)   $enable_comments    - text
 */
class News extends ActiveRecord
{

    /**
     * Active recording
     */
    const PUBLISHED_ON = 1;

    /**
     * Inactive recording
     */
    const PUBLISHED_OFF = 0;

    /**
     * Enable indexing
     */
    const NOINDEX_ON = 1;

    /**
     * Disable indexing
     */
    const NOINDEX_OFF = 0;

    /**
     * Date format in the database
     */
    const PROP_DATE_FORMAT = 'Y-m-d H:i:s';

    /**
     * Date output format
     */
    const OUT_DATE_FORMAT = 'm.d.Y H:i';

    /**
     * Sort news field
     */
    const ORDER_BY_FIXED = 'fixed';
    const ORDER_BY_SORTING = 'sorting';

    /**
     * Date in UNIX TIMESTAMP format
     * @var integer
     */
    public $date_timestamp;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news}}';
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'short_description', 'full_description'], 'required'],
            [['item_id', 'sorting', 'views', 'fixed', 'noindex', 'cat_id', 'published', 'enable_comments'], 'integer'],
            [
                'alias',
                'unique',
                'targetClass' => self::className(),
                'message' => '"{attribute}" уже используеться в другой новости',
            ],
            [['name', 'alias'], 'string', 'max' => '255'],
            ['meta_description', 'string', 'max' => '200'],
            ['meta_keywords', 'string', 'max' => '250'],
            [['short_description', 'full_description'], 'string', 'max' => '65535'],
            ['date', 'default', 'value' => date(self::PROP_DATE_FORMAT)],
            ['date', 'date', 'format' => 'php:' . self::PROP_DATE_FORMAT],
            [['fields', 'tags'], 'safe'],
            [['name', 'alias', 'meta_description', 'meta_keywords', 'fields'], 'trim'],
            ['alias', TranslitFilter::className(), 'translitAttribute' => 'name'],
            [['meta_description', 'meta_keywords'], 'filter', 'filter' => function ($value) {
                return \yii\helpers\Html::encode($value);
            }],

            // Default value
            [['cat_id', 'sorting', 'views', 'enable_comments'], 'default', 'value' => 0],
            ['tags', 'default', 'value' => null],
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'alias' => 'Алиас',
            'cat_id' => 'Категория',
            'date' => 'Дата публикации',
            'fixed' => 'Зафиксировать новость',
            'full_description' => 'Полное описание',
            'meta_description' => 'Описание (Description)',
            'meta_keywords' => 'Ключевые слова (Keywords)',
            'name' => 'Название новости',
            'noindex' => 'Запретить индексацию',
            'published' => 'Публикация новости',
            'short_description' => 'Краткое описания',
            'sorting' => 'Позиция в списке',
            'views' => 'Просмотры',
            'enable_comments' => 'Отключить коментарии',
            'tags' => 'Список всех тегов',
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->date_timestamp = strtotime($this->date);

        // Convert json string to array
        if($this->tags) {
            $this->tags = Json::decode($this->tags);
        }

        parent::afterFind();
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        // Save dependency files to a new record
        if (count(Yii::$app->request->post('FilesTemp'))) {
            Files::filesTemp(Yii::$app->request->post('FilesTemp'), $this->id);
        }
    }

    /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        // When creating a new record, the settings field must always be an empty array
        if ($this->getIsNewRecord()) {
            $this->fields = Field::DEFAULT_NEW_VALUE;
        }

        if(is_array($this->tags) && count($this->tags)) {
            $this->tags = Json::encode($this->tags);
        }

        return true;
    }

    /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }

        // Here you need to delete all files of additional fields
        // ...
        return true;
    }

    /**
     * Prewiev Widget files
     * @return array
     */
    public function getPrewiewsWidget()
    {
        $files = [];
        foreach ($this->files as $item) {
            $files[] = $item->getPrewiews();
        }
        return $files;
    }

    /**
     * Default Images
     * @param  integer $index
     * @return [string]
     */
    public function getPathLogo($index = 0)
    {
        if (count($this->files)) {
            return $this->files[$index]->getImageSize(NewsFeed::DEFAULT_SIZE);
        }
        return '';
    }

    /**
     * Get Images size
     * @param  string $size
     * @param  integer $index
     * @return [string]
     */
    public function getImage($size = null, $index = 0)
    {
        if (count($this->files)) {
            return $this->files[$index]->getImageSize(is_null($size) ? NewsFeed::DEFAULT_SIZE : $size);
        }
        return '';
    }

    /**
     * All related files
     * @return array
     */
    protected function getFiles()
    {
        return $this->hasMany(Files::className(), ['item_id' => 'id'])->andOnCondition([
            'module' => StringHelper::basename(self::className()),
        ])->select(['id', 'item_id', 'file', 'module', 'sizes']);
    }

    /**
     * Get news feed module
     * @return common\models\NewsFeed
     */
    public function getFeedModel()
    {
        return $this->hasOne(NewsFeed::className(), ['id' => 'item_id']);
    }

    /**
     * Check the existence of the field
     * @param  string $param
     * @return boolean
     */
    public function hasFileld($param)
    {
        $_fields = Json::decode($this->fields);
        if(ArrayHelper::keyExists($param, $_fields)) {
            return (boolean) ArrayHelper::getValue($_fields, $param, false);
        }
        return false;
    }

    /**
     * Return value of an additional field
     * @param  string $param
     * @param  string $default
     * @return string
     */
    public function getFieldValue($param, $default = '')
    {
        return ArrayHelper::getValue(Json::decode($this->fields), $param, $default);
    }

    /**
     * Category news
     * @return common\models\NewsCat
     */
    public function getCat()
    {
        return $this->hasOne(NewsCat::className(), ['id' => 'cat_id']);
    }

    /**
     * Number of comments
     * @return Integer
     */
    public function getCountComments() {
        $_condition = [
            'module' => StringHelper::basename(self::className())
        ];

        // Only comments that have been moderated
        if(Yii::$app->options->get('only_moderation')) {
            $_condition['moderation'] = Comment::MODERATION_ON;
        }

        return $this
            ->hasMany(Comment::className(), ['item_id' => 'id'])
            ->andOnCondition($_condition)
            ->count();
    }

    /**
     * Explode tags
     * @return Array
     */
    public function getTagsList() {
        $tags = [];
        if(is_array($this->tags) && count($this->tags)) {
            foreach ($this->tags as $i => $value) {
                $tags[$i] = explode(TagGroup::TAG_SEPARATOR, $value);
            }
            return $tags;
        }

        return [];
    }

    /**
     * Link Reference Code
     * @return string
     */
    public function getLinkcode()
    {
        return '{linkcode="' . StringHelper::basename(self::className()) . '" id="' . $this->id . '"}';
    }

    /**
     * link to the page
     * @return string
     */
    public function getLink()
    {
        return $this->feedModel->link . '/' . $this->id . '-' . $this->alias . '.html';
    }
}
