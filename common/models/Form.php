<?php
/**
 * Form Generator
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace common\models;

use common\ext\db\ActiveRecord;
use Yii;
use yii\helpers\ArrayHelper;
use \common\components\FormRunMethod;
use \yii\helpers\StringHelper;
use \yii\validators\EmailValidator;

/**
 * Database structure
 *
 * @property int(11)      $id                 - Unique identificator
 * @property varchar(255) $name               - text
 * @property varchar(255) $email              - text
 * @property text         $to_view            - text
 * @property text         $from_view          - text
 * @property text         $success_text       - text
 * @property tinyint(1)   $success_position   - text
 * @property tinyint(1)   $format_mail        - text
 * @property tinyint(1)   $is_reply_message   - text
 * @property tinyint(1)   $is_to_message      - text
 * @property tinyint(1)   $captcha            - text
 * @property tinyint(1)   $return_type        - text
 * @property varchar(255) $run_method         - text
 * @property tinyint(1)   $is_save_result     - text
 * @property tinyint(1)   $is_add_attachments - text
 * @property tinyint(1)   $published          - text
 * @property varchar(50)  $btn_name           - text
 * @property tinyint(1)   $btn_reset          - text
 */
class Form extends ActiveRecord {

    /**
     * Active recording
     */
    const PUBLISHED_ON = 1;

    /**
     * Inactive recording
     */
    const PUBLISHED_OFF = 0;

    /**
     * Active reply message
     */
    const IS_REPLY_MESSAGE_ON = 1;

    /**
     * Inactive reply message
     */
    const IS_REPLY_MESSAGE_OFF = 0;

    /**
     * Active to message
     */
    const IS_TO_MESSAGE_ON = 1;

    /**
     * Inactive to message
     */
    const IS_TO_MESSAGE_OFF = 0;

    /**
     * Active captcha
     */
    const CAPTCHA_ON = 1;

    /**
     * Inactive captcha
     */
    const CAPTCHA_OFF = 0;

    /**
     * Active results
     */
    const RESULT_ON = 1;

    /**
     * Inactive results
     */
    const RESULT_OFF = 0;

    /**
     * Text message format
     */
    const FORMAT_MESSAGE_TEXT = 0;

    /**
     * Html message format
     */
    const FORMAT_MESSAGE_HTML = 1;

    /**
     * Text from the field
     */
    const RETURN_TYPE_TEXT = 0;

    /**
     * The result of this method
     */
    const RETURN_TYPE_RUN_METHOD = 1;

    /**
     * Show text above form
     */
    const SUCCESS_POSITION_OVER_FORM = 0;

    /**
     * Show text below the form
     */
    const SUCCESS_POSITION_UNDER_FORM = 1;

    /**
     * Show text in the modal window
     */
    const SUCCESS_POSITION_MODAL = 2;

    /**
     * Separator for lists
     */
    const LIST_SEPARATOR = ',';

    /**
     * Default Button Name
     */
    const DEFAULT_BTN_NAME = 'Отправить';

    /**
     * Attach files to a message
     */
    const IS_ADD_ATTACHMENTS_ON = 1;

    /**
     * Do not attach files to a message
     */
    const IS_ADD_ATTACHMENTS_OFF = 0;

    /**
     * Default Sender name
     */
    const DEFAULT_EMAIL_USER = 'mailer';

    /**
     * List of message types
     * @return array
     */
    public static function getFormatMessage() {
        return [
            self::FORMAT_MESSAGE_TEXT => 'Текстовый формат',
            self::FORMAT_MESSAGE_HTML => 'HTML формат'
        ];
    }

    /**
     * List of actions when taking data from the form
     * @return array
     */
    public static function getSuccesPosition() {
        return [
            self::SUCCESS_POSITION_OVER_FORM => 'Показать ответ над формой',
            self::SUCCESS_POSITION_UNDER_FORM => 'Показать ответ под формой',
            self::SUCCESS_POSITION_MODAL => 'Показать ответ в модальном окне'
        ];
    }

    /**
     * List of actions for success
     * @return array
     */
    public static function getReturnTypes() {
        return [
            self::RETURN_TYPE_TEXT => 'Показать ответа из поля "Сообщение ответа"',
            self::RETURN_TYPE_RUN_METHOD => 'Вывести результат обработки скрипта'
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%form}}';
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules() {
        return [
            [
                ['name', 'email', 'to_view', 'return_type', 'success_text', 'success_position', 'format_mail'],
                'required',
            ],
            ['name', 'string', 'min' => '1', 'max' => '255'],

            ['email', 'validateEmailList'],

            [['to_view', 'from_view'], 'string', 'max' => '65535'],
            [
                [
                    'is_reply_message',
                    'format_mail',
                    'return_type',
                    'success_position',
                    'captcha',
                    'is_to_message',
                    'is_save_result',
                    'published',
                    'btn_reset',
                    'is_add_attachments'
                ],
                'integer',
            ],

            ['btn_name', 'trim'],

            // Begin Strict parameter check
            ['return_type', 'in', 'range' => [
                self::RETURN_TYPE_TEXT,
                self::RETURN_TYPE_RUN_METHOD,
            ]],
            ['success_position', 'in', 'range' => [
                self::SUCCESS_POSITION_OVER_FORM,
                self::SUCCESS_POSITION_UNDER_FORM,
                self::SUCCESS_POSITION_MODAL,
            ]],
            ['format_mail', 'in', 'range' => [
                self::FORMAT_MESSAGE_TEXT,
                self::FORMAT_MESSAGE_HTML,
            ]],
            // End Strict parameter check

            // Field filling control run_method
            ['return_type', 'required', 'when' => function ($model) {
                if ($this->return_type == self::RETURN_TYPE_RUN_METHOD) {
                    if (empty($this->run_method)) {
                        $this->addError('run_method', 'значение поля не может быть пустым!');
                    }
                } else {
                    $this->run_method = '';
                }

            }],

            // Verify the mandatory field for multi selection
            ['run_method', function ($attribute, $params, $validator) {
                if ($this->return_type == self::RETURN_TYPE_RUN_METHOD) {
                    if (!method_exists(new FormRunMethod, $this->run_method)) {
                        $this->addError($attribute, 'метод "FormRunMethod::' . $this->run_method . '()" не существует!');
                    }
                }
            }]
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels() {
        return [
            'is_add_attachments' => 'Добавить вложение в письмо',
            'btn_name' => 'Имя кнопки',
            'btn_reset' => 'Кнопка сбросить данные',
            'captcha' => 'Включить капчу',
            'email' => 'Email получателя',
            'format_mail' => 'Формат email сообщений',
            'from_view' => 'Шаблон ответа',
            'name' => 'Имя формы',
            'published' => 'Публикация формы',
            'is_reply_message' => 'Отправить ответ',
            'return_type' => 'Ответ формы',
            'run_method' => 'Имя метода',
            'is_save_result' => 'Сохранения данных',
            'success_position' => 'Позиция ответа',
            'success_text' => 'Сообщение ответа',
            'is_to_message' => 'Отправить получателю',
            'to_view' => 'Шаблон получателя'
        ];
    }

    /**
     * Validate the list of email addresses
     * @param  string $attribute
     * @param  string $params
     * @param  object $validator
     */
    public function validateEmailList($attribute, $params, $validator) {
        $validate = true;

        if ($this->$attribute) {
            foreach (@explode(self::LIST_SEPARATOR, $this->$attribute) as $item) {
                if ($item) {
                    $validate = $validate && (new EmailValidator)->validate($item);
                }
            }
        }

        if (!$validate) {
            $this->addError($attribute, $emailValidator->message);
        }
    }

    /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeDelete() {
        if (!parent::beforeDelete()) {
            return false;
        }

        // Deleting fields of this form
        FormField::deleteAll(['item_id' => $this->id]);

        // Deleting results of this form
        FormResult::deleteAll(['item_id' => $this->id]);
        return true;
    }

    /**
     * All fields of this form
     * @return array
     */
    public function getFields() {
        return $this->hasMany(FormField::className(), ['item_id' => 'id'])->orderBy([
            'sorting' => 'DESC',
        ]);
    }

    /**
     * Total number of messages
     * @return integer
     */
    public function getResultCount() {
        return $this->hasMany(FormResult::className(), ['item_id' => 'id'])->count();
    }

    /**
     * E-Mail address list
     * @return array
     */
    public function getEmailList() {
        return @explode(self::LIST_SEPARATOR, $this->email);
    }

    /**
     * Unique shape key
     * @return string
     */
    public function getUniqueKey() {
        return md5($this->id . $this->name);
    }

    /**
     * System variables for messages
     * @param  boolean $variables
     * @var array
     */
    public function getSystemVariables($variables = false) {
        if (!$variables) {
            return [
                'copyright' => 'Копирайт сайта',
                'date' => 'Дата в формате "дд.мм.гггг"',
                'datetime' => 'Дата и время в формате "дд.мм.гггг чч.мм.сс"',
                'domain' => 'Домен сайта в формате http(s)://domain.name/',
                'sitename' => 'Краткое название сайта'
            ];
        } else {
            return [
                'copyright' => Yii::$app->options->get('copyright'),
                'date' => date('d.m.Y'),
                'datetime' => date('d.m.Y H:i:s'),
                'domain' => Yii::$app->params['frontentUrl'],
                'sitename' => Yii::$app->options->get('home_title')
            ];
        }
    }

    /**
     * Apply variables to messages
     * @param boolean
     */
    public function setVariables($variables) {
        $variables = ArrayHelper::merge($this->getSystemVariables(true), $variables);

        foreach (['to_view', 'from_view', 'success_text'] as $item) {
            $this->$item = Yii::t('app', $this->$item, $variables);
        }

        return true;
    }

    /**
     * Slider widget code
     * @return string
     */
    public function getShortcode() {
        return '{shortcode="' . StringHelper::basename(self::className()) . '" id="' . $this->id . '"}';
    }
}