<?php

/**
 * Mass actions for entries in the database
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace common\models;

use common\ext\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;

/**
 * Database structure
 *
 * @property string      $action          - text
 * @property array       $selected_id     - text
 */
class GroupActions extends Model {

    /**
     * Delete records from the database
     */
    const ACTION_GROUP_REMOVE = 'remove';

    /**
     * Pattern for the attribute name
     */
    const PATTERN_ATTRIBUTE = '/^([A-z0-9\_]+)$/i';

    /**
     * Selected action
     * @var [integer]
     */
    public $action;

    /**
     * Selected records by ID
     * @var [array]
     */
    public $selected_id = [];

    /**
     * List of all actions
     * @var array
     */
    private $_actions = [];

    /**
     * @inheritdoc
     */
    public function init() {
        $this->_actions = ArrayHelper::merge($this->_actions, [
            self::ACTION_GROUP_REMOVE => 'Удалить записи'
        ]);
    }

    /**
     * List of all actions
     * @return [array]
     */
    public function getActions() {
        return $this->_actions;
    }

    /**
     * Add new actions
     * @param [array] $extend_actions
     * @return [array]
     */
    public function addActions($extend_actions = []) {
        if(is_array($extend_actions)) {
            $this->_actions = ArrayHelper::merge($extend_actions, $this->_actions);
        }

        return $this->_actions;
    }

    /**
     * Get action name
     * @param  [integer] $action
     * @return [string]
     */
    public function getActionName($action) {
        return ArrayHelper::getValue($this->_actions, $action, 'Unknown');
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules() {
        return [
            [
                'action',
                'required',
                'message' => 'Выберите необходимое действие',
            ],
            [
                'selected_id',
                'each',
                'rule' => ['integer'],
                'message' => 'Неверные идентификаторы записей',
            ],
            ['action', 'in', 'range' => array_keys($this->_actions)]
        ];
    }

    /**
     * Checking the selection of identifiers for mass action
     * @return [boolean]
     */
    public function beforeValidate() {
        if (parent::beforeValidate()) {
            if (!is_array($this->selected_id) || !count($this->selected_id)) {
                $this->addError('selected_id', 'Не выбранные идентификаторы');
            } else {
                $this->selected_id = array_keys($this->selected_id);
                return true;
            }
        }

        return false;
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels() {
        return [
            'action' => 'Выбранное действие',
            'selected_id' => 'Выбранные записи по ID'
        ];
    }

    /**
     * Checking the set identifier
     * @param  [integer]  $id
     * @return boolean
     */
    public function isSelected($id) {
        return is_null($this->selected_id) ? null : ArrayHelper::isIn($id, $this->selected_id);
    }

    /**
     * Getting names for the attributes
     * @param  [array] $attributes
     * @return [string]
     */
    public static function inputName($attributes) {
        if (is_array($attributes) && count($attributes)) {
            $_attributes = implode('', preg_replace(self::PATTERN_ATTRIBUTE, '[$1]', $attributes));
            return StringHelper::basename(self::className()) . $_attributes;
        }

        return '';
    }
}