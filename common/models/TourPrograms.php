<?php

/**
 * Tour Programs
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace common\models;

use common\ext\db\ActiveRecord;

/**
 * Database structure
 *
 * @property int(11)       $id                 - Unique identificator
 * @property int(11)       $item_id            - text
 * @property varchar(255)  $name               - text
 * @property text          $description        - text
 * @property int(11)       $sorting            - text
 * @property tinyint(1)    $published          - text
 *
 */
class TourPrograms extends ActiveRecord
{
    /**
     * Active recording
     */
    const PUBLISHED_ON = 1;

    /**
     * Inactive recording
     */
    const PUBLISHED_OFF = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tour_programs}}';
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return [
            [['item_id', 'name', 'description'], 'required'],
            [['item_id', 'sorting', 'published'], 'integer'],
            ['description', 'safe'],

            // Default
            ['published', 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'description' => 'Описение',
            'item_id' => 'Идентификатор тура',
            'name' => 'Наименование',
            'published' => 'Публикация',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        // Default position
        if(!$this->sorting) {
            $this->sorting = self::find()
                ->where(['item_id' => $this->item_id])
                ->count() + 1;
        }

        // Add sorting
        return true;
    }

    /**
     * Tour model
     * @return common\models\Tours;
     */
    public function getTour()
    {
        return $this->hasOne(Tours::className(), ['id' => 'item_id']);
    }
}
