<?php

/**
 * Tours
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace common\models;

use common\ext\db\ActiveRecord;
use common\ext\validators\TranslitFilter;
use Yii;
use yii\helpers\Json;
use \yii\helpers\StringHelper;

/**
 * Database structure
 *
 * @property int(11)      $id                     - Unique identificator
 * @property varchar(255) $cat_id                 - text
 * @property int(11)      $type_id                - text
 * @property varchar(255) $restrictions           - text
 * @property varchar(255) $name                   - text
 * @property varchar(255) $alias                  - text
 * @property text         $description            - text
 * @property varchar(200) $meta_description       - text
 * @property varchar(250) $meta_keywords          - text
 * @property text         $included_in_price      - text
 * @property text         $not_included_in_price  - text
 * @property int(11)      $rating                 - text
 * @property int(11)      $rating_votes           - text
 * @property int(11)      $views                  - text
 * @property int(11)      $stars                  - text
 * @property tinyint(1)   $selling                - text
 * @property tinyint(1)   $noindex                - text
 * @property tinyint(1)   $fixed                  - text
 * @property tinyint(1)   $published              - text
 *
 */
class Tours extends ActiveRecord
{

    /**
     * Active tour for sale
     */
    const SELLING_ON = 1;

    /**
     * Sale tour is not active
     */
    const SELLING_OFF = 0;

    /**
     * Active recording
     */
    const PUBLISHED_ON = 1;

    /**
     * Inactive recording
     */
    const PUBLISHED_OFF = 0;

    /**
     * Enable indexing
     */
    const NOINDEX_ON = 1;

    /**
     * Disable indexing
     */
    const NOINDEX_OFF = 0;

    /**
     * Enable fixed tour
     */
    const FIXED_ON = 1;

    /**
     * Disable fixed tour
     */
    const FIXED_OFF = 0;

    /**
     * Date output format
     */
    const OUT_DATE_FORMAT = 'd.m.Y H:i';

    /**
     * Sort tours field
     */
    const ORDER_BY_FIXED = 'fixed';

    /**
     * Default image size
     */
    const DEFAULT_SIZE = '100x100';

    /**
     * Pattern for dividing lines
     */
    const PATTERN_PREG_SPLIT = '/\r\n|\r|\n/';

    /**
     * Default minimum price
     * @var String
     */
    public $default_price;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tours}}';
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'cat_id', 'description'], 'required'],
            [['name', 'alias'], 'string', 'max' => '255'],
            ['meta_description', 'string', 'max' => '200'],
            ['meta_keywords', 'string', 'max' => '250'],

            [
                [
                    'type_id', 'rating', 'rating_votes', 'selling',
                    'views', 'stars', 'noindex', 'fixed', 'published',
                ],
                'integer',
            ],

            [['cat_id', 'restrictions'], function ($attribute) {
                if (!is_array($this->$attribute)) {
                    $this->addError($attribute, 'Неверный формат данных, данные должны быть массивом.');
                }
            }],

            ['description', 'string', 'max' => '65535'],
            [['name', 'alias', 'meta_description', 'meta_keywords', 'description'], 'trim'],
            ['alias', TranslitFilter::className(), 'translitAttribute' => 'name'],

            [
                [
                    'meta_description', 'meta_keywords', 'included_in_price', 'not_included_in_price'
                ],
                'filter', 'filter' => function ($value) {
                return \yii\helpers\Html::encode($value);
            }],

            ['stars', 'in', 'range' => [0, 5]],

            // Default data
            [
                ['type_id', 'restrictions', 'rating', 'rating_votes', 'views', 'stars', 'selling'],
                'default', 'value' => 0,
            ],

            [['cat_id', 'restrictions'], 'default', 'value' => '[]'],
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'alias' => 'Алиас',
            'cat_id' => 'Категории',
            'description' => 'Полное описание',
            'fixed' => 'Зафиксировать запись',
            'included_in_price' => 'В стоимость входит',
            'meta_description' => 'Описание (Description)',
            'meta_keywords' => 'Ключевые слова (Keywords)',
            'name' => 'Название тура',
            'noindex' => 'Запретить индексацию',
            'not_included_in_price' => 'В стоимость не входит',
            'published' => 'Публикация записи',
            'restrictions' => 'Ограничения',
            'stars' => 'Количество звёзд',
            'type_id' => 'Тип',
            'views' => 'Просмотры',
            'selling' => 'Тур онлайн',
        ];
    }

    /**
     * Tour prices
     * @return Array|common\models\ToursPrice
     */
    public function getPrices()
    {
        return $this->hasMany(TourPrices::className(), ['item_id' => 'id']);
    }

    /**
     * Tour prices for frontend
     * @return Array|common\models\ToursPrice
     */
    public function getFilterPrices()
    {
        return $this->hasMany(TourPrices::className(), ['item_id' => 'id'])
            ->andOnCondition('start_date >= :current_date AND published = :published', [
                ':current_date' => date(TourPrices::PROP_DATE_FORMAT),
                ':published' => TourPrices::PUBLISHED_ON
            ]);
    }

    /**
     * Tour programs
     * @return Array|common\models\TourPrograms
     */
    public function getPrograms() {
        return $this->hasMany(TourPrograms::className(), ['item_id' => 'id']);
    }

    /**
     * The cheapest
     * @return common\models\TourPrices
     */
    public function getPrice()
    {
        return $this->hasOne(TourPrices::className(), ['item_id' => 'id'])
            ->from(TourPrices::tableName() . ' p')
            ->andOnCondition(
                'p.start_date >= :current_date AND p.published = :published',
                [
                    ':current_date' => date(TourPrices::PROP_DATE_FORMAT),
                    ':published' => TourPrices::PUBLISHED_ON
                ]
            )
            ->addOrderBy(['p.base_price' => SORT_ASC]);
    }

    /**
     * Default minimum price
     * @return String
     */
    public function getDefaultPrice()
    {
        if ($this->price !== null) {
            return $this->price->price;
        }

        return '';
    }

    /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeSave($insert)
    {
        if (is_array($this->cat_id)) {
            $this->cat_id = Json::encode($this->cat_id);
        }

        if (is_array($this->restrictions)) {
            $this->restrictions = Json::encode($this->restrictions);
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        // Save dependency files to a new record
        if (count(Yii::$app->request->post('FilesTemp'))) {
            Files::filesTemp(Yii::$app->request->post('FilesTemp'), $this->id);
        }
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        if (!is_null($this->cat_id) && !empty($this->cat_id)) {
            $this->cat_id = Json::decode($this->cat_id);
        }

        if (!is_null($this->restrictions) && !empty($this->restrictions)) {
            $this->restrictions = Json::decode($this->restrictions);
        }

        parent::afterFind();
    }

    /**
     * Prewiev Widget files
     * @return array
     */
    public function getPrewiewsWidget()
    {
        $files = [];
        foreach ($this->files as $item) {
            $files[] = $item->getPrewiews();
        }
        return $files;
    }

    /**
     * Default Images
     * @param  integer $index
     * @return [string]
     */
    public function getPathImage($index = 0)
    {
        if (count($this->files)) {
            return $this->files[$index]->getImageSize(self::DEFAULT_SIZE);
        }
        return '';
    }

    /**
     * Get Images size
     * @param  string $size
     * @param  integer $index
     * @return [string]
     */
    public function getImage($size = null, $index = 0)
    {
        if (count($this->files)) {
            return $this->files[$index]->getImageSize(is_null($size) ? self::DEFAULT_SIZE : $size);
        }
        return '';
    }

    /**
     * All related files
     * @return array
     */
    protected function getFiles()
    {
        return $this->hasMany(Files::className(), ['item_id' => 'id'])->andOnCondition([
            'module' => StringHelper::basename(self::className()),
        ])->select(['id', 'item_id', 'file', 'module', 'sizes']);
    }

    /**
     * Overall rating of the rating
     * @return integer
     */
    public function getRate() {
        if($this->rating) {
            return round(($this->rating / $this->rating_votes), 0, PHP_ROUND_HALF_UP);
        }
        return 0;
    }

    /**
     * Model category for display
     * @return  common\models\DataGr oup;
     */
    public function getCatModel() {
        if(is_array($this->cat_id) && count($this->cat_id) >= 1) {
            return DataGroup::findOne($this->cat_id[0]);
        }
        return null;
    }

    public function getCatList() {
        if(is_array($this->cat_id) && count($this->cat_id) >= 1) {
            return DataGroup::findAll($this->cat_id);
        }
        return [];
    }

    /**
     * link to the page
     * @return string
     */
    public function getLink()
    {
        return '/tours/' . $this->id . '-' . $this->alias . '.html';
    }
}
