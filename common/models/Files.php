<?php
/**
 * All downloaded files
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace common\models;

use Imagine\Image\Box;
use Yii;
use yii\behaviors\TimestampBehavior;
use common\ext\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\imagine\Image;
use yii\web\UploadedFile;

/**
 * Database structure
 *
 * @property integer $id        - Unique identificator
 * @property integer $item_id   - text
 * @property string  $module    - text
 * @property string  $file      - text
 * @property integer $create_at - text
 * @property integer $type      - text
 * @property boolean $basic     - text
 * @property string  $alt       - text
 */
class Files extends ActiveRecord {

    /**
     * Image files
     */
    const TYPE_IMAGE = 1;

    /**
     * Other files
     */
    const TYPE_FILE = 2;

    /**
     * Define the validation files
     */
    const SCENARIO_FILE = 'file';

    /**
     * Define the validation images
     */
    const SCENARIO_IMAGE = 'image';

    /**
     * Link for which you can delete the entry and files
     */
    const REMOVE_LINK = '/upload/remove';

    /**
     * Original image
     */
    const IMAGE_ORIGINAL = 'original';

    /**
     * Separator on the size of the file name url
     */
    const SIZE_SEPARATOR = '-';

     /**
     * Separator for image sizes list
     */
    const SIZES_LIST_SEPARATOR = ',';

    /**
     * File without reference
     */
    const TEMP_ITEM_ID = 0;

    /**
     * Permissions on the folder
     */
    const CHMOD = 0744;

    /**
     * Dimension validation for images
     * '/^(\,?[\d+]{1,4}x[\d+]{1,4})+$/i'
     */
    const SIZES_PATTERN = '/^\d{1,4}x\d{1,4}(,\d{1,4}x\d{1,4})*$/i';

    /**
     * Attributes to be validated by this validator.
     * @var string || object
     */
    public $uploadedFile;

    /**
     * The name file
     * @var string
     */
    public $name;

    /**
     * Path to upload and save the file
     * @var string
     */
    protected $_path;

    /**
     * File size
     * @var integer
     */
    protected $file_size = 0;

    /**
     * File Validation Rules
     * @var array
     */
    protected $_rules = [];
    //     'maxSize'   => 1024 * 1024 * 2, // 1024 * 1024 * 1
    //     'maxFiles'  => 1,
    //     'minWidth'  => 0, // Default not checked
    //     //'maxWidth'   => 0,
    //     'minHeight' => 0 // Default not checked
    //     //'maxHeight'  => 0,
    // ];

    /**
     * @inheritdoc
     * @return array
     */
    public function rules() {
        return [
            // Default Files
            ['uploadedFile', 'file', 'skipOnEmpty' => false,
                'extensions' => 'rar, zip, pdf, docs, mp4, mp3, jpg, jpeg, png, gif',
                'maxSize' => 1024 * 1024 * 5,
                'maxFiles' => 1,
                'on' => self::SCENARIO_FILE,
            ],

            // Images
            ['uploadedFile', 'image', 'skipOnEmpty' => false,
                'extensions' => 'jpg, jpeg, png, gif',
                'maxSize' => 1024 * 1024 * 5,
                'maxFiles' => 1,
                //'minWidth' => $this->_rules['minWidth'],
                //'maxWidth'   => $this->_rules['maxWidth'],
                //'minHeight' => $this->_rules['minHeight'],
                //'maxHeight'  => $this->_rules['maxHeight'],
                'on' => self::SCENARIO_IMAGE,
            ],

            [['module', 'file', 'type'], 'required'],
            [['name', 'module', 'sizes', 'alt'], 'trim'],
            [['item_id', 'type', 'basic'], 'integer'],

            [
                'type', 'in', 'range' => [
                    self::TYPE_IMAGE,
                    self::TYPE_FILE,
                ],
            ],

            [['file', 'sizes', 'alt'], 'string', 'max' => '255'],
            [
                'sizes',
                'match',
                'pattern' => self::SIZES_PATTERN,
                'message' => '{attribute} - Неверный формат данных',
            ],

            ['basic', 'default', 'value' => 0],

            // Default empty data
            ['alt', 'default', 'value' => ''],

            // Removing unnecessary delimiters
            ['sizes', function ($attribute, $params, $validator) {
                $this->$attribute = trim($this->$attribute, self::SIZES_LIST_SEPARATOR);
            }]
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_at',
                'updatedAtAttribute' => 'create_at'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%files}}';
    }

    // TODO: TEMPORARILY!
    public function getPrewiews() {
        return [
            'file' => Yii::getAlias('@files') . $this->getFileLink(),
            'id' => $this->id,
            'delete' => self::REMOVE_LINK,
            'size' => 0
        ];
    }

    /**
     * Add depending on the module files
     * @param array $temp_files
     * @param integer $id
     */
    public static function filesTemp($temp_files, $id) {
        $files = self::find()->where([
            'id' => $temp_files,
            'item_id' => self::TEMP_ITEM_ID,
        ])->all();

        foreach ($files as $file) {
            $file->item_id = $id;

            $path = Yii::getAlias('@upload') . '/' . mb_strtolower($file->module) . '/';

            // Create destination directory
            FileHelper::createDirectory($path . $file->item_id . '/' . $file->id . '/', self::CHMOD);

            // Move files
            FileHelper::copyDirectory(
                $path . 'temp/' . $file->id . '/',
                $path . $file->item_id . '/' . $file->id . '/',
                ['recursive' => true]
            );

            // Remove temp folder
            FileHelper::removeDirectory($path . 'temp/' . $file->id . '/');

            $file->update();
        }
    }

    /**
     * Delete dependent files
     * @param  string $module
     * @param  integer $item_id
     * @return boolean
     */
    public static function removeDependents($module, $item_id) {
        Files::deleteAll(['item_id' => $item_id, 'module' => $module]);

        return FileHelper::removeDirectory(
            Yii::getAlias('@upload') . '/' . $module . '/' . $item_id .'/', [
                'traverseSymlinks' => true,
            ]
        );
    }

    /**
     * Dimensions of all images
     * @return array
     */
    public function getImageSizes() {

        // If the image does not return an empty array
        if ($this->type != self::TYPE_IMAGE) {
            return [];
        }

        $imageSizes = [
            self::IMAGE_ORIGINAL => self::getImageSize(self::IMAGE_ORIGINAL),
        ];

        if ($this->sizes) {
            foreach (@explode(',', $this->sizes) as $size) {
                $imageSizes[$size] = self::getImageSize($size);
            }
        }

        return $imageSizes;
    }

    /**
     * Get the right size image
     * @param string $size
     * @return string
     */
    public function getImageSize($size) {
        if ($size == self::IMAGE_ORIGINAL) {
            return Yii::getAlias('@files') . $this->path . $this->file;
        }

        if (ArrayHelper::isIn($size, @explode(',', $this->sizes))) {
            return Yii::getAlias('@files') . $this->path . $size . self::SIZE_SEPARATOR . $this->file;
        }
    }

    /**
     * A link to the uploaded file
     * @return string
     */
    public function getFileLink($domain = false) {
        if($domain) {
            return Yii::getAlias('@files') . $this->path . $this->file;
        }

        return $this->path . $this->file;
    }

    /**
     * Link image
     * @param  String $size
     * @return String
     */
    public function getImageLink($size) {
        return Yii::getAlias('@files') . $this->path . $size . '-' . $this->file;
    }

    /**
     * Get filename
     * @return String
     */
    public function getFilename() {
        return $this->file;
    }

    /**
     * Get file size
     * @return Integer
     */
    public function getFilesize() {
        return $this->file_size;
    }

    /**
     * Module
     * @return string
     */
    public function getPath($id_dir = true) {
        $path = '/' . mb_strtolower($this->module);
        $path .= '/' . ($this->item_id ? $this->item_id : 'temp');

        if ($id_dir) {
            $path .= '/' . $this->id;
        }

        return $path . '/';
    }

    /**
     * Set validation options
     * @param array
     * @return boolean
     */
    public function setRules($rules) {
        if (ArrayHelper::isTraversable($rules)) {
            $this->_rules = ArrayHelper::merge($this->_rules, $rules);
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete() {
        if (parent::beforeDelete()) {

            // Deleting a file directory
            if (is_dir(Yii::getAlias('@upload') . $this->path)) {
                FileHelper::removeDirectory(Yii::getAlias('@upload') . $this->path, [
                    'traverseSymlinks' => true,
                ]);
            }

            // Deleting a module directory
            if (self::isDirectoryEmpty(Yii::getAlias('@upload') . $this->getPath(false))) {
                FileHelper::removeDirectory(Yii::getAlias('@upload') . $this->getPath(false));
            }

            return true;
        }
        return false;
    }

    /**
     * Check the directory for files
     * @param  string  $dir
     * @return boolean
     */
    private static function isDirectoryEmpty($dir) {
        if (!is_readable($dir)) {
            return NULL;
        }

        $handle = opendir($dir);

        while (false !== ($entry = readdir($handle))) {
            if ($entry != "." && $entry != "..") {
                return false;
            }

        }

        return true;
    }

    /**
     * Download and save the file to the database
     * @return boolean
     */
    public function upload() {

        $this->uploadedFile = UploadedFile::getInstance($this, 'uploadedFile');

        // File name
        $this->file = $this->name ? uniqid($this->name . '-') : uniqid();
        $this->file .= '.' . $this->uploadedFile->extension;
        if ($this->validate()) {
            $this->save(false);

            if (FileHelper::createDirectory(Yii::getAlias('@upload') . $this->path, self::CHMOD)) {
                $filePath = Yii::getAlias('@upload') . $this->path . $this->file;
                $this->uploadedFile->saveAs($filePath);
                $this->file_size = filesize($filePath);

                if($this->type == self::TYPE_IMAGE) {
                    // Image saving the "Imagine" through security purposes
                    Image::getImagine()->open($filePath)->save($filePath);

                    // For images, perform cutting images when needed
                    if ($this->type == self::TYPE_IMAGE && $this->sizes) {
                        foreach (@explode(',', $this->sizes) as $size) {
                            if (preg_match('/^([0-9]+)x([0-9]+)/i', $size, $matches)) {
                                Image::getImagine()
                                    ->open($filePath)
                                    ->thumbnail(new Box($matches[1], $matches[2])) // width, height
                                    ->save(Yii::getAlias('@upload') . $this->path . $matches[0] . self::SIZE_SEPARATOR . $this->file, [
                                        'quality' => 100,
                                    ]);
                            }
                        }
                    }
                }

                return true;
            }

            // Return errors
        } else {
            return $this->errors;
        }

        return false;
    }
}