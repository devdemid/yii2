<?php

/**
 * Tours related to the order
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace common\models;

/**
 * Database structure
 *
 * @property int(11)      $id - Unique identificator
 */
class OrderData extends \common\ext\db\ActiveRecord
{

    /**
     * Date format in the database
     */
    const PROP_DATE_FORMAT = 'Y-m-d';

    /**
     * Date output format
     */
    const OUT_DATE_FORMAT = 'd.m.Y';

    /**
     * Dates in UNIX TIMESTAMP
     * @var Integer
     */
    public $start_date_timestamp;
    public $end_date_timestamp;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%orders_data}}';
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return [
            [['item_id', 'adults', 'children', 'price_id'], 'integer'],
            ['adults', 'number', 'min' => '1', 'max' => '127'],
            ['children', 'number', 'min' => '0', 'max' => '127'],
            ['name', 'filter', 'filter' => 'trim'],
            [['price', 'children_price'], 'number'],
            [['start_date', 'end_date'], 'date', 'format' => 'php:' . self::PROP_DATE_FORMAT],
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'item_id' => 'Идентификатор заказа',
            'name' => 'Наименование тура',
            'adults' => 'Взрослые',
            'children' => 'Дети',
            'price_id' => 'Идентификатор стоимости',
            'price' => 'Стоимость',
            'children_price' => 'Цена для детей',
            'start_date' => 'Начало',
            'end_date' => 'Окончание',
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        if ($this->start_date) {
            $this->start_date_timestamp = strtotime($this->start_date);
        }

        if ($this->end_date) {
            $this->end_date_timestamp = strtotime($this->end_date);
        }

        parent::afterFind();
    }
}
