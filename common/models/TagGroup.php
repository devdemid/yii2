<?php

/**
 * Tag groups
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace common\models;

use common\ext\validators\TranslitFilter;

/**
 * Database structure
 *
 * @property int(11)      $id        - Unique identificator
 * @property int(11)      $item_id   - text
 * @property varchar(100) $module    - text
 * @property varchar(255) $name      - text
 * @property varchar(255) $alias     - text
 * @property tinyint(1)   $published - text
 *
 */
class TagGroup extends \common\ext\db\ActiveRecord
{
    /**
     * Active recording
     */
    const PUBLISHED_ON = 1;

    /**
     * Inactive recording
     */
    const PUBLISHED_OFF = 0;

    /**
     * Tag List Separator
     */
    const TAG_SEPARATOR = ',';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tag_group}}';
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return [
            [['item_id', 'module', 'name'], 'required'],
            ['module', 'string', 'max' => '100'],
            [['name', 'alias'], 'string', 'max' => '255'],
            [['item_id', 'published'], 'integer'],
            ['alias', TranslitFilter::className(), 'translitAttribute' => 'name'],
            [
                'alias',
                'unique',
                'targetClass' => self::className(),
                'message' => '"{attribute}" уже используеться в другой группе',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'item_id' => 'Идентификатор модуля',
            'module' => 'Модуль',
            'name' => 'Наименование группы',
            'alias' => 'Алиас',
            'published' => 'Опубликовать',
        ];
    }

    /**
     * Module model
     * @return ActiveRecordObject
     */
    public function getMod_model() {
        $className = '\\' .__NAMESPACE__ . '\\' . $this->module;
        return $this->hasOne($className, ['id' => 'item_id']);
    }

       /**
     * link to the page
     * @return string
     */
    public function getLink()
    {
        return $this->mod_model->link . '/' . $this->alias . '/';
    }
}
