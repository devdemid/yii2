<?php
/**
 * Form Result
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace common\models;

use common\ext\db\ActiveRecord;
use common\models\Actions;
use common\models\Form;
use common\models\FormField;
use frontend\models\FormModel;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Database structure
 *
 * @property int(11)      $id             - Unique identificator
 * @property int(11)      $item_id        - text
 * @property mediumtext   $json_result    - Dynamic data for fields in JSON String format
 * @property tinyint(1)   $viewed         - text
 * @property varchar(19)  $date           - text
 * @property varchar(255) $to_email       - text
 */
class FormResult extends ActiveRecord {

    /**
     * Active recording
     */
    const PUBLISHED_ON = 1;

    /**
     * Inactive recording
     */
    const PUBLISHED_OFF = 0;

    /**
     * Date format in the database
     */
    const PROP_DATE_FORMAT = 'Y-m-d H:i:s';

    /**
     * Date output format
     */
    const OUT_DATE_FORMAT = 'm.d.Y H:i:s';

    /**
     * Sort channels field
     */
    const ORDER_BY_SORTING = 'date';

    /**
     * Message status not viewed
     */
    const STATUS_VIEWED_NO = 0;

    /**
     * The status of the message was viewed
     */
    const STATUS_VIEWED_YES = 1;

    /**
     * Status viewed
     */
    const ACTION_GROUP_VIEWED_YES = 'viewed_yes';

    /**
     * Status not reviewed
     */
    const ACTION_GROUP_VIEWED_NO = 'viewed_no';

    /**
     * Date in UNIX TIMESTAMP format
     * @var integer
     */
    public $date_timestamp;

    /**
     * Form results in array format
     * @var array
     */
    public $results;

    /**
     * The first few elements for the main page
     * @var array
     */
    public $results_slice;

    /**
     * List of all group actions
     * @return [array]
     */
    public static function getGroupActions() {
        return [
            self::ACTION_GROUP_VIEWED_YES => 'Изменить статус на просмотренно',
            self::ACTION_GROUP_VIEWED_NO => 'Изменить статус новая запись'
        ];
    }

    /**
     * Get group action name
     * @param  [integer] $action
     * @return [string]
     */
    public static function getGroupActionsName($action) {
        return ArrayHelper::getValue(static::getGroupActions(), $action, 'Unknown');
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%form_result}}';
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules() {
        return [
            ['date', 'date', 'format' => 'php:' . self::PROP_DATE_FORMAT],
            ['date', 'default', 'value' => date(self::PROP_DATE_FORMAT)],
            [['item_id', 'viewed'], 'integer'],
            ['item_id', 'required'],
            [['json_result', 'to_email'], 'safe'],
            ['viewed', 'default', 'value' => self::STATUS_VIEWED_NO]
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels() {
        return [
            'date' => 'Дата получения данных',
            'item_id' => 'Идентификатор формы',
            'to_email' => 'Email получателя',
            'viewed' => 'Просмотрено'
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterFind() {
        $this->date_timestamp = strtotime($this->date);

        if ($this->json_result) {
            $this->results = Json::decode($this->json_result);
            $this->results_slice = array_slice($this->results, 0, 3);
        }

        parent::afterFind();
    }

    /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeDelete() {
        if (!parent::beforeDelete()) {
            return false;
        }

        // Here delete file
        if (is_array($this->results)) {
            foreach ($this->results as $item) {
                if ($item['type'] == FormField::TYPE_FILE && $item['value']) {

                    $files = Yii::getAlias(
                        '@upload' . FormModel::ATTACHMENTS_DIR . $this->item_id . '/' . $item['value']
                    );

                    if (file_exists($files)) {
                        unlink($files);
                    }
                }
            }
        }

        return true;
    }

    public function getForm() {
        return $this->hasOne(Form::className(), ['id' => 'item_id'])->select([
            'name',
        ]);
    }

    /**
     * E-Mail address list
     * @return array
     */
    public function getEmailList() {
        return @explode(Form::LIST_SEPARATOR, $this->to_email);
    }

    /**
     * Getting a link to a file
     * @param  string $file
     * @return string
     */
    public function getLinkAttachment($file = false) {
        if ($file) {
            return Yii::getAlias('@files' . FormModel::ATTACHMENTS_DIR . $this->item_id . '/' . $file);
        }

        return '#';
    }

    /**
     * Bulk actions with results
     * @param  [string] $action
     * @param  [array] $record_ids
     * @return [boolean]
     */
    public static function runGroupActions($action, $record_ids) {

        $models = self::find()->where(['IN', 'id', $record_ids])->all();

        // Deleting records by id
        if ($action == GroupActions::ACTION_GROUP_REMOVE) {
            foreach ($models as $item) {
                $item->delete();
            }

            Yii::$app->session->setFlash('success', 'Записи удалены успешно!');
        }

        // Status changes to viewed
        elseif ($action == self::ACTION_GROUP_VIEWED_YES) {
            foreach ($models as $item) {
                $item->viewed = self::STATUS_VIEWED_YES;
                $item->update(false);
            }

            Yii::$app->session->setFlash('success', 'Изменения статуса на просмотрено!');
        }

        // Changes to unvisited status
        else if ($action == self::ACTION_GROUP_VIEWED_NO) {
            foreach ($models as $item) {
                $item->viewed = self::STATUS_VIEWED_NO;
                $item->update(false);
            }

            Yii::$app->session->setFlash('success', 'Изменения статуса новая запись!');
        }

        // Unknown action
        else {
            Yii::$app->session->setFlash('danger', 'Неизвестное действие!');
        }

        unset($models);
    }
}