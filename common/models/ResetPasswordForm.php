<?php
namespace common\models;

use common\models\User;
use Yii;
use yii\base\InvalidParamException;
use yii\base\Model;

/**
 * Password reset form
 */
class ResetPasswordForm extends Model {

    /**
     * Minimum number of characters
     */
    const MIN_PASSWORD_STRING = 6;

    /**
     * Maximum number of characters
     */
    const MAX_PASSWORD_STRING = 30;

    public $password;
    public $password_repeat;

    /**
     * @var \common\models\User
     */
    private $_user;

    /**
     * Creates a form model given a token.
     *
     * @param  string                          $token
     * @param  array                           $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($token, $config = []) {
        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException('Password reset token cannot be blank.');
        }
        $this->_user = User::findByPasswordResetToken($token);
        if (!$this->_user) {
            throw new InvalidParamException('Wrong password reset token.');
        }
        parent::__construct($config);
    }

    public function rules() {
        return [
            ['password', 'required'],
            ['password', 'string', 'min' => self::MIN_PASSWORD_STRING, 'max' => self::MAX_PASSWORD_STRING],

            ['password_repeat', 'required'],
            ['password_repeat', 'string', 'min' => self::MIN_PASSWORD_STRING, 'max' => self::MAX_PASSWORD_STRING],
            ['password_repeat', 'compare', 'compareAttribute' => 'password']
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels() {
        return [
            'password' => 'Новый пароль',
            'password_repeat' => 'Повторите пароль'
        ];
    }

    public function resetPassword() {
        $user = $this->_user;
        $user->setPassword($this->password);
        $user->removePasswordResetToken();

        return $user->save(false);
    }
}
