<?php
/**
 * Block
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace common\models;

use Yii;
use common\ext\db\ActiveRecord;
use \yii\helpers\StringHelper;

/**
 * Database structure
 *
 * @property int(11)      $id        - Unique identificator
 * @property varchar(255) $name      - text
 * @property text         $content   - text
 * @property tinyint(1)   $published - text
 */
class Block extends ActiveRecord {

    /**
     * Active recording
     */
    const PUBLISHED_ON = 1;

    /**
     * Inactive recording
     */
    const PUBLISHED_OFF = 0;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%block}}';
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules() {
        return [
            ['name', 'required'],
            ['name', 'string', 'min' => '1', 'max' => '255'],
            ['content', 'string', 'max' => '65535'],
            [['name', 'content'], 'filter', 'filter' => 'trim'],
            ['published', 'integer']
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels() {
        return [
            'name' => 'Название блока',
            'content' => 'Полное описание',
            'published' => 'Публикация блока'
        ];
    }

    /**
     * Slider widget code
     * @return string
     */
    public function getShortcode() {
        return '{shortcode="' . StringHelper::basename(self::className()) . '" id="' . $this->id . '"}';
    }
}