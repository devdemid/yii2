<?php
/**
 * Hooks are a way to associate your code to some specific NC events.
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace common\models;

use common\ext\db\ActiveRecord;

/**
 * Database structure
 *
 * @property int(11) $id - Unique identificator
 */
class HookCode extends ActiveRecord {

    /**
     * Active recording
     */
    const PUBLISHED_ON = 1;

    /**
     * Inactive recording
     */
    const PUBLISHED_OFF = 0;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%hookcode}}';
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules() {
        return [];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels() {
        return [];
    }
}