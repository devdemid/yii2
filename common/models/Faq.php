<?php
/**
 * FAQ - Frequently Asked Questions
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace common\models;

use common\ext\db\ActiveRecord;
use common\ext\validators\TranslitFilter;
use common\models\FaqAnswers;
use \yii\helpers\StringHelper;

/**
 * Database structure
 *
 * @property int(11)      $id                - Unique identificator
 * @property varchar(255) $name              - text
 * @property varchar(255) $alias             - text
 * @property varchar(200) $meta_description  - text
 * @property varchar(250) $meta_keywords     - text
 * @property tinyint(1)   $enable_page       - text
 * @property tinyint(1)   $noindex           - text
 * @property tinyint(1)   $published         - text
 */
class Faq extends ActiveRecord {

    /**
     * Active recording
     */
    const PUBLISHED_ON = 1;

    /**
     * Inactive recording
     */
    const PUBLISHED_OFF = 0;

    /**
     * Active PAGE
     */
    const ENABLE_PAGE = 1;

     /**
     * Inactive PAGE
     */
    const DISABLE_PAGE = 0;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%faq}}';
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules() {
        return [
            ['name', 'required'],
            ['name', 'string', 'min' => '1', 'max' => '255'],
            ['name', 'filter', 'filter' => 'trim'],
            ['alias', TranslitFilter::className(), 'translitAttribute' => 'name'],
            ['meta_description', 'string', 'max' => '200'],
            ['meta_keywords', 'string', 'max' => '250'],
            [['meta_description', 'meta_keywords'], 'filter', 'filter' => function ($value) {
                return \yii\helpers\Html::encode($value);
            }],
            [['name', 'alias', 'meta_description', 'meta_keywords'], 'trim'],
            [['enable_page', 'noindex', 'published'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels() {
        return [
            'name' => 'Название ветки',
            'published' => 'Публикация ветки',
            'noindex' => 'Запретить индексацию',
            'enable_page' => 'Использовать страницы',
        ];
    }

    /**
     * Number of news in the stream
     * @return integer
     */
    public function getCountAnswers() {
        return $this->hasMany(FaqAnswers::className(), ['item_id' => 'id'])->count();
    }

    /**
     * List of all replies
     * @return [array]
     */
    public function getAnswers() {
        return $this->hasMany(FaqAnswers::className(), ['item_id' => 'id'])
            ->where(['published' => FaqAnswers::PUBLISHED_ON])
            ->orderBy([FaqAnswers::ORDER_BY_SORTING => SORT_ASC])
            ->all();
    }

    /**
     * Slider widget code
     * @return string
     */
    public function getShortcode() {
        return '{shortcode="' . StringHelper::basename(self::className()) . '" id="' . $this->id . '"}';
    }

    /**
     * Link Reference Code
     * @return string
     */
    public function getLinkcode() {
        return '{linkcode="' . StringHelper::basename(self::className()) . '" id="' . $this->id . '"}';
    }

    /**
     * link to the faq page
     * @return string
     */
    public function getLink() {
        return '/' . mb_strtolower(StringHelper::basename(self::className())) . '/branch/' . $this->alias;
    }
}