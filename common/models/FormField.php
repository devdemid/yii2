<?php
/**
 * Fields for the form generator
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace common\models;

use common\ext\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Database structure
 *
 * @property int(11)       $id               - Unique identificator
 * @property int(11)       $item_id          - text
 * @property varchar(50)   $attribute        - text
 * @property varchar(535)  $hint             - text
 * @property varchar(255)  $label            - text
 * @property text          $list             - text
 * @property enum          $type             - text
 * @property varchar(255)  $extension        - text
 * @property int(11)       $size             - text
 * @property int(11)       $sorting          - text
 * @property tinyint(1)    $unique           - text
 * @property tinyint(1)    $required         - text
 * @property tinyint(1)    $placeholder      - text
 * @property varchar(50)   $input_mask       - text
 *
 */
class FormField extends ActiveRecord {

    /**
     * Pattern for the attribute label
     */
    const PATTERN_LABEL = '/([А-яA-z0-9\s\-]+)/i';

    /**
     * Pattern for the attribute name
     */
    const PATTERN_ATTRIBUTE = '/^([A-z0-9\_]+)$/i';

    /**
     * Checking the validity of the lists
     */
    const PATTERN_LIST = '/^[A-zА-я0-9\s]+(,[A-zА-я0-9\s]+)*$/ui';

    /**
     * Valid characters for the field input mask
     */
    const PATTERN_INPUT_MASK = '/(^[\+?0-9\:\/\(\)\-\s\%\#\$\_\.\,]+)$/i';

    /**
     * Separator for lists
     */
    const LIST_SEPARATOR = ',';

    /**
     * Default min file size
     */
    const DEFAULT_MIN_FILE_SIZE = 1; // 1Mb (1024 * 1024)

    /**
     * Default max file size
     */
    const DEFAULT_MAX_FILE_SIZE = 50; // 50Mb (1024 * 1024 * 50)

    // The types of fields
    const TYPE_CHECKBOX = 'checkbox';
    const TYPE_EMAIL = 'email';
    const TYPE_FILE = 'file';
    const TYPE_INTEGER = 'integer';
    const TYPE_PHONE = 'phone';
    const TYPE_RADIO_GROUP = 'radio';
    const TYPE_SELECT = 'select';
    const TYPE_STRING = 'string';
    const TYPE_TEXTAREA = 'textarea';

    // State of the checkboxes
    const STATE_CHECKBOX_ON = 1;
    const STATE_CHECKBOX_OFF = 0;

    /**
     * State of the checkboxes
     * @return array
     */
    public static function getCheckboxStates() {
        return [
            self::STATE_CHECKBOX_ON => 'Да',
            self::STATE_CHECKBOX_OFF => 'Нет'
        ];
    }

    /**
     * List of field types
     * @return array
     */
    public static function getTypes() {
        return [
            self::TYPE_CHECKBOX => 'Флажок',
            self::TYPE_EMAIL => 'Email',
            self::TYPE_FILE => 'Файл',
            self::TYPE_INTEGER => 'Число',
            self::TYPE_PHONE => 'Номер телефона',
            self::TYPE_RADIO_GROUP => 'Переключатели',
            self::TYPE_SELECT => 'Выпадающий список',
            self::TYPE_STRING => 'Текстовая строка',
            self::TYPE_TEXTAREA => 'Текстовая область'
        ];
    }

    /**
     * Returns the name of the field type
     * @param  string $type
     * @return string
     */
    public function getTypeName($type) {
        return ArrayHelper::getValue(static::getTypes(), $type, 'Unknown');
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%form_field}}';
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules() {
        return [
            [['attribute', 'label', 'type'], 'required'],
            [['size', 'extension'], 'required', 'on' => self::TYPE_FILE],
            [['list'], 'required', 'on' => [self::TYPE_SELECT, self::TYPE_RADIO_GROUP]],
            [['attribute', 'extension', 'hint', 'label', 'list'], 'filter', 'filter' => 'trim'],
            [['sorting', 'required', 'size', 'unique', 'placeholder'], 'integer'],
            [
                'label',
                'match',
                'pattern' => self::PATTERN_LABEL,
                'message' => '"{attribute}" неверное имя, разрешено только [А-яA-z0-9:]',
            ],
            [
                'attribute',
                'match',
                'pattern' => self::PATTERN_ATTRIBUTE,
                'message' => '"{attribute}" неверное имя, разрешено только [A-z0-9\_]',
            ],

            [
                'input_mask',
                'match',
                'pattern' => self::PATTERN_INPUT_MASK,
                'message' => '"{attribute}" неверный формат данных, разрешено только [0-9:/()-%#+$_.,]',
            ],

            [
                'list',
                'match',
                'pattern' => self::PATTERN_LIST,
                'message' => '"{attribute}" неверный формат данных, разрешено только [A-zА-я0-9\s]',
            ],
            [
                'size',
                'integer',
                'min' => self::DEFAULT_MIN_FILE_SIZE,
                'max' => self::DEFAULT_MAX_FILE_SIZE,
                'on' => self::TYPE_FILE,
            ],

            // Control over the size of the base fields
            [['attribute', 'input_mask'], 'string', 'max' => '50'],
            ['hint', 'string', 'max' => '535'],
            ['list', 'string', 'max' => '65535'],
            [['label', 'extension'], 'string', 'max' => '255'],

            // Checking the existence of type
            ['type', 'string', 'when' => function ($model) {
                return ArrayHelper::keyExists($model->type, self::getTypes());
            }],

            // Email address can be only one
            [
                'type',
                'unique',
                'targetClass' => self::className(),
                'filter' => ['item_id' => $this->item_id],
                'message' => 'Форма может содержать только одно поле Email!',
                'when' => function ($model, $attribute) {
                    return $model->{$attribute} == self::TYPE_EMAIL;
                },
            ],
            [
                'attribute',
                'unique',
                'targetClass' => self::className(),
                'filter' => ['item_id' => $this->item_id],
                'message' => '"{attribute}" с таким параметром уже есть!',
            ],

            [['sorting', 'required', 'unique', 'placeholder'], 'default', 'value' => 0],
            ['size', 'default', 'value' => self::DEFAULT_MAX_FILE_SIZE],

            ['input_mask', 'default', 'value' => ''],
        ];
    }

    /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeValidate() {
        // Setting up a script for some field types
        if (ArrayHelper::isIn($this->type, [
            self::TYPE_FILE,
            self::TYPE_SELECT,
            self::TYPE_RADIO_GROUP,
        ])) {
            $this->setScenario($this->type);
        }

        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels() {
        return [
            'attribute' => 'Параметр',
            'extension' => 'Расширение файлов',
            'hint' => 'Описание под параметром',
            'label' => 'Метка атрибута',
            'list' => 'Список значений',
            'placeholder' => 'Метка в поле',
            'required' => 'Обязательное поле',
            'size' => 'Размер файла',
            'sorting' => 'Позиция в форме',
            'type' => 'Тип поля',
            'unique' => 'Уникальное значение',
            'input_mask' => 'Маска ввода'
        ];
    }

    /**
     * A broken list of values
     * @return array
     */
    public function getListExplode() {
        if (ArrayHelper::isIn($this->type, [self::TYPE_SELECT, self::TYPE_RADIO_GROUP])) {
            return explode(self::LIST_SEPARATOR, $this->list);
        } else {
            return [];
        }
    }
}