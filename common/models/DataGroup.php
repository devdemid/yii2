<?php

/**
 * Data group
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace common\models;

use common\ext\db\ActiveRecord;
use common\ext\validators\TranslitFilter;
use yii\helpers\ArrayHelper;
use \yii\helpers\StringHelper;

/**
 * Database structure
 *
 * @property int(11)      $id                 - Unique identificator
 * @property int(11)      $group_id           - text
 * @property varchar(255) $name               - text
 * @property varchar(255) $alias              - text
 * @property varchar(100) $icon_css           - text
 * @property varchar(50)  $icon_image         - text
 * @property varchar(200) $meta_description   - text
 * @property varchar(250) $meta_keywords      - text
 * @property int(11)      $sorting            - text
 * @property tinyint(1)   $published          - text
 *
 */
class DataGroup extends ActiveRecord
{

    /**
     * Active recording
     */
    const PUBLISHED_ON = 1;

    /**
     * Inactive recording
     */
    const PUBLISHED_OFF = 0;

    /**
     * List of groups
     */
    const GROUP_TOURS_CAT_ID = 1;
    const GROUP_TOURS_TYPE_ID = 2;
    const GROUP_TOURS_RESTRICTION_ID = 3;

    /**
     * Default Icon Image Size
     */
    const DEFAULT_ICON_IMAGE_SIZE = '25x25';

    /**
     * Sort field
     */
    const ORDER_BY_SORTING = 'sorting';

    /**
     * Default module
     * @var string
     */
    public $module = 'Tours';

    /**
     * Get all group
     * @return Array
     */
    public static function getGroupList() {
        return [
            self::GROUP_TOURS_CAT_ID => 'Категория тура',
            self::GROUP_TOURS_TYPE_ID => 'Тип тура',
            self::GROUP_TOURS_RESTRICTION_ID => 'Ограничения тура'
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%data_group}}';
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return [
            [['group_id', 'name'], 'required'],
            [['group_id', 'sorting', 'published'], 'integer'],
            [['name', 'alias', 'meta_description', 'meta_keywords', 'icon_css', 'icon_image'], 'trim'],
            ['alias', TranslitFilter::className(), 'translitAttribute' => 'name'],
            [['meta_description', 'meta_keywords', 'name'], 'filter', 'filter' => function ($value) {
                return \yii\helpers\Html::encode($value);
            }],

            // Default data
            ['published', 'default', 'value' => self::PUBLISHED_ON]
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'alias' => 'Алиас',
            'group_id' => 'Группа',
            'icon_css' => 'CSS Иконка',
            'icon_image' => 'Изображение иконки',
            'meta_description' => 'Описание (Description)',
            'meta_keywords' => 'Ключевые слова (Keywords)',
            'name' => 'Наименование записи',
            'published' => 'Публикация записи',
        ];
    }

    /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        // When creating a new record, the settings field must always be an empty array
        if ($this->getIsNewRecord()) {
            $this->sorting = self::find()->where([
                'group_id' => $this->group_id
            ])->count() + 1;
        }

        return true;
    }

    /**
     * Getting the data of the whole group
     * @param  Integer $group
     * @return Array
     */
    public function getGroup($group_id = null) {
        if($group_id !== null) {
            $group_data = self::find()
                ->where(['group_id' => (int) $group_id])
                ->orderBy(self::ORDER_BY_SORTING)
                ->all();

            return ArrayHelper::map($group_data, 'id', 'name');
        }
        return [];
    }

    /**
     * [getGroupModel description]
     * @param  [type] $group_id [description]
     * @return Array
     */
    public function getGroupModel($group_id = null) {
        if($group_id !== null) {
            return self::find()
                ->where([
                    'group_id' => (int) $group_id,
                    'published' => self::PUBLISHED_ON
                ])
                ->orderBy(self::ORDER_BY_SORTING)
                ->all();
        }
        return [];
    }

    /**
     * List icon
     * @return Array
     */
    public function getImagesrc() {
        return [
            'data-imagesrc' => '/img/icons_search/all_tours.png'
        ];
    }

    public function getFonticon() {
        return [
            'data-fonticon' => $this->icon_css
        ];
    }

    /**
     * Slider widget code
     * @return string
     */
    public function getShortcode() {
        return '{linkcode="' . StringHelper::basename(self::className()) . '" id="' . $this->id . '" module="'. $this->module .'"}';
    }

    /**
     * Link Reference Code
     * @return string
     */
    public function getLink()
    {
        return '/tours/cat/' . $this->alias;
    }
}