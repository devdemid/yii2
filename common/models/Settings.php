<?php
/**
 * Linking files to models
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace common\models;

use common\ext\db\ActiveRecord;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use \common\components\ParamHelper;
use \yii\helpers\StringHelper;

/**
 * Database structure
 *
 * @property integer $id           - Unique identificator
 * @property string  $module       - text
 * @property string  $group        - text
 * @property string  $param        - text
 * @property string  $value        - text
 * @property string  $defaultValue - text
 * @property integer $mask         - text
 * @property integer $label        - text
 * @property integer $description  - text
 * @property string  $placeholder  - text
 * @property string  $type         - text
 * @property integer $ordering     - text
 * @property boolean $disabled     - text
 * @property boolean $fixed        - text
 */
class Settings extends ActiveRecord {

    /**
     * The auxiliary array for multi-selection
     * @var array
     */
    public $multiSelection = [];

    // Delimiter
    const STRING_SEPARATOR = ',';

    // The module name for the distribution of opportunities between modules
    const SETTINGS_MODULE = 'settings';

    // The types of fields
    const TYPE_CHECKBOX = 'checkbox';
    const TYPE_EDITOR = 'editor';
    const TYPE_FILE = 'file';
    const TYPE_FILES = 'files';
    const TYPE_IMAGE = 'image';
    const TYPE_RADIO_GROUP = 'radio';
    const TYPE_SELECT = 'select';
    const TYPE_SELECT_MULTIPLE = 'select_multiple';
    const TYPE_STRING = 'string';
    const TYPE_SWITCHER = 'switcher';
    const TYPE_TEXTAREA = 'textarea';

    /**
     * Get all data types
     * @return array
     */
    public static function getTypes() {
        return [
            self::TYPE_CHECKBOX => 'Флажок',
            self::TYPE_EDITOR => 'Редактор',
            self::TYPE_FILE => 'Файл',
            self::TYPE_FILES => 'Файлы [группа файлов]',
            self::TYPE_RADIO_GROUP => 'Переключатели',
            self::TYPE_SELECT => 'Список',
            self::TYPE_SELECT_MULTIPLE => 'Список множественный',
            self::TYPE_STRING => 'Текстовое поле [text]',
            self::TYPE_SWITCHER => 'Switcher',
            self::TYPE_TEXTAREA => 'Текстовое поле [textarea]'
        ];
    }

    /**
     * The data type that can be an array
     */
    private function getAllowedTypes() {
        return [
            self::TYPE_SELECT_MULTIPLE,
            self::TYPE_FILES
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%settings}}';
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules() {
        return [
            [['group', 'param', 'label', 'type'], 'required'],
            ['param', 'unique', 'targetClass' => '\common\models\Settings', 'message' => 'Имя "{attribute}" уже зарегистрировано'],
            [['param', 'value', 'label', 'description', 'placeholder'], 'filter', 'filter' => 'trim'],
            [['value', 'defaultValue', 'group'], 'string', 'max' => 255, 'when' => function ($model) {
                return ArrayHelper::isIn($model->type, [
                    self::TYPE_STRING,
                    self::TYPE_IMAGE,
                ]);
            }],
            [['value', 'defaultValue'], 'string', 'max' => 65535, 'when' => function ($model) {
                return ArrayHelper::isIn($model->type, [
                    self::TYPE_TEXTAREA,
                    self::TYPE_EDITOR,
                    self::TYPE_FILES,
                ]);
            }],
            ['description', 'string', 'max' => '65535'],
            [['disabled', 'fixed'], 'integer'],

            ['value', 'string', 'when' => function ($model) {
                return ArrayHelper::isIn(gettype($model->value), [
                    'integer', 'string',
                ]) ? true : false;
            }],

            // Verify the mandatory field for multi selection
            ['run_method', 'required', 'when' => function ($model) {
                if (ArrayHelper::isIn($model->type, [
                    self::TYPE_RADIO_GROUP,
                    self::TYPE_SELECT,
                    self::TYPE_SELECT_MULTIPLE,
                ])) {
                    return !method_exists(new ParamHelper, $model->run_method);
                } else {
                    return false;
                }
            }, 'whenClient' => 'function (attribute, value) {
                var type = $("#settings-type").val(),
                    check_array = [
                        "' . self::TYPE_RADIO_GROUP . '",
                        "' . self::TYPE_SELECT . '",
                        "' . self::TYPE_SELECT_MULTIPLE . '"
                    ];
                if(!$.inArray(type, check_array)) {
                    return !value;
                } else {
                    return false;
                }
            }',
                'message' => 'Данный метод не существует',
            ],

            // Default
            [['mask', 'options'], 'default', 'value' => ''],
            ['ordering', 'default',  'value' => 0]
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels() {
        return [
            'group' => 'Группа параметров',
            'param' => 'Параметр',
            'value' => 'Значение',
            'defaultValue' => 'Значение по умолчанию',
            'mask' => 'Маска',
            'label' => 'Наименование параметра',
            'description' => 'Описание',
            'placeholder' => 'Текст внутри поля',
            'type' => 'Тип параметра',
            'run_method' => 'Метод получения данных',
            'disabled' => 'Заблокировать элемент',
            'fixed' => 'Запретить удаление параметра'
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterFind() {

        // If the field has an array parse the data
        if (ArrayHelper::isIn($this->type, $this->getAllowedTypes())
            && ArrayHelper::isTraversable($this->value)) // Yii is_array
        {
            $this->value = implode(self::STRING_SEPARATOR, $this->value);
        }

        // Checking the existence of different types or set the default
        if (!ArrayHelper::keyExists($this->type, $this->getTypes())) {
            $this->type = self::TYPE_TEXTAREA;
        }

        // Get data from the controller
        if (!empty($this->run_method) && ArrayHelper::isIn($this->type, [
            self::TYPE_RADIO_GROUP,
            self::TYPE_SELECT,
            self::TYPE_SELECT_MULTIPLE,
        ])) {

            $ParamHelper = new ParamHelper;
            // Has method
            if (method_exists($ParamHelper, $this->run_method)) {
                $this->multiSelection = forward_static_call([$ParamHelper, $this->run_method]);
            } else {
                throw new InvalidConfigException(
                    'Неверный метод для получения данных'
                );
            }
        }

        parent::afterFind();
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {

            // If the field is an array of data to parse the string
            if (ArrayHelper::isIn($this->type, $this->getAllowedTypes())
                && ArrayHelper::isTraversable($this->value)) // Yii is_array
            {
                $this->value = explode(self::STRING_SEPARATOR, $this->value);
            }

            // Set category settings
            $this->module = self::SETTINGS_MODULE;

            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        // Update cache
    }

    /**
     * All related files
     * @return array
     */
    protected function getFiles() {
        if (ArrayHelper::isIn($this->type, [self::TYPE_FILE, self::TYPE_FILES])) {
            return $this->hasMany(Files::className(), ['item_id' => 'id'])->andOnCondition([
                'module' => StringHelper::basename(self::className()),
            ])->select(['id', 'item_id', 'file', 'module']);
        }
        return [];
    }

    /**
     * Get file link
     * @return string
     */
    public function getFile() {
        $files = $this->files[0];
        if(count($files)) {
            return $this->files[0]->getFileLink(true);
        }
        return $this->defaultValue;
    }

    /**
     * Prewiev Widget files
     * @return array
     */
    public function getPrewiewsWidget() {
        $files = [];
        foreach ($this->files as $item) {
            $files[] = $item->getPrewiews();
        }
        return $files;
    }
}