<?php
/**
 * Menu Generator
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace common\models;

use common\ext\db\ActiveRecord;
use \yii\helpers\StringHelper;
use yii\helpers\ArrayHelper;

/**
 * Database structure
 *
 * @property int(11)      $id               - Unique identificator
 * @property tinyint(2)   $type             - text
 * @property varchar(255) $name             - text
 * @property tinyint(1)   $published        - text
 */
class Menu extends ActiveRecord {

    /**
     * Active recording
     */
    const PUBLISHED_ON = 1;

    /**
     * Inactive recording
     */
    const PUBLISHED_OFF = 0;

    /**
     * Default menu list
     */
    const TYPE_MENU_LIST = 0;

    /**
     * Category tree
     */
    const TYPE_MENU_TREE = 1;

    /**
     * [getTypes description]
     * @return [type] [description]
     */
    public function getTypes() {
        return [
            self::TYPE_MENU_LIST => 'Список ссылок',
            self::TYPE_MENU_TREE => 'Дерево категорий'
        ];
    }

    /**
     * [getTypeName description]
     * @param  [type] $type [description]
     * @return [type]       [description]
     */
    public function getTypeName($type) {
        return ArrayHelper::getValue(static::getTypes(), $type, 'Unknown');
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%menu}}';
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules() {
        return [
            ['name', 'required'],
            [['published', 'type'], 'integer'],
            ['name', 'string', 'max' => '255']
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels() {
        return [
            'name' => 'Название меню',
            'type' => 'Тип меню',
            'published' => 'Публикация меню',
        ];
    }

    /**
     * [getMenuTree description]
     * @return array
     */
    public function getMenuTree() {
        return $this->hasMany(MenuTree::className(), ['item_id' => 'id']);
    }

    /**
     * [getMenuTree description]
     * @return array
     */
    public function getTree() {
        return $this->hasMany(MenuTree::className(), ['item_id' => 'id'])->where([
            'published' => MenuTree::PUBLISHED_ON,
        ])->orderBy(MenuTree::ORDER_BY_SORTING);
    }

    /**
     * Slider widget code
     * @return string
     */
    public function getShortcode() {
        return '{shortcode="' . StringHelper::basename(self::className()) . '" id="' . $this->id . '"}';
    }
}