<?php
namespace common\models;

use common\models\User;

/**
 * Signup form
 */
class SignupForm extends \common\ext\base\Model {

    public $phone;
    public $first_name;
    public $second_name;
    public $email;
    public $email_repeat;
    public $password;
    public $password_repeat;
    public $rules;

    private $_user;

    /**
     * Verify phone number (RUSSIA)
     *
     * ======================================================
     * ^\+?[78][-\(]?\d{3}\)?-?\d{3}-?\d{2}-?\d{2}$
     *
     * +7(903)888-88-88
     * 8(999)99-999-99
     * +380(67)777-7-777
     * 001-541-754-3010
     * +1-541-754-3010
     * 19-49-89-636-48018
     * +233 205599853
     * ======================================================
     */
    const PATTERN_PHONE = '/^((8|\+7)[\-\s]?)?(\(?\d{3}\)?[\-\s]?)?[\d\-\s]{7,10}$/i';

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            ['phone', 'filter', 'filter' => 'trim'],
            ['phone', 'required'],
            ['phone', 'string', 'max' => 100],
            ['phone', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Телефон уже зарегистрирован'],
            [
                'phone',
                'match',
                'pattern' => self::PATTERN_PHONE,
                'message' => '"{attribute}" неверный номер',
            ],

            ['first_name', 'filter', 'filter' => 'trim'],
            ['first_name', 'required'],
            ['first_name', 'string', 'min' => 2, 'max' => 100],

            ['second_name', 'filter', 'filter' => 'trim'],
            ['second_name', 'required'],
            ['second_name', 'string', 'min' => 2, 'max' => 100],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Email уже зарегистрирован'],

            ['email_repeat', 'required'],
            ['email_repeat', 'string', 'min' => 3],
            ['email_repeat', 'compare', 'compareAttribute' => 'email'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            ['password_repeat', 'required'],
            ['password_repeat', 'string', 'min' => 6],
            ['password_repeat', 'compare', 'compareAttribute' => 'password'],

            ['rules', 'boolean']
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels() {
        return [
            'phone' => 'Ваш телефон',
            'first_name' => 'Имя',
            'second_name' => 'Фамилия',
            'email' => 'Ваш Email',
            'email_repeat' => 'Повторите Email',
            'password' => 'Пароль',
            'password_repeat' => 'Повторите пароль',
            'rules' => 'Условия использования'
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup() {
        if ($this->validate()) {
            $user = new User();
            $user->first_name = $this->first_name;
            $user->second_name = $this->second_name;
            $user->email = $this->email;
            $user->phone = $this->phone;
            $user->group = User::DEFAULT_USER_GROUP;
            $user->setPassword($this->password);
            $user->last_auth_ip = \Yii::$app->request->getUserIP();
            $user->auth_timestamp = date('U');
            $user->generateAuthKey();
            if($user->save()) {
                $this->_user = $user;
                return $user;
            }
        }
        return null;
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login() {
        if($this->_user !== null) {
            return \Yii::$app->user->login($this->_user, 3600 * 24 * 30);
        }
        return false;
    }
}
