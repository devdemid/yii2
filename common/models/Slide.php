<?php
/**
 * Slide Designer
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace common\models;

use backend\models\Field;
use common\ext\db\ActiveRecord;
use common\ext\validators\VideoFilter;
use common\models\Files;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use \yii\helpers\StringHelper;

/**
 * Database structure
 *
 * @property int(11)      $id        - Unique identificator
 * @property int(11)      $item_id   - text
 * @property varchar(255) $url       - text
 * @property varchar(200) $video     - text
 * @property mediumtext   $fields    - text
 * @property tinyint(1)   $target    - text
 * @property int(11)      $sorting   - text
 * @property tinyint(1)   $published - text
 */
class Slide extends ActiveRecord {

    /**
     * Active recording
     */
    const PUBLISHED_ON = 1;

    /**
     * Inactive recording
     */
    const PUBLISHED_OFF = 0;

    /**
     * Sort field
     */
    const ORDER_BY_SORTING = 'sorting';

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%slide}}';
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules() {
        return [
            ['item_id', 'required'],
            [['item_id', 'target', 'sorting', 'published'], 'integer'],
            [['video'], 'url'],
            ['video', VideoFilter::className()],
            [['fields', 'url'], 'safe'],
            [['url', 'video'], 'trim'],
            ['sorting', 'default', 'value' => 0]
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels() {
        return [
            'url' => 'Ссылка',
            'video' => 'Видео',
            'target' => 'Новое окно',
            'sorting' => 'Сортировка',
            'published' => 'Публикация слайда'
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);

        // Save dependency files to a new record
        if (count(Yii::$app->request->post('FilesTemp'))) {
            Files::filesTemp(Yii::$app->request->post('FilesTemp'), $this->id);
        }
    }

    /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        // When creating a new record, the settings field must always be an empty array
        if ($this->getIsNewRecord()) {
            $this->fields = Field::DEFAULT_NEW_VALUE;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete() {
        if (parent::beforeDelete()) {
            // Remove All
            Files::removeDependents(StringHelper::basename(self::className()), $this->id);
            return true;
        }
        return false;
    }

    /**
     * Slider model
     * @return ActiveRecord/Slider
     */
    public function getSlider() {
        return $this->hasOne(Slider::className(), ['id' => 'item_id']);
    }

    /**
     * Image Link
     * @param string $size
     * @param integer $index
     * @return string
     */
    public function getPathImage($size = false, $index = 0) {
        if (count($this->files)) {
            return $this->files[$index]->getImageSize($size ? $size : $this->slider->size);
        }
        return Yii::$app->params['noimage'];
    }

    /**
     * All related files
     * @return array
     */
    protected function getFiles() {
        return $this->hasMany(Files::className(), ['item_id' => 'id'])->andOnCondition([
            'module' => StringHelper::basename(self::className()),
        ])->select(['id', 'item_id', 'file', 'module', 'sizes']);
    }

    /**
     * Prewiev Widget files
     * @return array
     */
    public function getPrewiewsWidget() {
        $files = [];
        foreach ($this->files as $item) {
            $files[] = $item->getPrewiews();
        }
        return $files;
    }

    /**
     * Check the existence of the field
     * @param  string  $param
     * @return boolean
     */
    public function hasFileld($param) {
        return ArrayHelper::keyExists($param, Json::decode($this->fields));
    }

    /**
     * Return value of an additional field
     * @param  string $param
     * @param  string $default
     * @return string
     */
    public function getFieldValue($param, $default = '') {
        return ArrayHelper::getValue(Json::decode($this->fields), $param, $default);
    }

    /**
     * Get a video ID
     * @return string
     */
    public function getVideoId() {
        return VideoFilter::getId($this->video);
    }

    /**
     * Retrieving the name of the video hosting
     * @return string
     */
    public function getVideoHosting() {
        return VideoFilter::getHosting($this->video);
    }

    /**
     * Linkto the screenshot of the video
     * @return string
     */
    public function getVideoScreenshot($name = 'maxresdefault') {
        return VideoFilter::getScreenshot($this->video, $name);
    }
}