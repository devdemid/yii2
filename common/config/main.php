<?php
return [
    'defaultRoute' => 'main',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'language' => 'ru-RU',
    'sourceLanguage' => 'ru-RU',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'yii_Y06BVGiw899734hekfDFGisjdfPp6ksiNJ',
        ],

        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],

        'authManager' => [
            'class' => 'common\components\AuthManager', // Hybrid PhpManager
            'itemFile' => '@rbac/items.php',
            'assignmentFile' => '@rbac/assignments.php',
            'ruleFile' => '@rbac/rules.php',
            'defaultRoles' => ['guest'],
        ],

        // https://github.com/yii-cms/yii2-robokassa
        // 'robokassa' => [
        //     'class' => '\robokassa\Merchant',
        //     'baseUrl' => 'https://auth.robokassa.ru/Merchant/Index.aspx',
        //     'sMerchantLogin' => 'greeniberia',
        //     'sMerchantPass1' => '',
        //     'sMerchantPass2' => '',
        //     'isTest' => true, //!YII_ENV_PROD,
        // ],

        'db' => [
            'dsn'         => 'mysql:host=localhost;dbname=db',
            'class'       => 'yii\db\Connection',
            'charset'     => 'utf8',
            'username'    => 'group-fs',
            'password'    => 'U0y0G4k5',
            'tablePrefix' => 'nc_'
        ],

        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'smtphoster@gmail.com',
                'password' => 'oeryuaoquthazlvo',
                'port' => '587',
                'encryption' => 'tls',
            ]
        ],

        // Settings system
        'options' => [
            'class' => 'common\components\Options',
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ],
        ],
    ],
];
