<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,

    'files.backend'  => '/web/files/',
    'frontentUrl' => '//yii.lan',
    'noimage' => '/files/noimage.png',
];
