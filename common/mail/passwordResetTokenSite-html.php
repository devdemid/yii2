<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\Clients */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl([
	'reg/reset-password',
	'token' => $user->password_reset_token
]);
?>
<div class="password-reset">
    <p>Здравствуйте <?= Html::encode($user->fullname) ?>,</p>
    <p>Перейдите по ссылке для восстановления пароля:</p>
    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
    <p>--<br>С уважением,<br><?= \yii::$app->name ?></p>
</div>
