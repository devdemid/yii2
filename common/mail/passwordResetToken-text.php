<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl([
	'users/reset-password',
	'token' => $user->password_reset_token
]);
?>
Здравствуйте <?= $user->first_name ?>,

Перейдите по ссылке для восстановления пароля:

<?= $resetLink ?>

--
С уважением,
<?= \yii::$app->name ?>
