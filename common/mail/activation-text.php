<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\Clients */

$activationLink = Yii::$app->urlManager->createAbsoluteUrl([
    'reg/activation',
    'token' => $user->activation_code
]);
?>
Здравствуйте <?= Html::encode($user->fullname) ?>,

Перейдите по ссылке для активации пароля:
    
<?= $activationLink ?><

С уважением,<br><?= \yii::$app->name ?>

