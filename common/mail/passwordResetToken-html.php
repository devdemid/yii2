<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl([
	'users/reset-password',
	'token' => $user->password_reset_token
]);
?>
<div class="password-reset">
    <p>Здравствуйте <?= Html::encode($user->first_name) ?>,</p>
    <p>Перейдите по ссылке для восстановления пароля:</p>
    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
    <p>--<br>С уважением,<br><?= \yii::$app->name ?></p>
</div>
