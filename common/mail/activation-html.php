<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\Clients */

$activationLink = Yii::$app->urlManager->createAbsoluteUrl([
    'reg/activation',
    'token' => $user->activation_code
]);
?>
<div class="password-reset">
    <p>Здравствуйте <?= Html::encode($user->fullname) ?>,</p>
    <p>Перейдите по ссылке для активации пароля:</p>
    <p><?= Html::a(Html::encode($activationLink), $activationLink) ?></p>
    <p>--<br>С уважением,<br><?= \yii::$app->name ?></p>
</div>
