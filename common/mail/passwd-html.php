<?php
use yii\helpers\Html;
?>
<div class="password-reset">
    <p>Здравствуйте <?= Html::encode($nameTogether) ?>,</p>
    <p>Ваш новый пароль: <?= $password ?></p>
    <p>--<br>С уважением,<br><?= \yii::$app->name ?></p>
</div>
