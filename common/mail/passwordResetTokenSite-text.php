<?php

/* @var $this yii\web\View */
/* @var $user common\models\Clients */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl([
	'reg/reset-password',
	'token' => $user->password_reset_token
]);
?>
Здравствуйте <?= $user->fullname ?>,

Перейдите по ссылке для восстановления пароля:

<?= $resetLink ?>

--
С уважением,
<?= \yii::$app->name ?>
