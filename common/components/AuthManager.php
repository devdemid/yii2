<?php
/**
 * @name Hybrid PhpManager
 * All roles are stored in files, and bind the database
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace common\components;

use Yii;
use yii\rbac\Assignment;
use yii\rbac\PhpManager;

class AuthManager extends PhpManager {

    /**
     * @inheritdoc
     * @param  integer
     * @return array
     */
    public function getAssignments($userId) {
        if ($userId && $user = $this->getUser($userId)) {

            // TODO: Here it is possible to implement a number of roles
            $assignment = new Assignment;
            $assignment->userId = $userId;
            $assignment->roleName = $user->group;
            return [$assignment->roleName => $assignment];
        }
        return [];
    }

    /**
     * @inheritdoc
     * @param  string $roleName
     * @param  integer $userId
     * @return object || null
     */
    public function getAssignment($roleName, $userId) {
        if ($userId && $user = $this->getUser($userId)) {

            // TODO: Here it is possible to implement a number of roles
            $assignment = new Assignment;
            $assignment->userId = $userId;
            $assignment->roleName = $user->group;
            return $assignment;
        }
        return null;
    }

    /**
     * @inheritdoc
     * @param  string $role
     * @param  integer $userId
     */
    public function assign($role, $userId) {
        if ($userId && $user = $this->getUser($userId)) {
            $this->setRole($user, $role->name);
        }
    }

    /**
     * @inheritdoc
     * @param  object $role
     * @param  integer $userId
     */
    public function revoke($role, $userId) {
        if ($userId && $user = $this->getUser($userId)) {
            if ($user->group == $role->name) {
                $this->setRole($user, null);
            }
        }
    }

    /**
     * @inheritdoc
     * @param  integer $userId
     */
    public function revokeAll($userId) {
        if ($userId && $user = $this->getUser($userId)) {
            $this->setRole($user, null);
        }
    }

    /**
     * @inheritdoc
     * @param  integer $userId
     * @return null || \yii\web\IdentityInterface || User object
     * @since 1.0
     */
    private function getUser($userId) {
        if (!Yii::$app->user->isGuest && Yii::$app->user->id == $userId) {
            return Yii::$app->user->identity;
        } else {
            return User::findOne($userId);
        }
    }

    /**
     * Determine the role of
     * @param object $user
     * @param string $roleName
     */
    private function setRole($user, $roleName) {
        $user->group = $roleName;
        $user->updateAttributes(['group' => $roleName]);
    }
}