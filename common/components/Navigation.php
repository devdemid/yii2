<?php

namespace common\components;

use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class Navigation extends Component {

    /**
     * Application ID
     * @var string
     */
    private $id;

    /**
     * Full path to the navigation file
     * @var string
     */
    private $pathFile = '/config/nav.json';

    public function __construct($id) {
        $this->id = $id;
        $this->pathFile = Yii::getAlias($id) . $this->pathFile;
    }

    /**
     * Getting the Navigation Array
     * @return array
     */
    public function getItems() {
        if(file_exists($this->pathFile)) {
            return Json::decode(file_get_contents($this->pathFile));
        }

        return [];
    }

    /**
     * Add a new section
     */
    public function add($nav = []) {
        return $this->save($nav);
    }

    public function addItem($item = []) {
        return true;
    }

    public function removeItem($item) {
        return true;
    }

    /**
     * Navigation Information
     * @return array
     */
    Public function getInfo() {
        return [
            'filemtime' => filemtime($this->pathFile)
        ];
    }

    /**
     * Saving an array to a php file
     * @param array $nav
     * @return boolean
     */
    private function save($nav = []) {
        if(is_array($nav)) {
            $fp = fopen($this->pathFile, 'w+');
            fwrite($fp, Json::encode($nav));
            fclose($fp);
        }
    }

}