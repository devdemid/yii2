<?php
/**
 * Running a method to get data for a form
 *
 * NOTE: All fields data will be passed to the method, just as the string returns,
 * the result of the string will be displayed message otherwise the message will be output from the field.
 *
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace common\components;

use Yii;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;

class FormRunMethod {

    /**
     * My action [TEST]
     * @param  array $form_data
     * @return boolean or string
     */
    public static function test($form_data) {
        return Json::encode($form_data);
    }

    /**
     * Response to treatment
     * @param  array $form_data
     * @return boolean or string
     */
    public static function recourse($form_data) {

        // Default message
        $ResponseText = 'К сожалению, по данному номеру ответ не найден.';

        if(ArrayHelper::keyExists('phone', $form_data, false)) {
            $weclaim_video_answer = Yii::$app->db_lk->createCommand(
                'SELECT `users`.`id`, `users`.`phone`, `answer`.`id_user`, `answer`.`txt` AS answer
                 FROM weclaim_users AS users
                 LEFT JOIN weclaim_video_answer AS answer
                 ON `users`.`id` = `answer`.`id_user`
                 WHERE `users`.`phone` = :phone
                 LIMIT 1'
            )

            // Clearing unnecessary characters
            ->bindValue(':phone', preg_replace('/[^0-9\+?]+/i', '', $form_data['phone']))
            ->queryOne();

            if(is_array($weclaim_video_answer)) {
                $ResponseText = $weclaim_video_answer['answer'];
            }

            unset($weclaim_video_answer);
        }

        return $ResponseText;
    }
}