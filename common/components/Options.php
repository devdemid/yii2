<?php
/**
 * @name DB Settings
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace common\components;

use common\models\Settings;
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;

class Options extends Component
{

    /**
     * Parameters to be preprocessed
     */
    const PARAM_COPYRIGHT = 'copyright';

    /**
     * File options
     */
    const PARAM_FILE = 'file';

    /**
     * Parameters already received
     * @var array
     */
    private $params = [];

    /**
     * Get parameters
     * @param string || array
     * @return string || array
     */
    public function get()
    {
        $args = func_get_args();

        if (!count($args)) {
            throw new InvalidConfigException('Do not set the parameter(s)');
        }

        $_params = [];

        foreach ($args as $param) {
            if (!ArrayHelper::keyExists($param, $this->params)) {
                $_params[] = $param;
            }
        }

        if (count($_params)) {
            $settings = Settings::find()
                ->select(['id', 'param', 'value', 'defaultValue', 'type'])
                ->where([
                    'module' => Settings::SETTINGS_MODULE,
                    'param' => array_values($_params),
                ])
                ->indexBy('param')
                ->all();

            foreach ($_params as $item) {
                if (ArrayHelper::keyExists($item, $settings)) {
                    $this->params[$item] = $settings[$item]->value ? $settings[$item]->value : $settings[$item]->defaultValue;

                    if ($item == self::PARAM_COPYRIGHT) {
                        $this->params[$item] = Yii::t('app', $this->params[$item], ['Year' => date('Y')]);
                    }

                    if ($settings[$item]->type == self::PARAM_FILE) {
                        $this->params[$item] = $settings[$item]->file;
                    }

                } else {
                    throw new InvalidConfigException('Undefined parameter ' . $item);
                }
            }

            unset($settings, $_params);
        }

        // self::PARAM_FILE
        if (count($args) == 1) {
            return ArrayHelper::getValue($this->params, $args[0], 'Unknown');
        } else {
            $return = [];

            foreach ($args as $param) {
                $return[$param] = ArrayHelper::getValue($this->params, $param, 'Unknown');
            }

            return $return;
        }
    }

    /**
     * Update parameter values
     * @param array
     * @return boolean
     */
    public function set()
    {
        $args = func_get_args();

        if (!is_array($args) && !count($args)) {
            throw new InvalidConfigException('Do not set the parameter(s)');
        }

        $settings = Settings::find()
            ->select(['param', 'value', 'defaultValue'])
            ->where([
                'module' => Settings::SETTINGS_MODULE,
                'param' => array_keys($args),
            ])
            ->indexBy('param')
            ->all();

        foreach ($settings as $param => $model) {
            $model->param = $args[$param];
            $this->params[$param] = $model->param;
            $model->update();
        }

        unset($settings);
        return count($settings);
    }

    /**
     * Check the existence of a parameter
     * @param  string $param
     * @return boolean
     */
    public function hasParamValue($param)
    {

        if (ArrayHelper::keyExists($param, $this->params)) {
            return true;
        } else {
            $model = Settings::find()->where([
                'module' => Settings::SETTINGS_MODULE,
                'param' => $param,
            ])->one();

            if (!is_null($model)) {
                $this->params[$model->param] = $model->value ? $model->value : $model->defaultValue;
                unset($model);
                return true;
            }
        }

        return false;
    }
}
