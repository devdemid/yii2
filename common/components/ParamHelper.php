<?php
/**
 * Dynamic data acquisition for config
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace common\components;

use Yii;
use common\models\Cities;
use yii\helpers\ArrayHelper;

class ParamHelper {

    /**
     * Get parameters [TEST]
     * @return array
     */
    public static function test() {
        return [
            'ru' => 'Русский',
            'en' => 'English',
            'de' => 'Deutsch',
        ];
    }

    /**
     * Time zones
     * @return array
     */
    public static function timezone() {
        return [];
    }
}