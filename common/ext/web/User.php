<?php

/**
 * Additions to the user object
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace common\ext\web;

class User extends \yii\web\User {

    /**
     * List of custom links
     * @var Array
     */
    public $urls = [];

    /**
     * User links
     * @return Object
     */
    public function getUrl() {
        return (object) $this->urls;
    }
}