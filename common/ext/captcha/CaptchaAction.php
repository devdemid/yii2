<?php
/**
 * Customization of captcha
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace common\ext\captcha;

use Yii;
use yii\captcha\CaptchaAction as DefaultCaptchaAction;

class CaptchaAction extends DefaultCaptchaAction {

    /**
     * Maximum number of characters
     */
    const DEFAULT_MAX_LENGTH = 8;

    /**
     * Minimum number of characters
     */
    const DEFAULT_MIN_LENGTH = 3;

    /**
     * Default characters
     */
    const DEFAULT_CHARACTERS = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

    /**
     * Valid characters for captcha
     */
    const PATTERN_CLEAN_CHARACTERS = '/[^0-9A-z]/i';

    /**
     * @inheritdoc
     * @return string
     */
    protected function generateVerifyCode() {

        if ($this->minLength < self::DEFAULT_MIN_LENGTH) {
            $this->minLength = self::DEFAULT_MIN_LENGTH;
        }

        if ($this->maxLength > self::DEFAULT_MAX_LENGTH) {
            $this->maxLength = self::DEFAULT_MAX_LENGTH;
        }

        $length = mt_rand($this->minLength, $this->maxLength);
        $chr_captcha = preg_replace(self::PATTERN_CLEAN_CHARACTERS, '', Yii::$app->options->get('chr_captcha'));
        $return_chr = '';

        if (!$chr_captcha) {
            $chr_captcha = self::DEFAULT_CHARACTERS;
        }

        for ($i = 0; $i < $length; ++$i) {
            $return_chr .= $chr_captcha[mt_rand(0, 9)];
        }

        return $return_chr;
    }
}