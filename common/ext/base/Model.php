<?php
/**
 * Expansion class Model
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */
namespace common\ext\base;

class Model extends \yii\base\Model {

    /**
     * Adding a prefix if the attribute is required
     * @param  string $attribute
     * @return string
     */
    public function getAttributeLabelRequired($attribute, $css_classname = 'text-danger') {
        $label = $this->getAttributeLabel($attribute) . ':';

        if($this->isAttributeRequired($attribute)) {
            $label .= ' <span class="'. $css_classname .'">*</span>';
        }
        return $label;
    }

    /**
     * Attribute name for placeholder
     * @param  string $attribute
     * @return string
     */
    public function getAttributeLabelPlaceholder($attribute) {
        $label = $this->getAttributeLabel($attribute) . ':';

        if($this->isAttributeRequired($attribute)) {
            $label .= ' *';
        }
        return $label;
    }

    /**
     * Cleaning fields
     * @param  array $names
     */
    public function unsetAttributes($names = null) {
        if($names === null)
            $names = $this->attributes();

        foreach($names as $name)
            $this->$name = null;
    }
}