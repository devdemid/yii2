<?php
/**
 * Validating video resource links
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace common\ext\validators;

use yii\validators\Validator;

class VideoFilter extends Validator {

    /**
     * Checking link
     */
    const PATTERN_VIDEO_URL = '/(http:|https:|)\/\/(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/i';

    /**
     * Override default behavior of validator - do not skip empty values.
     *
     * @var boolean
     */
    public $skipOnEmpty = false;

    /**
     * String the name of the attribute to be translit.
     * @var string
     */
    public $translitAttribute;

    public function validateAttribute($model, $attribute) {
        if($this->skipOnEmpty && !$this->isEmpty($model->$attribute))
            return;

        if(!$this->isEmpty($model->$attribute)) {
            if(!preg_match(self::PATTERN_VIDEO_URL, $model->$attribute))
                $model->addError($attribute, 'Значение "'. $model->getAttributeLabel($attribute) .'" не является правильным URL.');
        }

        return $model->$attribute;
    }

    // public function clientValidateAttribute($model, $attribute, $view) {
    //     //return <<<JS ... JS;
    // }

    /**
     * Receiving Video IDs
     * @param  string $url
     * @return string
     */
    public static function getId($url) {
        preg_match(self::PATTERN_VIDEO_URL, $url, $matches);
        return $matches[6];
    }

    /**
     * Retrieving the name of the video hosting
     * @param  string $url
     * @return string
     */
    public static function getHosting($url) {
        preg_match(self::PATTERN_VIDEO_URL, $url, $matches);
        return $matches[3];
    }

    /**
     * Receiving a link to a screenshot of a video
     * @param  string $url
     * @return string
     */
    public static function getScreenshot($url, $name = 'maxresdefault') {
        return 'http://img.'. self::getHosting($url) .'/vi/'. self::getId($url) .'/'. $name .'.jpg';
    }
}