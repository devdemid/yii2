-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Сен 21 2017 г., 20:17
-- Версия сервера: 5.7.19
-- Версия PHP: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `nc_block`
--

CREATE TABLE `nc_block` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `published` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `nc_block`
--

INSERT INTO `nc_block` (`id`, `name`, `content`, `published`) VALUES
(1, 'Как нас найти', 'Если у вас есть какие-либо дополнительные вопросы или предложения относительно подключения и работы сервиса, вы можете связаться с нами по телефону (с 10:00 до 19:00), по скайпу или электронной почте. А так же следите за новостями и пишите нам в социальных сетях.\r\n\r\nРОССИЯ, Г. МОСКВА\r\nТелефон +7(495) 000-0000\r\nE-mail email@example.com', 1),
(2, 'Информация в подвале сайта', 'Если у вас есть вопросы, просто заполните контактную форму, и мы ответим вам в ближайшее время. Если вы живете неподалеку, приходите к нам.', 1),
(3, 'Контактная информация на стр. контактов', '<p class=\"text-uppercase\">Россия, г. Москва</p>\r\n<dl class=\"list-terms-inline\">\r\n   <dt>Телефон</dt>\r\n   <dd><a href=\"callto:+7(495) 000-0000\" class=\"link-secondary\">+7(495) 000-0000</a></dd>\r\n</dl>\r\n<dl class=\"list-terms-inline\">\r\n   <dt>E-mail</dt>\r\n   <dd>\r\n      <a href=\"mailto:email@example.com\" class=\"link-primary\">\r\n      <span>email@example.com</span>\r\n      </a>\r\n   </dd>\r\n</dl>', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `nc_files`
--

CREATE TABLE `nc_files` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `module` varchar(100) NOT NULL,
  `file` varchar(255) NOT NULL,
  `create_at` varchar(30) NOT NULL,
  `type` varchar(50) NOT NULL,
  `sizes` varchar(255) NOT NULL,
  `basic` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Дамп данных таблицы `nc_files`
--

INSERT INTO `nc_files` (`id`, `item_id`, `module`, `file`, `create_at`, `type`, `sizes`, `basic`) VALUES
(1, 53, 'Slide', '59944680d47ab.jpg', '1502889609', '1', '1920x837,135x70', 0),
(2, 54, 'Slide', '599446a2dbbdb.jpg', '1502889638', '1', '1920x837,135x70', 0),
(3, 55, 'Slide', '599446af1fd5c.jpg', '1502889650', '1', '1920x837,135x70', 0),
(9, 2, 'Settings', '59a43602429b2.png', '1503933954', '1', '', 0),
(30, 13, 'News', '59c3b11472cf4.jpg', '1505997076', '1', '322x219,570x321,870x412,100x100', 0),
(31, 14, 'News', '59c3b19098824.jpg', '1505997200', '1', '322x219,570x321,870x412,100x100', 0),
(32, 15, 'News', '59c3b1a5a455a.jpg', '1505997221', '1', '322x219,570x321,870x412,100x100', 0),
(33, 16, 'News', '59c3b1b98f7e3.jpg', '1505997241', '1', '322x219,570x321,870x412,100x100', 0),
(34, 17, 'News', '59c3b1d005cb7.jpg', '1505997264', '1', '322x219,570x321,870x412,100x100', 0),
(35, 18, 'News', '59c3b1e366cb6.jpg', '1505997283', '1', '322x219,570x321,870x412,100x100', 0),
(36, 19, 'News', '59c3b1f72a9e4.jpg', '1505997303', '1', '322x219,570x321,870x412,100x100', 0),
(37, 20, 'News', '59c3b20a3f19f.jpg', '1505997322', '1', '322x219,570x321,870x412,100x100', 0),
(38, 21, 'News', '59c3b2217ab53.jpg', '1505997345', '1', '322x219,570x321,870x412,100x100', 0),
(39, 22, 'News', '59c3e63293fff.jpg', '1506010674', '1', '100x100', 0),
(41, 23, 'News', '59c3e683cbe2a.jpg', '1506010755', '1', '100x100', 0),
(42, 24, 'News', '59c3e6b25d339.jpg', '1506010802', '1', '100x100', 0),
(43, 25, 'News', '59c3e9f2a91c7.jpg', '1506011634', '1', '100x100', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `nc_form`
--

CREATE TABLE `nc_form` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `to_view` text NOT NULL,
  `from_view` text NOT NULL,
  `success_text` text NOT NULL,
  `success_position` tinyint(2) NOT NULL,
  `reply_message` tinyint(1) NOT NULL,
  `format_mail` tinyint(1) NOT NULL,
  `captcha` tinyint(1) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `run_method` varchar(255) NOT NULL,
  `return_type` tinyint(1) NOT NULL,
  `save_result` tinyint(1) NOT NULL,
  `add_attachments` tinyint(1) NOT NULL,
  `btn_name` varchar(50) NOT NULL,
  `btn_reset` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Дамп данных таблицы `nc_form`
--

INSERT INTO `nc_form` (`id`, `name`, `email`, `to_view`, `from_view`, `success_text`, `success_position`, `reply_message`, `format_mail`, `captcha`, `published`, `run_method`, `return_type`, `save_result`, `add_attachments`, `btn_name`, `btn_reset`) VALUES
(1, 'Мы Вам перезвоним', 'email@example.com', 'Имя клиента: {name}<br>\r\nНомер телефона: {phone}<br>\r\nДата заявки: {datetime}<br>\r\n------------------------------------------------------------------------<br>\r\n{sitename}', '', 'Спасибо \"{name}\"! Ваша заявка успешно принята, наши операторы обязательно с Вами свяжутся.', 1, 0, 0, 0, 1, '', 0, 1, 0, 'Отправить заявку', 0),
(2, 'Есть Вопросы?', 'email@example.com', 'Ваше имя: {name}<br>\r\nВаш Email: {email}<br>\r\nВаш телефон: {phone}<br>\r\nТема сообщения: {theme<br>\r\nВаш вопрос: {message}<br>\r\nДата заявки: {datatime}<br>\r\n------------------------------------------------------------------------<br>\r\n{sitename}<br>\r\n', 'Спасибо {name} Ваш вопрос успешно принят, наши операторы обязательно ответят на все Ваши вопросы.<br>\r\n------------------------------------------------------------------------<br>\r\n{sitename}', 'Спасибо {name} Ваш вопрос успешно принят, наши операторы обязательно ответят на все Ваши вопросы.', 1, 1, 0, 0, 1, '', 0, 1, 0, '', 1),
(3, 'Получить ответ на обращение', 'email@example.com', 'Телефон: {phone}\r\nДата проверки: {datatime}<br>\r\n------------------------------------------------------------------------<br>\r\n{sitename}', '', 'Спасибо, Ваша жалоба рассмотрена и все необходимые меры будут приняты!', 2, 0, 0, 0, 1, 'recourse', 1, 0, 0, 'Получить', 0),
(4, 'Тестовая форма всех типов полей', 'email@example.com', 'Тест сообщения для получателя<br>\r\n------------------------------------------------------------------------------------------------------------<br>\r\nИдентификатор записи в базе данных: {id}<br>\r\nФлажок: {flag}<br>\r\nEmail: {email}<br>\r\nФайл: {file}<br>\r\nЧисло: {integer}<br>\r\nТелефон: {phone}<br>\r\nВыберите: {list}<br>\r\nВыберите в списке: {list2}<br>\r\nУникальное поле: {email2}<br>\r\nТекст: {text}<br>\r\n------------------------------------------------------------------------------------------------------------<br>\r\nМного текста: {text2}<br>\r\n------------------------------------------------------------------------------------------------------------<br>\r\nДата в формате \"дд.мм.гггг\": {date}<br>\r\nДата и время в формате \"дд.мм.гггг чч.мм.сс\": {datetime}<br>\r\nКраткое название сайта: {sitename}<br>\r\nКопирайт сайта: {copyright}<br>\r\n\r\n', 'Тест сообщение ответа<br>\r\n------------------------------------------------------------------------------------------------------------<br>\r\nИдентификатор записи в базе данных: {id}<br>\r\nФлажок: {flag}<br>\r\nEmail: {email}<br>\r\nФайл: {file}<br>\r\nЧисло: {integer}<br>\r\nТелефон: {phone}<br>\r\nВыберите: {list}<br>\r\nВыберите в списке: {list2}<br>\r\nУникальное поле: {email2}<br>\r\nТекст: {text}<br>\r\n------------------------------------------------------------------------------------------------------------<br>\r\nМного текста: {text2}\\r\\n\r\n------------------------------------------------------------------------------------------------------------<br>\r\nДата в формате \"дд.мм.гггг\": {date}<br>\r\nДата и время в формате \"дд.мм.гггг чч.мм.сс\": {datetime}<br>\r\nКраткое название сайта: {sitename}<br>\r\nКопирайт сайта: {copyright}<br>\r\n\r\n', 'Спасибо! {email} Тест пройден успешно!', 1, 1, 0, 1, 0, '', 0, 1, 1, 'Тестировать', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `nc_form_field`
--

CREATE TABLE `nc_form_field` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `attribute` varchar(50) NOT NULL,
  `hint` varchar(535) NOT NULL,
  `label` varchar(255) NOT NULL,
  `list` text NOT NULL,
  `type` enum('checkbox','email','file','integer','phone','radio','select','string','textarea') NOT NULL,
  `extension` varchar(255) NOT NULL,
  `size` int(11) NOT NULL,
  `sorting` int(11) NOT NULL,
  `unique` tinyint(1) NOT NULL,
  `required` tinyint(1) NOT NULL,
  `placeholder` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `nc_form_field`
--

INSERT INTO `nc_form_field` (`id`, `item_id`, `attribute`, `hint`, `label`, `list`, `type`, `extension`, `size`, `sorting`, `unique`, `required`, `placeholder`) VALUES
(1, 2, 'name', '', 'Ваше имя', '', 'string', '', 0, 1, 0, 1, 1),
(2, 2, 'email', '', 'Ваш Email', '', 'email', '', 0, 2, 0, 1, 1),
(3, 2, 'phone', '', 'Ваш телефон', '', 'phone', '', 0, 3, 0, 0, 1),
(4, 2, 'message', '', 'Ваш вопрос', '', 'textarea', '', 0, 5, 0, 1, 1),
(5, 1, 'name', '', 'Ваше имя', '', 'string', '', 0, 1, 0, 1, 1),
(6, 1, 'phone', '', 'Номер телефона', '', 'phone', '', 0, 2, 0, 1, 1),
(7, 3, 'phone', '', 'Номер телефона', '', 'phone', '', 0, 1, 0, 1, 1),
(14, 2, 'theme', '', 'Тема сообщения', 'Заявка на подключение,Технические вопросы,Предложения о сотрудничестве', 'select', '', 1, 4, 0, 1, 1),
(18, 4, 'flag', '', 'Флажок', '', 'checkbox', '', 5, 2, 0, 1, 0),
(19, 4, 'email', '', 'Email', '', 'email', '', 5, 1, 0, 1, 0),
(20, 4, 'file', '', 'Файл', '', 'file', 'pdf,xml,jpg,jpeg,png,docx', 10, 3, 0, 1, 0),
(21, 4, 'integer', '', 'Число', '', 'integer', '', 5, 4, 0, 1, 0),
(22, 4, 'phone', '', 'Телефон', '', 'phone', '', 5, 5, 0, 1, 0),
(23, 4, 'list', '', 'Выберите', 'Один,Два,Три', 'radio', '', 5, 6, 0, 1, 0),
(24, 4, 'list2', '', 'Выберите в списке', 'Заявка на подключение,Технические вопросы,Предложения о сотрудничестве', 'select', '', 5, 7, 0, 1, 0),
(25, 4, 'text', '', 'Текст', '', 'string', '', 5, 8, 0, 1, 0),
(26, 4, 'text2', '', 'Много текста', '', 'textarea', '', 5, 9, 0, 0, 0),
(27, 4, 'email2', '', 'Уникальное поле', '', 'phone', '', 5, 10, 1, 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `nc_form_result`
--

CREATE TABLE `nc_form_result` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `json_result` mediumtext NOT NULL,
  `date` varchar(19) NOT NULL,
  `to_email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Дамп данных таблицы `nc_form_result`
--

INSERT INTO `nc_form_result` (`id`, `item_id`, `json_result`, `date`, `to_email`) VALUES
(24, 1, '[{\"attribute\":\"name\",\"label\":\"Ваше имя\",\"type\":\"string\",\"value\":\"Иванов Иван Иванович\"},{\"attribute\":\"phone\",\"label\":\"Номер телефона\",\"type\":\"phone\",\"value\":\"+7 365 322 33 22\"}]', '2017-09-08 20:00:58', 'email@example.com'),
(92, 4, '[{\"attribute\":\"email\",\"label\":\"Email\",\"type\":\"email\",\"value\":\"demid@google.com\"},{\"attribute\":\"flag\",\"label\":\"Флажок\",\"type\":\"checkbox\",\"value\":\"Да\"},{\"attribute\":\"file\",\"label\":\"Файл\",\"type\":\"file\",\"value\":\"59baba357e1e0.png\"},{\"attribute\":\"integer\",\"label\":\"Число\",\"type\":\"integer\",\"value\":\"5000000\"},{\"attribute\":\"phone\",\"label\":\"Телефон\",\"type\":\"phone\",\"value\":\"+7 365 322 33 22\"},{\"attribute\":\"list\",\"label\":\"Выберите\",\"type\":\"radio\",\"value\":\"Один\"},{\"attribute\":\"list2\",\"label\":\"Выберите в списке\",\"type\":\"select\",\"value\":\"Заявка на подключение\"},{\"attribute\":\"text\",\"label\":\"Текст\",\"type\":\"string\",\"value\":\"Почему он используется?\"},{\"attribute\":\"text2\",\"label\":\"Много текста\",\"type\":\"textarea\",\"value\":\"Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).\"},{\"attribute\":\"email2\",\"label\":\"Уникальное поле\",\"type\":\"phone\",\"value\":\"+7 365 322 33 22\"}]', '2017-09-14 20:19:49', 'email2@example.com,email@example.com'),
(105, 2, '[{\"attribute\":\"name\",\"label\":\"Ваше имя\",\"type\":\"string\",\"value\":\"Иванов Иван Иванович\"},{\"attribute\":\"email\",\"label\":\"Ваш Email\",\"type\":\"email\",\"value\":\"email@example.com\"},{\"attribute\":\"phone\",\"label\":\"Ваш телефон\",\"type\":\"phone\",\"value\":\"+7 365 322 33 22\"},{\"attribute\":\"theme\",\"label\":\"Тема сообщения\",\"type\":\"select\",\"value\":\"Заявка на подключение\"},{\"attribute\":\"message\",\"label\":\"Ваш вопрос\",\"type\":\"textarea\",\"value\":\"Проверка  формы\"}]', '2017-09-19 15:28:14', 'email@example.com'),
(106, 1, '[{\"attribute\":\"name\",\"label\":\"Ваше имя\",\"type\":\"string\",\"value\":\"Иванов Иван Иванович\"},{\"attribute\":\"phone\",\"label\":\"Номер телефона\",\"type\":\"phone\",\"value\":\"+7 365 322 33 22\"}]', '2017-09-20 00:17:28', 'email@example.com'),
(107, 1, '[{\"attribute\":\"name\",\"label\":\"Ваше имя\",\"type\":\"string\",\"value\":\"Петров Иван\"},{\"attribute\":\"phone\",\"label\":\"Номер телефона\",\"type\":\"phone\",\"value\":\"+7777777777\"}]', '2017-09-21 19:42:48', 'email@example.com');

-- --------------------------------------------------------

--
-- Структура таблицы `nc_menu`
--

CREATE TABLE `nc_menu` (
  `id` int(11) NOT NULL,
  `type` tinyint(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `published` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `nc_menu`
--

INSERT INTO `nc_menu` (`id`, `type`, `name`, `published`) VALUES
(1, 0, 'Главное меню в шапке сайта', 1),
(2, 0, 'Меню в подвале \"Полезное\"', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `nc_menu_tree`
--

CREATE TABLE `nc_menu_tree` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `css_class` varchar(100) NOT NULL,
  `sorting` int(11) NOT NULL DEFAULT '0',
  `target_blank` tinyint(1) NOT NULL,
  `published` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `nc_menu_tree`
--

INSERT INTO `nc_menu_tree` (`id`, `item_id`, `parent_id`, `name`, `url`, `css_class`, `sorting`, `target_blank`, `published`) VALUES
(1, 1, 0, 'Главная', '/', '', 1, 0, 1),
(2, 1, 0, 'О проекте', '/o-proekte', '', 2, 0, 1),
(3, 1, 0, 'Заявка', '#', '', 3, 0, 1),
(4, 1, 0, 'Контакты', '/contact', '', 4, 0, 1),
(5, 2, 0, 'Пресса о нас', '/news/pressa-o-nas', '', 1, 0, 1),
(6, 2, 0, 'Меню 2', '#', '', 3, 0, 1),
(7, 2, 0, 'Меню 3', '#', '', 4, 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `nc_message`
--

CREATE TABLE `nc_message` (
  `id` int(11) NOT NULL DEFAULT '0',
  `language` varchar(16) NOT NULL DEFAULT '',
  `translation` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Структура таблицы `nc_migration`
--

CREATE TABLE `nc_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Структура таблицы `nc_news`
--

CREATE TABLE `nc_news` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `cat_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `short_description` text NOT NULL,
  `full_description` text NOT NULL,
  `meta_description` varchar(200) NOT NULL,
  `meta_keywords` varchar(250) NOT NULL,
  `date` varchar(19) NOT NULL,
  `fields` mediumtext NOT NULL,
  `sorting` int(11) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `views` int(11) NOT NULL,
  `fixed` tinyint(1) NOT NULL,
  `noindex` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `nc_news_feed`
--

CREATE TABLE `nc_news_feed` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `meta_description` varchar(200) NOT NULL,
  `meta_keywords` varchar(250) NOT NULL,
  `field_options` text NOT NULL,
  `img_sizes` varchar(255) NOT NULL,
  `view` varchar(100) NOT NULL,
  `img_enable` tinyint(1) NOT NULL,
  `is_count_views` tinyint(1) NOT NULL,
  `published` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Структура таблицы `nc_page`
--

CREATE TABLE `nc_page` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `full_description` text NOT NULL,
  `meta_description` varchar(200) NOT NULL,
  `meta_keywords` varchar(250) NOT NULL,
  `noindex` tinyint(1) NOT NULL,
  `published` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `nc_settings`
--

CREATE TABLE `nc_settings` (
  `id` int(11) NOT NULL,
  `module` varchar(100) NOT NULL,
  `group` varchar(50) NOT NULL,
  `param` varchar(100) NOT NULL,
  `value` text NOT NULL,
  `defaultValue` text NOT NULL,
  `mask` varchar(50) NOT NULL,
  `label` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `placeholder` varchar(255) NOT NULL,
  `type` enum('string','textarea','checkbox','switcher','radio','editor','select','select_multiple','file','files') NOT NULL,
  `run_method` varchar(50) NOT NULL,
  `ordering` int(11) NOT NULL,
  `options` varchar(255) NOT NULL,
  `disabled` tinyint(1) NOT NULL,
  `fixed` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Дамп данных таблицы `nc_settings`
--

INSERT INTO `nc_settings` (`id`, `module`, `group`, `param`, `value`, `defaultValue`, `mask`, `label`, `description`, `placeholder`, `type`, `run_method`, `ordering`, `options`, `disabled`, `fixed`) VALUES
(1, 'settings', 'Основные', 'offline', '0', '0', '', 'Сайт выключен', 'Перевести сайт в состояние offline. Например: для проведения технических работ.', '', 'switcher', '', 1, '', 0, 1),
(2, 'settings', 'Основные', 'offline_message', '<p style=\"text-align: center;\">\r\n	<img src=\"http://yii.lan/files/settings/2/9/59a43602429b2.png\">\r\n</p><p style=\"text-align: center;\">\r\n	<strong>Сайт находится на текущей реконструкции, после завершения всех работ сайт будет открыт.<br></strong>\r\n	Приносим вам свои извинения за доставленные неудобства.\r\n</p>', 'Сайт находится на текущей реконструкции, после завершения всех работ сайт будет открыт.\r\n\r\nПриносим вам свои извинения за доставленные неудобства.', '', 'Причина отключения сайта', 'Сообщение для отображения в режиме отключенного сайта', '', 'editor', '', 2, '', 0, 1),
(3, 'settings', 'Настройки E-Mail', 'system_email', 'email@example.com', '', '', 'Системный E-Mail адрес администратора', 'Введите E-Mail адрес администратора сайта. От имени данного адреса будут отправляться служебные сообщения скрипта, например уведомления для пользователей. А также на этот адрес будут отправляться системные уведомления для администрации сайта.', 'name@example.com', 'string', '', 3, '', 0, 1),
(4, 'settings', 'Основные', 'meta_description', 'Повышение качества обслуживания – залог успешного бизнеса. Современная книга жалоб WeClaim позволяет потребителям без труда подать жалобу, а руководителям – оперативно ответить на обращение', '', '', 'Описание (Description) сайта', 'Краткое описание до 200 (200 — максимум, ориентируйтесь на ударные первые 100 знаков).', '', 'string', '', 4, '', 0, 1),
(5, 'settings', 'Основные', 'meta_keywords', '', '', '', 'Ключевые слова (Keywords) для сайта', 'Введите через запятую основные ключевые слова для вашего сайта до 250 (250 — максимум, ориентируйтесь на ударные первые 150 знаков).', '', 'textarea', '', 6, '', 0, 1),
(6, 'settings', 'Основные', 'home_title', 'Сервис YII: оперативный способ подать жалобу', '', '', 'Краткое название сайта', 'Укажите краткое название сайта, которое будет отображаться в title, при отправке писем', 'Мой новый сайт', 'string', '', 5, '', 0, 1),
(7, 'settings', 'Основные', 'copyright', '2014 - {Year}  ©  Yii2.  Все права защищены', '', '', 'Копирайт', 'Копирайт (copyright - \"право на воспроизведение\") - это форма защиты интеллектуальной собственности.', '{Year} © Все права защищены', 'string', '', 7, '', 0, 1),
(8, 'settings', 'Медиа', 'logo', '', '', '', 'Логотип', 'Здесь можно загрузить новое изображение логотипа', '', 'files', '', 2, '', 0, 1),
(9, 'settings', 'Безопасность', 'chr_captcha', '0123456789', 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', '', 'Символы капчи', 'Доступные символы для капчи, которые будут отображены на изображении. Разрешено только: <code>/[^0-9A-z]/</code>', '0123456789', 'string', '', 0, '', 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `nc_slide`
--

CREATE TABLE `nc_slide` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `video` varchar(200) NOT NULL,
  `fields` mediumtext NOT NULL,
  `target` tinyint(1) NOT NULL,
  `sorting` int(11) NOT NULL,
  `published` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;


-- --------------------------------------------------------

--
-- Структура таблицы `nc_slider`
--

CREATE TABLE `nc_slider` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `field_options` text NOT NULL,
  `width` int(5) NOT NULL,
  `height` int(5) NOT NULL,
  `autoplay` tinyint(1) NOT NULL,
  `published` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Структура таблицы `nc_source_message`
--

CREATE TABLE `nc_source_message` (
  `id` int(11) NOT NULL,
  `category` varchar(255) DEFAULT NULL,
  `message` text,
  `str_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Структура таблицы `nc_users`
--

CREATE TABLE `nc_users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `sex` tinyint(1) NOT NULL DEFAULT '0',
  `second_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `group` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `last_auth_ip` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `created_timestamp` int(11) NOT NULL,
  `updated_timestamp` int(11) NOT NULL,
  `auth_timestamp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Дамп данных таблицы `nc_users`
--

INSERT INTO `nc_users` (`id`, `username`, `first_name`, `sex`, `second_name`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `group`, `last_auth_ip`, `created_timestamp`, `updated_timestamp`, `auth_timestamp`) VALUES
(1, 'Demid', 'Jon', 1, 'Doe', 'b3TOujsXFjo6RWZ-MOh9ozN37277Xb7Z', '$2y$13$BJ5UxQ9dNsUN89A1hn12kOmNloeG4Uh6VqtGA8UcZstWuBGOUPiwS', 'sx84MK3eCYwQFEb9sA5_b8h9WrZF8_1b_1504634509', 'demid@example.com', 1, 'root', '127.0.0.1', 1447418826, 1505935176, 1505935176);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `nc_block`
--
ALTER TABLE `nc_block`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `nc_files`
--
ALTER TABLE `nc_files`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `nc_form`
--
ALTER TABLE `nc_form`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `nc_form_field`
--
ALTER TABLE `nc_form_field`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `nc_form_result`
--
ALTER TABLE `nc_form_result`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `nc_menu`
--
ALTER TABLE `nc_menu`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `nc_menu_tree`
--
ALTER TABLE `nc_menu_tree`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `nc_message`
--
ALTER TABLE `nc_message`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_message_language` (`language`);

--
-- Индексы таблицы `nc_migration`
--
ALTER TABLE `nc_migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `nc_news`
--
ALTER TABLE `nc_news`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `nc_news_feed`
--
ALTER TABLE `nc_news_feed`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `nc_page`
--
ALTER TABLE `nc_page`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `nc_settings`
--
ALTER TABLE `nc_settings`
  ADD UNIQUE KEY `id` (`id`);

--
-- Индексы таблицы `nc_slide`
--
ALTER TABLE `nc_slide`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `nc_slider`
--
ALTER TABLE `nc_slider`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `nc_source_message`
--
ALTER TABLE `nc_source_message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_source_message_category` (`category`);

--
-- Индексы таблицы `nc_users`
--
ALTER TABLE `nc_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `nc_block`
--
ALTER TABLE `nc_block`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `nc_files`
--
ALTER TABLE `nc_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT для таблицы `nc_form`
--
ALTER TABLE `nc_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `nc_form_field`
--
ALTER TABLE `nc_form_field`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT для таблицы `nc_form_result`
--
ALTER TABLE `nc_form_result`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;
--
-- AUTO_INCREMENT для таблицы `nc_menu`
--
ALTER TABLE `nc_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `nc_menu_tree`
--
ALTER TABLE `nc_menu_tree`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT для таблицы `nc_news`
--
ALTER TABLE `nc_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT для таблицы `nc_news_feed`
--
ALTER TABLE `nc_news_feed`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `nc_page`
--
ALTER TABLE `nc_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `nc_settings`
--
ALTER TABLE `nc_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `nc_slide`
--
ALTER TABLE `nc_slide`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT для таблицы `nc_slider`
--
ALTER TABLE `nc_slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `nc_source_message`
--
ALTER TABLE `nc_source_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `nc_users`
--
ALTER TABLE `nc_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `nc_message`
--
ALTER TABLE `nc_message`
  ADD CONSTRAINT `fk_message_source_message` FOREIGN KEY (`id`) REFERENCES `nc_source_message` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
