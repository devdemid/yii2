<?php
/**
 * @name Dependency update
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace console\controllers;

use Yii;
use yii\console\Controller;
use \yii\helpers\ArrayHelper;

class RbacController extends Controller {

    /**
     * All roles
     * @var array
     */
    public $roles = [];

    /**
     * All permissions
     * @var array
     */
    public $permissions = [];

    /**
     * All inheritances
     * @var array
     */
    public $inheritances = [];

    // Default configure RBAC
    public function __construct($id, $module, $config = []) {

        // Create roles
        $this->roles = [
            'root'      => 'Разработчик',
            'admin'     => 'Администратор',
            'client'    => 'Клиент',
            //'moderator' => 'Модератор',
            //'user'      => 'Пользователь',
            'guest'     => 'Гость',
        ];

        // Create Permissions
        $this->permissions = [

            // Settings
            'editSettings'       => 'Доступ к настройкам системы',
            'createSettings'     => 'Создавать настройки',

            // Users
            'editAccess'         => 'Доступ к групам пользователей',
            'changePassword'     => 'Смена пароля учетной записи',
            'lockingProfiles'    => 'Блокировка учетной записи',
            'changeProfileGroup' => 'Изменить группу учетной записи',
            'accessProfile'      => 'Доступ к профилю учетной записи',

            // Sliders
            'accessSliders'      => 'Доступ к генератору слайдеров',

            // Forms
            'accessSettingForm'  => 'Доступ к настройкам форм',
            'readingResultForm'  => 'Доступ к результатам форм',

            // News
            'accessNews'         => 'Доступ к новостям',
            'accessNewsFeed'     => 'Доступ к новостным лентам',

            // Page
            'accessPage'         => 'Доступ к статическим страницам',

            // Block
            'accessBlock'        => 'Доступ к управлением блоками',

            // Menu
            'accessMenu'         => 'Доступ к конструктору меню',

            'accessSeo'          => 'Доступ к настройкам SEO'
        ];

        // Create Inheritances
        $this->inheritances = [
            // Permissions superuser
            'root' => array_keys($this->permissions),

            // Permissions admin
            'admin' => [
                'editAccess',
                'editSettings',
                'accessSliders',
                'client'
                //'user', // Role permissions inheritance
            ],
        ];

        parent::__construct($id, $module, $config);
    }

    // RBAC Init
    // @command: yii rbac/init
    public function actionInit() {
        $authManager = \Yii::$app->getAuthManager();
        $authManager->removeAll();

        $this->actionRoles();
        $this->actionPermissions();
        $this->actionInheritances();
    }

    // Create roles
    // @command: yii rbac/roles
    public function actionRoles() {
        $authManager = \Yii::$app->getAuthManager();
        $authManager->removeAllRoles();

        foreach ($this->roles as $role => $description) {
            $role = $authManager->createRole($role);
            $role->description = $description;
            $authManager->add($role);
        }

        $this->stdout('Roles Done!' . PHP_EOL);
    }

    // Create permissions
    // @command: yii rbac/permissions
    public function actionPermissions() {
        $authManager = \Yii::$app->getAuthManager();
        $authManager->removeAllPermissions();

        foreach ($this->permissions as $permission => $description) {
            $permission = $authManager->createPermission($permission);
            $permission->description = $description;
            $authManager->add($permission);
        }

        $this->stdout("Permissions Done!" . PHP_EOL);
    }

    // Create inheritances
    // @command: yii rbac/inheritances
    public function actionInheritances() {
        $authManager = \Yii::$app->getAuthManager();
        $rbacRoles = $authManager->getRoles();

        foreach ($rbacRoles as $role => $value) {
            $authManager->removeChildren($authManager->getItem($role));
        }

        foreach ($this->inheritances as $role => $permissions) {
            if (ArrayHelper::isTraversable($permissions)) {
                foreach ($permissions as $permission) {
                    $authManager->addChild(
                        $authManager->getItem($role),
                        $authManager->getItem($permission)
                    );
                }
            }
        }

        $this->stdout("Inheritances Done!" . PHP_EOL);
    }
}