<?php
/**
 * Add an optional field
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace backend\components\actions;

use backend\models\NewExtendField;
use backend\widgets\Notify;
use Yii;
use yii\base\Action;
use yii\helpers\Html;

class AddFieldAction extends Action {

    /**
     * The model in which the settings are stored
     * @var string
     */
    public $model;

    /**
     * The name of the field in which the array of dynamic field settings is stored
     * @var string
     */
    public $property;

    public function run() {
        if (Yii::$app->request->isAjax) {
            if ($this->model) {
                $model = $this->model::findOne((int) Yii::$app->request->get('id'));
                $model_options = new NewExtendField;

                if (is_null($model)) {
                    return Notify::danger('Не удалось найти запись по id');
                }

                $property = $this->property;
                $model_options->options = $model->$property;

                if ($model_options->load(Yii::$app->request->get()) && $model_options->validate()) {
                    $model->$property = $model_options->getAttrExtend();
                    $model->update();
                    return Notify::success('Новое поле успешно добавенно!', ['reload' => true]);
                } else {
                    return Notify::danger(Html::errorSummary($model_options));
                }
            }
        } else die('Access denied');
    }
}