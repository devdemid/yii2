<?php
/**
 * Edit additional field
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace backend\components\actions;

use Yii;
use yii\base\Action;

class EditFieldAction extends Action {
    public function run() {}
}