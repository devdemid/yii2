<?php
/**
 * Delete additional field
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace backend\components\actions;

use backend\widgets\Notify;
use Yii;
use yii\base\Action;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class RemoveFieldAction extends Action {

    /**
     * The model in which the settings are stored
     * @var string
     */
    public $model;

    /**
     * The name of the field in which the array of dynamic field settings is stored
     * @var string
     */
    public $property;

    public function run() {
        if (Yii::$app->request->isAjax) {
            if ($this->model) {
                $model = $this->model::findOne((int) Yii::$app->request->get('id'));

                if (is_null($model)) {
                    return Notify::danger('Не удалось найти запись по id');
                }

                $property = $this->property;
                $options = ArrayHelper::index(Json::decode($model->$property), function ($element) {
                    return md5($element['param']);
                });

                $key = Yii::$app->request->get('key');

                if (ArrayHelper::keyExists($key, $options) && ArrayHelper::remove($options, $key)) {
                    $model->$property = Json::encode(array_values($options));
                    $model->update();
                    return Notify::success('Динамическое поле успешно удалено!', ['key' => $key]);
                }

                return Notify::danger('Ошибка. Не удалось найти поле!');
            }
        } else die('Access denied');
    }
}