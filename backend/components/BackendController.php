<?php
/**
 * Сontroller for convenience
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace backend\components;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use common\models\User;

class BackendController extends Controller {

    // Default page title
    public $smallTitle;
    public $navigation = [];

    /**
     * Menu bar at the top
     * @var array
     */
    public $headMenu = [];

    /**
     * Bread crumbs
     * @var array
     */
    public $breadcrumbs = [];

    /**
     * Bread crumbs manu
     * @var array
     */
    public $actionsLink = [];

    /**
     * Template wrapper
     */
    const LAYOUT_WRAPPER_NAME = 'wrap';

    /**
     * Template blank
     */
    const LAYOUT_BLANK_NAME = 'blank';

    /**
     * CMS Version
     */
    const NC_VERSION = '1.0.0';

    /**
     * By default, the first stage distribute access
     * @return array
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'signin', 'request-password-reset', 'reset-password', 'error', 'singup',
                            'cron' // <- Loading TV events through the cron
                        ], // signup
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function __construct($id, $module, $config = []) {
        //\yii\helpers\VarDumper::dump([$id, $module, $config]);

        // Set formatter locale
        //Yii::$app->formatter->locale = 'ru-RU';
        parent::__construct($id, $module, $config);
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action) {
        if (parent::beforeAction($action)) {

            // Prevent authorization for all clients
            if(!is_null(Yii::$app->user->identity) && Yii::$app->user->identity->group == User::DEFAULT_USER_GROUP) {
                Yii::$app->user->logout();
                return $this->goHome();
            }

            // Private variable
            $_actions = ['signin', 'signup', 'request-password-reset', 'reset-password', 'error'];

            // Apply a wrapper
            if (ArrayHelper::isIn($action->id, $_actions)) {
                $this->layout = self::LAYOUT_WRAPPER_NAME;
            }

            // Set the number of items per page
            if (isset($_GET['per-page']) && intval($_GET['per-page'])) {
                $this->pageSize = (int) $_GET['per-page'];
            }

            return true;
        }
        return false;
    }

    /**
     * Opening modules by button type "btnAction" saving data
     * @param  string $url
     * @param  string $name
     * @return boolean
     */
    public function redirectBtnAction($url = false, $name = false) {
        if($url && $name)
            Url::remember($url, $name);

        $btnAction = Yii::$app->request->post('btnAction');
        if (!is_null($btnAction) && Url::previous($btnAction)) {
            return $this->redirect(Url::previous($btnAction));
        }

        throw new \yii\web\HttpException(400, 'Bad Request, Unknown redirect "btnAction"');
    }
}