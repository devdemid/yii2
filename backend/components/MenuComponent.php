<?php
/**
 * Component admin panel menu
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace backend\components;

use yii\base\Component;

class MenuComponent extends Component {

    /**
     * @var string
     */
    public $label;

    /**
     * @var string
     */
    public $url;

    /**
     * @var string
     */
    public $icon;

    /**
     * Sub Menu
     * @var array
     */
    public $items;

    /**
     * Target blank
     * @var Boolean
     */
    public $target;
}