<?php
/*
 * Finding data in the model
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */
namespace backend\models;

use yii\base\Model;

class Search extends Model {

    /**
     * The default text
     */
    const DEFAULT_BLANK = '';

    /**
     * The text you want to search
     * @var string
     */
    public $text;

    /**
     * @inheritdoc
     * @return array
     */
    public function rules() {
        return [
            ['text', 'filter', 'filter' => 'trim'],
            ['text', 'string'],
        ];
    }

    /**
     * Apply text search
     * @param  string $text
     * @return string
     */
    public function apply($text) {
        $this->text = $text;

        if (!$this->validate()) {
            $this->text = self::DEFAULT_BLANK;
        }
        return $this->text;
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels() {
        return [
            'text' => 'Поиск',
        ];
    }
}