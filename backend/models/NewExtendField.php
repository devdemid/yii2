<?php
/**
 * Model of the format of additional fields
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace backend\models;

use yii\base\Model;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use backend\models\Field;

class NewExtendField extends Model {

    public $options;

    /**
     * Field Type
     * @var string
     */
    public $type;

    /**
     * Attribute Label
     * @var string
     */
    public $label;

    /**
     * Param
     * @var string
     */
    public $param;
    /**
     * Obligatory field
     * @var boolean
     */
    public $required;

    /**
     * Short description under the field
     * @var string
     */
    public $hint;

    /**
     * List of values for select
     * @var String
     */
    public $list;

    /**
     * Pattern for the attribute label
     */
    const PATTERN_LABEL = '/([А-яA-z0-9\s\-]+)/i';

    /**
     * Pattern for the parameter
     */
    const PATTERN_PARAM = '/^([A-z0-9\_]+)$/i';

    /**
     * Checking the validity of the lists
     */
    const PATTERN_LIST = '/^[A-zА-я0-9\s]+(,[A-zА-я0-9\s]+)*$/ui';

    /**
     * @inheritdoc
     * @return array
     */
    public function rules() {
        return [
            [['type', 'label', 'param'], 'required'],
            [['label', 'param'], 'filter', 'filter' => 'trim'],
            [
                'label',
                'match',
                'pattern' => self::PATTERN_LABEL,
                'message' => '"{attribute}" неверное имя, разрешено только [А-яA-z0-9:]'
            ],
            [
                'param',
                'match',
                'pattern' => self::PATTERN_PARAM,
                'message' => '"{attribute}" неверное имя, разрешено только [A-z0-9\_]'
            ],
            ['param', 'string', 'length' => [2, 15]],
            ['label', 'string', 'length' => [2, 30]],
            ['hint', 'string', 'max' => '255'],
            ['type', 'string', 'when' => function ($model) {
                return ArrayHelper::keyExists($model->type, Field::getTypes());
            }],

            [
                'list',
                'match',
                'pattern' => self::PATTERN_LIST,
                'message' => '"{attribute}" неверный формат данных, разрешено только [0-9A-zА-я\s]',
            ],

            ['type', function ($attribute, $params, $validator) {
                if($this->type == Field::TYPE_SELECT) {
                    if(empty($this->list)) {
                        $this->addError('list', '"'. $this->getAttributeLabel('list') .'" не может быть пустым!');
                    }
                }
            }],

            ['required', 'in', 'range' => [0, 1]]
        ];
    }

    /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeValidate() {
        if($this->options) {
            $this->options = Json::decode($this->options);

            if(is_array($this->options)) {
                $params = ArrayHelper::getColumn($this->options, 'param');
                if(ArrayHelper::isIn($this->param, $params)) {
                    $this->addError('param', '"'. $this->getAttributeLabel('param') .'" - дополнительное поле с таким параметром уже есть!');
                    return false;
                }
            }
        }
        return parent::beforeValidate();
    }

    /**
     * Add new field
     * @return string
     */
    public function getAttrExtend() {
        $this->options = ArrayHelper::merge($this->options, [
            $this->getAttributes(['param', 'label', 'type', 'required', 'hint', 'list'])
        ]);
        return Json::encode($this->options);
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels() {
        return [
            'hint' => 'Описание под параметром',
            'label' => 'Метка атрибута',
            'param' => 'Параметр',
            'required' => 'Обязательное поле',
            'type' => 'Тип поля',
            'list' => 'Список значений',
        ];
    }

    /**
     * Adding a prefix if the attribute is required
     * @param  string $attribute
     * @return string
     */
    public function getAttributeLabelRequired($attribute) {
        $label = $this->getAttributeLabel($attribute) . ':';

        if($this->isAttributeRequired($attribute)) {
            $label .= ' *';
        }
        return $label;
    }
}