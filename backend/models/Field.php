<?php
/**
 * Additional fields for different modules
 *
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace backend\models;

use common\models\Files;
use Yii;
use yii\base\DynamicModel;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\imagine\Image;
use \yii\helpers\StringHelper;
use \yii\web\UploadedFile;

class Field extends DynamicModel {

    /**
     * Attribule labels list
     * @var array
     */
    private $attributeLabels = [];

    // The types of fields
    const TYPE_EDITOR = 'editor';
    const TYPE_FILE = 'file';
    const TYPE_IMAGE = 'image';
    const TYPE_STRING = 'string';
    const TYPE_TEXTAREA = 'textarea';
    const TYPE_CHECKBOX = 'checkbox';
    const TYPE_SELECT = 'select';

    // Unique field
    const RULE_UNIQUE = 'unique';

    // Required field
    const RULE_REQUIRED = 'required';

    /**
     * Setting the height of the textarea field
     */
    const ROWS_TEXTAREA = 10;

    /**
     * Maximum length of fields type [ string and textarea ]
     */
    const TYPE_STRING_MAX = 255;
    const TYPE_TEXTAREA_MAX = 65535;

    /**
     * The default value if the new record
     */
    const DEFAULT_NEW_VALUE = '[]';

    /**
     * Separator for lists
     */
    const LIST_SEPARATOR = ',';

    /**
     * Image formats that need to be cleaned just in case
     * @var array
     */
    private $images_filter = ['png', 'jpg', 'jpeg', 'gif'];

    /**
     * Fields for further uploading files
     * @var array
     */
    private $file_attributes = [];

    /**
     * Get all data types
     * @return array
     */
    public static function getTypes() {
        return [
            self::TYPE_STRING => 'Текстовое поле',
            self::TYPE_TEXTAREA => 'Текстовая область',
            self::TYPE_EDITOR => 'Редактор',
            self::TYPE_FILE => 'Файл',
            self::TYPE_CHECKBOX => 'Флажок',
            self::TYPE_SELECT => 'Выпадающий список',
        ];
    }

    /**
     * Returns the name of the field type
     * @param  string $name
     * @return string
     */
    public static function getTypeName($name) {
        return static::getTypes()[$name];
    }

    /**
     * Set label for attributes
     */
    public function setAttributeLabels($labels) {
        $this->attributeLabels = $labels;
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels() {
        return $this->attributeLabels;
    }

    /**
     * Getting the full path to the file directory
     * @param  string $file
     * @return string
     */
    protected function getFullPath($file = false) {
        return Yii::getAlias('@upload') . '/' . mb_strtolower(StringHelper::basename(self::className())) . '/';
    }

    /**
     * Link to file
     * @return string
     */
    public static function getFileLink($file = false) {
        return \Yii::getAlias('@files') . '/' . mb_strtolower(StringHelper::basename(self::className())) . '/' . $file;
    }

    /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeValidate() {
        $this->load(Yii::$app->request->post());
        return parent::beforeValidate();
    }

    /**
     * Obtaining these fields
     * @return string
     */
    public function getFieldAttributes() {

        // Uploading files
        if (is_array($this->file_attributes) && count($this->file_attributes)) {
            foreach ($this->file_attributes as $attribute) {
                $this->$attribute = $this->uploadFile($attribute);
            }
        }

        return Json::encode($this->attributes);
    }

    /**
     * Loading the Field Model
     * @param  string $options
     * @param  string $fields
     * @return backend\models\Field;
     */
    public static function getModel($options, $fields) {
        if (!empty($options)) {
            $options = Json::decode($options);
        }

        $attributes = array_values(ArrayHelper::getColumn($options, 'param')); // All attributes
        $labels = ArrayHelper::map($options, 'param', 'label'); // All labels
        $group_fields = [];

        foreach ($options as $item) {
            // An array of required fields to fill
            if ($item[self::RULE_REQUIRED] && !ArrayHelper::isIn($item['type'], [
                self::TYPE_FILE,
                self::TYPE_IMAGE,
            ])) {
                $group_fields[self::RULE_REQUIRED][] = $item['param'];
            }

            // Grouping of fields by type
            if (ArrayHelper::keyExists($item['type'], self::getTypes())) {
                $group_fields[$item['type']][] = $item['param'];
            }
        }

        $self = new Field($attributes);

        // Field Validation Rules
        if(ArrayHelper::keyExists(self::RULE_REQUIRED, $group_fields, false))
            $self->addRule($group_fields[self::RULE_REQUIRED], 'required');

        if(ArrayHelper::keyExists(self::TYPE_STRING, $group_fields, false))
            $self->addRule($group_fields[self::TYPE_STRING], 'string', [
                'max' => self::TYPE_STRING_MAX,
            ]);

        if(ArrayHelper::keyExists(self::TYPE_TEXTAREA, $group_fields, false))
            $self->addRule($group_fields[self::TYPE_TEXTAREA], 'string', [
                'max' => self::TYPE_TEXTAREA_MAX,
            ]);

        if(ArrayHelper::keyExists(self::TYPE_FILE, $group_fields, false))
            $self->addRule($group_fields[self::TYPE_FILE], 'file', [
                'skipOnEmpty' => true,
                'extensions' => 'rar, zip, pdf, docs, mp4, mp3, jpg, jpeg, png, gif, xsl',
                'maxSize' => 1024 * 1024 * 50, // 50Mb
                'maxFiles' => 1,
            ]);

        $self->addRule($attributes, 'trim');
        // End Field Validation Rules

        // Set attr labels name
        $self->setAttributeLabels($labels);

        // Set attr params
        $self->setAttributes(Json::decode($fields));

        // Save fields for further uploading files
        if (ArrayHelper::keyExists(self::TYPE_FILE, $group_fields)) {
            $self->file_attributes = $group_fields[self::TYPE_FILE];
        }

        unset($attributes, $labels, $group_fields, $options, $fields);

        return $self;
    }

    /**
     * Uploading files
     * @param  string $attribute
     * @param  string $controller
     * @return string
     */
    public function uploadFile($attribute) {
        if ($attribute) {
            $UploadedFile = UploadedFile::getInstance($this, $attribute);

            if (!is_null($UploadedFile)) {
                $fileName = uniqid() . '.' . $UploadedFile->extension;

                // Create destination directory
                FileHelper::createDirectory($this->fullPath, Files::CHMOD);

                // Delete old files
                if ($this->attributes[$attribute] && file_exists($this->fullPath . $this->attributes[$attribute])) {
                    @unlink($this->fullPath . $this->attributes[$attribute]);
                }

                $UploadedFile->saveAs($this->fullPath . $fileName);

                // If the file is an image, then it must be sent through Imagine
                if (ArrayHelper::isIn($UploadedFile->extension, $this->images_filter)) {
                    Image::frame($this->fullPath . $fileName, 0, 'fff', 0)
                        ->save($this->fullPath . $fileName);
                }

                return $fileName;
            }
        }

        return $this->attributes[$attribute];
    }
}