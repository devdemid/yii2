<?php
/**
 * Time interval
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace backend\models;

use yii\base\Model;

class RangeDate extends Model {

    /**
     * The date from
     * @var string
     */
    public $from;

    /**
     * The date to
     * @var string
     */
    public $to;

    /**
     * @inheritdoc
     * @return array
     */
    public function rules() {
        return [
            [['from', 'to'], 'filter', 'filter' => 'trim'],
            [['from', 'to'], 'date', 'format' => 'yyyy-M-d H:m:s'],
            ['from', 'required'],
        ];
    }

    public function getTimestamp() {
        return '';
    }

    public function getFormatting($format) {
        return '';
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels() {
        return [
            'from' => 'От',
            'to'   => 'до'
        ];
    }
}