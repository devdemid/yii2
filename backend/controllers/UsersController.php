<?php

/**
 * User Accounts
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace backend\controllers;

use backend\components\BackendController;
use backend\components\MenuComponent;
use common\models\PasswordResetRequestForm;
use common\models\ResetPasswordForm;
use common\models\SigninForm;
use common\models\SignupForm;
use common\models\User;
use Yii;
use yii\base\InvalidParamException;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;

class UsersController extends BackendController {

    public $pageSize = 15; // Number of entries per page

    /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeAction($action) {
        if (parent::beforeAction($action)) {

            // Add button in the top menu
            $this->headMenu[] = new MenuComponent([
                'label' => 'Добавить аккаунт',
                'icon' => 'icon-plus22 position-left',
                'url' => Url::to('/users/add'),
            ]);

            // Private variable
            $_actions = ['blocked', 'passwd'];

            // Checking for permission
            if(($action->id == 'blocked' && !\Yii::$app->user->can('lockingProfiles'))
                || ($action->id == 'passwd' && !\Yii::$app->user->can('changePassword'))
            ) {
                throw new ForbiddenHttpException('Access denied');
            }

            return true;
        }

        return false;
    }

    /**
     * [actionIndex description]
     * @return [type] [description]
     */
    public function actionIndex() {

        // Remember links for btnAction
        Url::remember(Yii::$app->request->url, 'save-exit');
        Url::remember('/' . Yii::$app->controller->id . '/add', 'save-new');

        // Header
        $this->view->title = 'Все аккаунты';
        $this->breadcrumbs[] = $this->view->title;
        //$this->pageTitle = $this->view->title;
        // $this->breadcrumbs[] = [
        //  'label' => 'Администраторы',
        //  'url' => ['/users']
        // ];

        $total = User::find()->count();
        $pages = new Pagination([
            'totalCount' => $total,
            'pageSize' => $this->pageSize,
        ]);

        $usersModel = User::find()
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', [
            'users' => $usersModel,
            'pages' => $pages,
            'total' => $total,
        ]);
    }

    /**
     * [actionProfile description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function actionProfile($id) {
        if (!Yii::$app->user->can('accessProfile') && Yii::$app->user->identity->id != $id) {
            throw new ForbiddenHttpException('Access denied');
        }

        return $this->render('profile', ['id' => $id]);
    }

    /**
     * Block access to resources
     * @param  int $id
     * @return string
     */
    public function actionBlocked($id) {
        $model = User::find()
            ->select(['id', 'status'])
            ->where(['id' => (int) $id])
            ->one();

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        if ($model->status !== User::STATUS_BLOCKED) {
            $model->updateAttributes(['status' => User::STATUS_BLOCKED]);
            Yii::$app->session->setFlash('success', 'Аккаунт успешно заблокирован!');
        } else {
            Yii::$app->session->setFlash('danger', 'Аккаунт уже заблокирован!');
        }

        return $this->redirect(Url::previous());
    }

    /**
     * Create an account
     * @return string
     */
    public function actionAdd() {
        $model = new User;

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            Yii::$app->session->setFlash('success', 'Аккаунт успешно добавлен!');
            return $this->redirectBtnAction('/' . Yii::$app->controller->id . '/edit?id=' . $model->id, 'save');
        }

        return $this->render('item', ['user' => $model]);
    }

    /**
     * Edit account
     * @param  int $id
     * @return string
     */
    public function actionEdit($id) {
        $model = User::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            Yii::$app->session->setFlash('success', 'Аккаунт успешно отредактирован!');
            return $this->redirectBtnAction(Yii::$app->request->url, 'save');
        }

        return $this->render('item', ['user' => $model]);
    }

    /**
     * Password changes in your account
     * @param  int $id
     * @return string
     */
    public function actionPasswd($id) {
        $send_email = (int) Yii::$app->request->post('send_email');

        $model = User::find()
            ->select(['id', 'password_reset_token', 'first_name', 'second_name', 'email'])
            ->where(['id' => (int) $id])
            ->one();

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        if (!User::isPasswordResetTokenValid($model->password_reset_token)) {
            $model->generatePasswordResetToken();
            $model->save(false);
        }

        try {
            $ResetPassword = new ResetPasswordForm($model->password_reset_token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($ResetPassword->load(Yii::$app->request->post()) && $ResetPassword->validate() && $ResetPassword->resetPassword()) {
            Yii::$app->session->setFlash('success', 'Новый пароль был сохранен.');

            // Send password to account email
            if($send_email) {
                $send_email = false;

                \Yii::$app->mailer->compose([
                    'html' => 'passwd-html',
                    'text' => 'passwd-text',
                ], [
                    'password' => $ResetPassword->password,
                    'nameTogether' => $model->nameTogether
                ])
                    ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' robot'])
                    ->setTo($model->email)
                    ->setSubject('Изменение пароля для ' . \Yii::$app->name)
                    ->send();
            }
        }

        return $this->render('passwd', [
            'user' => $model,
            'send_email' => $send_email,
            'ResetPassword' => $ResetPassword
        ]);
    }

    /**
     * Deleting an account
     * @param  int $id
     * @return string
     */
    public function actionRemove($id) {
        $model = User::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $model->delete();

        Yii::$app->session->setFlash('success', 'Аккаунт успешно удален!');
        return $this->redirect(Url::previous('save-exit'));
    }

    /**
     * [actionLogin description]
     * @return [type] [description]
     */
    public function actionSignin() {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $SigninForm = new SigninForm();

        if ($SigninForm->load(Yii::$app->request->post()) && $SigninForm->login()) {
            return $this->goBack();
        } else {
            return $this->render('signin', ['Signin' => $SigninForm]);
        }
    }

    /**
     * [actionRequestPasswordReset description]
     * @return [type] [description]
     */
    public function actionRequestPasswordReset() {
        $PasswordReset = new PasswordResetRequestForm();

        if ($PasswordReset->load(Yii::$app->request->post()) && $PasswordReset->validate()) {
            if ($PasswordReset->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }

        }

        return $this->render('requestPasswordResetToken', [
            'PasswordReset' => $PasswordReset,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token) {

        try {
            $ResetPassword = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($ResetPassword->load(Yii::$app->request->post()) && $ResetPassword->validate() && $ResetPassword->resetPassword()) {
            Yii::$app->session->setFlash('success', 'Новый пароль был сохранен.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'ResetPassword' => $ResetPassword,
        ]);
    }

    /**
     * [actionSignup description]
     * @return [type] [description]
     */
    public function actionSignup() {
        //invite
        $signup = new SignupForm();
        if ($signup->load(Yii::$app->request->post())) {
            if ($user = $signup->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'signup' => $signup,
        ]);
    }

    /**
     * [actionLogout description]
     * @return [type] [description]
     */
    public function actionLogout() {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    /**
     * [actionConfirmEmail description]
     * @param  [type] $token [description]
     * @return [type]        [description]
     */
    public function actionConfirmEmail($token) {
        return $this->goHome();
    }
}
