<?php
/**
 * User rights management
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace backend\controllers;

use backend\components\BackendController;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
use yii\helpers\Url;

class AccessController extends BackendController {

    /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeAction($action) {
        if (parent::beforeAction($action)) {

            // Checking for permission
            if (!\Yii::$app->user->can('editAccess')) {
                throw new ForbiddenHttpException('Access denied');
            }

            return true;
        }

        return false;
    }

    /**
     * All groups
     */
    public function actionIndex() {

        $authManager = Yii::$app->getAuthManager();
        $rbacRoles = $authManager->getRoles();

        Url::remember(Url::remember(), 'save');
        Url::remember('/', 'save-exit');

        // Update the data
        if (Yii::$app->request->isPost && Yii::$app->request->post('Access')) {
            $Access = Yii::$app->request->post('Access');

            // Clear children
            foreach ($rbacRoles as $roleName => $rbacRole) {

                // Clear root forbidden!
                if($roleName == 'root')
                    continue;

                $authManager->removeChildren($rbacRole);
            }

            foreach ($Access as $role => $permissions) {

                // Edit root forbidden!
                if($role == 'root') {
                    Yii::$app->session->setFlash('error', 'Редактировать суперпользователя запрещено!');
                    continue;
                }

                $role = $authManager->getItem($role);

                // Update permissions
                if(!is_null($role)) {
                    foreach (array_keys($permissions) as $permission) {
                        $authManager->addChild($role, $authManager->getItem($permission));
                    }
                }
            }

            Yii::$app->session->setFlash('success', 'Права доступа успешно отредактировано!');
        }

        $rbacPermissions = $authManager->getPermissions();

        // Create a structure to form
        $rolePermissions = [];
        foreach ($rbacRoles as $role) {
            foreach ($rbacPermissions as $permission) {
                $rolePermissions[$permission->name]['description'] = $permission->description;
                $rolePermissions[$permission->name]['roles'][$role->name] = ArrayHelper::keyExists(
                    $permission->name,
                    $authManager->getPermissionsByRole($role->name)
                );
            }
        }

        unset($rbacPermissions);

        return $this->render('index', [
            'roles' => $rbacRoles,
            'rolePermissions' => $rolePermissions,
        ]);
    }
}