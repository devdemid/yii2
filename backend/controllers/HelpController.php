<?php
/**
 * Main controller description
 */

namespace backend\controllers;

use backend\components\BackendController;

class HelpController extends BackendController {

    /**
     * [actionIndex description]
     * @return [type] [description]
     */
    public function actionIndex() {
        return $this->render('index');
    }
}
