<?php
/**
 * Ordering
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace backend\controllers;

use common\models\Order;
use backend\models\Search;
use Yii;
use yii\data\Pagination;
use yii\helpers\Url;

class OrderingController extends \backend\components\BackendController
{

    public $pageSize = 50; // Number of entries per page

    /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            return true;
        }
        return false;
    }

    /**
     * [actionIndex description]
     * @return [type] [description]
     */
    public function actionIndex()
    {
        // Remember links for btnAction
        Url::remember(Yii::$app->request->url, 'save-exit');
        Url::remember('/' . Yii::$app->controller->id . '/add', 'save-new');

        $this->view->title = 'Все заказы';

        $search = (new Search)->apply(Yii::$app->request->get('search'));
        $total = Order::find()
            ->andFilterWhere(['AND', ['like', 'name', $search]])
            ->orderBy([Order::ORDER_BY_SORTING => SORT_DESC])
            ->count();

        $pages = new Pagination([
            'totalCount' => $total,
            'pageSize' => $this->pageSize,
        ]);

        $models = Order::find()
        //->andFilterWhere(['AND', ['like', 'name', $search]])
            ->orderBy([Order::ORDER_BY_SORTING => SORT_DESC])
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', [
            'search' => $search,
            'pages' => $pages,
            'models' => $models,
        ]);
    }

    /**
     * Edit an order
     * @param  Integer $id
     * @return String
     */
    public function actionEdit($id) {
        $model = Order::findOne($id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        if ($model->load(Yii::$app->request->post()) && $model->update(true)) {
            Yii::$app->session->setFlash('success', 'Запись успешно отредактирована!');
            return $this->redirectBtnAction(Yii::$app->request->url, 'save');
        }

        return $this->render('item', ['model' => $model]);
    }

    /**
     * Remove order
     * @param  Integer $id
     * @return String
     */
    public function actionRemove($id) {
        $model = Order::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $model->delete();

        Yii::$app->session->setFlash('success', 'Заказ успешно удален!');
        return $this->redirect(Url::previous('save-exit'));
    }
}
