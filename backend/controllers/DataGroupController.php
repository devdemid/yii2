<?php

/**
 * Data Group
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace backend\controllers;

use backend\components\BackendController;
use backend\components\MenuComponent;
use backend\models\Search;
use backend\widgets\Notify;
use common\models\DataGroup;
use Yii;
use yii\helpers\Url;

class DataGroupController extends BackendController
{
    /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {

            $this->headMenu[] = new MenuComponent([
                'label' => 'CSS Иконки',
                'icon' => 'icon-file-css position-left',
                'url' => Url::to(Yii::$app->controller->id . '/helpers'),
                'target' => true
            ]);

            // Add button in the top menu
            $this->headMenu[] = new MenuComponent([
                'label' => 'Добавить данные',
                'icon' => 'icon-plus22 position-left',
                'url' => Url::to(Yii::$app->controller->id . '/add'),
            ]);
            return true;
        }
        return false;
    }

    /**
     * All group data
     * @return String
     */
    public function actionIndex()
    {
        // Remember links for btnAction
        Url::remember(Yii::$app->request->url, 'save-exit');
        Url::remember('/' . Yii::$app->controller->id . '/add', 'save-new');

        $search = (new Search)->apply(Yii::$app->request->get('search'));
        $models = DataGroup::find()
            ->andFilterWhere(['AND', ['like', 'name', $search]])
            ->orderBy(DataGroup::ORDER_BY_SORTING)
            ->all();

        $groups = [];
        foreach ($models as $item) {
            $groups[$item->group_id][] = $item;
        }

        return $this->render('index', [
            'groups' => $groups,
            'search' => $search,
            'total'  => count($models)
        ]);
    }

    /**
     * Add data
     * @return String
     */
    public function actionAdd()
    {
        $model = new DataGroup;

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            Yii::$app->session->setFlash('success', 'Запись успешно добавлена!');
            return $this->redirectBtnAction('/' . Yii::$app->controller->id . '/edit?id=' . $model->id, 'save');
        }

        return $this->render('item', ['model' => $model]);
    }

    /**
     * Edit data
     * @return String
     */
    public function actionEdit($id)
    {
        $model = DataGroup::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            Yii::$app->session->setFlash('success', 'Запись успешно отредактирована!');
            return $this->redirectBtnAction(Yii::$app->request->url, 'save');
        }

        return $this->render('item', ['model' => $model]);
    }

    /**
     * Remove data
     * @return String
     */
    public function actionRemove($id)
    {
        $model = DataGroup::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $model->delete();

        Yii::$app->session->setFlash('success', 'Запись успешно удалена!');
        return $this->redirect(Url::previous('save-exit'));
    }

    /**
     * Published Status Updates
     * @param  integer $id
     * @return JSONString
     */
    public function actionPublished($id)
    {
        if (Yii::$app->request->isAjax) {

            $model = DataGroup::find()
                ->select(['id', 'published'])
                ->where(['id' => (int) $id])
                ->one();

            if (is_null($model)) {
                return Notify::danger('Не удалось найти #ID:' . inval($id) . '!');
            }

            if ((boolean) Yii::$app->request->get('published')) {
                $model->published = filter_var(Yii::$app->request->get('published'), FILTER_VALIDATE_BOOLEAN);
            }

            $model->update(false);

            return Notify::success('Запись #ID:' . $model->id . ' успешно обновленна!');
        }
    }

    /**
     * Sorting categories
     */
    public function actionSorting() {
        if(Yii::$app->request->isAjax && Yii::$app->request->get('Sorting', false)) {
            $sorting = Yii::$app->request->get('Sorting', false);

            $models = DataGroup::find()
                ->where(['id' => array_keys($sorting)])
                ->all();

            if(count($models)) {
                foreach ($models as $item) {
                    $item->sorting = (int) $sorting[$item->id];
                    $item->update();
                }

                Yii::$app->session->setFlash('success', 'Сортировка выполнена успешно!');
                unset($models, $sorting);
            }
        }
    }

    /**
     * [actionHelper description]
     * @return String
     */
    public function actionHelpers() {
        $this->layout = self::LAYOUT_BLANK_NAME;
        return $this->render('helpers');
    }
}
