<?php
/**
 * Static pages
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace backend\controllers;

use backend\components\BackendController;
use backend\components\MenuComponent;
use backend\models\Search;
use backend\widgets\Notify;
use Yii;
use yii\data\Pagination;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use \common\models\Page;

class PageController extends BackendController {

    public $pageSize = 20; // Number of entries per page

    /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeAction($action) {
        if (parent::beforeAction($action)) {

            // Checking for permission
            if (!Yii::$app->user->can('accessPage')) {
                throw new ForbiddenHttpException('Access denied');
            }

            $this->headMenu[] = new MenuComponent([
                'label' => 'Добавить страницу',
                'icon' => 'icon-plus22 position-left',
                'url' => Url::to(Yii::$app->controller->id . '/add'),
            ]);
            return true;
        }
        return false;
    }

    /**
     * All pages
     * @return array
     */
    public function actionIndex() {

        // Remember links for btnAction
        Url::remember(Yii::$app->request->url, 'save-exit');
        Url::remember('/' . Yii::$app->controller->id . '/add', 'save-new');

        $this->view->title = 'Статические страницы';
        $search = (new Search)->apply(Yii::$app->request->get('search'));
        $total = Page::find()
            ->andFilterWhere(['AND', ['like', 'name', $search]])
            ->count();

        $pages = new Pagination([
            'totalCount' => $total,
            'pageSize' => $this->pageSize,
        ]);

        $models = Page::find()
            ->andFilterWhere(['AND', ['like', 'name', $search]])
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', [
            'pages' => $pages,
            'search' => $search,
            'static_pages' => $models,
            'total' => $total,
        ]);
    }

    /**
     * Add page
     * @return array
     */
    public function actionAdd() {
        $model = new Page;

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            Yii::$app->session->setFlash('success', 'Запись успешно добавлена!');
            return $this->redirectBtnAction('/' . Yii::$app->controller->id . '/edit?id=' . $model->id, 'save');
        }

        return $this->render('item', ['model' => $model]);
    }

    /**
     * Edit page
     * @param  integer $id
     * @return array
     */
    public function actionEdit($id) {
        $model = Page::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            Yii::$app->session->setFlash('success', 'Запись успешно отредактирована!');
            return $this->redirectBtnAction(Yii::$app->request->url, 'save');
        }

        return $this->render('item', ['model' => $model]);
    }

    /**
     * Remove page
     * @param  integer $id
     * @return array
     */
    public function actionRemove($id) {
        $model = Page::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $model->delete();

        Yii::$app->session->setFlash('success', 'Запись успешно удалена!');
        return $this->redirect(Url::previous('save-exit'));
    }

    /**
     * Published Status Updates
     * @param  integer $id [description]
     * @return array
     */
    public function actionPublished($id) {
        if (Yii::$app->request->isAjax) {

            $model = Page::findOne((int) $id);

            if (is_null($model)) {
                return Notify::danger('Не удалось найти #ID:' . inval($id) . '!');
            }

            if ((boolean) Yii::$app->request->get('published')) {
                $model->published = filter_var(Yii::$app->request->get('published'), FILTER_VALIDATE_BOOLEAN);
            }

            $model->update(false);

            return Notify::success('Запись #ID:' . $model->id . ' успешно обновленна!');
        }
    }
}