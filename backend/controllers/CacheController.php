<?php
/**
 * Clearing Cached Data
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace backend\controllers;

use backend\components\BackendController;
use Yii;

class CacheController extends BackendController {

    /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeAction($action) {
        if (parent::beforeAction($action)) {
            return true;
        }

        return false;
    }

    /**
     * Flush cache
     * @return [type] [description]
     */
    public function actionClearing() {
        if(Yii::$app->cache->flush()) {
            Yii::$app->session->setFlash('success', 'Данные кэша успешно очищено!');
        }

        return $this->goBack(Yii::$app->request->referrer ? Yii::$app->request->referrer : null);
    }
}