<?php

/**
 * Comments
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace backend\controllers;

use backend\components\BackendController;
use backend\models\Search;
use backend\widgets\Notify;
use common\models\Comment;
use Yii;
use yii\data\Pagination;
use yii\helpers\Url;

class CommentController extends BackendController
{

    public $pageSize = 20; // Number of entries per page

    /**
     * All comments
     * @return String
     */
    public function actionIndex()
    {
        // Remember links for btnAction
        Url::remember(Yii::$app->request->url, 'save-exit');

        $this->view->title = 'Все комментарии';
        $this->breadcrumbs[] = $this->view->title;
        $search = (new Search)->apply(Yii::$app->request->get('search'));
        $total = Comment::find()
        //->andFilterWhere(['AND', ['like', 'name', $search]])
            ->orderBy([Comment::ORDER_BY_SORTING => SORT_DESC])
            ->count();

        $pages = new Pagination([
            'totalCount' => $total,
            'pageSize' => $this->pageSize,
        ]);

        $comments = Comment::find()
        //->andFilterWhere(['AND', ['like', 'name', $search]])
            ->orderBy([Comment::ORDER_BY_SORTING => SORT_DESC])
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', [
            'comments' => $comments,
            'pages' => $pages,
            'total' => $total,
            'search' => $search,
        ]);
    }

    /**
     * Preview comment
     * @return [type] [description]
     */
    public function actionPreview($id)
    {
        if (!Yii::$app->request->isAjax) {
            die('Access denied');
        }

        $model = Comment::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        return $this->renderPartial('preview', [
            'model' => $model,
        ]);
    }

    /**
     * Moderation comment
     * @param Integer $id
     * @return String
     */
    public function actionModeration($id)
    {
        $model = Comment::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            $model->update(true);

            Yii::$app->session->setFlash('success', 'Комментарий успешно отредактирован!');
            return $this->redirectBtnAction(Yii::$app->request->url, 'save');
        }

        return $this->render('moderation', ['model' => $model]);
    }

    /**
     * Remove comment
     * @param Integer $id
     * @return Strinf
     */
    public function actionRemove($id)
    {
        $model = Comment::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $model->delete();

        Yii::$app->session->setFlash('success', 'Комментарий успешно удален!');
        return $this->redirect(Url::previous('save-exit'));
    }

    /**
     * Published Status Updates
     * @param  integer $id [description]
     * @return [array]
     */
    public function actionPublished($id)
    {
        if (Yii::$app->request->isAjax) {

            $model = Comment::find()
                ->select(['id', 'published'])
                ->where(['id' => (int) $id])
                ->one();

            if (is_null($model)) {
                return Notify::danger('Не удалось найти #ID:' . inval($id) . '!');
            }

            if ((boolean) Yii::$app->request->get('published')) {
                $model->published = filter_var(Yii::$app->request->get('published'), FILTER_VALIDATE_BOOLEAN);
            }

            $model->update(false);

            return Notify::success('Запись #ID:' . $model->id . ' успешно обновленна!');
        }
    }
}
