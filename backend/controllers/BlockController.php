<?php
/**
 * Block
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace backend\controllers;

use backend\components\BackendController;
use backend\components\MenuComponent;
use backend\models\Search;
use backend\widgets\Notify;
use common\models\Block;
use Yii;
use yii\data\Pagination;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;

class BlockController extends BackendController {

    public $pageSize = 20; // Number of entries per page

    /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeAction($action) {
        if (parent::beforeAction($action)) {

            // Checking for permission
            if (!Yii::$app->user->can('accessBlock')) {
                throw new ForbiddenHttpException('Access denied');
            }

            // Add button in the top menu
            $this->headMenu[] = new MenuComponent([
                'label' => 'Добавить блок',
                'icon' => 'icon-plus22 position-left',
                'url' => Url::to(Yii::$app->controller->id . '/add'),
            ]);

            return true;
        }

        return false;
    }

    /**
     * All blocks
     * @return string
     */
    public function actionIndex() {

        // Remember links for btnAction
        Url::remember(Yii::$app->request->url, 'save-exit');
        Url::remember('/' . Yii::$app->controller->id . '/add', 'save-new');

        $this->view->title = 'Блоки';
        $this->breadcrumbs[] = $this->view->title;
        $search = (new Search)->apply(Yii::$app->request->get('search'));
        $total = Block::find()->count();

        $pages = new Pagination([
            'totalCount' => $total,
            'pageSize' => $this->pageSize,
        ]);

        $models = Block::find()
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', [
            'blocks' => $models,
            'pages' => $pages,
            'total' => $total,
            'search' => $search,
        ]);
    }

    /**
     * Add block
     * @return string
     */
    public function actionAdd() {
        $model = new Block;

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            Yii::$app->session->setFlash('success', 'Запись успешно добавлена!');
            return $this->redirectBtnAction('/' . Yii::$app->controller->id . '/edit?id=' . $model->id, 'save');
        }

        return $this->render('item', ['model' => $model]);
    }

    /**
     * Edit block
     * @param  int $id
     * @return string
     */
    public function actionEdit($id) {
        $model = Block::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            Yii::$app->session->setFlash('success', 'Запись успешно отредактирована!');
            return $this->redirectBtnAction(Yii::$app->request->url, 'save');
        }

        return $this->render('item', ['model' => $model]);
    }

    /**
     * Remove block
     * @param  int $id
     * @return [string]
     */
    public function actionRemove($id) {
        $model = Block::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $model->delete();

        Yii::$app->session->setFlash('success', 'Запись успешно удалена!');
        return $this->redirect(Url::previous('save-exit'));
    }

    /**
     * Published Status Updates
     * @param  integer $id [description]
     * @return [array]
     */
    public function actionPublished($id) {
        if (Yii::$app->request->isAjax) {

            $model = Block::findOne((int) $id);

            if (is_null($model)) {
                return Notify::danger('Не удалось найти #ID:' . inval($id) . '!');
            }

            if ((boolean) Yii::$app->request->get('published')) {
                $model->published = filter_var(Yii::$app->request->get('published'), FILTER_VALIDATE_BOOLEAN);
            }

            $model->update(false);
            return Notify::success('Запись #ID:' . $model->id . ' успешно обновленна!');
        }
    }

}