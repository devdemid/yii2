<?php
/**
 * Filemanager controller
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */
namespace backend\controllers;

use backend\components\BackendController;

class FilemanagerController extends BackendController {

    /**
     * [actionIndex description]
     * @return [type] [description]
     */
    public function actionIndex() {
        return $this->render('index');
    }
}
