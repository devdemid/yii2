<?php
/**
 * Block
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace backend\controllers;

use backend\components\BackendController;
use Yii;

class SeoController extends BackendController {

        /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeAction($action) {
        if (parent::beforeAction($action)) {

            // Checking for permission
            if (!Yii::$app->user->can('accessSeo')) {
                throw new ForbiddenHttpException('Access denied');
            }

            return true;
        }

        return false;
    }

    /**
     * Editing a robots.txt file
     * @return string
     */
    public function actionRobots() {
        $this->view->title = 'Файл robots.txt';

        // Robots.txt file locations
        $robots_file = Yii::getAlias('@frontend/web/robots.txt');
        $robots_text = '';
        $robots_filemtime = false;

        if(file_exists($robots_file) && is_readable($robots_file)) {
            chmod($robots_file, 0755);

            // Write file
            if(Yii::$app->request->isPost && Yii::$app->request->post('robots')) {
                $robots_text = Yii::$app->request->post('robots');

                $handle = fopen($robots_file, "wt");
                foreach (explode("\r", $robots_text) as $str) {
                    if(fwrite($handle, $str) === FALSE) {
                        Yii::trace('I can not open the file (' . $robots_file . ')', $robots_text);
                    }
                }
                fclose($handle);
            }

            // Read file
            if(!$robots_text) {
                $handle = fopen($robots_file, "rt");
                $robots_text = htmlentities(fread($handle, filesize($robots_file)));
                fclose($handle);
            }

            $robots_filemtime = filemtime($robots_file);
        }

        return $this->render('robots', [
            'robots_text' => $robots_text,
            'robots_filemtime' => $robots_filemtime
        ]);
    }

    /**
     * Sitemap
     * @return string
     */
    public function actionSitemap() {
        return $this->render('sitemap');
    }

    public function actionAnalytics() {}

}