<?php
/**
 * Settings controller
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace backend\controllers;

use backend\components\BackendController;
use backend\components\MenuComponent;
use common\models\Settings;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;

class SettingsController extends BackendController {

    /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeAction($action) {
        if (parent::beforeAction($action)) {

            // Checking for permission
            if (!Yii::$app->user->can('editSettings')) {
                throw new ForbiddenHttpException('Access denied');
            }

            if(Yii::$app->user->can('createSettings') && ArrayHelper::isIn($action->id, ['index', 'add'])) {
                $this->headMenu[] = new MenuComponent([
                    'label' => 'Добавить новый параметр',
                    'icon' => 'icon-plus22 position-left',
                    'url' => Url::to(Yii::$app->controller->id . '/add'),
                ]);
            }

            // Private variable
            $_actions = ['add', 'edit', 'remove'];

            // Checking for permission
            if (ArrayHelper::isIn($action->id, $_actions) && !\Yii::$app->user->can('createSettings')) {
                throw new ForbiddenHttpException('Access denied');
            }

            return true;
        }

        return false;
    }

    /**
     * List of available options
     * @return string
     */
    public function actionIndex() {

        // Remember links for btnAction
        Url::remember(Yii::$app->request->url, 'save-exit');

        $settingsModel = Settings::find()
            ->where(['module' => Settings::SETTINGS_MODULE])
            ->indexBy('id')
            ->all();

        // Update the data
        if (Yii::$app->request->isPost) {
            $_settings = Yii::$app->request->post('Settings');

            foreach (array_keys($_settings) as $id) {
                $model = $settingsModel[$id];
                if (!is_null($model) && $model->validate()) {
                    $model->value = $_settings[$id];
                    $model->save(true);
                }
            }

            unset($model, $_settings);
            Yii::$app->session->setFlash('success', Yii::t('app', 'Данные успешно обновлены!'));
        }

        // Sorting options
        $settings = [];
        foreach ($settingsModel as $item) {
            $settings[$item->group][$item->id] = $item;
        }

        unset($settingsModel);

        $this->actionsLink = [[
            'name' => Yii::t('app', 'Добавить параметр'),
            'link' => Url::toRoute('settings/add'),
            'class' => 'plus22',
        ]];

        return $this->render('index', ['settings' => $settings]);
    }

    /**
     * Add option
     * @return string
     */
    public function actionAdd() {

        $settings = new Settings;

        if (Yii::$app->request->isPost) {
            if ($settings->load(Yii::$app->request->post()) && $settings->save(true)) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Новый параметр успешно добавлен'));
            }
        }

        return $this->render('item', ['settings' => $settings]);
    }

    /**
     * Edit option
     * @param integer $id
     * @return string
     */
    public function actionEdit($id) {}

    /**
     * Remove option
     * @param integer $id
     * @return string
     */
    public function actionRemove($id) {}

    /**
     * Временно! Нужно сделать добавление модулей и генерация навигации
     */
    public function actionNav() {
        $nav = (new \common\components\Navigation('@backend'));
        $nav->add([
                    [
                        'label' => 'Навигация',
                        'url'   => ['/'],
                        'options' => ['class' => 'navigation-header'],
                        'template' => '<span>{label}</span> <i class="icon-menu" title="{label}"></i>',
                    ],
                    [
                        'label' => 'Главная',
                        'url'   => ['/'],
                        'template' => '<a href="{url}"><i class="icon-home4"></i> <span>{label}</span></a>',
                    ],
                    // [
                    //     'label'    => 'Туры',
                    //     'url'      => ['/tours'],
                    //     'template' => '<a href="{url}"><i class="icon-split"></i> <span>{label}</span></a>',
                    //     'items'    => [
                    //         ['label' => 'Все записи', 'url' => ['tours/index']],
                    //         ['label' => 'Группы данных', 'url' => ['data-group/index']],
                    //         ['label' => 'Массовое управления', 'url' => ['tours/bulk-editing']],
                    //     ]
                    // ],
                    // [
                    //     'label'    => 'Заказы',
                    //     'url'      => ['/ordering'],
                    //     'template' => '<a href="{url}"><i class="icon-bag"></i> <span>{label}</span></a>',
                    //     'items'    => [
                    //         ['label' => 'Все заказы', 'url' => ['ordering/index']],
                    //         ['label' => 'Добавить заказ', 'url' => ['ordering/add']]
                    //     ]
                    // ],
                    [
                        'label'    => 'Комментарии',
                        'url'      => ['/comment/index'],
                        'template' => '<a href="{url}"><i class="icon-comments"></i> <span>{label}</span></a>'
                    ],
                    [
                        'label'    => 'Статические страницы',
                        'url'      => ['/page'],
                        'template' => '<a href="{url}"><i class="icon-file-text"></i> <span>{label}</span></a>',
                        'items'    => [
                            ['label' => 'Все страницы', 'url' => ['page/index']],
                            ['label' => 'Добавить страницу', 'url' => ['page/add']]
                        ]
                    ],
                    [
                        'label'    => 'Новостные ленты',
                        'url'      => ['/news'],
                        'template' => '<a href="{url}"><i class="icon-newspaper2"></i> <span>{label}</span></a>',
                        'items'    => [
                            ['label' => 'Все новостные ленты', 'url' => ['news/feed']],
                            ['label' => 'Добавить новостную ленту', 'url' => ['news/add-feed']],
                        ]
                    ],
                    [
                        'label'    => 'Блоки',
                        'url'      => ['block/index'],
                        'template' => '<a href="{url}"><i class="icon-cube"></i> <span>{label}</span></a>',
                    ],
                    [
                        'label'    => 'Генератор слайдеров',
                        'url'      => ['slider/index'],
                        'template' => '<a href="{url}"><i class="icon-stack4"></i> <span>{label}</span></a>',
                    ],
                    [
                        'label'    => 'Генератор форм',
                        'url'      => ['form/index'],
                        'template' => '<a href="{url}"><i class="icon-inbox"></i> <span>{label}</span></a>',
                    ],
                    [
                        'label'    => 'FAQ Вопросы и ответы',
                        'url'      => ['faq/index'],
                        'template' => '<a href="{url}"><i class="icon-info22"></i> <span>{label}</span></a>',
                    ],
                    [
                        'label'    => 'Конструктор меню',
                        'url'      => ['menu/index'],
                        'template' => '<a href="{url}"><i class="icon-menu4"></i> <span>{label}</span></a>',
                    ],
                    // End demo
                    [
                        'label'    => 'Аккаунты пользователей',
                        'url'      => ['/users'],
                        'template' => '<a href="{url}"><i class="icon-people"></i> <span>{label}</span></a>',
                        'items'    => [
                            ['label' => 'Все аккаунты', 'url' => ['users/index']],
                            ['label' => 'Группы пользователей', 'url' => ['access/index']],
                        ]
                    ],
                    [
                        'label'    => 'Настройки системы',
                        'url'      => ['/'],
                        'options'  => ['class' => 'navigation-header'],
                        'template' => '<span>{label}</span> <i class="icon-menu" title="{label}"></i>',
                    ],
                    [
                        'label'      => 'SEO Инструменты',
                        'url'        => ['/'],
                        'template'   => '<a href="{url}"><i class="icon-hammer-wrench"></i> <span>{label}</span></a>',
                        'items'      => [
                            ['label' => 'Файл robots.txt', 'url' => ['seo/robots']]

                        ]
                    ],
                    [
                        'label'      => 'Настройки',
                        'url'        => ['/'],
                        'template'   => '<a href="{url}"><i class="icon-gear"></i> <span>{label}</span></a>',
                        'items'      => [
                            ['label' => 'Настройка системы', 'url' => ['settings/index']]

                        ]
                    ],
                ]);

        var_dump($nav->getItems());
        var_dump($nav->getInfo()); exit;
    }
}