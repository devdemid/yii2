<?php
/**
 * Menu Designer
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace backend\controllers;

use backend\components\BackendController;
use backend\components\MenuComponent;
use backend\models\Search;
use backend\widgets\Notify;
use common\models\Menu;
use common\models\MenuTree;
use Yii;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class MenuController extends BackendController {

    public $pageSize = 50; // Number of entries per page

    /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeAction($action) {
        if (parent::beforeAction($action)) {

            // Checking for permission
            // if (!Yii::$app->user->can('accessMenu')) {
            //     throw new ForbiddenHttpException('Access denied');
            // }

            if (ArrayHelper::isIn($action->id, ['index', 'add', 'settings'])) {
                // Add button in the top menu
                $this->headMenu[] = new MenuComponent([
                    'label' => 'Добавить меню',
                    'icon' => 'icon-plus22 position-left',
                    'url' => Url::to(Yii::$app->controller->id . '/add'),
                ]);
            }

            if (ArrayHelper::isIn($action->id, ['nav', 'nav-add', 'nav-edit'])) {
                $menu_id = (int) Yii::$app->request->get('menu_id');

                // Add button in the top menu
                $this->headMenu[] = new MenuComponent([
                    'label' => 'Добавить элемент меню',
                    'icon' => 'icon-plus22 position-left',
                    'url' => Url::to(Yii::$app->controller->id . '/nav-add?menu_id=' . $menu_id),
                ]);
            }

            return true;
        }

        return false;
    }

    /**
     * [actionIndex description]
     * @return [type] [description]
     */
    public function actionIndex() {
        // Remember links for btnAction
        Url::remember(Yii::$app->request->url, 'save-exit');
        Url::remember('/' . Yii::$app->controller->id . '/add', 'save-new');

        $this->view->title = 'Конструктор меню';
        $this->breadcrumbs[] = $this->view->title;
        $search = (new Search)->apply(Yii::$app->request->get('search'));
        $total = Menu::find()->count();

        $pages = new Pagination([
            'totalCount' => $total,
            'pageSize' => $this->pageSize
        ]);

        $models = Menu::find()
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', [
            'models' => $models,
            'pages' => $pages,
            'total' => $total,
            'search' => $search
        ]);
    }

    /**
     * Add menu
     * @return string
     */
    public function actionAdd() {
        $model = new Menu;

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            Yii::$app->session->setFlash('success', 'Запись успешно добавлена!');
            return $this->redirectBtnAction('/' . Yii::$app->controller->id . '/settings?id=' . $model->id, 'save');
        }

        return $this->render('item', ['model' => $model]);
    }

    /**
     * Settings menu
     * @return string
     */
    public function actionSettings($id) {
        $model = Menu::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            Yii::$app->session->setFlash('success', 'Запись успешно отредактирована!');
            return $this->redirectBtnAction(Yii::$app->request->url, 'save');
        }

        return $this->render('item', ['model' => $model]);
    }

    /**
     * Remove menu
     * @return string
     */
    public function actionRemove($id) {
        $model = Menu::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $model->delete();

        Yii::$app->session->setFlash('success', 'Запись успешно удалена!');
        return $this->redirect(Url::previous('save-exit'));
    }


    /**
     * Published Status Updates
     * @param  integer $id [description]
     * @return [array]
     */
    public function actionPublished($id) {
        if (Yii::$app->request->isAjax) {
            $model = Menu::findOne((int) $id);

            if (is_null($model)) {
                return Notify::danger('Не удалось найти #ID:' . inval($id) . '!');
            }

            if ((boolean) Yii::$app->request->get('published')) {
                $model->published = filter_var(Yii::$app->request->get('published'), FILTER_VALIDATE_BOOLEAN);
            }

            $model->update(false);
            return Notify::success('Запись #ID:' . $model->id . ' успешно обновленна!');
        }
    }

    /**
     * Navigation controls
     * @param  integer $menu_id
     * @return string
     */
    public function actionNav($menu_id) {
        $model_menu = Menu::findOne((int) $menu_id);

        if (is_null($model_menu)) {
            throw new \yii\web\NotFoundHttpException();
        }

        // Remember links for btnAction
        Url::remember(Yii::$app->request->url, 'save-exit');
        Url::remember('/' . Yii::$app->controller->id . '/add', 'save-new');

        $this->view->title = $model_menu->name;
        $search = (new Search)->apply(Yii::$app->request->get('search'));
        $total = MenuTree::find()->where(['item_id' => $model_menu->id])->count();

        $pages = new Pagination([
            'totalCount' => $total,
            'pageSize' => $this->pageSize,
        ]);

        $models = MenuTree::find()
            ->where(['item_id' => $model_menu->id])
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy(MenuTree::ORDER_BY_SORTING)
            ->all();

        return $this->render('nav', [
            'model_menu' => $model_menu,
            'models' => $models,
            'pages' => $pages,
            'total' => $total,
            'search' => $search
        ]);
    }

    /**
     * Add new menu item
     * @param  integer $menu_id
     * @return string
     */
    public function actionNavAdd($menu_id) {

        $model_menu = Menu::findOne((int) $menu_id);

        if (is_null($model_menu)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $this->view->title = 'Добавить элемент навигации';

        $model = new MenuTree;
        $model->item_id = $model_menu->id;

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            Yii::$app->session->setFlash('success', 'Запись успешно добавлена!');
            return $this->redirectBtnAction('/' . Yii::$app->controller->id . '/nav-edit?id=' . $model->id, 'save');
        }

        if($model_menu->type == Menu::TYPE_MENU_TREE) {
            $parent_list = MenuTree::find()
                ->select(['id', 'name', 'item_id', 'level', 'sorting'])
                ->where(['item_id' => $model_menu->id])
                ->orderBy(MenuTree::ORDER_BY_SORTING)
                ->all();

            $parent_list = ArrayHelper::map($parent_list, 'id', 'name');
        } else {
            $parent_list = [];
        }

        return $this->render('nav-item', [
            'model_menu' => $model_menu,
            'parent_list' => $parent_list,
            'model' => $model
        ]);
    }

    /**
     * Edit menu item
     * @param  integer $id
     * @return array
     */
    public function actionNavEdit($id) {
        $model = MenuTree::findOne((int)$id);
        $this->view->title = 'Редактировать элемент навигации';

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $model_menu = Menu::findOne((int) $model->item_id);

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            Yii::$app->session->setFlash('success', 'Запись успешно отредактирована!');
            return $this->redirectBtnAction(Yii::$app->request->url, 'save');
        }

        if($model_menu->type == Menu::TYPE_MENU_TREE) {
            $parent_list = $parent_list = MenuTree::find()
                ->select(['id', 'name', 'item_id', 'level', 'sorting'])
                ->where(['item_id' => $model_menu->id])
                ->orderBy(MenuTree::ORDER_BY_SORTING)
                ->all();

            $parent_list = ArrayHelper::map($parent_list, 'id', 'name');
        } else {
            $parent_list = [];
        }

        return $this->render('nav-item', [
            'model_menu' => $model_menu,
            'parent_list' => $parent_list,
            'model' => $model
        ]);
    }

    /**
     * Sorting menu items
     * @return [type] [description]
     */
    public function actionNavSorting() {
        if (!Yii::$app->request->isAjax) {
            die('Access denied!');
        }

        $sorting = Yii::$app->request->get('Sorting');
        $model = MenuTree::find()
            ->where(['IN', 'id', array_keys($sorting)])
            ->all();

        foreach ($model as $item) {
            $item->sorting = (int) $sorting[$item->id];
            $item->update(false);
        }

        Yii::$app->session->setFlash('success', 'Сортировка выполнена успешно!');
        return true;
    }

    /**
     * Remove menu items
     * @return string
     */
    public function actionNavRemove($id) {
        $model = MenuTree::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $model->delete();

        Yii::$app->session->setFlash('success', 'Запись успешно удалена!');
        return $this->redirect(Url::previous('save-exit'));
    }

    /**
     * Published Nav Status Updates
     * @param  integer $id [description]
     * @return [array]
     */
    public function actionNavPublished($id) {
        if (Yii::$app->request->isAjax) {
            $model = MenuTree::findOne((int) $id);

            if (is_null($model)) {
                return Notify::danger('Не удалось найти #ID:' . inval($id) . '!');
            }

            if ((boolean) Yii::$app->request->get('published')) {
                $model->published = filter_var(Yii::$app->request->get('published'), FILTER_VALIDATE_BOOLEAN);
            }

            $model->update(false);
            return Notify::success('Запись #ID:' . $model->id . ' успешно обновленна!');
        }
    }
}
