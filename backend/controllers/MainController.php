<?php
/**
 * Main controller description
 */

namespace backend\controllers;

use Yii;
use backend\components\BackendController;

class MainController extends BackendController {

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Home page
     * @return array
     */
    public function actionIndex() {
        // System Information
        $server_info = [
            'Версия NC' => self::NC_VERSION,
            'Версия Yii' => Yii::getVersion(),
            'Версия PHP' => phpversion(),
            'Версия MySQL' => Yii::$app->db->createCommand("SELECT VERSION() as version")->queryColumn()[0],
            'Операционная система' => php_uname()
        ];

        return $this->render('index', ['server_info' => $server_info]);
    }
}
