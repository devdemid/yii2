<?php

/**
 * FAQ - Frequently Asked Questions
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace backend\controllers;

use backend\components\BackendController;
use backend\components\MenuComponent;
use backend\models\Search;
use backend\widgets\Notify;
use common\models\FaqAnswers;
use common\models\Faq;
use Yii;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class FaqController extends BackendController {

    public $pageSize = 20; // Number of entries per page

    /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeAction($action) {
        if (parent::beforeAction($action)) {

            if (ArrayHelper::isIn($action->id, ['index', 'settings', 'add'])) {
                $this->headMenu[] = new MenuComponent([
                    'label' => 'Добавить ветку',
                    'icon' => 'icon-plus22 position-left',
                    'url' => Url::to(Yii::$app->controller->id . '/add'),
                ]);
            }

            if (ArrayHelper::isIn($action->id, ['branch', 'sorting', 'add-answer', 'edit-answer'])) {
                $item_id = (int) Yii::$app->request->get('item_id');
                $this->headMenu[] = new MenuComponent([
                    'label' => 'Добавить запись',
                    'icon' => 'icon-plus22 position-left',
                    'url' => Url::to(Yii::$app->controller->id . '/add-answer?item_id=' . $item_id),
                ]);
            }

            return true;
        }

        return false;
    }

    /**
     * All faq`s
     * @return [type] [description]
     */
    public function actionIndex() {

        // Remember links for btnAction
        Url::remember(Yii::$app->request->url, 'save-exit');
        Url::remember('/' . Yii::$app->controller->id . '/add', 'save-new');

        $this->view->title = "FAQ Вопросы и ответы";
        $search = (new Search)->apply(Yii::$app->request->get('search'));
        $total = Faq::find()->andFilterWhere(['AND', ['like', 'name', $search]])->count();

        $pages = new Pagination([
            'totalCount' => $total,
            'pageSize' => $this->pageSize,
        ]);

        $models = Faq::find()
            ->andFilterWhere(['AND', ['like', 'name', $search]])
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', [
            'models' => $models,
            'pages' => $pages,
            'total' => $total,
            'search' => $search,
        ]);
    }

    /**
     * Add FAQ
     * @return [string]
     */
    public function actionAdd() {
        $model = new Faq;

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            Yii::$app->session->setFlash('success', 'Запись успешно добавлена!');
            return $this->redirectBtnAction('/' . Yii::$app->controller->id . '/settings?id=' . $model->id, 'save');
        }

        return $this->render('item', ['model' => $model]);
    }

    /**
     * Settings FAQ
     * @param  [integer] $id
     * @return [string]
     */
    public function actionSettings($id) {
        $model = Faq::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            Yii::$app->session->setFlash('success', 'Запись успешно отредактирована!');
            return $this->redirectBtnAction(Yii::$app->request->url, 'save');
        }

        return $this->render('item', ['model' => $model]);
    }

    /**
     * Remove FAQ
     * @param  int $id
     * @return [string]
     */
    public function actionRemove($id) {
        $model = Faq::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $model->delete();

        Yii::$app->session->setFlash('success', 'Запись успешно удалена!');
        return $this->redirect(Url::previous('save-exit'));
    }

    /**
     * Published Status Updates
     * @param  integer $id [description]
     * @return array
     */
    public function actionPublished($id) {
        if (Yii::$app->request->isAjax) {

            $model = Faq::findOne((int) $id);

            if (is_null($model)) {
                return Notify::danger('Не удалось найти #ID:' . inval($id) . '!');
            }

            if ((boolean) Yii::$app->request->get('published')) {
                $model->published = filter_var(Yii::$app->request->get('published'), FILTER_VALIDATE_BOOLEAN);
            }

            $model->update(false);

            return Notify::success('Запись #ID:' . $model->id . ' успешно обновленна!');
        }
    }

    /**
     * FAQ Branch
     * @param  [integer] $id
     * @return [string]
     */
    public function actionBranch($item_id) {
        $faqModel = Faq::findOne((int) $item_id);

        if (is_null($faqModel)) {
            throw new \yii\web\NotFoundHttpException();
        }

        // Remember links for btnAction
        Url::remember(Yii::$app->request->url, 'save-exit');
        Url::remember('/' . Yii::$app->controller->id . '/add-answer?item_id=' . $faqModel->id, 'save-new');

        $this->view->title = $faqModel->name;
        $search = (new Search)->apply(Yii::$app->request->get('search'));
        $total = FaqAnswers::find()
            ->where(['item_id' => $faqModel->id])
            ->andFilterWhere(['AND', ['like', 'name', $search]])
            ->count();

        $pages = new Pagination([
            'totalCount' => $total,
            'pageSize' => $this->pageSize,
        ]);

        $models = FaqAnswers::find()
            ->where(['item_id' => $faqModel->id])
            ->andFilterWhere(['AND', ['like', 'name', $search]])
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy([
                FaqAnswers::ORDER_BY_SORTING => SORT_ASC,
            ])
            ->all();

        return $this->render('branch', [
            'models' => $models,
            'pages' => $pages,
            'total' => $total,
            'search' => $search,
            'faqModel' => $faqModel,
        ]);
    }

    /**
     * Add FAQ Answer
     * @param  [integer] $item_id
     * @return [string]
     */
    public function actionAddAnswer($item_id) {
        $faqModel = Faq::findOne( (int)$item_id );

        if (is_null($faqModel)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $model = new FaqAnswers;
        $model->item_id = $faqModel->id;

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            Yii::$app->session->setFlash('success', 'Запись успешно добавлена!');
            return $this->redirectBtnAction('/' . Yii::$app->controller->id . '/edit-answer?id=' . $model->id, 'save');
        }

        return $this->render('answer-item', [
            'faqModel' => $faqModel,
            'model' => $model
        ]);
    }

    /**
     * Edit FAQ Answer
     * @param  [integer] $id
     * @return [string]
     */
    public function actionEditAnswer($id) {
         $model = FaqAnswers::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            Yii::$app->session->setFlash('success', 'Запись успешно отредактирована!');
            return $this->redirectBtnAction(Yii::$app->request->url, 'save');
        }

        return $this->render('answer-item', [
            'faqModel' => Faq::findOne( (int)$model->item_id ),
            'model' => $model
        ]);
    }

    /**
     * Remove FAQ Answer
     * @param  [integer] $id
     * @return [string]
     */
    public function actionRemoveAnswer($id) {
        $model = FaqAnswers::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $model->delete();

        Yii::$app->session->setFlash('success', 'Запись успешно удалена!');
        return $this->redirect(Url::previous('save-exit'));
    }

    /**
     * Published Status Updates
     * @param  integer $id [description]
     * @return array
     */
    public function actionPublishedAnswer($id) {
        if (Yii::$app->request->isAjax) {

            $model = FaqAnswers::findOne((int) $id);

            if (is_null($model)) {
                return Notify::danger('Не удалось найти #ID:' . inval($id) . '!');
            }

            if ((boolean) Yii::$app->request->get('published')) {
                $model->published = filter_var(Yii::$app->request->get('published'), FILTER_VALIDATE_BOOLEAN);
            }

            $model->update(false);

            return Notify::success('Запись #ID:' . $model->id . ' успешно обновленна!');
        }
    }

    /**
     * Sorting the order of answers
     * @param integer $item_id
     * @return array
     */
    public function actionSorting($item_id) {
        $faqModel = Faq::findOne((int) $item_id);

        if (is_null($faqModel)) {
            throw new \yii\web\NotFoundHttpException();
        }

        // Remember links for btnAction
        Url::remember(Yii::$app->request->url, 'save-exit');
        Url::remember('/' . Yii::$app->controller->id . '/add?item_id=' . $faqModel->id, 'save-new');

        $this->view->title = 'Сортировать ответы';

        if(Yii::$app->request->isAjax && Yii::$app->request->get('Sorting', false)) {
            $sorting = Yii::$app->request->get('Sorting');
            $model = FaqAnswers::find()
                ->where(['IN', 'id', array_keys($sorting)])
                ->all();

            foreach ($model as $item) {
                $item->sorting = (int) $sorting[$item->id];
                $item->update(false);
            }

            Yii::$app->session->setFlash('success', 'Сортировка выполнена успешно!');
            unset($model, $sorting);
        }

        $total = FaqAnswers::find()->where(['item_id' => $faqModel->id])->count();

        $pages = new Pagination([
            'totalCount' => $total,
            'pageSize' => $this->pageSize,
        ]);

        $models = FaqAnswers::find()
            ->where(['item_id' => $faqModel->id])
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy([FaqAnswers::ORDER_BY_SORTING => SORT_ASC])
            ->all();

        return $this->render('sorting', [
            'models' => $models,
            'pages' => $pages,
            'total' => $total,
            'search' => $search,
            'faqModel' => $faqModel,
        ]);
    }
}