<?php
/**
 * Dynamic form processing controller
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace backend\controllers;

use backend\components\BackendController;
use backend\components\MenuComponent;
use backend\models\Search;
use backend\widgets\Notify;
use common\models\GroupActions;
use common\models\Form;
use common\models\FormField;
use common\models\FormResult;
use Yii;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Response;

class FormController extends BackendController {

    public $pageSize = 20; // Number of entries per page

    /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeAction($action) {
        if (parent::beforeAction($action)) {

            // Checking for permission
            // if (!Yii::$app->user->can('accessForm')) {
            //     throw new ForbiddenHttpException('Access denied');
            // }
            $_actions = ['index', 'settings', 'add'];

            if (ArrayHelper::isIn($action->id, $_actions)) {
                $this->headMenu[] = new MenuComponent([
                    'label' => 'Добавить форму',
                    'icon' => 'icon-plus22 position-left',
                    'url' => Url::to(Yii::$app->controller->id . '/add'),
                ]);
            }
            return true;
        }
        return false;
    }

    /**
     * All forms
     * @return array
     */
    public function actionIndex() {

        // Remember links for btnAction
        Url::remember(Yii::$app->request->url, 'save-exit');
        Url::remember('/' . Yii::$app->controller->id . '/add', 'save-new');

        $this->view->title = 'Генератор форм';
        $this->breadcrumbs[] = $this->view->title;
        $search = (new Search)->apply(Yii::$app->request->get('search'));
        $total = Form::find()
            ->andFilterWhere(['AND', ['like', 'name', $search]])
            ->count();

        $pages = new Pagination([
            'totalCount' => $total,
            'pageSize' => $this->pageSize,
        ]);

        $models = Form::find()
            ->andFilterWhere(['AND', ['like', 'name', $search]])
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', [
            'forms' => $models,
            'pages' => $pages,
            'total' => $total,
            'search' => $search,
        ]);
    }

    /**
     * Add form
     * @return array
     */
    public function actionAdd() {
        $model = new Form;
        //$model_field = new FormField;

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            Yii::$app->session->setFlash('success', 'Форма успешно добавлена!');
            return $this->redirectBtnAction('/' . Yii::$app->controller->id . '/settings?id=' . $model->id, 'save');
        }

        return $this->render('item', ['model' => $model]);
    }

    /**
     * Settings form
     * @param int $id
     * @return array
     */
    public function actionSettings($id) {
        $model = Form::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            Yii::$app->session->setFlash('success', 'Форма успешно отредактирована!');
            return $this->redirectBtnAction(Yii::$app->request->url, 'save');
        }

        return $this->render('item', ['model' => $model]);
    }

    /**
     * Remove form
     * @param int $id
     * @return array
     */
    public function actionRemove($id) {
        $model = Form::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $model->delete();

        Yii::$app->session->setFlash('success', 'Форма успешно удалена!');
        return $this->redirect(Url::previous('save-exit'));
    }

    /**
     * Published Status Updates
     * @param  integer $id [description]
     * @return [array]
     */
    public function actionPublished($id) {
        if (Yii::$app->request->isAjax) {

            $model = Form::find()
                ->select(['id', 'published'])
                ->where(['id' => (int) $id])
                ->one();

            if (is_null($model)) {
                return Notify::danger('Не удалось найти #ID:' . inval($id) . '!');
            }

            if ((boolean) Yii::$app->request->get('published')) {
                $model->published = filter_var(Yii::$app->request->get('published'), FILTER_VALIDATE_BOOLEAN);
            }

            $model->update(false);

            return Notify::success('Запись #ID:' . $model->id . ' успешно обновленна!');
        }
    }

    /**
     * All form results
     * @param integer $id
     * @return array
     */
    public function actionResults($id) {
        // Remember links for btnAction
        Url::remember(Yii::$app->request->url, 'save-exit');

        // Begin mass group actions
        $group_actions = new GroupActions;
        $group_actions->addActions(FormResult::getGroupActions());

        if (Yii::$app->request->isPost) {
            if ($group_actions->load(Yii::$app->request->post()) && $group_actions->validate()) {
                FormResult::runGroupActions($group_actions->action, $group_actions->selected_id);
                $group_actions->unsetAttributes();
            } else {
                Yii::$app->session->setFlash('error', \yii\helpers\Html::errorSummary($group_actions));
            }
        }
        // End mass group actions

        $form = Form::findOne((int) $id);

        if (is_null($form)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $search = (new Search)->apply(Yii::$app->request->get('search'));

        $total = FormResult::find()
            ->where(['item_id' => (int) $id])
        //->andFilterWhere(['AND', ['like', 'to_email', $search]])
            ->count();

        $pages = new Pagination([
            'totalCount' => $total,
            'pageSize' => $this->pageSize,
        ]);

        $models = FormResult::find()
            ->where(['item_id' => (int) $id])
        //->andFilterWhere(['AND', ['like', 'to_email', $search]])
            ->orderBy([FormResult::ORDER_BY_SORTING => SORT_DESC])
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('results', [
            'form' => $form,
            'group_actions' => $group_actions,
            'models' => $models,
            'pages' => $pages,
            'search' => $search,
            'total' => $total,
        ]);
    }

    /**
     * Details form result
     * @param  integer $id
     * @return array
     */
    public function actionResultDetail($id) {
        $model = FormResult::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        // Update viewed
        if(!$model->viewed) {
            $model->viewed = FormResult::STATUS_VIEWED_YES;
            $model->update();
        }

        return $this->render('result-detail', ['model' => $model]);
    }

    /**
     * Deleting a form result
     * @param  integer $id
     * @return array
     */
    public function actionRemoveResult($id) {
        $model = FormResult::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $model->delete();

        Yii::$app->session->setFlash('success', 'Запись успешно удалена!');
        return $this->redirect(Url::previous('save-exit'));
    }

    /**
     * Add or edit a form field
     * @param  integer $id
     * @param  integer $item_id
     * @return string
     */
    public function actionField($id = false, $item_id = false) {
        if (!Yii::$app->request->isAjax) {
            die('Access denied!');
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        // Edit field
        if ($item_id) {
            $model = FormField::findOne((int) $item_id);
        }

        // New field
        elseif ($id) {
            $model = new FormField;
            $model->item_id = (int) $id;
            $model->sorting = FormField::find()
                ->where(['item_id' => $model->item_id])
                ->count() + 1;
        }

        if (is_null($model)) {
            return Notify::danger('Не удалось загрузить форму по ID!');
        }

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save(true)) {
                $Response['exit'] = true;
            } else {
                $Response['errors'] = $model->getErrors();
            }

        }

        $Response['body'] = $this->renderPartial('modal-body', ['model' => $model]);

        return $Response;
    }

    /**
     * Delete form field
     * @param  integer $id
     * @return json/string
     */
    public function actionRemoveField($id) {
        $model = FormField::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $item_id = $model->item_id;
        $model->delete();

        Yii::$app->session->setFlash('success', 'Поле успешно удалено!');
        return $this->redirect('/' . Yii::$app->controller->id . '/settings?id=' . $item_id);
    }

    /**
     * Sorting form fields
     * @return string
     */
    public function actionFieldSorting() {
        if (!Yii::$app->request->isAjax) {
            die('Access denied!');
        }

        $sorting = Yii::$app->request->get('Sorting');
        $model = FormField::find()
            ->where(['IN', 'id', array_keys($sorting)])
            ->all();

        foreach ($model as $item) {
            $item->sorting = (int) $sorting[$item->id];
            $item->update(false);
        }

        Yii::$app->session->setFlash('success', 'Сортировка выполнена успешно!');
        return true;
    }
}