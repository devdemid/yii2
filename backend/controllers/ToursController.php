<?php
/**
 * Tours
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace backend\controllers;

use backend\components\BackendController;
use backend\components\MenuComponent;
use backend\models\Search;
use backend\widgets\Notify;
use common\models\TourPrices;
use common\models\TourPrograms;
use common\models\Tours;
use Yii;
use yii\data\Pagination;
use yii\helpers\Url;

class ToursController extends BackendController
{

    public $pageSize = 20; // Number of entries per page

    /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->headMenu[] = new MenuComponent([
                'label' => 'Добавить запись',
                'icon' => 'icon-plus22 position-left',
                'url' => Url::to(Yii::$app->controller->id . '/add'),
            ]);

            return true;
        }
        return false;
    }

    /**
     * All tours
     * @return String
     */
    public function actionIndex()
    {
        // Remember links for btnAction
        Url::remember(Yii::$app->request->url, 'save-exit');
        Url::remember('/' . Yii::$app->controller->id . '/add', 'save-new');

        $search = (new Search)->apply(Yii::$app->request->get('search'));
        $total = Tours::find()
            ->andFilterWhere(['AND', ['like', 'name', $search]])
            ->count();

        $pages = new Pagination([
            'totalCount' => $total,
            'pageSize' => $this->pageSize,
        ]);

        $models = Tours::find()
        //->andFilterWhere(['AND', ['like', 'name', $search]])
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', [
            'search' => $search,
            'pages' => $pages,
            'models' => $models,
        ]);
    }

    /**
     * Add tour
     * @return string
     */
    public function actionAdd()
    {
        $model = new Tours;
        $model->published = Tours::PUBLISHED_ON;

        // Remember links for btnAction
        Url::remember(Yii::$app->request->url, 'save-exit');
        Url::remember('/' . Yii::$app->controller->id . '/add', 'save-new');

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            Yii::$app->session->setFlash('success', 'Тур успешно добавлена!');
            return $this->redirectBtnAction('/' . Yii::$app->controller->id . '/edit?id=' . $model->id, 'save');
        }

        return $this->render('item', [
            'model' => $model,
        ]);
    }

    /**
     * Edit tour
     * @return string
     */
    public function actionEdit($id)
    {
        $model = Tours::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        // Remember links for btnAction
        Url::remember(Yii::$app->request->url, 'save-exit');
        Url::remember('/' . Yii::$app->controller->id . '/add', 'save-new');

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            Yii::$app->session->setFlash('success', 'Тур успешно отредактирована!');
            return $this->redirectBtnAction(Yii::$app->request->url, 'save');
        }

        return $this->render('item', [
            'model' => $model,
        ]);
    }

    /**
     * Remove tour
     * @return string
     */
    public function actionRemove($id)
    {
        $model = Tours::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $model->delete();

        Yii::$app->session->setFlash('success', 'Тур успешно удалена!');
        return $this->redirect(Url::previous('save-exit'));
    }

    /**
     * Published Status Updates
     * @param  integer $id [description]
     * @return [array]
     */
    public function actionPublished($id)
    {
        if (Yii::$app->request->isAjax) {

            $model = Tours::find()
                ->select(['id', 'published'])
                ->where(['id' => (int) $id])
                ->one();

            if (is_null($model)) {
                return Notify::danger('Не удалось найти #ID:' . inval($id) . '!');
            }

            if ((boolean) Yii::$app->request->get('published')) {
                $model->published = filter_var(Yii::$app->request->get('published'), FILTER_VALIDATE_BOOLEAN);
            }

            $model->update(false);

            return Notify::success('Запись #ID:' . $model->id . ' успешно обновленна!');
        }
    }

    /**
     * [actionBulkEditing description]
     * @return [type] [description]
     */
    public function actionBulkEditing() {

        $search = (new Search)->apply(Yii::$app->request->get('search'));
        $models = TourPrices::find()
            ->andFilterWhere(['AND', ['like', 'name', $search]]);

        $pages = new Pagination([
            'totalCount' => $models->count(),
            'pageSize' => 50,
        ]);

        $models
            ->offset($pages->offset)
            ->limit($pages->limit);

        return $this->render('bulk-editing', [
            'models' => $models->all(),
            'pages' => $pages,
            'search' => $search->text
        ]);
    }

    /**
     * Add price
     * @param  Integer $item_id
     * @return String
     */
    public function actionAddPrice($item_id)
    {
        if (!$item_id || !is_numeric($item_id)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $model = new TourPrices;
        $model->item_id = (int) $item_id;

        // Remember links for btnAction
        Url::remember(Yii::$app->request->url, 'save-new');

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            Yii::$app->session->setFlash('success', 'Стоимость успешно добавлена!');
            return $this->redirectBtnAction('/' . Yii::$app->controller->id . '/edit-price?id=' . $model->id, 'save');
        }

        return $this->render('item-price', [
            'model' => $model,
        ]);
    }

    /**
     * Edit price
     * @param  Integer $id
     * @return String
     */
    public function actionEditPrice($id)
    {
        $model = TourPrices::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        // Remember links for btnAction
        Url::remember('/' . Yii::$app->controller->id . '/add-price?item_id=' . $model->item_id, 'save-new');

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            Yii::$app->session->setFlash('success', 'Стоимость успешно отредактирована!');
            return $this->redirectBtnAction(Yii::$app->request->url, 'save');
        }

        return $this->render('item-price', [
            'model' => $model,
        ]);
    }

    /**
     * Remove price
     * @param  Integer $id
     * @return String
     */
    public function actionRemovePrice($id)
    {
        $model = TourPrices::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $model->delete();

        Yii::$app->session->setFlash('success', 'Стоимость успешно удалена!');
        return $this->redirect(Url::previous('save-exit'));
    }

    /**
     * Published Status Updates
     * @param  integer $id [description]
     * @return [array]
     */
    public function actionPublishedPrice($id) {
        if (Yii::$app->request->isAjax) {

            $model = TourPrices::find()
                ->select(['id', 'published'])
                ->where(['id' => (int) $id])
                ->one();

            if (is_null($model)) {
                return Notify::danger('Не удалось найти #ID:' . inval($id) . '!');
            }

            if ((boolean) Yii::$app->request->get('published')) {
                $model->published = filter_var(Yii::$app->request->get('published'), FILTER_VALIDATE_BOOLEAN);
            }

            $model->update(false);

            return Notify::success('Запись #ID:' . $model->id . ' успешно обновленна!');
        }
    }

    /**
     * Add program
     * @return String
     */
    public function actionAddProgram($item_id) {

        if (!$item_id || !is_numeric($item_id)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $model = new TourPrograms;
        $model->item_id = (int) $item_id;
        $model->published = TourPrograms::PUBLISHED_ON;

        // Remember links for btnAction
        Url::remember(Yii::$app->request->url, 'save-new');

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            Yii::$app->session->setFlash('success', 'Программа успешно добавлена!');
            return $this->redirectBtnAction('/' . Yii::$app->controller->id . '/edit-program?id=' . $model->id, 'save');
        }

        return $this->render('item-program', [
            'model' => $model
        ]);
    }

    /**
     * Edit program
     * @return String
     */
    public function actionEditProgram($id) {
        $model = TourPrograms::findOne((int)$id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        // Remember links for btnAction
        Url::remember('/' . Yii::$app->controller->id . '/add-program?item_id=' . $model->item_id, 'save-new');

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            Yii::$app->session->setFlash('success', 'Программа успешно отредактирована!');
            return $this->redirectBtnAction(Yii::$app->request->url, 'save');
        }

        return $this->render('item-program', [
            'model' => $model
        ]);
    }

    /**
     * Remove program
     * @return String
     */
    public function actionRemoveProgram($id) {
         $model = TourPrograms::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $model->delete();

        Yii::$app->session->setFlash('success', 'Программа успешно удалена!');
        return $this->redirect(Url::previous('save-exit'));
    }

    /**
     * Sorting categories
     */
    public function actionSortingProgram() {
        if(Yii::$app->request->isAjax && Yii::$app->request->get('Sorting', false)) {
            $sorting = Yii::$app->request->get('Sorting', false);

            $models = TourPrograms::find()
                ->where(['id' => array_keys($sorting)])
                ->all();

            if(count($models)) {
                foreach ($models as $item) {
                    $item->sorting = (int) $sorting[$item->id];
                    $item->update();
                }

                Yii::$app->session->setFlash('success', 'Сортировка выполнена успешно!');
                unset($models, $sorting);
            }
        }
    }

    /**
     * Published Status Updates
     * @param  integer $id [description]
     * @return [array]
     */
    public function actionPublishedProgram($id) {
        if (Yii::$app->request->isAjax) {

            $model = TourPrograms::find()
                ->select(['id', 'published'])
                ->where(['id' => (int) $id])
                ->one();

            if (is_null($model)) {
                return Notify::danger('Не удалось найти #ID:' . inval($id) . '!');
            }

            if ((boolean) Yii::$app->request->get('published')) {
                $model->published = filter_var(Yii::$app->request->get('published'), FILTER_VALIDATE_BOOLEAN);
            }

            $model->update(false);

            return Notify::success('Запись #ID:' . $model->id . ' успешно обновленна!');
        }
    }
}
