<?php
/**
 * Upload files
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace backend\controllers;

use backend\components\BackendController;
use common\models\Files;
//use yii\imagine\Image;
//use Imagine\Image\Box;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

class UploadController extends BackendController {

    public function beforeAction($action) {
        if (parent::beforeAction($action)) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return true;
        }
        return false;
    }

    /**
     * Upload files
     * @return array
     */
    public function actionFile() {
        if (Yii::$app->request->isPost) {

            $file = new Files(['scenario' => Files::SCENARIO_FILE]);

            // Load form data
            if ($file->load(Yii::$app->request->post())) {

                //$file->setPath('/files/upload/');
                $file->setRules(Yii::$app->params['upload'][Files::SCENARIO_FILE]);

                if ($file->upload()) {
                    return [
                        'filelink' => $file->getFileLink(),
                        'item_id' => $file->item_id,
                        'url' => Files::REMOVE_LINK,
                        'key' => $file->id
                    ];
                }
            }
        }
        return [];
    }

    /**
     * Upload images
     * @return array
     */
    public function actionImage() {
        if (Yii::$app->request->isPost) {

            $file = new Files(['scenario' => Files::SCENARIO_IMAGE]);

            // Load form data
            if ($file->load(Yii::$app->request->post())) {

                //$file->setPath('/files/upload/');
                $file->setRules(Yii::$app->params['upload'][Files::SCENARIO_IMAGE]);

                if ($file->upload()) {
                    return [
                        'filelink' => Yii::getAlias('@files') . $file->getFileLink(),
                        'item_id' => $file->item_id,
                        'url' => Files::REMOVE_LINK,
                        'key' => $file->id
                    ];
                }
            }
        }
        return [];
    }

    /**
     * Upload module images
     * @return array
     */
    public function actionModuleImage() {
        return [];
    }

    /**
     * Delete the file from the server
     * @return [array
     */
    public function actionRemove() {
        if (Yii::$app->request->isPost) {
            $id = (int) Yii::$app->request->post('key');
            $file = Files::findOne($id);

            if (is_null($file)) {
                throw new NotFoundHttpException('Нет такого файла');
            }

            $file->delete();

            return ['delete' => $id];
        }

        return [];
    }

    /**
     * Description for images
     * @return [array]
     */
    public function actionImageAlt() {
        return [];
    }
}