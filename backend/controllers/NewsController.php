<?php
/**
 * Managing the News Feeds
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace backend\controllers;

use backend\components\BackendController;
use backend\components\MenuComponent;
use backend\models\Field;
use backend\models\Search;
use backend\widgets\Notify;
use common\models\News;
use common\models\NewsCat;
use common\models\NewsFeed;
use common\models\TagGroup;
use Yii;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;

class NewsController extends BackendController
{

    public $pageSize = 20; // Number of entries per page

    /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {

            // Checking for permission News Feed
            $_actions_newsfeed = [
                'feed', 'add-feed', 'edit-feed', 'remove-feed', 'published-feed', 'removefield', 'addfield',
                'cat-add', 'cat-edit', 'cat-remove', 'cat-sorting',
            ];
            $_accsessNewsFeed = ArrayHelper::isIn($action->id, $_actions_newsfeed) && !Yii::$app->user->can('accessNewsFeed');

            // Checking for permission News
            $_actions_news = ['index', 'add', 'edit', 'remove', 'sorting', 'published'];
            $_accsessNews = ArrayHelper::isIn($action->id, $_actions_news) && !Yii::$app->user->can('accessNews');

            // Verify permissions
            if ($_accsessNewsFeed || $_accsessNews) {
                throw new ForbiddenHttpException('Access denied');
            }

            // Button on the main page of the news line
            if ($action->id == 'feed') {
                // Add button in the top menu
                $this->headMenu[] = new MenuComponent([
                    'label' => 'Добавить ленту',
                    'icon' => 'icon-plus22 position-left',
                    'url' => Url::to(Yii::$app->controller->id . '/add-feed'),
                ]);
            }

            // Button on the main news page
            if (ArrayHelper::isIn($action->id, ['index', 'add', 'edit', 'sorting'])) {
                $item_id = (int) Yii::$app->request->get('item_id');

                // Add button in the top menu
                if ($item_id) {
                    $this->headMenu[] = new MenuComponent([
                        'label' => 'Добавить новость',
                        'icon' => 'icon-plus22 position-left',
                        'url' => Url::to(Yii::$app->controller->id . '/add?item_id=' . $item_id),
                    ]);
                }
            }

            return true;
        }
        return false;
    }

    /**
     * All news feed
     * @return string
     */
    public function actionFeed()
    {

        // Remember links for btnAction
        Url::remember(Yii::$app->request->url, 'save-exit');
        Url::remember('/' . Yii::$app->controller->id . '/add-feed', 'save-new');

        $this->view->title = 'Новостные ленты';
        $this->breadcrumbs[] = $this->view->title;
        $search = (new Search)->apply(Yii::$app->request->get('search'));
        $total = NewsFeed::find()
            ->andFilterWhere(['AND', ['like', 'name', $search]])
            ->count();

        $pages = new Pagination([
            'totalCount' => $total,
            'pageSize' => $this->pageSize,
        ]);

        $models = NewsFeed::find()
            ->andFilterWhere(['AND', ['like', 'name', $search]])
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('news-feed', [
            'feeds' => $models,
            'pages' => $pages,
            'total' => $total,
            'search' => $search,
        ]);
    }

    /**
     * All news
     * @return string
     */
    public function actionIndex($item_id)
    {
        $newsFeed = NewsFeed::findOne((int) $item_id);

        if (is_null($newsFeed)) {
            throw new \yii\web\NotFoundHttpException();
        }

        // Remember links for btnAction
        Url::remember(Yii::$app->request->url, 'save-exit');
        Url::remember('/' . Yii::$app->controller->id . '/add?item_id=' . $newsFeed->id, 'save-new');

        $this->view->title = $newsFeed->name;
        $search = (new Search)->apply(Yii::$app->request->get('search'));
        $total = News::find()
            ->where(['item_id' => $newsFeed->id])
            ->andFilterWhere(['AND', ['like', 'name', $search]])
            ->count();

        $pages = new Pagination([
            'totalCount' => $total,
            'pageSize' => $this->pageSize,
        ]);

        $models = News::find()
            ->where(['item_id' => $newsFeed->id])
            ->andFilterWhere(['AND', ['like', 'name', $search]])
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy([
                News::ORDER_BY_FIXED => SORT_DESC,
                News::ORDER_BY_SORTING => SORT_ASC,
            ])
            ->all();

        return $this->render('index', [
            'news' => $models,
            'pages' => $pages,
            'total' => $total,
            'search' => $search,
            'newsFeed' => $newsFeed,
        ]);
    }

    /**
     * Add feed
     * @return string
     */
    public function actionAddFeed()
    {
        $model = new NewsFeed;

        // Remember links for btnAction
        Url::remember(Yii::$app->request->url, 'save-exit');

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            Yii::$app->session->setFlash('success', 'Новостная лента успешно добавлена!');
            return $this->redirectBtnAction('/' . Yii::$app->controller->id . '/edit-feed?id=' . $model->id, 'save');
        }

        return $this->render('item-feed', ['model' => $model]);
    }

    /**
     * Add news
     * @return string
     */
    public function actionAdd()
    {
        $model = new News;
        $modelFeed = NewsFeed::findOne((int) Yii::$app->request->get('item_id'));

        if (is_null($modelFeed)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $model->item_id = $modelFeed->id;

        // Loading the Field Model
        $field_model = Field::getModel($model->feedModel->field_options, $model->fields);

        if ($model->load(Yii::$app->request->post()) && $field_model->validate() && $model->save(true)) {

            // Data updates for dynamic fields
            $model->fields = $field_model->fieldAttributes;
            $model->update(true);

            Yii::$app->session->setFlash('success', 'Новость успешно добавлена!');
            return $this->redirectBtnAction('/' . Yii::$app->controller->id . '/edit?id=' . $model->id, 'save');
        }

        return $this->render('item', [
            'model' => $model,
            'field_model' => $field_model,
            'newsFeed' => NewsFeed::findOne((int) $model->item_id),
        ]);
    }

    /**
     * Edit feed
     * @param int $id
     * @return string
     */
    public function actionEditFeed($id)
    {
        $model = NewsFeed::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        // Remember links for btnAction
        Url::remember(Yii::$app->request->url, 'save-exit');
        Url::remember('/' . Yii::$app->controller->id . '/cat-add?item_id=' . $model->id, 'save-new');

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            Yii::$app->session->setFlash('success', 'Новостная лента успешно отредактирована!');
            return $this->redirectBtnAction(Yii::$app->request->url, 'save');
        }

        return $this->render('item-feed', ['model' => $model]);
    }

    /**
     * Edit news
     * @param int $id
     * @return string
     */
    public function actionEdit($id)
    {
        $model = News::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        // Loading the Field Model
        $field_model = Field::getModel($model->feedModel->field_options, $model->fields);

        if ($model->load(Yii::$app->request->post()) && $field_model->validate() && $model->save(true)) {

            // Data updates for dynamic fields
            $model->fields = $field_model->fieldAttributes;
            $model->update(true);

            Yii::$app->session->setFlash('success', 'Новость успешно отредактирована!');
            return $this->redirectBtnAction(Yii::$app->request->url, 'save');
        }

        return $this->render('item', [
            'model' => $model,
            'field_model' => $field_model,
            'newsFeed' => NewsFeed::findOne((int) $model->item_id),
        ]);
    }

    /**
     * Remove feed
     * @param int $id
     * @return string
     */
    public function actionRemoveFeed($id)
    {
        $model = NewsFeed::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $model->delete();

        Yii::$app->session->setFlash('success', 'Новостная лента успешно удалена!');
        return $this->redirect(Url::previous('save-exit'));
    }

    /**
     * Remove news
     * @param int $id
     * @return string
     */
    public function actionRemove($id)
    {
        $model = News::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $model->delete();

        Yii::$app->session->setFlash('success', 'Новость успешно удалена!');
        return $this->redirect(Url::previous('save-exit'));
    }

    /**
     * Sorting the order of news
     * @param integer $item_id
     * @return array
     */
    public function actionSorting($item_id)
    {
        $newsFeed = NewsFeed::findOne((int) $item_id);

        if (is_null($newsFeed)) {
            throw new \yii\web\NotFoundHttpException();
        }

        // Remember links for btnAction
        Url::remember(Yii::$app->request->url, 'save-exit');
        Url::remember('/' . Yii::$app->controller->id . '/add?item_id=' . $newsFeed->id, 'save-new');

        $this->view->title = 'Сортировать новости';

        if (Yii::$app->request->isAjax && Yii::$app->request->get('Sorting', false)) {
            $sorting = Yii::$app->request->get('Sorting');
            $model = News::find()
                ->where(['IN', 'id', array_keys($sorting)])
                ->all();

            foreach ($model as $item) {
                $item->sorting = (int) $sorting[$item->id];
                $item->update(false);
            }

            Yii::$app->session->setFlash('success', 'Сортировка выполнена успешно!');
            unset($model, $sorting);
        }

        $total = News::find()->where(['item_id' => $newsFeed->id])->count();

        $pages = new Pagination([
            'totalCount' => $total,
            'pageSize' => $this->pageSize,
        ]);

        $models = News::find()
            ->where(['item_id' => $newsFeed->id])
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy([
                News::ORDER_BY_FIXED => SORT_DESC,
                News::ORDER_BY_SORTING => SORT_ASC,
            ])
            ->all();

        return $this->render('sorting', [
            'news' => $models,
            'pages' => $pages,
            'total' => $total,
            'newsFeed' => $newsFeed,
        ]);
    }

    /**
     * Published Status Updates
     * @param integer $id [description]
     * @return [array]
     */
    public function actionPublishedFeed($id)
    {
        if (Yii::$app->request->isAjax) {
            $model = NewsFeed::find()
                ->select(['id', 'published'])
                ->where(['id' => (int) $id])
                ->one();

            if (is_null($model)) {
                return Notify::danger('Не удалось найти #ID:' . inval($id) . '!');
            }

            //var_dump($model); exit;

            if ((boolean) Yii::$app->request->get('published')) {
                $model->published = filter_var(Yii::$app->request->get('published'), FILTER_VALIDATE_BOOLEAN);
            }

            $model->update(false);

            return Notify::success('Запись #ID:' . $model->id . ' успешно обновленна!');
        }
    }

    /**
     * Published Status Updates
     * @param  integer $id [description]
     * @return [array]
     */
    public function actionPublished($id)
    {
        if (Yii::$app->request->isAjax) {

            $model = News::find()
                ->select(['id', 'published'])
                ->where(['id' => (int) $id])
                ->one();

            if (is_null($model)) {
                return Notify::danger('Не удалось найти #ID:' . inval($id) . '!');
            }

            if ((boolean) Yii::$app->request->get('published')) {
                $model->published = filter_var(Yii::$app->request->get('published'), FILTER_VALIDATE_BOOLEAN);
            }

            $model->update(false);

            return Notify::success('Запись #ID:' . $model->id . ' успешно обновленна!');
        }
    }

    /**
     * Add categories
     * @return [array]
     */
    public function actionCatAdd($item_id)
    {
        $model = new NewsCat;
        $model->item_id = (int) $item_id;

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            $model->sorting = (int) NewsCat::find()
                ->where(['item_id' => $model->item_id])
                ->count();

            $model->update(true);

            Yii::$app->session->setFlash('success', 'Категория успешно добавлена!');
            return $this->redirectBtnAction('/' . Yii::$app->controller->id . '/cat-edit?id=' . $model->id, 'save');
        }

        return $this->render('item-cat', ['model' => $model]);
    }

    /**
     * Edit categories
     * @param  [integer] $id
     * @return [array]
     */
    public function actionCatEdit($id)
    {
        $model = NewsCat::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            $model->update(true);

            Yii::$app->session->setFlash('success', 'Категория успешно отредактирована!');
            return $this->redirectBtnAction(Yii::$app->request->url, 'save');
        }

        return $this->render('item-cat', ['model' => $model]);
    }

    /**
     * Remove categorie
     * @param  [integer] $id
     * @return [array]
     */
    public function actionCatRemove($id)
    {
        $model = NewsCat::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $model->delete();

        Yii::$app->session->setFlash('success', 'Категория успешно удалена!');
        return $this->redirect(Url::previous('save-exit'));
    }

    /**
     * Sorting categories
     */
    public function actionCatSorting()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->get('Sorting', false)) {
            $sorting = Yii::$app->request->get('Sorting', false);

            $models = NewsCat::find()
                ->where(['id' => array_keys($sorting)])
                ->all();

            if (count($models)) {
                foreach ($models as $item) {
                    $item->sorting = (int) $sorting[$item->id];
                    $item->update();
                }

                Yii::$app->session->setFlash('success', 'Сортировка выполнена успешно!');
                unset($models, $sorting);
            }
        }
    }

    /**
     * Add Tag Group
     * @param  Integer $item_id
     * @return String
     */
    public function actionAddTagGroup($item_id)
    {
        $model = new TagGroup;
        $model->published = TagGroup::PUBLISHED_ON;
        $model->item_id = (int) $item_id;
        $model->module = 'NewsFeed';

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            $model->update(true);

            Yii::$app->session->setFlash('success', 'Группа тегов успешно добавлена!');
            return $this->redirectBtnAction('/' . Yii::$app->controller->id . '/edit-tag-group?id=' . $model->id, 'save');
        }

        return $this->render('item-tag-group', ['model' => $model]);
    }

    /**
     * Edit Tag Group
     * @param  Integer $id
     * @return String
     */
    public function actionEditTagGroup($id)
    {
        $model = TagGroup::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            $model->update(true);

            Yii::$app->session->setFlash('success', 'Группа тегов успешно отредактирована!');
            return $this->redirectBtnAction(Yii::$app->request->url, 'save');
        }

        return $this->render('item-tag-group', ['model' => $model]);
    }

    public function actionRemoveTagGroup($id)
    {
        $model = TagGroup::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $model->delete();

        Yii::$app->session->setFlash('success', 'Группа тегов успешно удалена!');
        return $this->redirect(Url::previous('save-exit'));
    }

    /**
     * Processing Dynamic Field Settings
     * @return array
     */
    public function actions()
    {
        return [
            'removefield' => [
                'class' => 'backend\components\actions\RemoveFieldAction',
                'model' => NewsFeed::className(),
                'property' => 'field_options',
            ],
            'addfield' => [
                'class' => 'backend\components\actions\AddFieldAction',
                'model' => NewsFeed::className(),
                'property' => 'field_options',
            ],
        ];
    }
}
