<?php
/**
 * Management sliders
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace backend\controllers;

use backend\components\BackendController;
use backend\components\MenuComponent;
use backend\models\Field;
use backend\widgets\Notify;
use common\models\Slide;
use common\models\Slider;
use Yii;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class SliderController extends BackendController {

    public $pageSize = 20; // Number of entries per page

    /**
     * @inheritdoc
     * @return boolean
     */
    public function beforeAction($action) {
        if (parent::beforeAction($action)) {

            // Checking for permission
            if (!Yii::$app->user->can('accessSliders')) {
                throw new ForbiddenHttpException('Access denied');
            }

            // Add button in the top menu
            if (ArrayHelper::isIn(Yii::$app->controller->action->id, [
                'index', 'add', 'edit', 'remove',
            ])) {
                $this->headMenu[] = new MenuComponent([
                    'label' => 'Добавить слайдер',
                    'icon' => 'icon-plus22 position-left',
                    'url' => Url::to(Yii::$app->controller->id . '/add'),
                ]);
            }

            // Add button in the top menu
            if (ArrayHelper::isIn(Yii::$app->controller->action->id, [
                'items', 'item', 'remove_item',
            ])) {
                $this->headMenu[] = new MenuComponent([
                    'label' => 'Добавить новый слайд',
                    'icon' => 'icon-plus22 position-left',
                    'url' => Url::to(Yii::$app->controller->id . '/' . Yii::$app->request->get('id') . '/item'),
                ]);
            }

            // // Private variable
            // $_actions = ['add', 'edit', 'remove'];

            // // Checking for permission
            // if (ArrayHelper::isIn($action->id, $_actions) && !\Yii::$app->user->can('createSettings')) {
            //     throw new ForbiddenHttpException('Access denied');
            // }

            return true;
        }

        return false;
    }

    /**
     * All tv channels
     * @return string
     */
    public function actionIndex() {

        // Remember links for btnAction
        Url::remember(Yii::$app->request->url, 'save-exit');
        Url::remember('/' . Yii::$app->controller->id . '/add', 'save-new');

        $this->view->title = 'Генератор слайдеров';
        $this->breadcrumbs[] = $this->view->title;

        $total = Slider::find()->count();

        $pages = new Pagination([
            'totalCount' => $total,
            'pageSize' => $this->pageSize,
        ]);

        $slidersModel = Slider::find()
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', [
            'sliders' => $slidersModel,
            'pages' => $pages,
            'total' => $total,
            'search' => '',
        ]);
    }

    /**
     * Add slider
     * @return string
     */
    public function actionAdd() {
        $model = new Slider;

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            Yii::$app->session->setFlash('success', 'Запись успешно добавлена!');
            return $this->redirectBtnAction('/' . Yii::$app->controller->id . '/settings?id=' . $model->id, 'save');
        }

        return $this->render('item', ['model' => $model]);
    }

    /**
     * Settings slider
     * @param  int $id
     * @return string
     */
    public function actionSettings($id) {
        $model = Slider::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            Yii::$app->session->setFlash('success', 'Запись успешно отредактирована!');
            return $this->redirectBtnAction(Yii::$app->request->url, 'save');
        }

        return $this->render('item', [
            'model' => $model,
        ]);
    }

    /**
     * Remove slider
     * @param  int $id
     * @return string
     */
    public function actionRemove($id) {
        $model = Slider::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $model->delete();

        Yii::$app->session->setFlash('success', 'Запись успешно удалена!');
        return $this->redirect(Url::previous('save-exit'));
    }

    /**
     * Translation Status Updates
     * @param  integer $id [description]
     * @return array
     */
    public function actionPublished($id) {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $model = Slider::find()
                ->select(['id', 'published'])
                ->where(['id' => (int) $id])
                ->one();

            if (is_null($model)) {
                return Notify::danger('Не удалось найти #ID:' . inval($id) . '!');
            }

            if ((boolean) Yii::$app->request->get('published')) {
                $model->published = filter_var(Yii::$app->request->get('published'), FILTER_VALIDATE_BOOLEAN);
            }

            $model->update(false);

            return Notify::success('Запись #ID:' . $model->id . ' успешно обновленна!');
        }
    }

    /**
     * Translation Status Updates
     * @param  integer $id [description]
     * @return array
     */
    public function actionPublishedSlide($id) {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $model = Slide::find()
                ->select(['id', 'published'])
                ->where(['id' => (int) $id])
                ->one();

            if (is_null($model)) {
                return Notify::danger('Не удалось найти #ID:' . inval($id) . '!');
            }

            if ((boolean) Yii::$app->request->get('published')) {
                $model->published = filter_var(Yii::$app->request->get('published'), FILTER_VALIDATE_BOOLEAN);
            }

            $model->update(false);

            return Notify::success('Запись #ID:' . $model->id . ' успешно обновленна!');
        }
    }

    /**
     * Slider images
     * @param  int $id
     * @return string
     */
    public function actionItems($id) {
        $sliderModel = Slider::findOne((int) $id);

        if (is_null($sliderModel)) {
            throw new \yii\web\NotFoundHttpException();
        }

        // Remember links for btnAction
        Url::remember(Yii::$app->request->url, 'save-exit');
        Url::remember('/' . Yii::$app->controller->id . '/' . $sliderModel->id . '/item', 'save-new');

        $this->view->title = $sliderModel->name;

        $total = Slide::find()
            ->where('item_id = ' . $sliderModel->id)
            ->count();

        $pages = new Pagination([
            'totalCount' => $total,
            'pageSize' => $this->pageSize,
        ]);

        $slidesModel = Slide::find()
            ->where('item_id = ' . $sliderModel->id)
            ->orderBy(Slide::ORDER_BY_SORTING)
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('slides', [
            'slides' => $slidesModel,
            'slider' => $sliderModel,
            'total' => $total,
            'pages' => $pages,
        ]);
    }

    /**
     * Additions, Slide editing
     * @param  integer $id
     * @param  integer $slide
     * @return string
     */
    public function actionItem($id, $slide = null) {
        $sliderModel = Slider::findOne((int) $id);

        if (!is_null($slide) && intval($slide)) {
            $slideModel = Slide::findOne((int) $slide);
        } else {
            $slideModel = new Slide;
            $slideModel->item_id = $sliderModel->id;
        }

        if (is_null($slideModel) && is_null($sliderModel) || is_null($sliderModel)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $this->view->title = ($slideModel->isNewRecord ? 'Добавить' : 'Редактировать') . ' слайд';

        // Loading the Field Model
        $field_model = Field::getModel($sliderModel->field_options, $slideModel->fields);

        if ($slideModel->load(Yii::$app->request->post()) && $field_model->validate() && $slideModel->validate()) {
            if ($slideModel->isNewRecord) {
                Yii::$app->session->setFlash('success', 'Запись успешно добавлена!');
            } else {
                Yii::$app->session->setFlash('success', 'Запись успешно отредактирована!');
            }

            $slideModel->save(true);

            // Data updates for dynamic fields
            $slideModel->fields = $field_model->fieldAttributes;
            $slideModel->update(true);

            return $this->redirectBtnAction('/' . Yii::$app->controller->id . '/' . $sliderModel->id . '/item?slide=' . $slideModel->id, 'save');
        }

        return $this->render('slide-item', [
            'model' => $slideModel,
            'slider' => $sliderModel,
            'field_model' => $field_model
        ]);
    }

    /**
     * Remove slide
     * @param  int $id
     * @return string
     */
    public function actionItemRemove() {

        $id = Yii::$app->request->get('id');

        $model = Slide::findOne((int) $id);

        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $model->delete();

        Yii::$app->session->setFlash('success', 'Запись успешно удалена!');
        return $this->redirect(Url::previous('save-exit'));
    }

    /**
     * Sort slides
     * @param  [integer] $id
     */
    public function actionSorting($id) {
        $sliderModel = Slider::findOne((int) $id);

        if (is_null($sliderModel)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $this->view->title = 'Сортировать';

        $slidesModel = Slide::find()
            ->where('item_id = ' . $sliderModel->id)
            ->orderBy(Slide::ORDER_BY_SORTING)
            ->all();

        // Update sorting
        if (Yii::$app->request->isPost) {
            $Sorting = Yii::$app->request->post('Sorting');

            foreach ($slidesModel as $item) {
                $item->sorting = $Sorting[$item->id];
                $item->update(false);
            }

            Yii::$app->session->setFlash('success', 'Записи успешно отсортированы!');
            //$this->redirect('/' . Yii::$app->controller->id . '/sorting', 302);
        }

        return $this->render('sorting', [
            'sliderModel' => $sliderModel,
            'slidesModel' => $slidesModel,
        ]);
    }

    /**
     * Processing Dynamic Field Settings
     * @return array
     */
    public function actions() {
        return [
            'removefield' => [
                'class' => 'backend\components\actions\RemoveFieldAction',
                'model' => Slider::className(),
                'property' => 'field_options',
            ],
            'addfield' => [
                'class' => 'backend\components\actions\AddFieldAction',
                'model' => Slider::className(),
                'property' => 'field_options',
            ],
        ];
    }
}