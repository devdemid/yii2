<?php
// Global Editor settings
return [
    // Some options, see http://imperavi.com/redactor/docs/
    // Fix: https://github.com/vova07/yii2-imperavi-widget/issues/89
    'imperavi' => [

        'options' => [
            //'toolbar' => false,

            'minHeight' => '400px',
            'toolbarFixedTopOffset' => '100px',

            // Images
            'imageUpload' => '/upload/image/',
            'imageUploadParam' => 'Files[uploadedFile]',
            'uploadImageFields' => [],

            //Files
            'fileUpload' => '/upload/files/',
            'fileUploadParam' => 'Files[uploadedFile]',
            'uploadFileFields' => [],

            //'fileUpload' => '/upload/files/',
            //'fileManagerJson' => '/upload/filemanager/',
        ],

        'plugins' => [
            //'clips',
            //'imagemanager',
            //'filemanager',

            'video',
            'fontfamily',
            'fontsize',
            'fontcolor',
            'properties',
            'table',
            'filemanager',
            //'codemirror',
            'fullscreen'
        ],

        // 'codemirror' => [
        //     'lineNumbers' => true,
        //     'mode' => 'html',
        //     'indentUnit' => 4
        // ]
    ],
];