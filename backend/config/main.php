<?php
$params = array_merge(
    require (__DIR__ . '/../../common/config/params.php'),
    require (__DIR__ . '/params.php'),
    require (__DIR__ . '/editors.php')
);

if(YII_DEV) {
    $params = array_merge($params,
        require (__DIR__ . '/../../common/config/params-local.php'),
        require (__DIR__ . '/params-local.php')
    );
}

return [
    'id' => 'YII',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'language' => 'ru-RU',
    //'on beforeRequest' => '',
    //'on afterRequest' => '',
    'modules' => [],
    'components' => [

        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => '/users/signin',
        ],

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],

        'errorHandler' => [
            'errorAction' => 'main/error',
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<controller:\w+>/<id:\d+>/<action:\w+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ],
        ],

        'assetManager' => [
            'linkAssets' => false,
            'appendTimestamp' => true,

            // 'bundles' => [
            //     'yii\web\JqueryAsset' => [
            //         'jsOptions' => [ 'position' => \yii\web\View::POS_HEAD ],
            //     ],
            // ],
            // 'converter' => [
            //  'class' => 'yii\web\AssetConverter',
            //  'commands' => [
            //      'less' => ['css', 'lessc {from} {to} --no-color'],
            //      'ts' => ['js', 'tsc --out {to} {from}'],
            //  ],
            // ],
        ],
    ],

    'params' => $params,
];
