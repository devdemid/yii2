$(function () {
    var
        $table = $('.tree-table'),
        rows = $table.find('tr');

    rows.each(function (index, row) {
        var
            $row = $(row),
            level = $row.data('level'),
            id = $row.data('id'),
            parent = $row.data('parent'),
            $columnName = $row.find('td[data-column="name"]'),
            children = $table.find('tr[data-parent="' + id + '"]');

        $columnName.prepend('<span class="treegrid-indent" style="margin-left:'+ (level * 16) +'px"></span>');
        var $indent = $columnName.find('.treegrid-indent');

        if (children.length) {
            var $expander = $indent.prepend('<span class="treegrid-expander icon-arrow-right22"></span>');

            children.hide();
            $expander.on('click', function (e) {
                var $target = $(e.target);
                if ($target.hasClass('icon-arrow-right22')) {
                    $target
                        .removeClass('icon-arrow-right22')
                        .addClass('icon-arrow-down22');

                    children.show();
                    $row.addClass('open');
                } else {
                    $target
                        .removeClass('icon-arrow-down22')
                        .addClass('icon-arrow-right22');

                    $row.removeClass('open');
                    reverseHide($table, $row);
                }
            });
        }

        else {
            $indent.css('width', 0);
        }
    });

    // Reverse hide all elements
    reverseHide = function (table, element) {
        var
            $element = $(element),
            id = $element.data('id'),
            children = table.find('tr[data-parent="' + id + '"]');

        if (children.length) {
            children.each(function (i, e) {
                reverseHide(table, e);
            });

            $element
                .find('.icon-arrow-down22')
                .removeClass('icon-arrow-down22')
                .addClass('icon-arrow-right22');

            children.hide();
        }
    };
});