$(function() {

    // Initializing Libraries for Modal Windows
    $(document).ajaxComplete(function(e, xhr, settings) {
        var $modal = $('.modal .modal-body');

        $('.select', $modal).select2({
            minimumResultsForSearch: Infinity
        });

        $(".styled, .multiselect-container input", $modal).uniform({
            radioClass: 'choice'
        });
    });


    // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        // save the latest tab; use cookies if you like 'em better:
        localStorage.setItem('lastTab', $(this).attr('href'));
    });

    // go to the latest tab, if it exists:
    if($('ul.nav-tabs').length) {
        var lastTab = localStorage.getItem('lastTab');
        if (lastTab) {
            $('[href="' + lastTab + '"]').tab('show');
        } else {
            $('ul.nav-tabs > li:first-child > a').tab('show');
        }
    }

    // Initialize responsive functionality
    $('.table-togglable').footable();

    // Modal ajax
    $('a[data-toggle="modal-ajax"]').click(function() {
        var $this = $(this),
            modal = $($this.data('target')),
            href = $this.attr('href') || false;

        $.get(href, {}, function(Response) {
            $('.modal-content', modal).html(Response);
            modal.modal('show');
        });

        return false;
    });

    // Primary file input
    $(".file-styled-primary").uniform({
        fileButtonClass: 'action btn bg-blue'
    });

    // https://github.com/kartik-v/bootstrap-fileinput/
    // Display preview on load
    $(".file-input-overwrite").fileinput({
        language: 'ru',
        browseIcon: '<i class="icon-file-plus"></i>',
        uploadIcon: '<i class="icon-file-upload2"></i>',
        removeIcon: '<i class="icon-cross3"></i>',
        // initialPreview: [
        //  "<img src='/images/placeholder.jpg' class='file-preview-image' alt=''>",
        //  "<img src='/images/placeholder.jpg' class='file-preview-image' alt=''>",
        // ],
        overwriteInitial: true
    });

    // AJAX upload
    $(".file-input-ajax").each(function() {
        var $this = $(this), $previews = [], $initialPreviewConfig = [];

        $this.on('filesuccessremove', function(event, id) {
            console.log('Remove', event, id);
        });

        // This event is triggered only for ajax uploads and after upload is completed for each thumbnail file
        $this.on('fileuploaded', function(event, data, previewId, index) {
            if(data.response.item_id == false) {
                $(event.target).after('<input type="hidden" name="FilesTemp[]" value="' + data.response.key + '">');
            }
        });

        $($this.data('preview')).each(function(key, preview) {
            $previews.push("<img src='" + preview.file + "' class='file-preview-image'>");
            $initialPreviewConfig.push({
                url: preview.delete,
                key: preview.id,
                size: preview.size
            });
        });

        $this.fileinput({
            language: $this.data('lang'),
            uploadUrl: $this.data('upload') || '/upload/files', // server upload action
            uploadAsync: true,
            maxFileCount: $this.data('maxFileCount') || 1,

            // Previews option
            initialPreview: $previews,
            initialPreviewConfig: $initialPreviewConfig,

            initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
            overwriteInitial: false,
            uploadExtraData: function() {
                return $this.data('params') || {};
            },
            fileActionSettings: {
                removeIcon: '<i class="icon-bin"></i>',
                removeClass: 'btn btn-link btn-xs btn-icon',
                uploadIcon: '<i class="icon-upload"></i>',
                uploadClass: 'btn btn-link btn-xs btn-icon',
                indicatorNew: '<i class="icon-file-plus text-slate"></i>',
                indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
                indicatorError: '<i class="icon-cross2 text-danger"></i>',
                indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>',
            }
        });
    });

    // Switchery
    // ------------------------------

    //Initialize multiple switches

    $('.switchery').each(function() {
        var $this = $(this);
        $this.click(function() {
            $this.prop('checked', $this.is(':checked') ? true : false);
        });

        var switchery = new Switchery($this.get(0));
    });

    // Date
    $('[data-format-pattern]').each(function() {
        var $this = $(this);
        $this.formatter({
            pattern: $this.data('format-pattern')
        });
    });



    // Checkboxes/radios (Uniform)
    // ------------------------------

    // Default initialization
    $(".styled, .multiselect-container input").uniform({
        radioClass: 'choice'
    });

    // File input
    $(".file-styled").uniform({
        fileButtonClass: 'action btn btn-primary btn-file',
        fileButtonHtml: '<i class="glyphicon glyphicon-folder-open mr-5"></i> Выберите файл',
        fileDefaultHtml: 'Файл не выбран'
    });


    //
    // Contextual colors
    //

    // Primary
    $(".control-primary").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-primary-600 text-primary-800'
    });

    // Danger
    $(".control-danger").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-danger-600 text-danger-800'
    });

    // Success
    $(".control-success").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-success-600 text-success-800'
    });

    // Warning
    $(".control-warning").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-warning-600 text-warning-800'
    });

    // Info
    $(".control-info").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-info-600 text-info-800'
    });

    // Custom color
    $(".control-custom").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-indigo-600 text-indigo-800'
    });



    // Bootstrap switch
    // ------------------------------

    $(".switch").bootstrapSwitch();


    // Basic examples
    // ------------------------------

    // Default initialization
    $('.select').select2({
        minimumResultsForSearch: Infinity
    });


    // Select API
    $('select[data-src-api]').each(function() {
        $(this).select2({
            ajax: {
                url: $(this).data('src-api') || '/',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        search: params.term
                    };
                },
                processResults: function (data, params) {
                    return {
                        results: data.items
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
            minimumInputLength: 1,
            templateResult: function(Result) {
                if(Result.loading) return Result.text;
                return "<div class='select2-result-repository clearfix'>" +
                            "<div class='select2-result-repository__title'>" + Result.name + "</div>" +
                       "</div>";
            },
            templateSelection: function(Selection) {
                return Selection.name || Selection.text;
            }
        });
    });

    // Numeric date
    $("#anytime-month-numeric").AnyTime_picker({
        format: "%Z-%m-%d"
    });

     // Single picker
    $('.daterange-single').daterangepicker({
        singleDatePicker: true,
        locale: {
            format: 'YYYY-DD-MM h:mm:ss'
        }
    });

    // Dropdown selectors
    $('.pickadate-selectors').pickadate({
        monthsFull: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октября', 'Ноябрь', 'Декабрь'],
        weekdaysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        selectYears: true,
        selectMonths: true,
        selectYears: 10,
        format: 'yyyy-mm-dd'
    });

     // Attach typeahead
    $('.tagsinput-typeahead').tagsinput('input');/*.typeahead(
        {
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            name: 'states',
            displayKey: 'value',
            source: substringMatcher(['Test'])
        }
    ).bind('typeahead:selected', $.proxy(function (obj, datum) {
        this.tagsinput('add', datum.value);
        this.tagsinput('input').typeahead('val', '');
    }, $('.tagsinput-typeahead')));*/


    //     // Format selection
    // function formatRepoSelection (repo) {
    //     return repo.full_name || repo.text;
    // }

    // //
    // // Loading remote data
    // //

    // // Format displayed data
    // function formatRepo (repo) {
    //     if (repo.loading) return repo.text;

    //     var markup = "<div class='select2-result-repository clearfix'>" +
    //         "<div class='select2-result-repository__title'>" + repo.full_name + "</div>";
    //     markup += "</div>";

    //     return markup;
    // }

    // // Format selection
    // function formatRepoSelection (repo) {
    //     return repo.full_name || repo.text;
    // }

    // // Initialize
    // $(".select-remote-data").select2({
    //     ajax: {
    //         url: "https://api.github.com/search/repositories",
    //         dataType: 'json',
    //         delay: 250,
    //         data: function (params) {
    //             return {
    //                 q: params.term, // search term
    //                 page: params.page
    //             };
    //         },
    //         processResults: function (data, params) {

    //             // parse the results into the format expected by Select2
    //             // since we are using custom formatting functions we do not need to
    //             // alter the remote JSON data, except to indicate that infinite
    //             // scrolling can be used
    //             params.page = params.page || 1;

    //             return {
    //                 results: data.items,
    //                 pagination: {
    //                     more: (params.page * 30) < data.total_count
    //                 }
    //             };
    //         },
    //         cache: true
    //     },
    //     escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
    //     minimumInputLength: 1,
    //     templateResult: formatRepo, // omitted for brevity, see the source of this page
    //     templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
    // });


    //
    // Table tree
    //
});

// Matcher
var substringMatcher = function(strs) {
    return function findMatches(q, cb) {
        var matches, substringRegex;

        // an array that will be populated with substring matches
        matches = [];

        // regex used to determine if a string contains the substring `q`
        substrRegex = new RegExp(q, 'i');

        // iterate through the pool of strings and for any string that
        // contains the substring `q`, add it to the `matches` array
        $.each(strs, function(i, str) {
            if (substrRegex.test(str)) {

                // the typeahead jQuery plugin expects suggestions to a
                // JavaScript object, refer to typeahead docs for more info
                matches.push({ value: str });
            }
        });
        cb(matches);
    };
};

/**
 * Request information changes
 * @param DomElement el
 * @param string method
 * @param string url
 */
function ChangeRequest(url, params) {
    $.get(url, params || {}, function(Response) {
        if(Response.Notify) {
            new PNotify(Response.Notify);
        }
    });
}

/**
 * Random Password Generation
 * @param  {int} length
 * @return {string}
 */
function randomPassword(min, max) {
    var chars  = 'abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890',
        length = randomInteger(min, max),
        passwd = '';

    for (var x = 0; x < length; x++) {
        passwd += chars.charAt(Math.floor(Math.random() * chars.length));
    }
    return passwd;
}

/**
 * Random integer from min to max
 * @param  {int} min
 * @param  {int} max
 * @return {int}
 */
function randomInteger(min, max) {
    return Math.round(min - 0.5 + Math.random() * (max - min + 1));
}

/**
 * Ajax server requests
 * @param  {object} form
 * @param  {string} return_id
 * @param  {Function} callback
 * @return {boolean}
 */
function ajaxRequest(form, return_id, callback) {
    var $form = $(form);
    $[$form.attr('method')]($form.attr('action'), $form.serialize(), function(Response) {
        $(return_id).html(Response.body);

        if(Response.Notify) {
            new PNotify(Response.Notify);
        }

        if($.isFunction(callback)) {
            callback.call(this, Response);
        }
    }, 'json');
    return false;
}

/**
 * Sort records
 * @param {string} url
 * @param {string} name
 * @return {boolean}
 */
function Sorting(url, name) {
    var params = {};
    $('[name^='+ name +']').each(function() {
        var $this = $(this)
        params[$this.attr('name')] = $this.val();
    });

    $.get(url, params, function(Response) {
        window.location.reload();
    });

    return false;
}

/**
 * [checkboxAll description]
 * @param  {string} name
 * @return {boolean}
 */
function checkboxAll(checkbox, name) {
    var $checkbox = $(checkbox);
    $('[name^='+ name +']').each(function() {
        $(this).prop('checked', $checkbox.is(':checked'));
        $.uniform.update(this);
    });
    return true;
}