<?php

$timezonesList = [
    'Africa/Abidjan' => '+0000',
    'Africa/Abidjan' => '+0000',
    'Africa/Accra' => '+0000',
    'Africa/Addis_Ababa' => '+0300',
    'Africa/Algiers' => '+0100',
    'Africa/Asmara' => '+0300',
    'Africa/Asmera' => '+0300',
    'Africa/Bamako' => '+0000',
    'Africa/Bangui' => '+0100',
    'Africa/Banjul' => '+0000',
    'Africa/Bissau' => '+0000',
    '11' => 'Africa/Blantyre',
    '12' => 'Africa/Brazzaville',
    '13' => 'Africa/Bujumbura',
    '14' => 'Africa/Cairo',
    '15' => 'Africa/Casablanca',
    '16' => 'Africa/Ceuta',
    '17' => 'Africa/Conakry',
    '18' => 'Africa/Dakar',
    '19' => 'Africa/Dar_es_Salaam',
    '20' => 'Africa/Djibouti',
    '21' => 'Africa/Douala',
    '22' => 'Africa/El_Aaiun',
    '23' => 'Africa/Freetown',
    '24' => 'Africa/Gaborone',
    '25' => 'Africa/Harare',
    '26' => 'Africa/Johannesburg',
    '27' => 'Africa/Juba',
    '28' => 'Africa/Kampala',
    '29' => 'Africa/Khartoum',
    '30' => 'Africa/Kigali',
    '31' => 'Africa/Kinshasa',
    '32' => 'Africa/Lagos',
    '33' => 'Africa/Libreville',
    '34' => 'Africa/Lome',
    '35' => 'Africa/Luanda',
    '36' => 'Africa/Lubumbashi',
    '37' => 'Africa/Lusaka',
    '38' => 'Africa/Malabo',
    '39' => 'Africa/Maputo',
    '40' => 'Africa/Maseru',
    '41' => 'Africa/Mbabane',
    '42' => 'Africa/Mogadishu',
    '43' => 'Africa/Monrovia',
    '44' => 'Africa/Nairobi',
    '45' => 'Africa/Ndjamena',
    '46' => 'Africa/Niamey',
    '47' => 'Africa/Nouakchott',
    '48' => 'Africa/Ouagadougou',
    '49' => 'Africa/Porto-Novo',
    '50' => 'Africa/Sao_Tome',
    '51' => 'Africa/Timbuktu',
    '52' => 'Africa/Tripoli',
    '53' => 'Africa/Tunis',
    '54' => 'Africa/Windhoek',
    '56' => 'America/Adak',
    '57' => 'America/Anchorage',
    '58' => 'America/Anguilla',
    '59' => 'America/Antigua',
    '60' => 'America/Araguaina',
    '61' => 'America/Argentina/Buenos_Aires',
    '62' => 'America/Argentina/Catamarca',
    '63' => 'America/Argentina/ComodRivadavia',
    '64' => 'America/Argentina/Cordoba',
    '65' => 'America/Argentina/Jujuy',
    '66' => 'America/Argentina/La_Rioja',
    '67' => 'America/Argentina/Mendoza',
    '68' => 'America/Argentina/Rio_Gallegos',
    '69' => 'America/Argentina/Salta',
    '70' => 'America/Argentina/San_Juan',
    '71' => 'America/Argentina/San_Luis',
    '72' => 'America/Argentina/Tucuman',
    '73' => 'America/Argentina/Ushuaia',
    '74' => 'America/Aruba',
    '75' => 'America/Asuncion',
    '76' => 'America/Atikokan',
    '77' => 'America/Atka',
    '78' => 'America/Bahia',
    '79' => 'America/Bahia_Banderas',
    '80' => 'America/Barbados',
    '81' => 'America/Belem',
    '82' => 'America/Belize',
    '83' => 'America/Blanc-Sablon',
    '84' => 'America/Boa_Vista',
    '85' => 'America/Bogota',
    '86' => 'America/Boise',
    '87' => 'America/Buenos_Aires',
    '88' => 'America/Cambridge_Bay',
    '89' => 'America/Campo_Grande',
    '90' => 'America/Cancun',
    '91' => 'America/Caracas',
    '92' => 'America/Catamarca',
    '93' => 'America/Cayenne',
    '94' => 'America/Cayman',
    '95' => 'America/Chicago',
    '96' => 'America/Chihuahua',
    '97' => 'America/Coral_Harbour',
    '98' => 'America/Cordoba',
    '99' => 'America/Costa_Rica',
    '578' => 'America/Creston',
    '100' => 'America/Cuiaba',
    '101' => 'America/Curacao',
    '102' => 'America/Danmarkshavn',
    '103' => 'America/Dawson',
    '104' => 'America/Dawson_Creek',
    '105' => 'America/Denver',
    '106' => 'America/Detroit',
    '107' => 'America/Dominica',
    '108' => 'America/Edmonton',
    '109' => 'America/Eirunepe',
    '110' => 'America/El_Salvador',
    '111' => 'America/Ensenada',
    '113' => 'America/Fortaleza',
    '112' => 'America/Fort_Wayne',
    '114' => 'America/Glace_Bay',
    '115' => 'America/Godthab',
    '116' => 'America/Goose_Bay',
    '117' => 'America/Grand_Turk',
    '118' => 'America/Grenada',
    '119' => 'America/Guadeloupe',
    '120' => 'America/Guatemala',
    '121' => 'America/Guayaquil',
    '122' => 'America/Guyana',
    '123' => 'America/Halifax',
    '124' => 'America/Havana',
    '125' => 'America/Hermosillo',
    '126' => 'America/Indiana/Indianapolis',
    '127' => 'America/Indiana/Knox',
    '128' => 'America/Indiana/Marengo',
    '129' => 'America/Indiana/Petersburg',
    '130' => 'America/Indiana/Tell_City',
    '131' => 'America/Indiana/Vevay',
    '132' => 'America/Indiana/Vincennes',
    '133' => 'America/Indiana/Winamac',
    '134' => 'America/Indianapolis',
    '135' => 'America/Inuvik',
    '136' => 'America/Iqaluit',
    '137' => 'America/Jamaica',
    '138' => 'America/Jujuy',
    '139' => 'America/Juneau',
    '140' => 'America/Kentucky/Louisville',
    '141' => 'America/Kentucky/Monticello',
    '142' => 'America/Knox_IN',
    '143' => 'America/Kralendijk',
    '144' => 'America/La_Paz',
    '145' => 'America/Lima',
    '146' => 'America/Los_Angeles',
    '147' => 'America/Louisville',
    '148' => 'America/Lower_Princes',
    '149' => 'America/Maceio',
    '150' => 'America/Managua',
    '151' => 'America/Manaus',
    '152' => 'America/Marigot',
    '153' => 'America/Martinique',
    '154' => 'America/Matamoros',
    '155' => 'America/Mazatlan',
    '156' => 'America/Mendoza',
    '157' => 'America/Menominee',
    '158' => 'America/Merida',
    '159' => 'America/Metlakatla',
    '160' => 'America/Mexico_City',
    '161' => 'America/Miquelon',
    '162' => 'America/Moncton',
    '163' => 'America/Monterrey',
    '164' => 'America/Montevideo',
    '165' => 'America/Montreal',
    '166' => 'America/Montserrat',
    '167' => 'America/Nassau',
    '168' => 'America/New_York',
    '169' => 'America/Nipigon',
    '170' => 'America/Nome',
    '171' => 'America/Noronha',
    '172' => 'America/North_Dakota/Beulah',
    '173' => 'America/North_Dakota/Center',
    '174' => 'America/North_Dakota/New_Salem',
    '175' => 'America/Ojinaga',
    '176' => 'America/Panama',
    '177' => 'America/Pangnirtung',
    '178' => 'America/Paramaribo',
    '179' => 'America/Phoenix',
    '180' => 'America/Port-au-Prince',
    '182' => 'America/Porto_Acre',
    '183' => 'America/Porto_Velho',
    '181' => 'America/Port_of_Spain',
    '184' => 'America/Puerto_Rico',
    '185' => 'America/Rainy_River',
    '186' => 'America/Rankin_Inlet',
    '187' => 'America/Recife',
    '188' => 'America/Regina',
    '189' => 'America/Resolute',
    '190' => 'America/Rio_Branco',
    '191' => 'America/Rosario',
    '193' => 'America/Santarem',
    '192' => 'America/Santa_Isabel',
    '194' => 'America/Santiago',
    '195' => 'America/Santo_Domingo',
    '196' => 'America/Sao_Paulo',
    '197' => 'America/Scoresbysund',
    '198' => 'America/Shiprock',
    '199' => 'America/Sitka',
    '200' => 'America/St_Barthelemy',
    '201' => 'America/St_Johns',
    '202' => 'America/St_Kitts',
    '203' => 'America/St_Lucia',
    '204' => 'America/St_Thomas',
    '205' => 'America/St_Vincent',
    '206' => 'America/Swift_Current',
    '207' => 'America/Tegucigalpa',
    '208' => 'America/Thule',
    '209' => 'America/Thunder_Bay',
    '210' => 'America/Tijuana',
    '211' => 'America/Toronto',
    '212' => 'America/Tortola',
    '213' => 'America/Vancouver',
    '214' => 'America/Virgin',
    '215' => 'America/Whitehorse',
    '216' => 'America/Winnipeg',
    '217' => 'America/Yakutat',
    '218' => 'America/Yellowknife',
    '219' => 'Antarctica/Casey',
    '220' => 'Antarctica/Davis',
    '221' => 'Antarctica/DumontDUrville',
    '222' => 'Antarctica/Macquarie',
    '223' => 'Antarctica/Mawson',
    '224' => 'Antarctica/McMurdo',
    '225' => 'Antarctica/Palmer',
    '226' => 'Antarctica/Rothera',
    '227' => 'Antarctica/South_Pole',
    '228' => 'Antarctica/Syowa',
    '579' => 'Antarctica/Troll',
    '229' => 'Antarctica/Vostok',
    '230' => 'Arctic/Longyearbyen',
    '231' => 'Asia/Aden',
    '232' => 'Asia/Almaty',
    '233' => 'Asia/Amman',
    '234' => 'Asia/Anadyr',
    '235' => 'Asia/Aqtau',
    '236' => 'Asia/Aqtobe',
    '237' => 'Asia/Ashgabat',
    '238' => 'Asia/Ashkhabad',
    '239' => 'Asia/Baghdad',
    '240' => 'Asia/Bahrain',
    '241' => 'Asia/Baku',
    '242' => 'Asia/Bangkok',
    '243' => 'Asia/Beirut',
    '244' => 'Asia/Bishkek',
    '245' => 'Asia/Brunei',
    '246' => 'Asia/Calcutta',
    '247' => 'Asia/Choibalsan',
    '248' => 'Asia/Chongqing',
    '249' => 'Asia/Chungking',
    '250' => 'Asia/Colombo',
    '251' => 'Asia/Dacca',
    '252' => 'Asia/Damascus',
    '253' => 'Asia/Dhaka',
    '254' => 'Asia/Dili',
    '255' => 'Asia/Dubai',
    '256' => 'Asia/Dushanbe',
    '257' => 'Asia/Gaza',
    '258' => 'Asia/Harbin',
    '259' => 'Asia/Hebron',
    '261' => 'Asia/Hong_Kong',
    '262' => 'Asia/Hovd',
    '260' => 'Asia/Ho_Chi_Minh',
    '263' => 'Asia/Irkutsk',
    '264' => 'Asia/Istanbul',
    '265' => 'Asia/Jakarta',
    '266' => 'Asia/Jayapura',
    '267' => 'Asia/Jerusalem',
    '268' => 'Asia/Kabul',
    '269' => 'Asia/Kamchatka',
    '270' => 'Asia/Karachi',
    '271' => 'Asia/Kashgar',
    '272' => 'Asia/Kathmandu',
    '273' => 'Asia/Katmandu',
    '580' => 'Asia/Khandyga',
    '274' => 'Asia/Kolkata',
    '275' => 'Asia/Krasnoyarsk',
    '276' => 'Asia/Kuala_Lumpur',
    '277' => 'Asia/Kuching',
    '278' => 'Asia/Kuwait',
    '279' => 'Asia/Macao',
    '280' => 'Asia/Macau',
    '281' => 'Asia/Magadan',
    '282' => 'Asia/Makassar',
    '283' => 'Asia/Manila',
    '284' => 'Asia/Muscat',
    '285' => 'Asia/Nicosia',
    '286' => 'Asia/Novokuznetsk',
    '287' => 'Asia/Novosibirsk',
    '288' => 'Asia/Omsk',
    '289' => 'Asia/Oral',
    '290' => 'Asia/Phnom_Penh',
    '291' => 'Asia/Pontianak',
    '292' => 'Asia/Pyongyang',
    '293' => 'Asia/Qatar',
    '294' => 'Asia/Qyzylorda',
    '295' => 'Asia/Rangoon',
    '296' => 'Asia/Riyadh',
    '297' => 'Asia/Saigon',
    '298' => 'Asia/Sakhalin',
    '299' => 'Asia/Samarkand',
    '300' => 'Asia/Seoul',
    '301' => 'Asia/Shanghai',
    '302' => 'Asia/Singapore',
    '303' => 'Asia/Taipei',
    '304' => 'Asia/Tashkent',
    '305' => 'Asia/Tbilisi',
    '306' => 'Asia/Tehran',
    '307' => 'Asia/Tel_Aviv',
    '308' => 'Asia/Thimbu',
    '309' => 'Asia/Thimphu',
    '310' => 'Asia/Tokyo',
    '311' => 'Asia/Ujung_Pandang',
    '312' => 'Asia/Ulaanbaatar',
    '313' => 'Asia/Ulan_Bator',
    '314' => 'Asia/Urumqi',
    '581' => 'Asia/Ust-Nera',
    '315' => 'Asia/Vientiane',
    '316' => 'Asia/Vladivostok',
    '317' => 'Asia/Yakutsk',
    '318' => 'Asia/Yekaterinburg',
    '319' => 'Asia/Yerevan',
    '320' => 'Atlantic/Azores',
    '321' => 'Atlantic/Bermuda',
    '322' => 'Atlantic/Canary',
    '323' => 'Atlantic/Cape_Verde',
    '324' => 'Atlantic/Faeroe',
    '325' => 'Atlantic/Faroe',
    '326' => 'Atlantic/Jan_Mayen',
    '327' => 'Atlantic/Madeira',
    '328' => 'Atlantic/Reykjavik',
    '329' => 'Atlantic/South_Georgia',
    '331' => 'Atlantic/Stanley',
    '330' => 'Atlantic/St_Helena',
    '332' => 'Australia/ACT',
    '333' => 'Australia/Adelaide',
    '334' => 'Australia/Brisbane',
    '335' => 'Australia/Broken_Hill',
    '336' => 'Australia/Canberra',
    '337' => 'Australia/Currie',
    '338' => 'Australia/Darwin',
    '339' => 'Australia/Eucla',
    '340' => 'Australia/Hobart',
    '341' => 'Australia/LHI',
    '342' => 'Australia/Lindeman',
    '343' => 'Australia/Lord_Howe',
    '344' => 'Australia/Melbourne',
    '345' => 'Australia/North',
    '346' => 'Australia/NSW',
    '347' => 'Australia/Perth',
    '348' => 'Australia/Queensland',
    '349' => 'Australia/South',
    '350' => 'Australia/Sydney',
    '351' => 'Australia/Tasmania',
    '352' => 'Australia/Victoria',
    '353' => 'Australia/West',
    '354' => 'Australia/Yancowinna',
    '466' => 'Brazil/Acre',
    '467' => 'Brazil/DeNoronha',
    '468' => 'Brazil/East',
    '469' => 'Brazil/West',
    '470' => 'Canada/Atlantic',
    '471' => 'Canada/Central',
    '472' => 'Canada/East-Saskatchewan',
    '473' => 'Canada/Eastern',
    '474' => 'Canada/Mountain',
    '475' => 'Canada/Newfoundland',
    '476' => 'Canada/Pacific',
    '477' => 'Canada/Saskatchewan',
    '478' => 'Canada/Yukon',
    '479' => 'CET',
    '480' => 'Chile/Continental',
    '481' => 'Chile/EasterIsland',
    '482' => 'CST6CDT',
    '483' => 'Cuba',
    '484' => 'EET',
    '485' => 'Egypt',
    '486' => 'Eire',
    '487' => 'EST',
    '488' => 'EST5EDT',
    '489' => 'Etc/GMT',
    '490' => 'Etc/GMT+0',
    '491' => 'Etc/GMT+1',
    '492' => 'Etc/GMT+10',
    '493' => 'Etc/GMT+11',
    '494' => 'Etc/GMT+12',
    '495' => 'Etc/GMT+2',
    '496' => 'Etc/GMT+3',
    '497' => 'Etc/GMT+4',
    '498' => 'Etc/GMT+5',
    '499' => 'Etc/GMT+6',
    '500' => 'Etc/GMT+7',
    '501' => 'Etc/GMT+8',
    '502' => 'Etc/GMT+9',
    '503' => 'Etc/GMT-0',
    '504' => 'Etc/GMT-1',
    '505' => 'Etc/GMT-10',
    '506' => 'Etc/GMT-11',
    '507' => 'Etc/GMT-12',
    '508' => 'Etc/GMT-13',
    '509' => 'Etc/GMT-14',
    '510' => 'Etc/GMT-2',
    '511' => 'Etc/GMT-3',
    '512' => 'Etc/GMT-4',
    '513' => 'Etc/GMT-5',
    '514' => 'Etc/GMT-6',
    '515' => 'Etc/GMT-7',
    '516' => 'Etc/GMT-8',
    '517' => 'Etc/GMT-9',
    '518' => 'Etc/GMT0',
    '519' => 'Etc/Greenwich',
    '520' => 'Etc/UCT',
    '521' => 'Etc/Universal',
    '522' => 'Etc/UTC',
    '523' => 'Etc/Zulu',
    '355' => 'Europe/Amsterdam',
    '356' => 'Europe/Andorra',
    '357' => 'Europe/Athens',
    '358' => 'Europe/Belfast',
    '359' => 'Europe/Belgrade',
    '360' => 'Europe/Berlin',
    '361' => 'Europe/Bratislava',
    '362' => 'Europe/Brussels',
    '363' => 'Europe/Bucharest',
    '364' => 'Europe/Budapest',
    '582' => 'Europe/Busingen',
    '365' => 'Europe/Chisinau',
    '366' => 'Europe/Copenhagen',
    '367' => 'Europe/Dublin',
    '368' => 'Europe/Gibraltar',
    '369' => 'Europe/Guernsey',
    '370' => 'Europe/Helsinki',
    '371' => 'Europe/Isle_of_Man',
    '372' => 'Europe/Istanbul',
    '373' => 'Europe/Jersey',
    '374' => 'Europe/Kaliningrad',
    '375' => 'Europe/Kiev',
    '376' => 'Europe/Lisbon',
    '377' => 'Europe/Ljubljana',
    '378' => 'Europe/London',
    '379' => 'Europe/Luxembourg',
    '380' => 'Europe/Madrid',
    '381' => 'Europe/Malta',
    '382' => 'Europe/Mariehamn',
    '383' => 'Europe/Minsk',
    '384' => 'Europe/Monaco',
    '385' => 'Europe/Moscow',
    '386' => 'Europe/Nicosia',
    '387' => 'Europe/Oslo',
    '388' => 'Europe/Paris',
    '389' => 'Europe/Podgorica',
    '390' => 'Europe/Prague',
    '391' => 'Europe/Riga',
    '392' => 'Europe/Rome',
    '393' => 'Europe/Samara',
    '394' => 'Europe/San_Marino',
    '395' => 'Europe/Sarajevo',
    '396' => 'Europe/Simferopol',
    '397' => 'Europe/Skopje',
    '398' => 'Europe/Sofia',
    '399' => 'Europe/Stockholm',
    '400' => 'Europe/Tallinn',
    '401' => 'Europe/Tirane',
    '402' => 'Europe/Tiraspol',
    '403' => 'Europe/Uzhgorod',
    '404' => 'Europe/Vaduz',
    '405' => 'Europe/Vatican',
    '406' => 'Europe/Vienna',
    '407' => 'Europe/Vilnius',
    '408' => 'Europe/Volgograd',
    '409' => 'Europe/Warsaw',
    '410' => 'Europe/Zagreb',
    '411' => 'Europe/Zaporozhye',
    '412' => 'Europe/Zurich',
    '524' => 'Factory',
    '525' => 'GB',
    '526' => 'GB-Eire',
    '527' => 'GMT',
    '528' => 'GMT+0',
    '529' => 'GMT-0',
    '530' => 'GMT0',
    '531' => 'Greenwich',
    '532' => 'Hongkong',
    '533' => 'HST',
    '534' => 'Iceland',
    '413' => 'Indian/Antananarivo',
    '414' => 'Indian/Chagos',
    '415' => 'Indian/Christmas',
    '416' => 'Indian/Cocos',
    '417' => 'Indian/Comoro',
    '418' => 'Indian/Kerguelen',
    '419' => 'Indian/Mahe',
    '420' => 'Indian/Maldives',
    '421' => 'Indian/Mauritius',
    '422' => 'Indian/Mayotte',
    '423' => 'Indian/Reunion',
    '535' => 'Iran',
    '536' => 'Israel',
    '537' => 'Jamaica',
    '538' => 'Japan',
    '539' => 'Kwajalein',
    '540' => 'Libya',
    '541' => 'MET',
    '542' => 'Mexico/BajaNorte',
    '543' => 'Mexico/BajaSur',
    '544' => 'Mexico/General',
    '545' => 'MST',
    '546' => 'MST7MDT',
    '547' => 'Navajo',
    '548' => 'NZ',
    '549' => 'NZ-CHAT',
    '424' => 'Pacific/Apia',
    '425' => 'Pacific/Auckland',
    '426' => 'Pacific/Chatham',
    '427' => 'Pacific/Chuuk',
    '428' => 'Pacific/Easter',
    '429' => 'Pacific/Efate',
    '430' => 'Pacific/Enderbury',
    '431' => 'Pacific/Fakaofo',
    '432' => 'Pacific/Fiji',
    '433' => 'Pacific/Funafuti',
    '434' => 'Pacific/Galapagos',
    '435' => 'Pacific/Gambier',
    '436' => 'Pacific/Guadalcanal',
    '437' => 'Pacific/Guam',
    '438' => 'Pacific/Honolulu',
    '439' => 'Pacific/Johnston',
    '440' => 'Pacific/Kiritimati',
    '441' => 'Pacific/Kosrae',
    '442' => 'Pacific/Kwajalein',
    '443' => 'Pacific/Majuro',
    '444' => 'Pacific/Marquesas',
    '445' => 'Pacific/Midway',
    '446' => 'Pacific/Nauru',
    '447' => 'Pacific/Niue',
    '448' => 'Pacific/Norfolk',
    '449' => 'Pacific/Noumea',
    '450' => 'Pacific/Pago_Pago',
    '451' => 'Pacific/Palau',
    '452' => 'Pacific/Pitcairn',
    '453' => 'Pacific/Pohnpei',
    '454' => 'Pacific/Ponape',
    '455' => 'Pacific/Port_Moresby',
    '456' => 'Pacific/Rarotonga',
    '457' => 'Pacific/Saipan',
    '458' => 'Pacific/Samoa',
    '459' => 'Pacific/Tahiti',
    '460' => 'Pacific/Tarawa',
    '461' => 'Pacific/Tongatapu',
    '462' => 'Pacific/Truk',
    '463' => 'Pacific/Wake',
    '464' => 'Pacific/Wallis',
    '465' => 'Pacific/Yap',
    '550' => 'Poland',
    '551' => 'Portugal',
    '552' => 'PRC',
    '553' => 'PST8PDT',
    '554' => 'ROC',
    '555' => 'ROK',
    '556' => 'Singapore',
    '557' => 'Turkey',
    '558' => 'UCT',
    '559' => 'Universal',
    '560' => 'US/Alaska',
    '561' => 'US/Aleutian',
    '562' => 'US/Arizona',
    '563' => 'US/Central',
    '564' => 'US/East-Indiana',
    '565' => 'US/Eastern',
    '566' => 'US/Hawaii',
    '567' => 'US/Indiana-Starke',
    '568' => 'US/Michigan',
    '569' => 'US/Mountain',
    '570' => 'US/Pacific',
    '571' => 'US/Pacific-New',
    '572' => 'US/Samoa',
    '573' => 'UTC',
    '574' => 'W-SU',
    '575' => 'WET',
    '576' => 'Zulu',
];

echo "<list>\n";
foreach ($timezonesList as $zone) {
    echo "<timezone>\n";
        echo "<name>". $zone ."</name>\n";
        echo "<utc></utc>\n";
    echo "<timezone>\n";
}
echo "<list>\n";