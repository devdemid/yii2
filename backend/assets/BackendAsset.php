<?php

namespace backend\assets;

use yii\web\AssetBundle;

class BackendAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        // Global stylesheets
        '/css/bootstrap.css',
        '/css/icons/icomoon/styles.css',
        '/css/core.css',
        '/css/components.css',
        '/css/colors.css',
        '/css/customize.css?dev',
    ];

    public $js = [
        // Core JS files
        '/js/plugins/loaders/pace.min.js',
        //'/js/core/libraries/jquery.min.js',
        '/js/core/libraries/bootstrap.min.js',
        '/js/plugins/loaders/blockui.min.js',

        // Theme JS files
        '/js/plugins/forms/styling/uniform.min.js',
        '/js/plugins/forms/styling/switchery.js',
        '/js/plugins/forms/styling/switch.min.js',
        '/js/plugins/tables/footable/footable.min.js',
        '/js/plugins/notifications/pnotify.min.js',
        '/js/core/libraries/jquery_ui/interactions.min.js',
        '/js/plugins/forms/selects/select2.min.js',
        '/js/plugins/uploaders/fileinput.min.js',
        '/js/plugins/uploaders/fileinput/locales/ru.js',
        '/js/plugins/forms/inputs/jquery.mask.min.js',
        '/js/plugins/forms/inputs/formatter.min.js',
        '/js/plugins/pickers/anytime.min.js',
        '/js/plugins/trees/fancytree_all.min.js',
        '/js/plugins/trees/fancytree_childcounter.js',

        // Tags
        '/js/plugins/forms/tags/tagsinput.min.js',
        '/js/plugins/forms/tags/tokenfield.min.js',
        '/js/plugins/ui/prism.min.js',
        '/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js',


        // Date
        '/js/plugins/ui/moment/moment.min.js',
        '/js/plugins/pickers/daterangepicker.js',
        '/js/plugins/pickers/anytime.min.js',
        '/js/plugins/pickers/pickadate/picker.js',
        '/js/plugins/pickers/pickadate/picker.date.js',
        '/js/plugins/pickers/pickadate/picker.time.js',
        '/js/plugins/pickers/pickadate/legacy.js',

        // Componnents
        '/componnents/treetable/treetable.js?v1.0.0',

        // Init
        '/js/core/app.js',
        '/js/core/common.js?v1.0.71',
        //'/js/pages/table_responsive.js',
        //'/js/pages/components_notifications_pnotify.js',
        //'/js/pages/form_checkboxes_radios.js',
        //'/js/pages/form_select2.js',
        //'/js/pages/uploader_bootstrap.js',
        '/js/pages/login.js',

    ];

    public $depends = [
        'backend\assets\ltIEAsset', // Fix IE 9
        'backend\assets\GoogleFonts',
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
        //'yii\bootstrap\BootstrapPluginAsset',
    ];
}
