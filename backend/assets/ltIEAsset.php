<?php

namespace backend\assets;

use yii\web\AssetBundle;

class ltIEAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $jsOptions = [
        'condition' => 'lt IE 9',
    ];

    public $js = [
        '/js/lib/html5shiv/3.7.2/html5shiv.min.js',
        '/js/lib/respond/1.4.2/respond.min.js',
    ];
}