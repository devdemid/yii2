<?php

namespace backend\assets;

use yii\web\AssetBundle;

class GoogleFonts extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900'
    ];
}