<?php

namespace backend\assets;

use yii\web\AssetBundle;

class FontIcons extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        '/css/font-icons/font-awesome.min.css',
        '/css/font-icons/themify-icons.min.css',
    ];
}