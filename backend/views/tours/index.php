<?

use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
// Set the component description
$this->context->smallTitle = 'Список всех туров, Вы можете изменить, удалить или создать новую запись.';

$this->context->view->title = 'Все туры';
$this->context->breadcrumbs[] = $this->context->view->title;

?>
<? if(count($models)): ?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Все туры</h5>
        <small class="display-block text-muted">
            <?= $search ? 'Всего найдено' :  'Всего туров' ?>: <?= $pages->totalCount ?>
        </small>
        <div class="heading-elements">
            <?= Html::beginForm(Url::to([Yii::$app->controller->id . '/index']), 'get', ['class' => 'heading-form']) ?>
                <div class="form-group has-feedback">
                    <?= Html::textInput('search', $search, ['class' => 'form-control input-xs', 'placeholder' => 'Поиск...']) ?>
                    <div class="form-control-feedback">
                        <i class="icon-search4 text-size-base text-muted"></i>
                    </div>
                </div>
            <?= Html::endForm() ?>
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>
    <div class="panel-body">
        <table class="table table-togglable table-hover">
            <thead>
                <tr>
                    <th data-toggle="true" width="5%">#ID</th>
                    <th data-toggle="true" width="55%">Название тура</th>
                    <th data-toggle="true" class="text-center">Рейтинг</th>
                    <th data-toggle="true" class="text-center">Просмотров</th>
                    <th class="text-center" data-toggle="true">Публикация</th>
                    <th class="text-center" style="width: 30px;">
                        <i class="icon-menu-open2" data-popup="tooltip" title="Действие" data-placement="left"></i>
                    </th>
                </tr>
            </thead>
            <tbody>
            <? foreach($models as $item): ?>
                <tr>
                    <td><?= $item->id ?></td>
                    <td>
                        <?= Html::a($item->name, [
                            Yii::$app->controller->id . '/edit?id=' . $item->id
                        ]) ?>
                        <? if($item->fixed): ?>
                            <span class="label label-info ml-5" data-popup="tooltip" title="Запись зафиксирована">
                                Fixed record
                            </span>
                        <? endif ?>
                        <? if($item->selling): ?>
                            <i class="icon-credit-card ml-5" title="Продажа тура" data-popup="tooltip"></i>
                        <? endif; ?>
                    </td>
                    <td class="text-center">
                        <?= $item->rate ?> *
                    </td>
                    <td class="text-center">
                        <?= $item->views ?>
                    </td>
                    <td class="text-center">
                        <div class="checkbox checkbox-switchery switchery-xs" title="Вкл/Выкл">
                            <label>
                                <?= Html::checkbox('Published['. $item->id .']', $item->published, [
                                    'class' => 'switchery',
                                    'onclick' => "ChangeRequest('" . Url::to([
                                        Yii::$app->controller->id . '/published',
                                        'id' => $item->id
                                    ]) . "', {published: this.checked});"
                                ]); ?>
                            </label>
                        </div>
                    </td>
                    <td class="text-center">
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Действия">
                                    <i class="icon-menu7"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <?= Html::a('<i class="icon-pencil5"></i>Редактировать', [
                                            Yii::$app->controller->id . '/edit?id=' . $item->id
                                        ]) ?>
                                    </li>
                                    <li>
                                        <?= Html::a('<i class="icon-bin"></i>Удалить', [
                                            Yii::$app->controller->id . '/remove?id=' . $item->id
                                        ], [
                                            'onclick' => "return confirm('Вы уверены что хотите удалить данные?');"
                                        ]) ?>
                                    </li>
                                </ul>
                            </li>
                        </ul><!-- /.icons-list -->
                    </td>
                </tr>
            <? endforeach; ?>
            </tbody>
        </table>
        </div>
    </div><!-- /.panel-body -->
</div>
<div class="mb-20">
    <?= LinkPager::widget([
        'pagination' => $pages,
        'options'    => [
            'class' => 'pagination pagination-xs'
        ]
    ]); ?>
</div>
<? else: Alert::begin(['options' => ['class' => 'bg-info alert-styled-left']]); ?>
    Не удается найти туры для отображения
<? Alert::end(); endif;?>
<!-- /table with togglable columns -->