<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

if($model->isNewRecord)
    $this->context->view->title = 'Добавить стоимость';
else
    $this->context->view->title = 'Редактировать стоимость';

$this->context->smallTitle = 'Управления стоимостью на заданный период времени.';
$this->context->breadcrumbs[] = [
    'label' => 'Все туры',
    'url'   => '/'. $this->context->id .'/index'
];

$this->context->breadcrumbs[] = [
    'label' => Html::encode($model->tour->name),
    'url'   => '/'. $this->context->id .'/edit?id=' . $model->tour->id
];


$pageName = $this->context->view->title;
$this->context->breadcrumbs[] = $pageName;

?>

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title"><?= $pageName ?></h5>
    </div>
    <div class="panel-body">
    <? $form = ActiveForm::begin([
        'id' => $this->context->id . '-form',
        'options' => [
            'role'    => 'form',
            'class' => 'form-horizontal',
            'enctype' => 'multipart/form-data',
        ]
    ]); ?>
        <div class="row">
            <div class="col-sm-12">
                <?= $form->field($model, 'base_price', [
                    // Begin input theme
                    'template' => '{label}
                        <div class="col-lg-10">
                            <div class="input-group">
                                {input}<span class="input-group-addon">'. $model::DEFAULT_CURRENCY_NAME .'</span>
                            </div>
                            {hint}{error}
                        </div>',
                    // End input theme
                    'inputOptions' => [
                        'class' => 'form-control',
                        'placeholder' => '0.00'
                    ],
                    'options' => [
                        'class' => 'form-group'
                    ]
                ])->hint(false)->label(
                    $model->getAttributeLabelRequired('base_price'),
                    ['class' => 'control-label col-lg-2']
                ) ?>
                <?= $form->field($model, 'discount_price', [
                    // Begin input theme
                    'template' => '{label}
                        <div class="col-lg-10">
                            <div class="input-group">
                                {input}<span class="input-group-addon">'. $model::DEFAULT_CURRENCY_NAME .'</span>
                            </div>
                            {hint}{error}
                        </div>',
                    // End input theme
                    'inputOptions' => [
                        'class' => 'form-control',
                        'placeholder' => '0.00'
                    ],
                    'options' => [
                        'class' => 'form-group'
                    ]
                ])->hint(false)->label(
                    $model->getAttributeLabelRequired('discount_price'),
                    ['class' => 'control-label col-lg-2']
                ) ?>

                <?= $form->field($model, 'children_price', [
                    // Begin input theme
                    'template' => '{label}
                        <div class="col-lg-10">
                            <div class="input-group">
                                {input}<span class="input-group-addon">'. $model::DEFAULT_CURRENCY_NAME .'</span>
                            </div>
                            {hint}{error}
                        </div>',
                    // End input theme
                    'inputOptions' => [
                        'class' => 'form-control',
                        'placeholder' => '0.00'
                    ],
                    'options' => [
                        'class' => 'form-group'
                    ]
                ])->hint(false)->label(
                    $model->getAttributeLabelRequired('children_price'),
                    ['class' => 'control-label col-lg-2']
                ) ?>

                <?= $form->field($model, 'start_date', [
                    // Begin input theme
                    'template' => '{label}
                        <div class="col-lg-10">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="icon-calendar5"></i>
                                </span>
                                {input}
                            </div>
                            {hint}{error}
                        </div>',
                    // End input theme
                    'inputOptions' => [
                        'class' => 'form-control pickadate-selectors',
                        'placeholder' => date($model::PROP_DATE_FORMAT)
                    ],
                    'options' => [
                        'class' => 'form-group'
                    ]
                ])->hint(false)->label(
                    $model->getAttributeLabelRequired('start_date'),
                    ['class' => 'control-label col-lg-2']
                ) ?>

                <?= $form->field($model, 'end_date', [
                    // Begin input theme
                    'template' => '{label}
                        <div class="col-lg-10">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="icon-calendar5"></i>
                                </span>
                                {input}
                            </div>
                            {hint}{error}
                        </div>',
                    // End input theme
                    'inputOptions' => [
                        'class' => 'form-control pickadate-selectors',
                        'placeholder' => date($model::PROP_DATE_FORMAT)
                    ],
                    'options' => [
                        'class' => 'form-group'
                    ]
                ])->hint(false)->label(
                    $model->getAttributeLabelRequired('end_date'),
                    ['class' => 'control-label col-lg-2']
                ) ?>
                <?= $form->field($model, 'short_description', [
                    // Begin input theme
                    'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                    // End input theme
                    'inputOptions' => [
                        'class' => 'form-control',
                        'rows' => '5'
                    ],
                    'options' => [
                        'class' => 'form-group'
                    ]
                ])->hint('Краткое описание или информация об установленном периоде.')->label(
                    $model->getAttributeLabelRequired('short_description'),
                    ['class' => 'control-label col-lg-2']
                )->textarea() ?>
                <hr>
                <div class="col-lg-10 col-lg-offset-2">
                    <div class="form-group">
                        <?= $form->field($model, 'published', [
                            'template' => '<label class="checkbox-inline">{input}'. $model->getAttributeLabel('published') .'{hint}</label>',
                        ])->label(false)->checkbox(['class' => 'styled'], false)->hint('Стоимось будет доступен для просмотра или оформления тура') ?>
                    </div>
                </div>
            </div>
        </div><!-- /.row -->
        <hr>
        <div class="text-right">
            <button type="submit" name="btnAction" value="save" class="btn bg-teal-400 btn-sm">
                <i class="icon-checkmark3 position-left"></i> Сохранить
            </button>
            <button type="submit" name="btnAction" value="save-exit" class="btn btn-primary btn-sm">
                <i class="icon-undo2 mr-5"></i> Сохр. и закрыть
            </button>
            <button type="submit" name="btnAction" value="save-new" class="btn btn-primary btn-sm">
                <i class="icon-new-tab mr-5"></i> Сохр. и создать новую запись
            </button>
            <? if(!$model->isNewRecord): ?>
            <?= Html::a('<i class="icon-bin"></i>Удалить', [
                Yii::$app->controller->id . '/remove-price?id=' . $model->id
            ], [
                'onclick' => "return confirm('Вы уверены что хотите удалить данные?');",
                'class' => 'btn btn-danger btn-sm'
            ]) ?>
            <? endif; ?>
        </div>
    <? ActiveForm::end(); ?>
    </div><!-- /.panel-body -->
</div>