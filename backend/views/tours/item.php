<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \common\models\Files;
use \common\models\DataGroup;
use yii\helpers\ArrayHelper;
use backend\widgets\FilesWidget;
use yii\helpers\Url;
use \yii\helpers\StringHelper;

if($model->isNewRecord)
    $this->context->view->title = 'Добавить запись';
else
    $this->context->view->title = 'Редактировать запись';

$this->context->smallTitle = 'Управления всеми данными которые относятся к данному туру.';

$this->context->breadcrumbs[] = [
    'label' => 'Все туры',
    'url'   => '/'. $this->context->id .'/index'
];

$pageName = $this->context->view->title;
$this->context->breadcrumbs[] = $pageName;

$className = StringHelper::basename($model::className());

?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title"><?= $pageName ?></h5>
    </div>
    <div class="panel-body">
    <? $form = ActiveForm::begin([
        'id' => $this->context->id . '-form',
        'options' => [
            'role'    => 'form',
            'class' => 'form-horizontal',
            'enctype' => 'multipart/form-data',
        ]
    ]); ?>
        <div class="row">
            <div class="col-sm-12">
                <div class="tabbable">
                    <ul class="nav nav-tabs nav-tabs-bottom">
                        <li class="active">
                            <a href="#<?= Html::getInputId($model, 'basic') ?>" data-toggle="tab">
                                Основные поля
                            </a>
                        </li>
                        <li>
                            <a href="#<?= Html::getInputId($model, 'seo') ?>" data-toggle="tab">
                                СЕО продвижение
                            </a>
                        </li>
                        <li>
                            <a href="#<?= Html::getInputId($model, 'images') ?>" data-toggle="tab">
                                Изображения
                            </a>
                        </li>
                        <li>
                            <a href="#<?= Html::getInputId($model, 'program') ?>" data-toggle="tab">
                                Программа
                            </a>
                        </li>
                        <li>
                            <a href="#<?= Html::getInputId($model, 'prices') ?>" data-toggle="tab">
                                Стоимость
                            </a>
                        </li>
                        <? if(!$model->isNewRecord): ?>
                        <li>
                            <a href="#<?= Html::getInputId($model, 'linkcode') ?>" data-toggle="tab">
                                <i class=" icon-embed mr-5 text-size-small"></i> Короткий код
                            </a>
                        </li>
                        <? endif; ?>
                    </ul>
                    <div class="tab-content pt-5">
                        <div class="tab-pane active" id="<?= Html::getInputId($model, 'basic') ?>">
                            <?= $form->field($model, 'name', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint(false)->label(
                                $model->getAttributeLabelRequired('name'),
                                ['class' => 'control-label col-lg-2']
                            ) ?>

                            <?= $form->field($model, 'cat_id', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'select',
                                    'multiple' => 'multiple'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->dropDownList(DataGroup::getGroup(DataGroup::GROUP_TOURS_CAT_ID), [
                                'prompt' => '--Выберите категорию--'
                            ])->label(
                                $model->getAttributeLabelRequired('cat_id'),
                                ['class' => 'control-label col-lg-2']
                            )->hint(false) ?>

                            <?= $form->field($model, 'type_id', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'select'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->dropDownList(DataGroup::getGroup(DataGroup::GROUP_TOURS_TYPE_ID), [
                                'prompt' => '--Выберите тип--'
                            ])->label(
                                $model->getAttributeLabelRequired('type_id'),
                                ['class' => 'control-label col-lg-2']
                            )->hint(false) ?>

                            <?= $form->field($model, 'restrictions', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'select',
                                    'multiple' => 'multiple'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->dropDownList(DataGroup::getGroup(DataGroup::GROUP_TOURS_RESTRICTION_ID), [
                                'prompt' => '--Выберите ограничение--'
                            ])->label(
                                $model->getAttributeLabelRequired('restrictions'),
                                ['class' => 'control-label col-lg-2']
                            )->hint(false) ?>

                            <!-- Begin editor -->
                            <div class="form-group">
                                <label class="control-label col-lg-2" for="<?= Html::getInputId($model, 'description') ?>">
                                    <?= $model->getAttributeLabelRequired('description') ?>
                                </label>
                                <div class="col-lg-10 clearfix">
                                    <?= \yii\imperavi\Widget::widget(ArrayHelper::merge(
                                        [
                                            'attribute' => Html::getInputName($model, 'description'),
                                            'value' => $model->description
                                        ],
                                        Yii::$app->params['imperavi'], [
                                            'id' => Html::getInputId($model, 'description'),
                                            'options' => [
                                                'minHeight' => '150px',
                                                'uploadImageFields' => [
                                                    Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
                                                    'Files[item_id]' => $model->id,
                                                    'Files[module]' => $className,
                                                    'Files[type]' => Files::TYPE_IMAGE
                                                ],
                                                'uploadFileFields' => [
                                                    Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
                                                    'Files[item_id]' => $model->id,
                                                    'Files[module]' => $className,
                                                    'Files[type]' => Files::TYPE_FILE
                                                ]
                                            ]
                                        ]
                                    )); ?>
                                    <? if($model->hasErrors('description')): ?>
                                    <p class="help-block help-block-error text-danger">
                                        <?= $model->getFirstError('description') ?>
                                    </p>
                                    <? endif; ?>
                                    <p class="help-block">
                                        Полное описания тура, например вся основная информация о туре.
                                    </p>
                                </div>
                            </div>
                            <!-- End editor -->
                            <hr>
                            <div class="col-lg-10 col-lg-offset-2">
                                <div class="form-group">
                                    <?= $form->field($model, 'selling', [
                                        'template' => '<label class="checkbox-inline">{input}'. $model->getAttributeLabel('selling') .'{hint}</label>',
                                    ])->label(false)->checkbox(['class' => 'styled'], false)->hint('Тур будет доступный для поупки на сайте в режиме онлайн.') ?>
                                </div>
                            </div>
                            <div class="col-lg-10 col-lg-offset-2">
                                <div class="form-group">
                                    <?= $form->field($model, 'fixed', [
                                        'template' => '<label class="checkbox-inline">{input}'. $model->getAttributeLabel('fixed') .'{hint}</label>',
                                    ])->label(false)->checkbox(['class' => 'styled'], false)->hint('Тур будет выводиться на первых местах в списке туров, с высшим приоритетом над сортировкой.') ?>
                                </div>
                            </div>
                            <div class="col-lg-10 col-lg-offset-2">
                                <div class="form-group">
                                    <?= $form->field($model, 'noindex', [
                                        'template' => '<label class="checkbox-inline">{input}'. $model->getAttributeLabel('noindex') .'{hint}</label>',
                                    ])->label(false)->checkbox(['class' => 'styled'], false)->hint('Тур будет доступен для просмотра, но не будет индексироваться поисковыми роботами') ?>
                                </div>
                            </div>
                            <div class="col-lg-10 col-lg-offset-2">
                                <div class="form-group">
                                    <?= $form->field($model, 'published', [
                                        'template' => '<label class="checkbox-inline">{input}'. $model->getAttributeLabel('published') .'{hint}</label>',
                                    ])->label(false)->checkbox(['class' => 'styled'], false)->hint('Тур будет доступен для просмотра посетителями ресурса') ?>
                                </div>
                            </div>
                        </div><!-- /.tab-pane -->
                        <div class="tab-pane" id="<?= Html::getInputId($model, 'seo') ?>">
                            <?= $form->field($model, 'alias', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint('Кириллическая версия имя для использования в ссылках')->label(
                                $model->getAttributeLabelRequired('alias'),
                                ['class' => 'control-label col-lg-2']
                            ) ?>
                             <?= $form->field($model, 'meta_description', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control',
                                    'rows' => '5'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint('Краткое описание до 200 (200 — максимум, ориентируйтесь на ударные первые 100 знаков).')->label(
                                $model->getAttributeLabelRequired('meta_description'),
                                ['class' => 'control-label col-lg-2']
                            ) ?>

                            <?= $form->field($model, 'meta_keywords', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control',
                                    'rows' => '5'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint('Введите через запятую основные ключевые слова для вашего сайта до 250 (250 — максимум, ориентируйтесь на ударные первые 150 знаков).')->label(
                                $model->getAttributeLabelRequired('meta_keywords'),
                                ['class' => 'control-label col-lg-2']
                            )->textarea() ?>
                        </div><!-- /.tab-pane -->
                        <div class="tab-pane" id="<?= Html::getInputId($model, 'images') ?>">
                            <!-- Begin files module -->
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <label for="" class="control-label">Основное изображения:</label>
                                    <?= FilesWidget::widget([
                                        'id' => $model->id,
                                        'type' => \common\models\Files::TYPE_IMAGE,
                                        'files' => $model->prewiewsWidget,
                                        'module' => $model::className(),
                                        'sizes' => '100x100,800x533',
                                        'hint' => 'Изображения, который будет отображаться на странице тура',
                                        'labels' => [
                                            'Установленные размеры изображений' => ' (100x100, 800x533) px.'
                                        ]
                                    ]) ?>
                                </div>
                            </div>
                            <!-- End files module -->
                        </div><!-- /.tab-pane -->
                        <div class="tab-pane" id="<?= Html::getInputId($model, 'program') ?>">
                            <? if(count($model->programs)): ?>
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th width="5%">#ID</th>
                                                <th width="50%">Наименование</th>
                                                <th class="text-center">Публикация</th>
                                                <th class="text-center">
                                                    <a href="#" onclick="return Sorting('/<?= Yii::$app->controller->id ?>/sorting-program', 'Sorting');" title="" data-popup="tooltip" data-original-title="Сохранить позиции">
                                                        <i class="icon-sort text-size-mini text-default"></i>
                                                    </a>
                                                </th>
                                                <th class="text-center" data-toggle="true">Публикация</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <? foreach($model->programs as $item): ?>
                                            <tr>
                                                <td><?= $item->id ?></td>
                                                <td><?= $item->name ?></td>
                                                <td class="text-center">
                                                    <? if($item->published): ?>
                                                        <i class="icon-checkmark3 text-success" title="Да"></i>
                                                    <? else: ?>
                                                        <i class="icon-cross2 text-danger" title="Нет"></i>
                                                    <? endif; ?>
                                                </td>
                                                <td>
                                                    <?= Html::input('text', 'Sorting[' . $item->id . ']', $item->sorting, [
                                                        'class' => 'form-control input-xs text-center',
                                                        'style' => 'width: 50px; margin: 0 auto'
                                                    ]) ?>
                                                </td>
                                                <td class="text-center">
                                                    <div class="checkbox checkbox-switchery switchery-xs" title="Вкл/Выкл">
                                                        <label>
                                                            <?= Html::checkbox('Published['. $item->id .']', $item->published, [
                                                                'class' => 'switchery',
                                                                'onclick' => "ChangeRequest('" . Url::to([
                                                                    Yii::$app->controller->id . '/published-program',
                                                                    'id' => $item->id
                                                                ]) . "', {published: this.checked});"
                                                            ]); ?>
                                                        </label>
                                                    </div>
                                                </td>
                                                <td class="text-right">
                                                    <a href="/<?= Yii::$app->controller->id ?>/edit-program?id=<?= $item->id ?>" class="mr-5" title="Редактировать программу">
                                                        <i class="icon-pencil5 text-primary"></i>
                                                    </a>
                                                    <a href="/<?= Yii::$app->controller->id ?>/remove-program?id=<?= $item->id ?>" title="Удалить программу" onclick="return confirm('Вы уверены что хотите удалить программу?');">
                                                        <i class="icon-trash text-danger"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        <? endforeach ?>
                                        </tbody>
                                    </table>
                                </div>
                            <? else: ?>
                                <div class="bg-info alert-styled-left alert fade in">
                                    Для этого тура нет программы.
                                </div>
                            <? endif; ?>
                            <hr>
                            <a href="/<?= Yii::$app->controller->id ?>/add-program?item_id=<?= $model->id ?>" class="btn btn-default btn-xs mr-5">
                                <i class="icon-plus3 position-left"></i>
                                Добавить новую запись
                            </a>
                        </div><!-- /.tab-pane -->
                        <div class="tab-pane" id="<?= Html::getInputId($model, 'prices') ?>">
                            <? if(count($model->prices)): ?>
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#ID</th>
                                            <th class="text-center">
                                                <i class="icon-calendar5 text-size-mini mr-5"></i>
                                                Начало
                                            </th>
                                            <th class="text-center">
                                                <i class="icon-calendar5 text-size-mini mr-5"></i>
                                                Окончание
                                            </th>
                                            <th class="text-center">Цена</th>
                                            <th class="text-center">Цена со скидкий</th>
                                            <th class="text-center">Цена для ребенка</th>
                                            <th class="text-center">Описание</th>
                                            <th class="text-center" data-toggle="true">Публикация</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <? foreach($model->prices as $item): ?>
                                        <tr>
                                            <td><?= $item->id ?></td>
                                            <td class="text-center">
                                                <?= date($item::OUT_DATE_FORMAT, $item->start_date_timestamp) ?>
                                            </td>
                                            <td class="text-center">
                                                <?= date($item::OUT_DATE_FORMAT, $item->end_date_timestamp) ?>
                                            </td>
                                            <td class="text-center">
                                                <?= $item->price ?>
                                            </td>
                                            <td class="text-center">
                                                <?= $item->discountPrice ?>
                                            </td>
                                            <td class="text-center">
                                                <?= $item->childrenPrice ?>
                                            </td>
                                            <td class="text-center">
                                                <? if($item->short_description): ?>
                                                    <i class="icon-checkmark3 text-success" title="Описание есть"></i>
                                                <? else: ?>
                                                    <i class="icon-cross2 text-danger" title="Нет описания"></i>
                                                <? endif ?>
                                            </td>
                                            <td class="text-center">
                                                <div class="checkbox checkbox-switchery switchery-xs" title="Вкл/Выкл">
                                                    <label>
                                                        <?= Html::checkbox('Published['. $item->id .']', $item->published, [
                                                            'class' => 'switchery',
                                                            'onclick' => "ChangeRequest('" . Url::to([
                                                                Yii::$app->controller->id . '/published-price',
                                                                'id' => $item->id
                                                            ]) . "', {published: this.checked});"
                                                        ]); ?>
                                                    </label>
                                                </div>
                                            </td>
                                            <td class="text-right">
                                                <a href="/<?= Yii::$app->controller->id ?>/edit-price?id=<?= $item->id ?>" class="mr-5" title="Редактировать цену">
                                                    <i class="icon-pencil5 text-primary"></i>
                                                </a>
                                                <a href="/<?= Yii::$app->controller->id ?>/remove-price?id=<?= $item->id ?>" title="Удалить цену" onclick="return confirm('Вы уверены что хотите удалить цену?');">
                                                    <i class="icon-trash text-danger"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    <? endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                            <br>
                            <? else: ?>
                                <div class="bg-info alert-styled-left alert fade in">
                                    Для этого тура нет записей.
                                </div>
                            <? endif;?>
                            <hr>
                            <a href="/<?= Yii::$app->controller->id ?>/add-price?item_id=<?= $model->id ?>" class="btn btn-default btn-xs mr-5">
                                <i class="icon-plus3 position-left"></i>
                                Добавить новую запись
                            </a>
                            <em class="text-danger">Обратите внимание цена указывается за одного человека!</em>
                            <br><br>
                            <div class="row mt-20">
                                <?= $form->field($model, 'included_in_price', [
                                    // Begin input theme
                                    'template' => '{label}{input}{hint}{error}',
                                    // End input theme
                                    'inputOptions' => [
                                        'class' => 'form-control',
                                        'placeholder' => 'Каждая услуга с новой строки',
                                        'rows' => '15'
                                    ],
                                    'options' => [
                                        'class' => 'col-sm-6'
                                    ]
                                ])->hint('Укажите все услуги которые будут включены в стоимость, каждая услуга с новой строки.')->label(
                                    $model->getAttributeLabelRequired('included_in_price'),
                                    ['class' => 'control-label']
                                )->textarea() ?>

                                <?= $form->field($model, 'not_included_in_price', [
                                    // Begin input theme
                                    'template' => '{label}{input}{hint}{error}',
                                    // End input theme
                                    'inputOptions' => [
                                        'class' => 'form-control',
                                        'placeholder' => 'Каждая услуга с новой строки',
                                        'rows' => '15'
                                    ],
                                    'options' => [
                                        'class' => 'col-sm-6'
                                    ]
                                ])->hint('Укажите все услуги которые не будут включены в стоимость, каждая услуга с новой строки.')->label(
                                    $model->getAttributeLabelRequired('not_included_in_price'),
                                    ['class' => 'control-label']
                                )->textarea() ?>
                            </div>
                        </div><!-- /.tab-pane -->
                        <? if(!$model->isNewRecord): ?>
                        <div class="tab-pane" id="<?= Html::getInputId($model, 'linkcode') ?>">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Ссылка:</label>
                                    <div class="col-lg-10">
                                        <div class="form-control-static">
                                            <code><?= $model->link ?></code>
                                            <p class="help-block">URL текущей записи</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.tab-pane -->
                        <? endif; ?>
                    </div><!-- /.tab-content -->
                </div>
            </div>
        </div><!-- /.row -->
        <hr>
        <div class="text-right">
            <button type="submit" name="btnAction" value="save" class="btn bg-teal-400 btn-sm">
                <i class="icon-checkmark3 position-left"></i> Сохранить
            </button>
            <button type="submit" name="btnAction" value="save-exit" class="btn btn-primary btn-sm">
                <i class="icon-undo2 mr-5"></i> Сохр. и закрыть
            </button>
            <button type="submit" name="btnAction" value="save-new" class="btn btn-primary btn-sm">
                <i class="icon-new-tab mr-5"></i> Сохр. и создать новую запись
            </button>
            <? if(!$model->isNewRecord): ?>
            <?= Html::a('<i class="icon-bin"></i>Удалить', [
                Yii::$app->controller->id . '/'. $model->id .'/remove'
            ], [
                'onclick' => "return confirm('Вы уверены что хотите удалить данные?');",
                'class' => 'btn btn-danger btn-sm'
            ]) ?>
            <? endif; ?>
        </div>
    <? ActiveForm::end(); ?>
    </div><!-- /.panel-body -->
</div>