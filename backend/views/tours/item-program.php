<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use \common\models\Files;
use backend\widgets\FilesWidget;
use \yii\helpers\StringHelper;

if($model->isNewRecord)
    $this->context->view->title = 'Добавить прграмму';
else
    $this->context->view->title = 'Редактировать программу';

$this->context->smallTitle = 'Управления стоимостью на заданный период времени.';
$this->context->breadcrumbs[] = [
    'label' => 'Все туры',
    'url'   => '/'. $this->context->id .'/index'
];

$this->context->breadcrumbs[] = [
    'label' => Html::encode($model->tour->name),
    'url'   => '/'. $this->context->id .'/edit?id=' . $model->tour->id
];


$pageName = $this->context->view->title;
$this->context->breadcrumbs[] = $pageName;

$className = StringHelper::basename($model::className());

?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title"><?= $pageName ?></h5>
    </div>
    <div class="panel-body">
    <? $form = ActiveForm::begin([
        'id' => $this->context->id . '-form',
        'options' => [
            'role'    => 'form',
            'class' => 'form-horizontal',
            'enctype' => 'multipart/form-data',
        ]
    ]); ?>
        <div class="row">
            <div class="col-sm-12">
                <?= $form->field($model, 'name', [
                    // Begin input theme
                    'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                    // End input theme
                    'inputOptions' => [
                        'class' => 'form-control'
                    ],
                    'options' => [
                        'class' => 'form-group'
                    ]
                ])->hint(false)->label(
                    $model->getAttributeLabelRequired('name'),
                    ['class' => 'control-label col-lg-2']
                ) ?>
                <!-- Begin editor -->
                <div class="form-group">
                    <label class="control-label col-lg-2" for="<?= Html::getInputId($model, 'description') ?>">
                        <?= $model->getAttributeLabelRequired('description') ?>
                    </label>
                    <div class="col-lg-10 clearfix">
                        <?= \yii\imperavi\Widget::widget(ArrayHelper::merge(
                            [
                                'attribute' => Html::getInputName($model, 'description'),
                                'value' => $model->description
                            ],
                            Yii::$app->params['imperavi'], [
                                'id' => Html::getInputId($model, 'description'),
                                'options' => [
                                    'minHeight' => '150px',
                                    'uploadImageFields' => [
                                        Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
                                        'Files[item_id]' => $model->id,
                                        'Files[module]' => $className,
                                        'Files[type]' => Files::TYPE_IMAGE
                                    ],
                                    'uploadFileFields' => [
                                        Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
                                        'Files[item_id]' => $model->id,
                                        'Files[module]' => $className,
                                        'Files[type]' => Files::TYPE_FILE
                                    ]
                                ]
                            ]
                        )); ?>
                        <? if($model->hasErrors('description')): ?>
                        <p class="help-block help-block-error text-danger">
                            <?= $model->getFirstError('description') ?>
                        </p>
                        <? endif; ?>
                        <p class="help-block">
                            Полное описания программы для тура.
                        </p>
                    </div>
                </div>
                <!-- End editor -->
                <hr>
                <div class="col-lg-10 col-lg-offset-2">
                    <div class="form-group">
                        <?= $form->field($model, 'published', [
                            'template' => '<label class="checkbox-inline">{input}'. $model->getAttributeLabel('published') .'{hint}</label>',
                        ])->label(false)->checkbox(['class' => 'styled'], false)->hint('Программа будет доступен для просмотра посетителями ресурса') ?>
                    </div>
                </div>
            </div>
        </div><!-- /.row -->
        <hr>
        <div class="text-right">
            <button type="submit" name="btnAction" value="save" class="btn bg-teal-400 btn-sm">
                <i class="icon-checkmark3 position-left"></i> Сохранить
            </button>
            <button type="submit" name="btnAction" value="save-exit" class="btn btn-primary btn-sm">
                <i class="icon-undo2 mr-5"></i> Сохр. и закрыть
            </button>
            <button type="submit" name="btnAction" value="save-new" class="btn btn-primary btn-sm">
                <i class="icon-new-tab mr-5"></i> Сохр. и создать новую запись
            </button>
            <? if(!$model->isNewRecord): ?>
            <?= Html::a('<i class="icon-bin"></i>Удалить', [
                Yii::$app->controller->id . '/remove-price?id=' . $model->id
            ], [
                'onclick' => "return confirm('Вы уверены что хотите удалить данные?');",
                'class' => 'btn btn-danger btn-sm'
            ]) ?>
            <? endif; ?>
        </div>
    <? ActiveForm::end(); ?>
    </div><!-- /.panel-body -->
</div>