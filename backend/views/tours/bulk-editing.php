<?php

use yii\helpers\Html;
use yii\bootstrap\Alert;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;

$pageName = 'Массовое управления стоимостью';

?>
<? if(count($models)): ?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title"><?= $pageName ?></h5>
        <small class="display-block text-muted">
            <?= $search ? 'Всего найдено' :  'Всего' ?>: <?= $pages->totalCount ?>
        </small>
        <div class="heading-elements">
            <?= Html::beginForm(Url::to([Yii::$app->controller->id . '/index']), 'get', ['class' => 'heading-form']) ?>
                <div class="form-group has-feedback">
                    <?= Html::textInput('search', $search, ['class' => 'form-control input-xs', 'placeholder' => 'Поиск...']) ?>
                    <div class="form-control-feedback">
                        <i class="icon-search4 text-size-base text-muted"></i>
                    </div>
                </div>
            <?= Html::endForm() ?>
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>
    <div class="panel-body">
        <? $form = ActiveForm::begin([
            'id' => $this->context->id . '-form',
            'options' => [
                'role'    => 'form',
                'class' => 'form-horizontal'
            ]
        ]); ?>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th width="5%">#ID</th>
                            <th width="25%">Идентификатор тура</th>
                            <th class="text-center">Начало</th>
                            <th class="text-center">Окончание</th>
                            <th class="text-center">Цена</th>
                            <th class="text-center">Цена со скидкой</th>
                            <th class="text-center">Цена для ребенка</th>
                            <th>Публикация</th>
                        </tr>
                    </thead>
                    <tbody>
                    <? foreach($models as $item): ?>
                        <tr>
                            <td><?= $item->id ?></td>
                            <td>
                                <?= Html::a($item->tour->name, [
                                    '/tours/edit?id=' . $item->item_id . '#tours-prices'
                                ], [
                                    'target' => '_blank'
                                ])?>
                            </td>
                            <td class="form-group-xs">
                                <?= Html::input('text', 'TourPrices['. $item->id .'][start_date]', $item->start_date, [
                                    'class' => 'form-control text-center',
                                    'placeholder' => date($item::PROP_DATE_FORMAT),
                                    'data-format-pattern' => '{{9999}}-{{99}}-{{99}}',
                                ]); ?>
                            </td>
                            <td class="form-group-xs">
                                <?= Html::input('text', 'TourPrices['. $item->id .'][end_date]', $item->end_date, [
                                    'class' => 'form-control text-center',
                                    'placeholder' => date($item::PROP_DATE_FORMAT),
                                    'data-format-pattern' => '{{9999}}-{{99}}-{{99}}',
                                ]); ?>
                            </td>
                            <td class="form-group-xs">
                                <?= Html::input('text', 'TourPrices['. $item->id .'][base_price]', $item->base_price, [
                                    'class' => 'form-control text-center'
                                ]); ?>
                            </td>
                            <td class="form-group-xs">
                                <?= Html::input('text', 'TourPrices['. $item->id .'][discount_price]', $item->discount_price, [
                                    'class' => 'form-control text-center'
                                ]); ?>
                            </td>
                            <td class="form-group-xs">
                                <?= Html::input('text', 'TourPrices['. $item->id .'][children_price]', $item->children_price, [
                                    'class' => 'form-control text-center'
                                ]); ?>
                            </td>
                            <td class="text-center">
                                <div class="checkbox checkbox-switchery switchery-xs" title="Вкл/Выкл">
                                    <label>
                                        <?= Html::checkbox('Published['. $item->id .']', $item->published, [
                                            'class' => 'switchery',
                                            'onclick' => "ChangeRequest('" . Url::to([
                                                Yii::$app->controller->id . '/published-price',
                                                'id' => $item->id
                                            ]) . "', {published: this.checked});"
                                        ]); ?>
                                    </label>
                                </div>
                            </td>
                        </tr>
                    <? endforeach; ?>
                    </tbody>
                </table>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-8">
                    <?= LinkPager::widget([
                        'pagination' => $pages,
                        'options'    => [
                            'class' => 'pagination pagination-xs'
                        ]
                    ]); ?>
                </div>
                <div class="col-sm-4 text-right">
                    <button type="submit" name="btnAction" value="save" class="btn bg-teal-400 btn-sm">
                        <i class="icon-checkmark3 position-left"></i> Сохранить
                    </button>
                    <button type="submit" name="btnAction" value="save-exit" class="btn btn-primary btn-sm">
                        <i class="icon-undo2 mr-5"></i> Сохр. и закрыть
                    </button>
                </div>
            </div>
        <? ActiveForm::end(); ?>
    </div>
</div>
<? else: Alert::begin(['options' => ['class' => 'bg-info alert-styled-left']]); ?>
    Не удается найти записи для отображения
<? Alert::end(); endif;?>
<!-- /table with togglable columns -->