<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\Menu;
use backend\widgets\FilesWidget;

if($model->isNewRecord)
    $this->context->view->title = 'Добавить элемент меню';
else
    $this->context->view->title = 'Редактировать элемент меню';

$this->context->breadcrumbs[] = [
    'label' => 'Конструктор меню',
    'url'   => '/'. $this->context->id .'/index'
];

$this->context->breadcrumbs[] = [
    'label' => $model_menu->name,
    'url'   => '/'. $this->context->id .'/nav?menu_id=' . $model_menu->id
];

$pageName = $this->context->view->title;
$this->context->breadcrumbs[] = $pageName;

?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title"><?= $pageName ?></h5>
    </div>
    <div class="panel-body">
    <? $form = ActiveForm::begin([
        'id' => $this->context->id . '-form',
        'options' => [
            'role'    => 'form',
            'class' => 'form-horizontal'
        ]
    ]); ?>
        <div class="row">
            <div class="col-sm-12">

                <? if($model_menu->type == Menu::TYPE_MENU_TREE): ?>
                    <?/*<div class="form-group">
                        <label for="<?= Html::getInputId($model, 'parent_id') ?>" class="control-label col-lg-2">
                            <?= $model->getAttributeLabelRequired('parent_id') ?>
                        </label>
                        <div class="col-lg-10">
                            <select name="<?= Html::getInputName($model, 'parent_id') ?>" id="<?= Html::getInputId($model, 'parent_id') ?>" class="select">
                                <? foreach($parent_list as $item): ?>
                                    <option value="<?= $item->id ?>" data-level="<?= $item->level ?>">
                                        <?= $item->name ?>
                                    </option>
                                <? endforeach; ?>
                            </select>
                        </div>
                    </div>*/?>

                    <?= $form->field($model, 'parent_id', [
                        // Begin input theme
                        'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                        // End input theme
                        'inputOptions' => [
                            'class' => 'select'
                        ],
                        'options' => [
                            'class' => 'form-group'
                        ]
                    ])->dropDownList($parent_list, [
                        'prompt' => '-- Выберите идентификатор родителя --'
                    ])->hint(false)->label(
                        $model->getAttributeLabelRequired('parent_id'),
                        ['class' => 'control-label col-lg-2']
                    ) ?>
                <? endif; ?>

                <?= $form->field($model, 'name', [
                    // Begin input theme
                    'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                    // End input theme
                    'inputOptions' => [
                        'class' => 'form-control'
                    ],
                    'options' => [
                        'class' => 'form-group'
                    ]
                ])->hint(false)->label(
                    $model->getAttributeLabel('name') .
                    ($model->isAttributeRequired('name')? ' : <span class="text-danger">*</span>' : ' :'),
                    ['class' => 'control-label col-lg-2']
                ) ?>

                <?= $form->field($model, 'url', [
                    // Begin input theme
                    'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                    // End input theme
                    'inputOptions' => [
                        'class' => 'form-control'
                    ],
                    'options' => [
                        'class' => 'form-group'
                    ]
                ])->hint(false)->label(
                    $model->getAttributeLabel('url') .
                    ($model->isAttributeRequired('url')? ' : <span class="text-danger">*</span>' : ' :'),
                    ['class' => 'control-label col-lg-2']
                ) ?>

                <?= $form->field($model, 'css_class', [
                    // Begin input theme
                    'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                    // End input theme
                    'inputOptions' => [
                        'class' => 'form-control'
                    ],
                    'options' => [
                        'class' => 'form-group'
                    ]
                ])->hint('Вспомогательные классы для оформления элемента навигации.')->label(
                    $model->getAttributeLabel('css_class') .
                    ($model->isAttributeRequired('css_class')? ' : <span class="text-danger">*</span>' : ' :'),
                    ['class' => 'control-label col-lg-2']
                ) ?>

                <div class="col-lg-10 col-lg-offset-2">
                    <div class="form-group">
                        <?= $form->field($model, 'target_blank', [
                            'template' => '<label class="checkbox-inline">{input}'. $model->getAttributeLabel('target_blank') .'{hint}</label>',
                        ])->label(false)->checkbox(['class' => 'styled'], false)->hint('По умолчанию, при переходе по ссылке документ открывается в текущем окне или фрейме. При необходимости, это условие может быть изменено этим параметром ссылка будет открына в новом окне или фрейме') ?>
                    </div>
                </div>

                <div class="col-lg-10 col-lg-offset-2">
                    <div class="form-group">
                        <?= $form->field($model, 'published', [
                            'template' => '<label class="checkbox-inline">{input}'. $model->getAttributeLabel('published') .'{hint}</label>',
                        ])->label(false)->checkbox(['class' => 'styled'], false)->hint('Ссылка будет включена в меню на страницаз ресурса') ?>
                    </div>
                </div>

            </div>
        </div><!-- /.row -->
        <hr>
        <div class="text-right">
            <button type="submit" name="btnAction" value="save" class="btn bg-teal-400 btn-sm">
                <i class="icon-checkmark3 position-left"></i> Сохранить
            </button>
            <button type="submit" name="btnAction" value="save-exit" class="btn btn-primary btn-sm">
                <i class="icon-undo2 mr-5"></i> Сохр. и закрыть
            </button>
            <button type="submit" name="btnAction" value="save-new" class="btn btn-primary btn-sm">
                <i class="icon-new-tab mr-5"></i> Сохр. и создать новую запись
            </button>
            <? if(!$model->isNewRecord): ?>
            <?= Html::a('<i class="icon-bin"></i>Удалить', [
                Yii::$app->controller->id . '/'. $model->id .'/remove'
            ], [
                'onclick' => "return confirm('Вы уверены что хотите удалить данные?');",
                'class' => 'btn btn-danger btn-sm'
            ]) ?>
            <? endif; ?>
        </div>
    <? ActiveForm::end(); ?>
    </div><!-- /.panel-body -->
</div>