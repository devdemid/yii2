<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use common\models\Menu;

$this->context->breadcrumbs[] = [
    'label' => 'Конструктор меню',
    'url'   => '/'. $this->context->id .'/index'
];

// Set the component description
//$this->context->view->title = $this->context->view->title;
$this->context->smallTitle = 'Все элементы навигации, это список всех ссылок или категорий которые относяться к этой навигации.';
$this->context->breadcrumbs[] = $this->context->view->title;

?>

<? if(count($models)): ?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Все элементы навигации</h5>
        <small class="display-block text-muted">
            <?= $search ? 'Всего найдено' :  'Всего элементов меню' ?>: <?= $total ?>
        </small>
    </div>
    <div class="panel-body">
    <? if($model_menu->type == Menu::TYPE_MENU_LIST): ?>
        <table class="table table-togglable table-hover">
            <thead>
                <tr>
                    <th data-toggle="true" style="width: 30px;">#ID</th>
                    <th data-toggle="true" width="70%">Название атрибута</th>
                    <th class="text-center">
                        <a href="#" onclick="return Sorting('/<?= Yii::$app->controller->id ?>/nav-sorting', 'Sorting');" title="Сохранить позиции" data-popup="tooltip">
                            <i class="icon-sort text-size-mini text-default"></i>
                        </a>
                    </th>
                    <th class="text-center" data-toggle="true">Публикация</th>
                    <th class="text-center" style="width: 30px;">
                        <i class="icon-menu-open2" data-popup="tooltip" title="Действие" data-placement="left"></i>
                    </th>
                </tr>
            </thead>
            <tbody>
            <? foreach($models as $item): ?>
                <tr>
                    <td><?= $item->id ?></td>
                    <td>
                        <?= Html::a($item->name, [
                            Yii::$app->controller->id . '/nav-edit?id=' . $item->id
                        ]) ?>
                    </td>
                    <td>
                        <?= Html::input('text', 'Sorting[' . $item->id . ']', $item->sorting, [
                            'class' => 'form-control input-xs text-center',
                            'style' => 'width: 50px; margin: 0 auto'
                        ]) ?>
                    </td>
                    <td class="text-center">
                        <div class="checkbox checkbox-switchery switchery-xs" title="Вкл/Выкл">
                            <label>
                                <?= Html::checkbox('Published['. $item->id .']', $item->published, [
                                    'class' => 'switchery',
                                    'onclick' => "ChangeRequest('" . Url::to([
                                        Yii::$app->controller->id . '/nav-published',
                                        'id' => $item->id
                                    ]) . "', {published: this.checked});"
                                ]); ?>
                            </label>
                        </div>
                    </td>
                    <td class="text-center">
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Действия">
                                    <i class="icon-menu7"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <?= Html::a('<i class="icon-pencil5"></i>Редактировать', [
                                            Yii::$app->controller->id . '/nav-edit?id=' . $item->id
                                        ]) ?>
                                    </li>
                                    <li>
                                        <?= Html::a('<i class="icon-bin"></i>Удалить', [
                                            Yii::$app->controller->id . '/nav-remove?id=' . $item->id
                                        ], [
                                            'onclick' => "return confirm('Вы уверены что хотите удалить данные?');"
                                        ]) ?>
                                    </li>
                                </ul>
                            </li>
                        </ul><!-- /.icons-list -->
                    </td>
                </tr>
            <? endforeach; ?>
            </tbody>
        </table>
    <? else: ?>
        <!-- Begin table tree -->
        <table class="table table-hover table-bordered tree-table">
            <thead>
                <tr>
                    <th width="30px">#ID</th>
                    <th>Название атрибута</th>
                    <th class="text-center" style="width: 30px;">
                        <a href="#" onclick="return Sorting('/<?= Yii::$app->controller->id ?>/nav-sorting', 'Sorting');" title="Сохранить позиции" data-popup="tooltip">
                            <i class="icon-sort text-size-mini text-default"></i>
                        </a>
                    </th>
                    <th class="text-center" style="width: 30px;">Публикация</th>
                    <th class="text-center" style="width: 30px;">
                        <i class="icon-menu-open2" data-popup="tooltip" title="Действие" data-placement="left"></i>
                    </th>
                </tr>
            </thead>
            <tbody>
            <? foreach($models as $item): ?>
                <tr data-id="<?= $item->id ?>" data-parent="<?= $item->parent_id ?>" data-level="<?= $item->level ?>">
                    <td><?= $item->id ?></td>
                    <td data-column="name">
                        <?= $item->name ?>
                    </td>
                    <td>
                        <?= Html::input('text', 'Sorting[' . $item->id . ']', $item->sorting, [
                            'class' => 'form-control input-xs text-center',
                            'style' => 'width: 50px; margin: 0 auto'
                        ]) ?>
                    </td>
                    <td class="text-center">
                        <div class="checkbox checkbox-switchery switchery-xs" title="Вкл/Выкл">
                            <label>
                                <?= Html::checkbox('Published['. $item->id .']', $item->published, [
                                    'class' => 'switchery',
                                    'onclick' => "ChangeRequest('" . Url::to([
                                        Yii::$app->controller->id . '/nav-published',
                                        'id' => $item->id
                                    ]) . "', {published: this.checked});"
                                ]); ?>
                            </label>
                        </div>
                    </td>
                    <td class="text-center">
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Действия">
                                    <i class="icon-menu7"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <?= Html::a('<i class="icon-pencil5"></i>Редактировать', [
                                            Yii::$app->controller->id . '/nav-edit?id=' . $item->id
                                        ]) ?>
                                    </li>
                                    <li>
                                        <?= Html::a('<i class="icon-bin"></i>Удалить', [
                                            Yii::$app->controller->id . '/nav-remove?id=' . $item->id
                                        ], [
                                            'onclick' => "return confirm('Вы уверены что хотите удалить данные?');"
                                        ]) ?>
                                    </li>
                                </ul>
                            </li>
                        </ul><!-- /.icons-list -->
                    </td>
                </tr>
            <? endforeach; ?>
            </tbody>
        </table>
        <!-- End table tree -->
    <? endif; ?>
    </div><!-- /.panel-body -->
</div>
<div class="mb-20">
    <?= LinkPager::widget([
        'pagination' => $pages,
        'options'    => [
            'class' => 'pagination pagination-xs'
        ]
    ]); ?>
</div>
<? else: Alert::begin(['options' => ['class' => 'bg-info alert-styled-left']]); ?>
    Не удается найти элементы навигации для отображения
<? Alert::end(); endif;?>
<!-- /table with togglable columns -->