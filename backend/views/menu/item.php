<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\Groupmodels;
use backend\widgets\FilesWidget;

if($model->isNewRecord)
    $this->context->view->title = 'Добавить меню';
else
    $this->context->view->title = 'Настроить меню';

$this->context->breadcrumbs[] = [
    'label' => 'Конструктор меню',
    'url'   => '/'. $this->context->id .'/index'
];

if($model->isNewRecord)
    $pageName = 'Добавить меню';
else
    $pageName = 'Редактировать меню';

$this->context->breadcrumbs[] = $pageName;

?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title"><?= $pageName ?></h5>
    </div>
    <div class="panel-body">
    <? $form = ActiveForm::begin([
        'id' => $this->context->id . '-form',
        'options' => [
            'role'    => 'form',
            'class' => 'form-horizontal'
        ]
    ]); ?>
        <div class="row">
            <div class="col-sm-12">
                <div class="tabbable">
                    <ul class="nav nav-tabs nav-tabs-bottom">
                        <li class="active">
                            <a href="#<?= Html::getInputId($model, 'basic') ?>" data-toggle="tab">
                                Основные поля
                            </a>
                        </li>
                        <? if(!$model->isNewRecord): ?>
                        <li>
                            <a href="#<?= Html::getInputId($model, 'shortcode') ?>" data-toggle="tab">
                                <i class=" icon-embed mr-5 text-size-small"></i> Короткий код
                            </a>
                        </li>
                        <? endif; ?>
                    </ul>
                    <div class="tab-content pt-5">
                        <div class="tab-pane active" id="<?= Html::getInputId($model, 'basic') ?>">
                            <?= $form->field($model, 'name', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint(false)->label(
                                $model->getAttributeLabel('name') .
                                ($model->isAttributeRequired('name')? ' : <span class="text-danger">*</span>' : ' :'),
                                ['class' => 'control-label col-lg-2']
                            ) ?>

                            <?= $form->field($model, 'type', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'select'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->dropDownList($model::getTypes())->label(
                                $model->getAttributeLabelRequired('type'),
                                ['class' => 'control-label col-lg-2']
                            )->hint(false) ?>

                            <div class="col-lg-10 col-lg-offset-2">
                                <div class="form-group">
                                    <?= $form->field($model, 'published', [
                                        'template' => '<label class="checkbox-inline">{input}'. $model->getAttributeLabel('published') .'{hint}</label>',
                                    ])->label(false)->checkbox(['class' => 'styled'], false)->hint('Меню будет доступно на страницах ресурса') ?>
                                </div>
                            </div>
                        </div><!-- /.tab-pane -->
                        <? if(!$model->isNewRecord): ?>
                        <div class="tab-pane" id="<?= Html::getInputId($model, 'shortcode') ?>">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Короткий код меню:</label>
                                    <div class="col-lg-10">
                                        <div class="form-control-static">
                                            <code><?= $model->shortcode ?></code>
                                            <p class="help-block">Короткий код меню который можно установить в редакторе или в шаблон, в результате которого меню будет выведено в этом месте.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <? endif; ?>
                    </div><!-- /.tab-content -->
                </div>
            </div>
        </div><!-- /.row -->
        <hr>
        <div class="text-right">
            <button type="submit" name="btnAction" value="save" class="btn bg-teal-400 btn-sm">
                <i class="icon-checkmark3 position-left"></i> Сохранить
            </button>
            <button type="submit" name="btnAction" value="save-exit" class="btn btn-primary btn-sm">
                <i class="icon-undo2 mr-5"></i> Сохр. и закрыть
            </button>
            <button type="submit" name="btnAction" value="save-new" class="btn btn-primary btn-sm">
                <i class="icon-new-tab mr-5"></i> Сохр. и создать новую запись
            </button>
            <? if(!$model->isNewRecord): ?>
            <?= Html::a('<i class="icon-bin"></i>Удалить', [
                Yii::$app->controller->id . '/'. $model->id .'/remove'
            ], [
                'onclick' => "return confirm('Вы уверены что хотите удалить данные?');",
                'class' => 'btn btn-danger btn-sm'
            ]) ?>
            <? endif; ?>
        </div>
    <? ActiveForm::end(); ?>
    </div><!-- /.panel-body -->
</div>