<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

// Set the component description
$this->context->view->title = $this->context->view->title;
$this->context->smallTitle = 'Конструктор меню - позволяет создавать неограниченное количество навигаций';

?>

<? if(count($models)): ?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Все навигации</h5>
        <small class="display-block text-muted">
            <?= $search ? 'Всего найдено' :  'Всего меню' ?>: <?= $total ?>
        </small>
    </div>
    <div class="panel-body">
        <table class="table table-togglable table-hover">
            <thead>
                <tr>
                    <th data-toggle="true" style="width: 30px;">#ID</th>
                    <th data-toggle="true" width="50%">Название меню</th>
                    <th data-toggle="true" width="20%" class="text-center">Тип меню</th>
                    <th class="text-center" data-toggle="true">Публикация</th>
                    <th class="text-center" style="width: 30px;">
                        <i class="icon-menu-open2" data-popup="tooltip" title="Действие" data-placement="left"></i>
                    </th>
                </tr>
            </thead>
            <tbody>
            <? foreach($models as $item): ?>
                <tr>
                    <td><?= $item->id ?></td>
                    <td>
                        <?= Html::a($item->name, [
                            Yii::$app->controller->id . '/nav?menu_id=' . $item->id
                        ]) ?>
                    </td>
                    <td class="text-center">
                        <?= $item::getTypeName($item->type) ?>
                    </td>
                    <td class="text-center">
                        <div class="checkbox checkbox-switchery switchery-xs" title="Вкл/Выкл">
                            <label>
                                <?= Html::checkbox('Published['. $item->id .']', $item->published, [
                                    'class' => 'switchery',
                                    'onclick' => "ChangeRequest('" . Url::to([
                                        Yii::$app->controller->id . '/published',
                                        'id' => $item->id
                                    ]) . "', {published: this.checked});"
                                ]); ?>
                            </label>
                        </div>
                    </td>
                    <td class="text-center">
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Действия">
                                    <i class="icon-menu7"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <?= Html::a('<i class="icon-gear"></i>Настроить', [
                                            Yii::$app->controller->id . '/settings?id=' . $item->id
                                        ]) ?>
                                    </li>
                                    <li>
                                        <?= Html::a('<i class="icon-bin"></i>Удалить', [
                                            Yii::$app->controller->id . '/remove?id=' . $item->id
                                        ], [
                                            'onclick' => "return confirm('Вы уверены что хотите удалить данные?');"
                                        ]) ?>
                                    </li>
                                </ul>
                            </li>
                        </ul><!-- /.icons-list -->
                    </td>
                </tr>
            <? endforeach; ?>
            </tbody>
        </table>
    </div><!-- /.panel-body -->
</div>
<div class="mb-20">
    <?= LinkPager::widget([
        'pagination' => $pages,
        'options'    => [
            'class' => 'pagination pagination-xs'
        ]
    ]); ?>
</div>
<? else: Alert::begin(['options' => ['class' => 'bg-info alert-styled-left']]); ?>
    Не удается найти навигации для отображения
<? Alert::end(); endif;?>
<!-- /table with togglable columns -->