<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \backend\widgets\ExtendField;
use yii\bootstrap\Alert;
use \yii\helpers\StringHelper;
use \common\models\Files;
use yii\helpers\ArrayHelper;

if($model->isNewRecord)
    $this->context->view->title = 'Добавить новостную ленту';
else
    $this->context->view->title = 'Настроить новостную ленту';

$this->context->smallTitle = 'В этом разделе Вы можете создать или настроить новостную ленту, имеется ввиду основные настройки.';

$this->context->breadcrumbs[] = [
    'label' => 'Новостные ленты',
    'url'   => '/'. $this->context->id .'/feed'
];

$pageName = $this->context->view->title;
$this->context->breadcrumbs[] = $pageName;

$className = StringHelper::basename($model::className());

?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title"><?= $pageName ?></h5>
    </div>
    <div class="panel-body">
    <? $form = ActiveForm::begin([
        'id' => $this->context->id . '-form',
        'options' => [
            'role'    => 'form',
            'class' => 'form-horizontal'
        ]
    ]); ?>
        <div class="row">
            <div class="col-sm-12">
                <div class="tabbable">
                    <ul class="nav nav-tabs nav-tabs-bottom">
                        <li class="active">
                            <a href="#<?= Html::getInputId($model, 'basic') ?>" data-toggle="tab">
                                Основные поля
                            </a>
                        </li>
                        <li>
                            <a href="#<?= Html::getInputId($model, 'seo') ?>" data-toggle="tab">
                                СЕО продвижение
                            </a>
                        </li>
                        <li>
                            <a href="#<?= Html::getInputId($model, 'additional_fields') ?>" data-toggle="tab">
                                Дополнительные поля
                            </a>
                        </li>
                        <li>
                            <a href="#<?= Html::getInputId($model, 'categories') ?>" data-toggle="tab">
                                Категории
                            </a>
                        </li>
                        <li>
                            <a href="#<?= Html::getInputId($model, 'tags') ?>" data-toggle="tab">
                                Теги
                            </a>
                        </li>
                        <li>
                            <a href="#<?= Html::getInputId($model, 'additionally') ?>" data-toggle="tab">
                                Дополнительно
                            </a>
                        </li>
                        <? if(!$model->isNewRecord): ?>
                        <li>
                            <a href="#<?= Html::getInputId($model, 'shortcode') ?>" data-toggle="tab">
                                <i class=" icon-embed mr-5 text-size-small"></i> Короткий код
                            </a>
                        </li>
                        <? endif; ?>
                    </ul>
                    <div class="tab-content pt-5">
                        <div class="tab-pane active" id="<?= Html::getInputId($model, 'basic') ?>">
                            <?= $form->field($model, 'name', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint(false)->label(
                                $model->getAttributeLabelRequired('name'),
                                ['class' => 'control-label col-lg-2']
                            ) ?>
                            <!-- Begin editor -->
                            <div class="form-group">
                                <label class="control-label col-lg-2" for="<?= Html::getInputId($model, 'content') ?>">
                                    <?= $model->getAttributeLabelRequired('content') ?>
                                </label>
                                <div class="col-lg-10 clearfix">
                                    <?= \yii\imperavi\Widget::widget(ArrayHelper::merge(
                                        [
                                            'attribute' => Html::getInputName($model, 'content'),
                                            'value' => $model->content
                                        ],
                                        Yii::$app->params['imperavi'], [
                                            'options' => [
                                                'minHeight' => '250px',
                                                'uploadImageFields' => [
                                                    Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
                                                    'Files[item_id]' => $model->id,
                                                    'Files[module]' => $className,
                                                    'Files[type]' => Files::TYPE_IMAGE
                                                ],
                                                'uploadFileFields' => [
                                                    Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
                                                    'Files[item_id]' => $model->id,
                                                    'Files[module]' => $className,
                                                    'Files[type]' => Files::TYPE_FILE
                                                ]
                                            ]
                                        ]
                                    )); ?>
                                    <? if($model->hasErrors('content')): ?>
                                    <p class="help-block help-block-error text-danger">
                                        <?= $model->getFirstError('content') ?>
                                    </p>
                                    <? endif; ?>
                                    <p class="help-block">
                                        Краткое описания - информации об тематическом направлении данных в этом разделе
                                    </p>
                                </div>
                            </div>
                            <!-- End editor -->
                            <hr>
                            <div class="col-lg-10 col-lg-offset-2">
                                <div class="form-group">
                                    <?= $form->field($model, 'published', [
                                        'template' => '<label class="checkbox-inline">{input}'. $model->getAttributeLabel('published') .'{hint}</label>',
                                    ])->label(false)->checkbox(['class' => 'styled'], false)->hint('Публикация всех доступных новостей из этой новостной ленты') ?>
                                </div>
                            </div>
                        </div><!-- /.tab-pane -->
                        <div class="tab-pane" id="<?= Html::getInputId($model, 'seo') ?>">
                            <?= $form->field($model, 'alias', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint('Кириллическая версия имя для использования в ссылках')->label(
                                $model->getAttributeLabelRequired('alias'),
                                ['class' => 'control-label col-lg-2']
                            ) ?>
                            <?= $form->field($model, 'meta_description', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control',
                                    'rows' => '5'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint('Краткое описание до 200 (200 — максимум, ориентируйтесь на ударные первые 100 знаков).')->label(
                                $model->getAttributeLabelRequired('meta_description'),
                                ['class' => 'control-label col-lg-2']
                            ) ?>
                            <?= $form->field($model, 'meta_keywords', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control',
                                    'rows' => '5'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint('Введите через запятую основные ключевые слова для вашего сайта до 250 (250 — максимум, ориентируйтесь на ударные первые 150 знаков).')->label(
                                $model->getAttributeLabelRequired('meta_keywords'),
                                ['class' => 'control-label col-lg-2']
                            )->textarea() ?>
                        </div><!-- /.tab-pane -->
                        <div class="tab-pane" id="<?= Html::getInputId($model, 'additional_fields') ?>">
                            <!-- Begin fileld widget -->
                            <?= ExtendField::widget([
                                'id' => $model->id,
                                'options' => $model->field_options,
                                'controller' => Yii::$app->controller->id
                            ]) ?>
                            <!-- End fileld widget -->
                        </div><!-- /.tab-pane -->
                        <div class="tab-pane" id="<?= Html::getInputId($model, 'categories') ?>">
                            <?= $this->context->renderPartial('index-cat', [
                                'model' => $model,
                                'item_id' => $model->id
                            ])?>
                        </div><!-- /.tab-pane -->
                        <div class="tab-pane" id="<?= Html::getInputId($model, 'tags') ?>">
                            <?= $this->context->renderPartial('index-tag-group', [
                                'model' => $model,
                                'item_id' => $model->id
                            ])?>
                        </div><!-- /.tab-pane -->
                        <div class="tab-pane" id="<?= Html::getInputId($model, 'additionally') ?>">
                            <?= $form->field($model, 'view', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control',
                                    'placeholder' => 'templaate-shortcode'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint('Рекомендуется использовать когда есть дополнительные поля')->label(
                                $model->getAttributeLabelRequired('view'),
                                ['class' => 'control-label col-lg-2']
                            ) ?>
                            <?= $form->field($model, 'view_index', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control',
                                    'placeholder' => 'trmplate-index'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint('Рекомендуется использовать когда есть дополнительные поля')->label(
                                $model->getAttributeLabelRequired('view_index'),
                                ['class' => 'control-label col-lg-2']
                            ) ?>
                            <?= $form->field($model, 'view_detail', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control',
                                    'placeholder' => 'template-detail'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint('Рекомендуется использовать когда есть дополнительные поля')->label(
                                $model->getAttributeLabelRequired('view_detail'),
                                ['class' => 'control-label col-lg-2']
                            ) ?>

                            <?= $form->field($model, 'page_size', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint('Количество выводимых новостей для постраничной навигации.')->label(
                                $model->getAttributeLabelRequired('page_size'),
                                ['class' => 'control-label col-lg-2']
                            ) ?>
                            <hr>
                            <div class="col-lg-10 col-lg-offset-2">
                                <div class="form-group">
                                    <?= $form->field($model, 'img_enable', [
                                        'template' => '<label class="checkbox-inline">{input}'. $model->getAttributeLabel('img_enable') .'{hint}</label>',
                                    ])->label(false)->checkbox(['class' => 'styled'], false)->hint('Включить загрузку изображений для текущей новостной ленты') ?>
                                </div>
                            </div>
                            <?= $form->field($model, 'img_sizes', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint('Все дополнительные размеры изображений в формате "Ширина[x]Высота" которые разделяются запятой.<br> Например: 100x100,200x200,300x300... (где 100x100 это 100px на 100px).')->label(
                                $model->getAttributeLabelRequired('img_sizes'),
                                ['class' => 'control-label col-lg-2']
                            ) ?>
                            <hr>
                            <div class="col-lg-10 col-lg-offset-2">
                                <div class="form-group">
                                    <?= $form->field($model, 'is_count_views', [
                                        'template' => '<label class="checkbox-inline">{input}'. $model->getAttributeLabel('is_count_views') .'{hint}</label>',
                                    ])->label(false)->checkbox(['class' => 'styled'], false)->hint('Подсчёт количество просмотров новостей для текущей новостной ленты') ?>
                                </div>
                            </div>
                            <div class="col-lg-10 col-lg-offset-2">
                                <div class="form-group">
                                    <?= $form->field($model, 'is_date_published', [
                                        'template' => '<label class="checkbox-inline">{input}'. $model->getAttributeLabel('is_date_published') .'{hint}</label>',
                                    ])->label(false)->checkbox(['class' => 'styled'], false)->hint('Включить отображение даты публикации в новостях.') ?>
                                </div>
                            </div>
                            <div class="col-lg-10 col-lg-offset-2">
                                <div class="form-group">
                                    <?= $form->field($model, 'enable_comments', [
                                        'template' => '<label class="checkbox-inline">{input}'. $model->getAttributeLabel('enable_comments') .'{hint}</label>',
                                    ])->label(false)->checkbox(['class' => 'styled'], false)->hint('Подключит модуль и позволяет оставлять комментарии в новости.') ?>
                                </div>
                            </div>
                        </div><!-- /.tab-pane -->
                        <? if(!$model->isNewRecord): ?>
                        <div class="tab-pane" id="<?= Html::getInputId($model, 'shortcode') ?>">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Короткий код ленты:</label>
                                    <div class="col-lg-10">
                                        <div class="form-control-static">
                                            <code><?= $model->shortcode ?></code>
                                            <p class="help-block">Короткий код который можно установить в редакторе или в шаблон, в результате которого будет выведено в этом месте лента новостей.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Код ссылки:</label>
                                    <div class="col-lg-10">
                                        <div class="form-control-static">
                                            <code><?= $model->linkcode ?></code>
                                            <p class="help-block">Короткий код который можно установить в адресс ссылки, в результате которого будет выведена ссылка на запись.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Ссылка:</label>
                                    <div class="col-lg-10">
                                        <div class="form-control-static">
                                            <code><?= $model->link ?></code>
                                            <p class="help-block">URL текущей новостной ленты</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <? endif; ?>
                    </div><!-- /.tab-content -->
                </div>
            </div>
        </div><!-- /.row -->
        <hr>
        <div class="text-right">
            <button type="submit" name="btnAction" value="save" class="btn bg-teal-400 btn-sm">
                <i class="icon-checkmark3 position-left"></i> Сохранить
            </button>
            <button type="submit" name="btnAction" value="save-exit" class="btn btn-primary btn-sm">
                <i class="icon-undo2 mr-5"></i> Сохр. и закрыть
            </button>
            <button type="submit" name="btnAction" value="save-new" class="btn btn-primary btn-sm">
                <i class="icon-new-tab mr-5"></i> Сохр. и создать новую запись
            </button>
            <? if(!$model->isNewRecord): ?>
            <?= Html::a('<i class="icon-bin"></i>Удалить', [
                Yii::$app->controller->id . '/'. $model->id .'/feed-remove'
            ], [
                'onclick' => "return confirm('Вы уверены что хотите удалить данные?');",
                'class' => 'btn btn-danger btn-sm'
            ]) ?>
            <? endif; ?>
        </div>
    <? ActiveForm::end(); ?>
    </div><!-- /.panel-body -->
</div>