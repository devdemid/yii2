<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use common\models\News;

// Set the component description
$this->context->smallTitle = 'Здесь список всех новостей этой ленты тут можно отредактировать, удалить или создать новую запись';
$this->context->breadcrumbs[] = [
    'label' => 'Новостные ленты',
    'url'   => '/'. $this->context->id .'/feed'
];

$this->context->breadcrumbs[] = [
    'label' => $newsFeed->name,
    'url'   => '/'. $this->context->id .'/index?item_id=' . $newsFeed->id
];

$this->context->breadcrumbs[] = $this->context->view->title;

?>

<? if(count($news)): ?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Все новости</h5>
        <small class="display-block text-muted">
            Всего новостей: <?= $total ?>
        </small>
    </div>
    <div class="panel-body">
        <table class="table table-togglable table-hover">
            <thead>
                <tr>
                    <th data-toggle="true" width="5%">#ID</th>
                    <th data-toggle="true" >Название новости</th>
                    <th class="text-center" width="10%" data-toggle="true">Публикация</th>
                    <th class="text-center" width="10%">
                        <a href="#" onclick="return Sorting('/<?= Yii::$app->controller->id ?>/sorting?item_id=<?= $newsFeed->id ?>', 'Sorting');" title="Сохранить позиции" data-popup="tooltip">
                            <i class="icon-sort text-size-mini text-default"></i>
                        </a>
                    </th>
                </tr>
            </thead>
            <tbody>
            <? foreach($news as $item): ?>
                <tr>
                    <td><?= $item->id ?></td>
                    <td>
                        <?= Html::a($item->name, [
                            Yii::$app->controller->id . '/edit?id=' . $item->id
                        ]) ?>
                        <? if($item->fixed): ?>
                            <span class="label label-info ml-5" data-popup="tooltip" title="Новость зафиксирована">
                                Fixed record
                            </span>
                        <? endif ?>
                    </td>
                    <td class="text-center">
                        <div class="checkbox checkbox-switchery switchery-xs" title="Вкл/Выкл">
                            <label>
                                <?= Html::checkbox('Published['. $item->id .']', $item->published, [
                                    'class' => 'switchery',
                                    'onclick' => "ChangeRequest('" . Url::to([
                                        Yii::$app->controller->id . '/published',
                                        'id' => $item->id
                                    ]) . "', {published: this.checked});"
                                ]); ?>
                            </label>
                        </div>
                    </td>
                    <td>
                        <?= Html::input('text', 'Sorting[' . $item->id . ']', $item->sorting, [
                            'class' => 'form-control input-xs text-center',
                            'style' => 'width: 50px; margin: 0 auto'
                        ]) ?>
                    </td>
                </tr>
            <? endforeach; ?>
            </tbody>
        </table>
    </div><!-- /.panel-body -->
</div>
<div class="mb-20">
    <?= LinkPager::widget([
        'pagination' => $pages,
        'options'    => [
            'class' => 'pagination pagination-xs'
        ]
    ]); ?>
</div>
<? else: Alert::begin(['options' => ['class' => 'bg-info alert-styled-left']]); ?>
    Не удается найти новости для отображения
<? Alert::end(); endif;?>
<!-- /table with togglable columns -->