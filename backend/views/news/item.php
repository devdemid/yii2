<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \backend\widgets\ExtendFieldForm;
use yii\bootstrap\Alert;
use \yii\helpers\StringHelper;
use \common\models\Files;
use \common\models\News;
use yii\helpers\ArrayHelper;
use backend\widgets\FilesWidget;

if($model->isNewRecord)
    $this->context->view->title = 'Добавить новость';
else
    $this->context->view->title = 'Редактировать новость';

$this->context->smallTitle = 'Нвости относятся к новостной ленте ' . $newsFeed->name;

$this->context->breadcrumbs[] = [
    'label' => 'Новостные ленты',
    'url'   => '/'. $this->context->id .'/feed'
];

$this->context->breadcrumbs[] = [
    'label' => $newsFeed->name,
    'url'   => '/'. $this->context->id .'/index?item_id=' . $newsFeed->id
];

$pageName = $this->context->view->title;
$this->context->breadcrumbs[] = $pageName;

$className = StringHelper::basename($model::className());

?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title"><?= $pageName ?></h5>
    </div>
    <div class="panel-body">
    <? $form = ActiveForm::begin([
        'id' => $this->context->id . '-form',
        'options' => [
            'role'    => 'form',
            'class' => 'form-horizontal',
            'enctype' => 'multipart/form-data',
        ]
    ]); ?>
        <div class="row">
            <div class="col-sm-12">
                <div class="tabbable">
                    <ul class="nav nav-tabs nav-tabs-bottom">
                        <li class="active">
                            <a href="#<?= Html::getInputId($model, 'basic') ?>" data-toggle="tab">
                                Основные поля
                            </a>
                        </li>
                        <li>
                            <a href="#<?= Html::getInputId($model, 'seo') ?>" data-toggle="tab">
                                СЕО продвижение
                            </a>
                        </li>
                        <? if($model->feedModel->isFields): ?>
                        <li>
                            <a href="#<?= Html::getInputId($model, 'additional_fields') ?>" data-toggle="tab">
                                Дополнительные поля
                            </a>
                        </li>
                        <? endif; ?>
                        <? if($model->feedModel->countTagGroup): ?>
                            <li>
                            <a href="#<?= Html::getInputId($model, 'tag_group') ?>" data-toggle="tab">
                                Теги
                            </a>
                        </li>
                        <? endif; ?>
                        <? if($newsFeed->img_enable): ?>
                            <li>
                                <a href="#<?= Html::getInputId($model, 'images') ?>" data-toggle="tab">
                                    Изображения
                                </a>
                            </li>
                        <? endif; ?>
                        <? if(!$model->isNewRecord): ?>
                        <li>
                            <a href="#<?= Html::getInputId($model, 'linkcode') ?>" data-toggle="tab">
                                <i class=" icon-embed mr-5 text-size-small"></i> Короткий код
                            </a>
                        </li>
                        <? endif; ?>
                    </ul>
                    <div class="tab-content pt-5">
                        <div class="tab-pane active" id="<?= Html::getInputId($model, 'basic') ?>">

                            <? if(count($newsFeed->categories)) echo $form->field($model, 'cat_id', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'select'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->dropDownList(ArrayHelper::map($newsFeed->categories, 'id', 'name'), [
                                'prompt' => '--Выберите категорию--'
                            ])->label(
                                $model->getAttributeLabelRequired('cat_id'),
                                ['class' => 'control-label col-lg-2']
                            )->hint(false) ?>

                            <?= $form->field($model, 'name', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint(false)->label(
                                $model->getAttributeLabelRequired('name'),
                                ['class' => 'control-label col-lg-2']
                            ) ?>
                            <!-- Begin editor -->
                            <div class="form-group">
                                <label class="control-label col-lg-2" for="<?= Html::getInputId($model, 'short_description') ?>">
                                    <?= $model->getAttributeLabelRequired('short_description') ?>
                                </label>
                                <div class="col-lg-10 clearfix">
                                    <?= \yii\imperavi\Widget::widget(ArrayHelper::merge(
                                        [
                                            'attribute' => Html::getInputName($model, 'short_description'),
                                            'value' => $model->short_description
                                        ],
                                        Yii::$app->params['imperavi'], [
                                            'id' => Html::getInputId($model, 'short_description'),
                                            'options' => [
                                                'minHeight' => '150px',
                                                'uploadImageFields' => [
                                                    Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
                                                    'Files[item_id]' => $model->id,
                                                    'Files[module]' => $className,
                                                    'Files[type]' => Files::TYPE_IMAGE
                                                ],
                                                'uploadFileFields' => [
                                                    Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
                                                    'Files[item_id]' => $model->id,
                                                    'Files[module]' => $className,
                                                    'Files[type]' => Files::TYPE_FILE
                                                ]
                                            ]
                                        ]
                                    )); ?>
                                    <? if($model->hasErrors('short_description')): ?>
                                    <p class="help-block help-block-error text-danger">
                                        <?= $model->getFirstError('short_description') ?>
                                    </p>
                                    <? endif; ?>
                                    <p class="help-block">
                                        Краткое описания новости будет выводиться на странице разделов или в блоках списка новостей
                                    </p>
                                </div>
                            </div>
                            <!-- End editor -->
                            <!-- Begin editor -->
                            <div class="form-group">
                                <label class="control-label col-lg-2" for="<?= Html::getInputId($model, 'full_description') ?>">
                                    <?= $model->getAttributeLabelRequired('full_description') ?>
                                </label>
                                <div class="col-lg-10 clearfix">
                                    <?= \yii\imperavi\Widget::widget(ArrayHelper::merge(
                                        [
                                            'attribute' => Html::getInputName($model, 'full_description'),
                                            'value' => $model->full_description
                                        ],
                                        Yii::$app->params['imperavi'], [
                                            'id' => Html::getInputId($model, 'full_description'),
                                            'options' => [
                                                'minHeight' => '250px',
                                                'uploadImageFields' => [
                                                    Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
                                                    'Files[item_id]' => $model->id,
                                                    'Files[module]' => $className,
                                                    'Files[type]' => Files::TYPE_IMAGE
                                                ],
                                                'uploadFileFields' => [
                                                    Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
                                                    'Files[item_id]' => $model->id,
                                                    'Files[module]' => $className,
                                                    'Files[type]' => Files::TYPE_FILE
                                                ]
                                            ]
                                        ]
                                    )); ?>
                                    <? if($model->hasErrors('full_description')): ?>
                                    <p class="help-block help-block-error text-danger">
                                        <?= $model->getFirstError('full_description') ?>
                                    </p>
                                    <? endif; ?>
                                    <p class="help-block">
                                        Полное описание будет выведено на странице новости
                                    </p>
                                </div>
                            </div>
                            <!-- End editor -->

                            <? if($newsFeed->is_date_published): ?>
                            <?= $form->field($model, 'date', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10"><div class="input-group"><span class="input-group-addon"><i class="icon-calendar3"></i></span>{input}</div>{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control',
                                    //'id' => 'anytime-month-numeric',
                                    'placeholder' => date(News::PROP_DATE_FORMAT),
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint(false)->label(
                                $model->getAttributeLabelRequired('date'),
                                ['class' => 'control-label col-lg-2']
                            ) ?>
                            <? endif; ?>

                            <hr>
                            <div class="col-lg-10 col-lg-offset-2">
                                <div class="form-group">
                                    <?= $form->field($model, 'fixed', [
                                        'template' => '<label class="checkbox-inline">{input}'. $model->getAttributeLabel('fixed') .'{hint}</label>',
                                    ])->label(false)->checkbox(['class' => 'styled'], false)->hint('Новость будет выводиться на первых местах в списке новостей ленты, с высшим приоритетом над сортировкой.') ?>
                                </div>
                            </div>
                            <div class="col-lg-10 col-lg-offset-2">
                                <div class="form-group">
                                    <?= $form->field($model, 'noindex', [
                                        'template' => '<label class="checkbox-inline">{input}'. $model->getAttributeLabel('noindex') .'{hint}</label>',
                                    ])->label(false)->checkbox(['class' => 'styled'], false)->hint('Новость будет доступна для просмотра, но не будет индексироваться поисковыми роботами') ?>
                                </div>
                            </div>
                            <? if($newsFeed->enable_comments): ?>
                                <div class="col-lg-10 col-lg-offset-2">
                                <div class="form-group">
                                    <?= $form->field($model, 'enable_comments', [
                                        'template' => '<label class="checkbox-inline">{input}'. $model->getAttributeLabel('enable_comments') .'{hint}</label>',
                                    ])->label(false)->checkbox(['class' => 'styled'], false)->hint('Отключить комментарии для этой записи. (Отображения и добавление комментариев)') ?>
                                </div>
                            </div>
                            <? endif ?>
                            <div class="col-lg-10 col-lg-offset-2">
                                <div class="form-group">
                                    <?= $form->field($model, 'published', [
                                        'template' => '<label class="checkbox-inline">{input}'. $model->getAttributeLabel('published') .'{hint}</label>',
                                    ])->label(false)->checkbox(['class' => 'styled'], false)->hint('Новость будет доступна для просмотра посетителями ресурса') ?>
                                </div>
                            </div>
                        </div><!-- /.tab-pane -->
                        <div class="tab-pane" id="<?= Html::getInputId($model, 'seo') ?>">
                            <?= $form->field($model, 'alias', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint('Кириллическая версия имя для использования в ссылках')->label(
                                $model->getAttributeLabelRequired('alias'),
                                ['class' => 'control-label col-lg-2']
                            ) ?>
                             <?= $form->field($model, 'meta_description', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control',
                                    'rows' => '5'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint('Краткое описание до 200 (200 — максимум, ориентируйтесь на ударные первые 100 знаков).')->label(
                                $model->getAttributeLabelRequired('meta_description'),
                                ['class' => 'control-label col-lg-2']
                            ) ?>

                            <?= $form->field($model, 'meta_keywords', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control',
                                    'rows' => '5'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint('Введите через запятую основные ключевые слова для вашего сайта до 250 (250 — максимум, ориентируйтесь на ударные первые 150 знаков).')->label(
                                $model->getAttributeLabelRequired('meta_keywords'),
                                ['class' => 'control-label col-lg-2']
                            )->textarea() ?>
                        </div><!-- /.tab-pane -->
                        <? if($model->feedModel->isFields): ?>
                        <div class="tab-pane" id="<?= Html::getInputId($model, 'additional_fields') ?>">
                            <?= ExtendFieldForm::widget([
                                'id' => $model->id,
                                'model' => $field_model,
                                'options' => $model->feedModel->field_options
                            ]) ?>
                        </div><!-- /.tab-pane -->
                        <? endif; ?>

                        <? if($model->feedModel->countTagGroup): ?>
                        <div class="tab-pane" id="<?= Html::getInputId($model, 'tag_group') ?>">
                        <? foreach($model->feedModel->tagGroup as $item): ?>
                            <div class="form-group">
                                <label for="tags-<?= $item->id ?>" class="control-label col-lg-2">
                                    <?= $item->name ?>
                                </label>
                                <div class="col-lg-10">
                                    <?= Html::input(
                                        'text',
                                        StringHelper::basename($model::className()). '[tags]['. $item->id .']',
                                        ArrayHelper::getValue($model->tags, $item->id, ''),
                                        [
                                            'data-role' => 'tagsinput',
                                            'class' => 'tagsinput-typeahead',
                                            'id' => 'tags-' . $item->id
                                        ]
                                    ) ?>
                                </div>
                            </div>
                        <? endforeach; ?>
                        </div><!-- /.tab-pane -->
                        <? endif; ?>

                        <? if($newsFeed->img_enable): ?>
                            <div class="tab-pane" id="<?= Html::getInputId($model, 'images') ?>">
                                <!-- Begin files module -->
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <label for="" class="control-label">Изображения:</label>
                                        <?= FilesWidget::widget([
                                            'id' => $model->id,
                                            'type' => \common\models\Files::TYPE_IMAGE,
                                            'files' => $model->prewiewsWidget,
                                            'module' => $model::className(),
                                            'sizes' => $newsFeed->imgSizes,
                                            'hint' => 'Изображения, который будет отображаться на страницах сайта',
                                            'labels' => [
                                                'Установленные размеры изображений' => ' ('. $newsFeed->imgSizeString .') px.'
                                            ]
                                        ]) ?>
                                    </div>
                                </div>
                                <!-- End files module -->
                            </div><!-- /.tab-pane -->
                        <? endif; ?>
                        <? if(!$model->isNewRecord): ?>
                        <div class="tab-pane" id="<?= Html::getInputId($model, 'linkcode') ?>">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Код ссылки:</label>
                                    <div class="col-lg-10">
                                        <div class="form-control-static">
                                            <code><?= $model->linkcode ?></code>
                                            <p class="help-block">Короткий код который можно установить в адресс ссылки, в результате которого будет выведена ссылка на запись.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Ссылка:</label>
                                    <div class="col-lg-10">
                                        <div class="form-control-static">
                                            <code><?= $model->link ?></code>
                                            <p class="help-block">URL текущей новости</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <? endif; ?>
                    </div><!-- /.tab-content -->
                </div>
            </div>
        </div><!-- /.row -->
        <hr>
        <div class="text-right">
            <button type="submit" name="btnAction" value="save" class="btn bg-teal-400 btn-sm">
                <i class="icon-checkmark3 position-left"></i> Сохранить
            </button>
            <button type="submit" name="btnAction" value="save-exit" class="btn btn-primary btn-sm">
                <i class="icon-undo2 mr-5"></i> Сохр. и закрыть
            </button>
            <button type="submit" name="btnAction" value="save-new" class="btn btn-primary btn-sm">
                <i class="icon-new-tab mr-5"></i> Сохр. и создать новую запись
            </button>
            <? if(!$model->isNewRecord): ?>
            <?= Html::a('<i class="icon-bin"></i>Удалить', [
                Yii::$app->controller->id . '/'. $model->id .'/remove'
            ], [
                'onclick' => "return confirm('Вы уверены что хотите удалить данные?');",
                'class' => 'btn btn-danger btn-sm'
            ]) ?>
            <? endif; ?>
        </div>
    <? ActiveForm::end(); ?>
    </div><!-- /.panel-body -->
</div>