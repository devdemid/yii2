<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

if($model->isNewRecord)
    $this->context->view->title = 'Добавить группу тегов';
else
    $this->context->view->title = 'Редактировать группу тегов';

$this->context->smallTitle = 'В этом разделе Вы можете создать или отредактировать группу тегов для новостной ленты.';

$this->context->breadcrumbs[] = [
    'label' => 'Новостные ленты',
    'url'   => '/'. $this->context->id .'/feed'
];

$this->context->breadcrumbs[] = [
    'label' => 'Настроить новостную ленту',
    'url'   => '/'. $this->context->id .'/edit-feed?id=' . $model->item_id
];

$pageName = $this->context->view->title;
$this->context->breadcrumbs[] = $pageName;

?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title"><?= $pageName ?></h5>
    </div>
    <div class="panel-body">
    <? $form = ActiveForm::begin([
        'id' => $this->context->id . '-form',
        'options' => [
            'role'    => 'form',
            'class' => 'form-horizontal',
            'enctype' => 'multipart/form-data',
        ]
    ]); ?>
            <div class="row">
            <div class="col-sm-12">
                <div class="tabbable">
                    <ul class="nav nav-tabs nav-tabs-bottom">
                        <li class="active">
                            <a href="#<?= Html::getInputId($model, 'basic') ?>" data-toggle="tab">
                                Основные поля
                            </a>
                        </li>
                        <li>
                            <a href="#<?= Html::getInputId($model, 'seo') ?>" data-toggle="tab">
                                СЕО продвижение
                            </a>
                        </li>
                        <? if(!$model->isNewRecord): ?>
                        <li>
                            <a href="#<?= Html::getInputId($model, 'shortcode') ?>" data-toggle="tab">
                                <i class=" icon-embed mr-5 text-size-small"></i> Короткий код
                            </a>
                        </li>
                        <? endif; ?>
                    </ul>
                    <div class="tab-content pt-5">
                        <div class="tab-pane active" id="<?= Html::getInputId($model, 'basic') ?>">
                            <?= $form->field($model, 'name', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint(false)->label(
                                $model->getAttributeLabelRequired('name'),
                                ['class' => 'control-label col-lg-2']
                            ) ?>
                            <hr>
                            <div class="col-lg-10 col-lg-offset-2">
                                <div class="form-group">
                                    <?= $form->field($model, 'published', [
                                        'template' => '<label class="checkbox-inline">{input}'. $model->getAttributeLabel('published') .'{hint}</label>',
                                    ])->label(false)->checkbox(['class' => 'styled'], false)->hint('Публикация группы тегов в новостной ленте') ?>
                                </div>
                            </div>
                        </div><!-- /.tab-pane -->
                        <div class="tab-pane" id="<?= Html::getInputId($model, 'seo') ?>">
                            <?= $form->field($model, 'alias', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint('Кириллическая версия имя для использования в ссылках')->label(
                                $model->getAttributeLabelRequired('alias'),
                                ['class' => 'control-label col-lg-2']
                            ) ?>
                        </div><!-- /.tab-pane -->
                        <? if(!$model->isNewRecord): ?>
                        <div class="tab-pane" id="<?= Html::getInputId($model, 'shortcode') ?>">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Ссылка:</label>
                                    <div class="col-lg-10">
                                        <div class="form-control-static">
                                            <code><?= $model->link ?></code>
                                            <p class="help-block">URL текущей группы тегов</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.tab-pane -->
                        <? endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="text-right">
            <button type="submit" name="btnAction" value="save" class="btn bg-teal-400 btn-sm">
                <i class="icon-checkmark3 position-left"></i> Сохранить
            </button>
            <button type="submit" name="btnAction" value="save-exit" class="btn btn-primary btn-sm">
                <i class="icon-undo2 mr-5"></i> Сохр. и закрыть
            </button>
            <button type="submit" name="btnAction" value="save-new" class="btn btn-primary btn-sm">
                <i class="icon-new-tab mr-5"></i> Сохр. и создать новую группу тегов
            </button>
            <? if(!$model->isNewRecord): ?>
            <?= Html::a('<i class="icon-bin"></i>Удалить', [
                Yii::$app->controller->id . '/remove-tag-group?id=' . $model->id
            ], [
                'onclick' => "return confirm('Вы уверены что хотите удалить шруппу тегов?');",
                'class' => 'btn btn-danger btn-sm'
            ]) ?>
            <? endif; ?>
        </div>
    <? ActiveForm::end(); ?>
</div>