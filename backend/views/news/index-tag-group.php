<?php

use yii\helpers\Html;

?>
<? if($model->tagGroup): ?>
<div class="table-responsive mb-20">
    <table class="table table-striped">
        <thead>
            <tr>
                <th width="5%">#ID</th>
                <th width="85%">Группа</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        <? foreach($model->tagGroup as $item): ?>
            <tr>
                <td><?= $item->id ?></td>
                <td><?= $item->name ?></td>
                <td class="text-right">
                    <a href="/<?= Yii::$app->controller->id ?>/edit-tag-group?id=<?= $item->id ?>" class="mr-5" title="Редактировать группу">
                        <i class="icon-pencil5 text-primary"></i>
                    </a>
                    <a href="/<?= Yii::$app->controller->id ?>/remove-tag-group?id=<?= $item->id ?>" class="ml-5" onclick="return confirm('Вы уверены что хотите удалить группу тегов?');" data-popup="tooltip" title="Удалить группу">
                        <i class="icon-trash text-danger"></i>
                    </a>
                </td>
            </tr>
        <? endforeach; ?>
        </tbody>
    </table>
</div>
<? else: ?>
<div id="w1" class="bg-info alert-styled-left alert fade in">
    Группы тегов в этой новостной ленте нет.
</div>
<? endif; ?>
<br>
<div class="text-right">
    <a href="/<?= Yii::$app->controller->id ?>/add-tag-group?item_id=<?= $item_id ?>" class="btn btn-primary btn-xs">
        <i class="icon-plus3 position-left"></i>
        Добавить группу тегов
    </a>
</div>