<?php

use yii\helpers\Html;

?>
<? if(count($model->categories)): ?>
<div class="table-responsive mb-20">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>#ID</th>
                <th>Категория</th>
                <th>Ссылка</th>
                <th class="text-center">
                    <a href="#" onclick="return Sorting('/<?= Yii::$app->controller->id ?>/cat-sorting', 'Sorting');" title="Сохранить позиции" data-popup="tooltip">
                        <i class="icon-sort text-size-mini text-default"></i>
                    </a>
                </th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        <? foreach($model->categories as $item): ?>
            <tr>
                <td><?= $item->id ?></td>
                <td><?= $item->name ?></td>
                <td><code><?= $item->link ?></code></td>
                <td>
                    <?= Html::input('text', 'Sorting[' . $item->id . ']', $item->sorting, [
                        'class' => 'form-control input-xs text-center',
                        'style' => 'width: 50px; margin: 0 auto'
                    ]) ?>
                </td>
                <td class="text-center">
                    <a href="/<?= Yii::$app->controller->id ?>/cat-edit?id=<?= $item->id ?>" class="mr-5" title="Редактировать категорию">
                        <i class="icon-pencil5 text-primary"></i>
                    </a>
                    <a href="/<?= Yii::$app->controller->id ?>/cat-remove?id=<?= $item->id ?>" class="ml-5" onclick="return confirm('Вы уверены что хотите удалить категорию?');" data-popup="tooltip" title="Удалить категорию">
                        <i class="icon-trash text-danger"></i>
                    </a>
                </td>
            </tr>
        <? endforeach; ?>
        </tbody>
    </table>
</div>
<? else: ?>
<div id="w1" class="bg-info alert-styled-left alert fade in">
    Категорий в этой новостной ленте нет.
</div>
<? endif; ?>
<br>
<div class="text-right">
    <a href="/<?= Yii::$app->controller->id ?>/cat-add?item_id=<?= $item_id ?>" class="btn btn-primary btn-xs">
        <i class="icon-plus3 position-left"></i>
        Добавить категорию
    </a>
</div>