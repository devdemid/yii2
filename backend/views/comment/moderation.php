<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\Groupmodels;
use backend\widgets\FilesWidget;

$this->context->breadcrumbs[] = [
    'label' => 'Все комментарии',
    'url'   => '/'. $this->context->id .'/index'
];

$pageName = 'Редактировать комментарий';
$this->context->view->title = $pageName;
$this->context->breadcrumbs[] = $pageName;

?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title"><?= $pageName ?></h5>
    </div>
    <div class="panel-body">
    <? $form = ActiveForm::begin([
        'id' => $this->context->id . '-form',
        'options' => [
            'role'    => 'form',
            'class' => 'form-horizontal'
        ]
    ]); ?>
        <div class="row">
            <div class="col-sm-12">
            <? if($model->user !== null): ?>
                <div class="form-group">
                    <label class="control-label col-lg-2">Пользователь</label>
                    <div class="col-lg-10">
                        <div class="form-control-static">
                            <a href="<?= $model->user->profileLink ?>" target="_blank">
                                <?= $model->user->nameTogether ?>
                            </a>
                        </div>
                    </div>
                </div>
            <? else: ?>
                <?= $form->field($model, 'name', [
                    // Begin input theme
                    'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                    // End input theme
                    'inputOptions' => [
                        'class' => 'form-control'
                    ],
                    'options' => [
                        'class' => 'form-group'
                    ]
                ])->hint(false)->label(
                    $model->getAttributeLabel('name') .
                    ($model->isAttributeRequired('name')? ' : <span class="text-danger">*</span>' : ' :'),
                    ['class' => 'control-label col-lg-2']
                ) ?>

                <?= $form->field($model, 'email', [
                    // Begin input theme
                    'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                    // End input theme
                    'inputOptions' => [
                        'class' => 'form-control'
                    ],
                    'options' => [
                        'class' => 'form-group'
                    ]
                ])->hint(false)->label(
                    $model->getAttributeLabel('email') .
                    ($model->isAttributeRequired('email')? ' : <span class="text-danger">*</span>' : ' :'),
                    ['class' => 'control-label col-lg-2']
                ) ?>
            <? endif ?>

                <?= $form->field($model, 'content', [
                    // Begin input theme
                    'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                    // End input theme
                    'inputOptions' => [
                        'class' => 'form-control',
                        'rows'  => 15
                    ],
                    'options' => [
                        'class' => 'form-group'
                    ]
                ])->textarea()->hint('Полное описание или HTML шаблон')->label(
                    $model->getAttributeLabel('content') .
                    ($model->isAttributeRequired('content')? ' : <span class="text-danger">*</span>' : ' :'),
                    ['class' => 'control-label col-lg-2']
                ) ?>

                <div class="col-lg-10 col-lg-offset-2">
                    <div class="form-group">
                        <?= $form->field($model, 'moderation', [
                            'template' => '<label class="checkbox-inline">{input}'. $model->getAttributeLabel('moderation') .'{hint}</label>',
                        ])->label(false)->checkbox(['class' => 'styled'], false)->hint('Проверка содержимого комментария была выполнена администратором сайта.') ?>
                    </div>
                </div>

                <div class="col-lg-10 col-lg-offset-2">
                    <div class="form-group">
                        <?= $form->field($model, 'published', [
                            'template' => '<label class="checkbox-inline">{input}'. $model->getAttributeLabel('published') .'{hint}</label>',
                        ])->label(false)->checkbox(['class' => 'styled'], false)->hint('Комментарий будет выведен на странице ресурса') ?>
                    </div>
                </div>
            </div>
        </div><!-- /.row -->
        <hr>
        <div class="text-right">
            <button type="submit" name="btnAction" value="save" class="btn bg-teal-400 btn-sm">
                <i class="icon-checkmark3 position-left"></i> Сохранить
            </button>
            <button type="submit" name="btnAction" value="save-exit" class="btn btn-primary btn-sm">
                <i class="icon-undo2 mr-5"></i> Сохр. и закрыть
            </button>
            <? if(!$model->isNewRecord): ?>
            <?= Html::a('<i class="icon-bin"></i>Удалить', [
                Yii::$app->controller->id . '/'. $model->id .'/remove'
            ], [
                'onclick' => "return confirm('Вы уверены что хотите удалить данные?');",
                'class' => 'btn btn-danger btn-sm'
            ]) ?>
            <? endif; ?>
        </div>
    <? ActiveForm::end(); ?>
    </div><!-- /.panel-body -->
</div>