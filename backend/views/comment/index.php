<?php

use yii\helpers\StringHelper;
use yii\helpers\Html;
use yii\bootstrap\Alert;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->context->smallTitle = 'Модуль "Комментарии" предназначен для управления и настройки комментариев на сайте.';

?>
<? if(count($comments)): ?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Все комментарии</h5>
        <small class="display-block text-muted">
            <?= $search ? 'Всего найдено' :  'Всего комментариев' ?>: <?= $pages->totalCount ?>
        </small>
        <div class="heading-elements">
            <?= Html::beginForm(Url::to([Yii::$app->controller->id . '/index']), 'get', ['class' => 'heading-form']) ?>
                <div class="form-group has-feedback">
                    <?//= Html::hiddenInput('module', $newsFeed->id)?>
                    <?= Html::textInput('search', $search, ['class' => 'form-control input-xs', 'placeholder' => 'Поиск...']) ?>
                    <div class="form-control-feedback">
                        <i class="icon-search4 text-size-base text-muted"></i>
                    </div>
                </div>
            <?= Html::endForm() ?>
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table text-nowrap">
                <thead>
                    <tr>
                        <th width="5%">#ID</th>
                        <th width="30%">Автор</th>
                        <th width="10%">Модерация</th>
                        <th width="35%">Комментарий</th>
                        <th class="text-center" width="10%" data-toggle="true">Публикация</th>
                        <th class="text-center" width="10%">
                            <i class="icon-menu-open2" data-popup="tooltip" title="Действие" data-placement="left"></i>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <? foreach($comments as $item): ?>
                    <tr>
                        <td><?= $item->id ?></td>
                        <td>
                            <div class="media-left media-middle">
                                <? if($item->user !== null): ?>
                                <img src="<?= $item->user->pathLogo ?>" alt="<?= Html::encode($item->user->nameTogether) ?>" style="width:32px;height:32px;" class="btn-rounded">
                                <? else: ?>
                                <span class="btn bg-primary-400 btn-rounded btn-icon btn-xs">
                                    <span class="letter-icon">
                                        <?= $item->symbolName ?>
                                    </span>
                                </span>
                                <? endif ?>
                            </div>
                            <div class="media-body">
                                <? if($item->user !== null): ?>
                                    <a href="<?= $item->user->profileLink ?>" class="display-inline-block text-semibold letter-icon-title">
                                        <?= $item->user->nameTogether ?>
                                    </a>
                                    <div class="text-muted text-size-small">
                                        <span>Зарегистрированный</span>
                                    </div>
                                <? else: ?>
                                    <span class="display-inline-block text-default text-semibold letter-icon-title">
                                        <?= $item->name ?>
                                    </span>
                                    <div class="text-muted text-size-small">
                                        <a href="mailto:<?= $item->email ?>">
                                            <?= $item->email ?>
                                        </a>
                                    </div>
                                <? endif ?>
                                <span class="text-muted">
                                    Без модерации
                                </span>
                            </div>
                        </td>
                        <td class="text-center">
                            <? if($item->moderation): ?>
                                <i class="icon-checkmark3 text-success" title="Модерация выполнена"></i>
                            <? else: ?>
                                <i class="icon-warning text-muted" title="Без модерации"></i>
                            <? endif; ?>
                        </td>
                        <td class="text-left">
                            <div class="text-muted">
                                <div>
                                    <?= StringHelper::truncate($item->content, 50, '...') ?>
                                </div>
                                <a href="/<?= Yii::$app->controller->id ?>/preview?id=<?= $item->id ?>" data-toggle="modal-ajax" data-target="#preview-comment">
                                    Быстрый просмотр...
                                </a>
                            </div>
                        </td>
                        <td class="text-center">
                            <div class="checkbox checkbox-switchery switchery-xs" title="Вкл/Выкл">
                                <label>
                                    <?= Html::checkbox('Published['. $item->id .']', $item->published, [
                                        'class' => 'switchery',
                                        'onclick' => "ChangeRequest('" . Url::to([
                                            Yii::$app->controller->id . '/published',
                                            'id' => $item->id
                                        ]) . "', {published: this.checked});"
                                    ]); ?>
                                </label>
                            </div>
                        </td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Действия">
                                        <i class="icon-menu7"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li>
                                            <?= Html::a('<i class="icon-pencil5"></i>Редактировать', [
                                                Yii::$app->controller->id . '/moderation?id=' . $item->id
                                            ]) ?>
                                        </li>
                                        <li>
                                            <?= Html::a('<i class="icon-bin"></i>Удалить', [
                                                Yii::$app->controller->id . '/remove?id=' . $item->id
                                            ], [
                                                'onclick' => "return confirm('Вы уверены что хотите удалить данные?');"
                                            ]) ?>
                                        </li>
                                    </ul>
                                </li>
                            </ul><!-- /.icons-list -->
                        </td>
                    </tr>
                    <? endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="mb-20">
    <?= LinkPager::widget([
        'pagination' => $pages,
        'options'    => [
            'class' => 'pagination pagination-xs'
        ]
    ]); ?>
</div>

<!-- Basic modal -->
<div id="preview-comment" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content"></div>
    </div>
</div>
<!-- /basic modal -->

<? else: Alert::begin(['options' => ['class' => 'bg-info alert-styled-left']]); ?>
    Не удается найти комментарии для отображения
<? Alert::end(); endif;?>
<!-- /table with togglable columns -->