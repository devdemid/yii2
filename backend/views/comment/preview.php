<div class="modal-header bg-primary">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h5 class="modal-title">
        Комментарий
        <span class="mr-10">
            #<?= $model->id ?>
        </span>
    </h5>
</div>
<div class="modal-body">
    <?= $model->content ?>
</div>
<hr class="mt-5">
<div class="modal-footer row">
    <div class="col-sm-6 text-left">
        <span class="text-muted mr-5">Автор:</span>
        <span class="text-semibold">
        <? if($item->user !== null): ?>
            <?= $model->user->nameTogether ?>
        <? else: ?>
            <?= $model->name ?>
        <? endif; ?>

        </span>
    </div>
    <div class="col-sm-6">
        <?= date('d M Y H:s', $model->date_timestamp) ?>
    </div>
</div>