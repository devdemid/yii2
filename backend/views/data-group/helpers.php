<?php

$this->registerCssFile(Yii::$app->params['frontentUrl'] . '/css/fontello/css/all-fontello.min.css');

?>
<div class="panel panel-flat m-15">
    <div class="panel-heading">
        <h5 class="panel-title">CSS Иконки</h5>
        <small class="text-muted">CSS Иконки которые используются в шаблоне сайта.</small>
    </div>
    <div class="panel-body">
        <div class="row">
            <? for($i = 100; $i > 0; $i--): ?>
            <div class="col-sm-4 p-5">
                <i class="icon_set_1_icon-<?= $i ?> mr-5" style="font-size: 2em;"></i>
                <code>icon_set_1_icon-<?= $i ?></code>
            </div>
            <? endfor; ?>
            <? for($i = 118; $i > 101; $i--): ?>
            <div class="col-sm-4 p-5">
                <i class="icon_set_2_icon-<?= $i ?> mr-5" style="font-size: 2em;"></i>
                <code>icon_set_2_icon-<?= $i ?></code>
            </div>
            <? endfor; ?>
        </div>
    </div>
</div>