<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

if($model->isNewRecord)
    $this->context->view->title = 'Добавить данные';
else
    $this->context->view->title = 'Редактировать данные';

//$this->context->smallTitle = '';

$this->context->breadcrumbs[] = [
    'label' => 'Группы данных',
    'url'   => '/'. $this->context->id .'/index'
];

$pageName = $this->context->view->title;
$this->context->breadcrumbs[] = $pageName;

?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title"><?= $pageName ?></h5>
    </div>
    <div class="panel-body">
    <? $form = ActiveForm::begin([
        'id' => $this->context->id . '-form',
        'options' => [
            'role'    => 'form',
            'class' => 'form-horizontal'
        ]
    ]); ?>
        <div class="row">
            <div class="col-sm-12">
                <div class="tabbable">
                    <ul class="nav nav-tabs nav-tabs-bottom">
                        <li class="active">
                            <a href="#<?= Html::getInputId($model, 'basic') ?>" data-toggle="tab">
                                Основные поля
                            </a>
                        </li>
                        <li>
                            <a href="#<?= Html::getInputId($model, 'seo') ?>" data-toggle="tab">
                                СЕО продвижение
                            </a>
                        </li>
                        <? if(!$model->isNewRecord): ?>
                        <li>
                            <a href="#<?= Html::getInputId($model, 'shortcode') ?>" data-toggle="tab">
                                <i class=" icon-embed mr-5 text-size-small"></i> Короткий код
                            </a>
                        </li>
                        <? endif; ?>
                    </ul>
                    <div class="tab-content pt-5">
                        <div class="tab-pane active" id="<?= Html::getInputId($model, 'basic') ?>">
                            <?= $form->field($model, 'group_id', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'select'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->dropDownList($model::getGroupList(), [
                                'prompt' => '--Выберите группу--'
                            ])->label(
                                $model->getAttributeLabelRequired('group_id'),
                                ['class' => 'control-label col-lg-2']
                            )->hint(false) ?>
                            <?= $form->field($model, 'name', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint(false)->label(
                                $model->getAttributeLabelRequired('name'),
                                ['class' => 'control-label col-lg-2']
                            ) ?>

                            <hr>
                            <?= $form->field($model, 'icon_css', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint(false)->label(
                                $model->getAttributeLabelRequired('icon_css'),
                                ['class' => 'control-label col-lg-2']
                            ) ?>

                            <?/*= $form->field($model, 'icon_image', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control file-styled-primary'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint(false)->label(
                                $model->getAttributeLabelRequired('icon_image'),
                                ['class' => 'control-label col-lg-2']
                            )->fileInput() */?>
                            <hr>
                            <div class="col-lg-10 col-lg-offset-2">
                                <div class="form-group">
                                    <?= $form->field($model, 'published', [
                                        'template' => '<label class="checkbox-inline">{input}'. $model->getAttributeLabel('published') .'{hint}</label>',
                                    ])->label(false)->checkbox(['class' => 'styled'], false)->hint('Включит вывод данной записи в группе') ?>
                                </div>
                            </div>
                        </div><!-- /.tab-pane -->
                        <div class="tab-pane" id="<?= Html::getInputId($model, 'seo') ?>">
                            <?= $form->field($model, 'alias', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint('Кириллическая версия имя для использования в ссылках')->label(
                                $model->getAttributeLabelRequired('alias'),
                                ['class' => 'control-label col-lg-2']
                            ) ?>
                            <?= $form->field($model, 'meta_description', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control',
                                    'rows' => '5'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint('Краткое описание до 200 (200 — максимум, ориентируйтесь на ударные первые 100 знаков).')->label(
                                $model->getAttributeLabelRequired('meta_description'),
                                ['class' => 'control-label col-lg-2']
                            ) ?>
                            <?= $form->field($model, 'meta_keywords', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control',
                                    'rows' => '5'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint('Введите через запятую основные ключевые слова для вашего сайта до 250 (250 — максимум, ориентируйтесь на ударные первые 150 знаков).')->label(
                                $model->getAttributeLabelRequired('meta_keywords'),
                                ['class' => 'control-label col-lg-2']
                            )->textarea() ?>
                        </div><!-- /.tab-pane -->
                        <? if(!$model->isNewRecord): ?>
                        <div class="tab-pane" id="<?= Html::getInputId($model, 'shortcode') ?>">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Код ссылки:</label>
                                    <div class="col-lg-10">
                                        <div class="form-control-static">
                                            <code><?= $model->shortcode ?></code>
                                            <p class="help-block">Короткий код который можно установить в редакторе или в шаблон, в результате которого будет выведен список данных.</p>
                                        </div>
                                    </div>
                                </div>
                                <? if($model->group_id === $model::GROUP_TOURS_CAT_ID): ?>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Код ссылки:</label>
                                    <div class="col-lg-10">
                                        <div class="form-control-static">
                                            <code><?= $model->link ?></code>
                                            <p class="help-block">URL текущей категории.</p>
                                        </div>
                                    </div>
                                </div>
                                <? endif ?>
                            </div>
                        </div>
                        <? endif; ?>
                    </div><!-- /.tab-content -->
                </div>
            </div>
        </div><!-- /.row -->
        <hr>
        <div class="text-right">
            <button type="submit" name="btnAction" value="save" class="btn bg-teal-400 btn-sm">
                <i class="icon-checkmark3 position-left"></i> Сохранить
            </button>
            <button type="submit" name="btnAction" value="save-exit" class="btn btn-primary btn-sm">
                <i class="icon-undo2 mr-5"></i> Сохр. и закрыть
            </button>
            <button type="submit" name="btnAction" value="save-new" class="btn btn-primary btn-sm">
                <i class="icon-new-tab mr-5"></i> Сохр. и создать новую запись
            </button>
            <? if(!$model->isNewRecord): ?>
            <?= Html::a('<i class="icon-bin"></i>Удалить', [
                Yii::$app->controller->id . '/'. $model->id .'/remove'
            ], [
                'onclick' => "return confirm('Вы уверены что хотите удалить данные?');",
                'class' => 'btn btn-danger btn-sm'
            ]) ?>
            <? endif; ?>
        </div>
    <? ActiveForm::end(); ?>
    </div><!-- /.panel-body -->
</div>