<?php

use yii\helpers\Html;
use common\models\DataGroup;
use yii\helpers\Url;

$pageName = 'Группы данных';
$this->context->view->title = $pageName;
$this->context->smallTitle = 'Управления группами данных, редактирования удаление, добавления данных в группу.';

$this->context->breadcrumbs[] = $this->context->view->title;

$active_tab = false;

?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title"><?= $pageName ?></h5>
        <small class="display-block text-muted">
            <?= $search ? 'Всего найдено' :  'Всего записей' ?>: <?= $total ?>
        </small>
        <div class="heading-elements">
            <?= Html::beginForm(Url::to([Yii::$app->controller->id . '/index']), 'get', ['class' => 'heading-form']) ?>
                <div class="form-group has-feedback">
                    <?= Html::textInput('search', $search, ['class' => 'form-control input-xs', 'placeholder' => 'Поиск...']) ?>
                    <div class="form-control-feedback">
                        <i class="icon-search4 text-size-base text-muted"></i>
                    </div>
                </div>
            <?= Html::endForm() ?>
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="tabbable">
                    <ul class="nav nav-tabs nav-tabs-bottom">
                        <? foreach(DataGroup::getGroupList() as $group_id => $name): ?>
                        <li <? if(!$active_tab): $active_tab = true; ?>class="active"<? endif; ?>>
                            <a href="#group-<?= $group_id ?>" data-toggle="tab">
                                <?= $name ?>
                            </a>
                        </li>
                        <? endforeach; ?>
                    </ul>
                    <div class="tab-content pt-5">
                        <? foreach(DataGroup::getGroupList() as $group_id => $name): ?>
                        <div class="tab-pane <? if($active_tab): $active_tab = false; ?>active<? endif; ?>" id="group-<?= $group_id ?>">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th width="5%">#ID</th>
                                        <th width="80%">Наименование записи</th>
                                        <th class="text-center">
                                            <a href="#" onclick="return Sorting('/<?= Yii::$app->controller->id ?>/sorting', 'Sorting');" title="" data-popup="tooltip" data-original-title="Сохранить позиции">
                                                <i class="icon-sort text-size-mini text-default"></i>
                                            </a>
                                        </th>
                                        <th class="text-center" data-toggle="true">Публикация</th>
                                        <th class="text-center" style="width: 30px;">
                                            <i class="icon-menu-open2" data-popup="tooltip" title="Действие" data-placement="left"></i>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                <? if(is_array($groups) && array_key_exists($group_id, $groups)): ?>
                                    <? foreach($groups[$group_id] as $item): ?>
                                    <tr>
                                        <td><?= $item->id ?></td>
                                        <td>
                                            <?= Html::a($item->name, [
                                                Yii::$app->controller->id . '/edit?id=' . $item->id
                                            ]) ?>
                                        </td>
                                        <td>
                                            <?= Html::input('text', 'Sorting[' . $item->id . ']', $item->sorting, [
                                                'class' => 'form-control input-xs text-center',
                                                'style' => 'width: 50px; margin: 0 auto'
                                            ]) ?>
                                        </td>
                                        <td class="text-center">
                                            <div class="checkbox checkbox-switchery switchery-xs" title="Вкл/Выкл">
                                                <label>
                                                    <?= Html::checkbox('Published['. $item->id .']', $item->published, [
                                                        'class' => 'switchery',
                                                        'onclick' => "ChangeRequest('" . Url::to([
                                                            Yii::$app->controller->id . '/published',
                                                            'id' => $item->id
                                                        ]) . "', {published: this.checked});"
                                                    ]); ?>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <ul class="icons-list">
                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Действия">
                                                        <i class="icon-menu7"></i>
                                                    </a>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li>
                                                            <?= Html::a('<i class="icon-gear"></i>Редактировать', [
                                                                Yii::$app->controller->id . '/edit?id=' . $item->id
                                                            ]) ?>
                                                        </li>
                                                        <li>
                                                            <?= Html::a('<i class="icon-bin"></i>Удалить', [
                                                                Yii::$app->controller->id . '/remove?id=' . $item->id
                                                            ], [
                                                                'onclick' => "return confirm('Вы уверены что хотите удалить данные?');"
                                                            ]) ?>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul><!-- /.icons-list -->
                                        </td>
                                    </tr>
                                    <? endforeach; ?>
                                <? else: ?>
                                    <tr>
                                        <td colspan="4">
                                            <div class="bg-info alert-styled-left alert fade in">
                                                Для этой группы данных нет.
                                            </div>
                                        </td>
                                    </tr>
                                <? endif; ?>
                                </tbody>
                            </table>
                        </div><!-- /.tab-pane -->
                        <? endforeach; ?>
                    </div><!-- /.tab-content -->
                </div>
            </div>
        </div>
    </div><!-- /.panel-body -->
</div>