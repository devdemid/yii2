<?php

use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\bootstrap\ActiveForm;
use common\models\FormResult;
use common\models\FormField;
use yii\helpers\ArrayHelper;
use common\models\GroupActions;

$this->context->smallTitle = 'Список всех сообщений принятых формой.';

$this->context->breadcrumbs[] = [
    'label' => 'Генератор форм',
    'url'   => '/'. $this->context->id .'/index'
];

$this->context->view->title = $form->name;
$this->context->breadcrumbs[] = $this->context->view->title;

//var_dump($actions_model); exit;

?>
<? if(count($models)): ?>

<?/* Alert::begin(['options' => ['class' => 'bg-info alert-styled-left']]) ?>
    Форма "<?= $form->name ?>" содержит поля для загрузки файлов, <u>рекомендуется обязательно</u> файлы проверить вашей антивирусной программой!
<? Alert::end() */?>

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title"><?= $this->context->view->title ?></h5>
        <small class="display-block text-muted">
            Всего записей: <?= $total ?>
        </small>
        <div class="heading-elements">
            <?= Html::beginForm(Url::to([Yii::$app->controller->id . '/index']), 'get', ['class' => 'heading-form']) ?>
                <div class="form-group has-feedback">
                    <?= Html::hiddenInput('item_id', $form->id)?>
                    <?= Html::textInput('search', $search, ['class' => 'form-control input-xs', 'placeholder' => 'Поиск...']) ?>
                    <div class="form-control-feedback">
                        <i class="icon-search4 text-size-base text-muted"></i>
                    </div>
                </div>
            <?= Html::endForm() ?>
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>
    <div class="panel-body">
        <?= Html::beginForm([Yii::$app->request->url]) ?>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th data-toggle="true" width="5%">
                            <div class="checkbox-inline">
                                <label title="Выбор всех записей">
                                    <?= Html::checkbox(null, null, [
                                        'class' => 'styled',
                                        'onchange' => "checkboxAll(this, 'GroupActions'); return false;"
                                    ]) ?>#ID
                                </label>
                            </div>
                        </th>
                        <th>Данные формы</th>
                        <th></th>
                        <th class="text-center" width="20%">Дата заполнения формы</th>
                        <th class="text-center" width="10%">
                            <i class="icon-menu-open2" data-popup="tooltip" title="Действие" data-placement="left"></i>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <? foreach($models as $item): ?>
                    <tr>
                        <td>
                            <div class="checkbox-inline">
                                <label>
                                    <?= Html::checkbox(
                                        GroupActions::inputName(['selected_id', $item->id]),
                                        $group_actions->isSelected($item->id),
                                        ['class' => 'styled']
                                    ) ?>
                                    <?= $item->id ?>
                                </label>
                            </div>
                        </td>
                        <td>
                            <? if(count($item->results)): ?>
                            <ul class="list-circle">
                                <? foreach($item->results_slice as $result): ?>
                                <li>
                                    <span class="text-muted mr-5"><?= $result['label'] ?>:</span>
                                    <? if($result['type'] == FormField::TYPE_EMAIL): ?>
                                        <a href="mailto:<?= $result['value'] ?>"><?= $result['value'] ?></a>
                                    <? elseif($result['type'] == FormField::TYPE_FILE): ?>
                                        <a href="<?= $item->getLinkAttachment($result['value']) ?>" target="_blank" onclick="return confirm('Рекомендуется обязательно файл проверить вашей антивирусной программой!');">
                                            <i class="icon-attachment2 text-default mr-5"></i><?= $result['value'] ?>
                                        </a>
                                    <? else: ?>
                                        <?= $result['value'] ?>
                                    <? endif; ?>
                                </li>
                                <? endforeach ?>
                            </ul>
                            <? else: ?>
                                <span class="text-muted">Нет данных</span>
                            <? endif; ?>
                        </td>
                        <td class="text-center">
                            <? if(!$item->viewed): ?>
                                <span class="label bg-blue" title="Новая запись" data-popup="tooltip">New</span>
                            <? endif; ?>
                        </td>
                        <td class="text-center">
                            <?= date(FormResult::OUT_DATE_FORMAT, $item->date_timestamp) ?>
                        </td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Действия">
                                        <i class="icon-menu7"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li>
                                            <?= Html::a('<i class="icon-file-text"></i>Детально', [
                                                Url::to([Yii::$app->controller->id . '/result-detail', 'id' => $item->id])
                                            ]) ?>
                                        </li>
                                        <li>
                                            <?= Html::a('<i class="icon-bin"></i>Удалить', [
                                                Url::to([Yii::$app->controller->id . '/remove-result', 'id' => $item->id])
                                            ], [
                                                'onclick' => "return confirm('Вы уверены что хотите удалить данные?');"
                                            ]) ?>
                                        </li>
                                    </ul>
                                </li>
                            </ul><!-- /.icons-list -->
                        </td>
                    </tr>
                    <? endforeach; ?>
                </tbody>
            </table>
            <hr>
            <div class="row">
                <div class="col-sm-5">
                    <?= Html::dropDownList(
                        GroupActions::inputName(['action']),
                        $group_actions->action,
                        $group_actions->getActions(),
                        [
                            'class' => 'select',
                            'prompt' => 'Выберите действие',
                            'id' => 'group-actions_action'
                        ]
                    ); ?>
                </div>
                <div class="col-sm-6">
                    <script>
                        function confirmGroupActionRemove() {
                            if($('#group-actions_action').val() == '<?= GroupActions::ACTION_GROUP_REMOVE ?>') {
                                return confirm('Вы уверены что хотите удалить данные?');
                            }
                            return true;
                        }
                    </script>
                    <button type="submit" onclick="return confirmGroupActionRemove();" class="btn bg-teal-400 btn-sm">
                        <i class="icon-checkmark3 position-left"></i> Выполнить
                    </button>
                </div>
            </div>
        <?= Html::endForm() ?>
    </div><!-- /.panel-body -->
</div>
<div class="mb-20">
    <?= LinkPager::widget([
        'pagination' => $pages,
        'options'    => [
            'class' => 'pagination pagination-xs'
        ]
    ]); ?>
</div>
<? else: Alert::begin(['options' => ['class' => 'bg-info alert-styled-left']]); ?>
    Не удается найти результаты формы для отображения
<? Alert::end(); endif;?>