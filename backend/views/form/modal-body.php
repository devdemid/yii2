<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\FormField;

?>
<? if(!Yii::$app->request->isPost): ?>
<!-- Begin field modal -->
<div id="field-modal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" title="Закрыть">&times;</button>
                <h5 class="modal-title">
                    <? if($model->isNewRecord): ?>
                        Создать новое поле в форме
                    <? else: ?>
                        Редактировать поле в форме
                    <? endif ?>
                </h5>
            </div>
            <? $form = ActiveForm::begin([
                'options' => [
                    'role'    => 'form',
                    'class' => 'form-horizontal',
                    'onsubmit' => "ajaxRequest(this, '#form-field-block', formResponse); return false;"
                ]
            ]); ?>
            <div class="modal-body" id="<?= Yii::$app->controller->id ?>-field-block" style="padding-bottom: 0">
<? else: ?>
<? $form = new ActiveForm;?>
<? endif; ?>

                <!-- Begin form -->
                <div class="form-horizontal">
                    <div class="form-group" id="field-form-inputs">
                        <?= $form->field($model, 'type', [
                            // Begin input theme
                            'template' => '{input}{error}',
                            // End input theme
                            'inputOptions' => [
                                'class' => 'select',
                                'prompt' => 'Выберите тип поля *',
                                'placeholder' => $model->getAttributeLabelRequired('type'),
                                'onchange' => 'changeTypeControl(this.value); clearRelatedInput(); return false;'
                            ],
                            'options' => [
                                'class' => 'col-md-4'
                            ]
                        ])->dropDownList($model::getTypes()) ?>

                        <?= $form->field($model, 'label', [
                            // Begin input theme
                            'template' => '{input}{error}',
                            // End input theme
                            'inputOptions' => [
                                'class' => 'form-control',
                                'placeholder' => $model->getAttributeLabelPlaceholder('label')
                            ],
                            'options' => [
                                'class' => 'col-md-4'
                            ]
                        ])?>

                        <? if($model->isNewRecord): ?>
                            <?= $form->field($model, 'attribute', [
                                // Begin input theme
                                'template' => '{input}{error}',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control',
                                    'placeholder' => $model->getAttributeLabelPlaceholder('attribute')
                                ],
                                'options' => [
                                    'class' => 'col-md-4'
                                ]
                            ])?>
                        <? else: ?>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <?= $model->getAttributeLabelRequired('attribute') ?>
                                    </span>
                                    <?= Html::input('text', null, $model->attribute, [
                                        'class' => 'form-control',
                                        'disabled' => 'disabled'
                                    ])?>
                                </div>
                            </div>
                        <? endif; ?>

                        <?= $form->field($model, 'extension', [
                            // Begin input theme
                            'template' => '{input}{error}',
                            // End input theme
                            'inputOptions' => [
                                'class' => 'form-control',
                                'placeholder' => $model->getAttributeLabelPlaceholder('extension')
                            ],
                            'options' => [
                                'class' => 'col-md-9 mt-10'
                            ]
                        ])?>

                        <?= $form->field($model, 'size', [
                            // Begin input theme
                            'template' => '<div class="input-group">{input}<span class="input-group-addon">Mb</span></div>{error}',
                            // End input theme
                            'inputOptions' => [
                                'class' => 'form-control',
                                'placeholder' => $model->getAttributeLabelPlaceholder('size'),
                                'min' => FormField::DEFAULT_MIN_FILE_SIZE,
                                'max' => FormField::DEFAULT_MAX_FILE_SIZE
                            ],
                            'options' => [
                                'class' => 'col-md-3 mt-10'
                            ]
                        ])?>

                        <?= $form->field($model, 'list', [
                            // Begin input theme
                            'template' => '{input}{hint}{error}',
                            // End input theme
                            'inputOptions' => [
                                'class' => 'form-control',
                                'placeholder' => $model->getAttributeLabelPlaceholder('list')
                            ],
                            'options' => [
                                'class' => 'col-md-12 mt-10'
                            ]
                        ])->hint('Список значений для типов полей: "'. $model->getTypeName($model::TYPE_SELECT) .'" или "' . $model->getTypeName($model::TYPE_RADIO_GROUP) .'". Необходимые значения перечисляются через запятую.') ?>

                        <?= $form->field($model, 'hint', [
                            // Begin input theme
                            'template' => '{input}{hint}{error}',
                            // End input theme
                            'inputOptions' => [
                                'class' => 'form-control',
                                'placeholder' => $model->getAttributeLabelPlaceholder('hint')
                            ],
                            'options' => [
                                'class' => 'col-md-6 mt-10'
                            ]
                        ])->hint('При необходимости можно вывести описание под полем.') ?>

                        <?= $form->field($model, 'input_mask', [
                            // Begin input theme
                            'template' => '{input}{hint}{error}',
                            // End input theme
                            'inputOptions' => [
                                'class' => 'form-control',
                                'placeholder' => $model->getAttributeLabelPlaceholder('input_mask')
                            ],
                            'options' => [
                                'class' => 'col-md-6 mt-10'
                            ]
                        ])->hint('Маска для ввода данных в поле формы. [0-9:/()-%#+$_.,]') ?>

                        <div class="col-lg-10">
                            <div class="form-group">
                                <?= $form->field($model, 'required', [
                                    'template' => '<label class="checkbox-inline">{input}'. $model->getAttributeLabel('required') .'{hint}</label>',
                                ])->label(false)->checkbox(['class' => 'styled'], false)
                                ->hint('При проверке формы это поле будет обязательным!') ?>
                            </div>
                        </div>

                        <div class="col-lg-10">
                            <div class="form-group">
                                <?= $form->field($model, 'unique', [
                                    'template' => '<label class="checkbox-inline">{input}'. $model->getAttributeLabel('unique') .'{hint}</label>',
                                ])->label(false)->checkbox(['class' => 'styled'], false)
                                ->hint('При проверке формы это поле будет проверяться на уникальное значение!') ?>
                            </div>
                        </div>

                        <div class="col-lg-10">
                            <div class="form-group">
                                <?= $form->field($model, 'placeholder', [
                                    'template' => '<label class="checkbox-inline">{input}'. $model->getAttributeLabel('placeholder') .'{hint}</label>',
                                ])->label(false)->checkbox(['class' => 'styled'], false)
                                ->hint('Выводить описание "Метка атрибута" внутри поля.') ?>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- End form -->

<? if(!Yii::$app->request->isPost): ?>
            </div>
            <hr>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">
                    Закрыть окно
                </button>
                <button type="submit" class="btn bg-teal-400 btn-xs">
                    <i class="icon-add position-left"></i> Сохранить поле
                </button>
            </div>
            <? ActiveForm::end(); ?>
        </div>
    </div>
</div>
<!-- End field modal -->
<? endif; ?>
<!-- Begin script -->
<script type="text/javascript">
    // Init control type
    changeTypeControl($('#<?= Html::getInputId($model, 'type') ?>').val());

    /**
     * Close the window after successfully saving the data
     * @param  {object} Response
     */
    function formResponse(Response) {
        if(Response.exit) {
            $('.modal').modal('hide');
            window.location.reload();
        }
    }
</script>
<!-- End script-->