<?php

use yii\helpers\Html;
use common\models\FormField;
use yii\bootstrap\Alert;

// Model name for identifiers
$modelName = mb_strtolower(basename(FormField::className()));

?>
<div class="table-responsive mb-20">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Метка атрибута</th>
                <th>Тип поля</th>
                <th class="text-center">Параметр</th>
                <th class="text-center">Уникальное значение</th>
                <th class="text-center">
                    <a href="#" onclick="return Sorting('/<?= Yii::$app->controller->id ?>/field-sorting', 'Sorting');" title="Сохранить позиции" data-popup="tooltip">
                        <i class="icon-sort text-size-mini text-default"></i>
                    </a>
                </th>
                <th></th>
            </tr>
        </thead>
        <tbody id="field-list">
        <? if(count($model->fields)): ?>
            <? foreach($model->fields as $item): ?>
            <tr id="field-<?= $item->id ?>">
                <td>
                    <?= $item->label ?>
                    <? if($item->required): ?>
                        <span class="text-danger" data-popup="tooltip" title="Обязательное поле">*</span>
                    <? endif ?>
                    <? if($item->input_mask): ?>
                        <span class="label label-success ml-5" data-popup="tooltip" title="Включена маска ввода">
                            Input Mask
                        </span>
                    <? endif; ?>
                </td>
                <td>
                    <?= $item::getTypeName($item->type) ?>
                </td>
                <td class="text-center">
                    <code style="display: block">{<?= $item->attribute ?>}</code>
                </td>
                <td class="text-center">
                    <? if($item->unique): ?>
                        <i class="icon-checkmark3 text-success" title="Да"></i>
                    <? else: ?>
                        <i class="icon-cross2 text-danger" title="Нет"></i>
                    <? endif; ?>
                </td>
                <td>
                    <?= Html::input('text', 'Sorting[' . $item->id . ']', $item->sorting, [
                        'class' => 'form-control input-xs text-center',
                        'style' => 'width: 50px; margin: 0 auto'
                    ]) ?>
                </td>
                <td class="text-center">
                    <a href="#" class="mr-5" onclick="return itemField({item_id: <?= $item->id ?>});" title="Редактировать поле">
                        <i class="icon-pencil5 text-primary"></i>
                    </a>
                    <a href="/<?= Yii::$app->controller->id ?>/remove-field?id=<?= $item->id ?>" class="ml-5" onclick="return confirm('Вы уверены что хотите удалить поле?');" data-popup="tooltip" title="Удалить поле">
                        <i class="icon-trash text-danger"></i>
                    </a>
                </td>
            </tr>
            <? endforeach; ?>
        <? else: ?>
            <tr>
                <td colspan="6">
                    <? Alert::begin([
                        'options' => ['class' => 'bg-info alert-styled-left no-margin'],
                        'closeButton' => false
                    ]); ?>
                        Форма не содержит полей!
                    <? Alert::end(); ?>
                </td>
            </tr>
        <? endif ?>
        </tbody>
    </table>
</div>
<? if(!$model->isNewRecord): ?>
<hr>
<div class="mt-20">
    <button type="button" class="btn btn-default btn-xs" onclick="return itemField({id: <?= $model->id ?>});">
        <i class="icon-plus3 position-left"></i> Добавить новое поле
    </button>
</div>
<? endif ?>
<!-- Begin script -->
<script type="text/javascript">
    /**
     * Add or Edit filed
     * @param [integer] params
     * @return boolean
     */
    function itemField(params) {
        $.get('/<?= Yii::$app->controller->id ?>/field', params, function(Response) {
            var $modalContainer = $('#ajax-modal-container');
            $modalContainer.html(Response.body)
                .removeClass('hidden')
                .find('#field-modal')
                .modal('show');
        }, 'json');
        return false;
    }

    /**
     * Field control by type
     * @param [string] type
     */
    function changeTypeControl(type) {

        // Field knitted from a list
        var isList = $.inArray(type, [
            '<?= FormField::TYPE_SELECT ?>',
            '<?= FormField::TYPE_RADIO_GROUP ?>'
        ]) == -1 ? true : false;

        $('#<?= $modelName ?>-list')
            .attr('disabled', isList);

        // Field knitted from a file
        var isFile = (type != '<?= FormField::TYPE_FILE ?>' ? true : false);
        $('#<?= $modelName ?>-extension, #<?= $modelName ?>-size')
            .attr('disabled', isFile);

        // Allow input mask
        var isInputMask = $.inArray(type, [
            '<?= FormField::TYPE_EMAIL ?>',
            '<?= FormField::TYPE_INTEGER ?>',
            '<?= FormField::TYPE_PHONE ?>',
            '<?= FormField::TYPE_STRING ?>'
        ]) == -1 ? true : false;

        $('#<?= $modelName ?>-input_mask')
            .attr('disabled', isInputMask);
    }

    /**
     * Clear associated fields
     */
    function clearRelatedInput() {
        $('#<?= $modelName ?>-input_mask,#<?= $modelName ?>-extension,#<?= $modelName ?>-size,#<?= $modelName ?>-list').val('');
    }
</script>
<!-- End script-->