<?
use yii\helpers\Html;
use common\models\FormField;

$this->context->smallTitle = 'Детальные данные установленных полей, которые были заполнены и приняты в формой.';

$this->context->breadcrumbs[] = [
    'label' => 'Генератор форм',
    'url'   => '/'. $this->context->id .'/index'
];

$this->context->breadcrumbs[] = [
    'label' => $model->form->name,
    'url'   => '/'. $this->context->id .'/results?id=' . $model->item_id
];

$this->context->view->title =  $model->form->name . ' #ID ' . $model->id;
$this->context->breadcrumbs[] = $this->context->view->title;
$pageName = $model->form->name;

?>
<? if($model):?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title"><?= $pageName ?></h5>
        <div class="heading-elements">
            <a href="#" onclick="print(); return false;" class="btn btn-default btn-xs">
                <i class="icon-printer mr-5"></i> Печать
            </a>
        </div>
    </div>
    <div class="panel-body"></div>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th width="25%">Метка атрибута</th>
                    <th>Значение поля</th>
                </tr>
            </thead>
            <tbody>
            <? foreach($model->results as $item): ?>
                <tr>
                    <td><?= $item['label'] ?>:</td>
                    <td>
                        <? if($item['type'] == FormField::TYPE_EMAIL): ?>
                            <a href="mailto:<?= $item['value'] ?>"><?= $item['value'] ?></a>
                        <? elseif($item['type'] == FormField::TYPE_FILE): ?>
                            <a href="<?= $model->getLinkAttachment($item['value']) ?>" target="_blank" class="btn btn-xs bg-teal" onclick="return confirm('Рекомендуется обязательно файл проверить вашей антивирусной программой!');">
                                <i class="icon-attachment2 mr-5"></i><?= $item['value'] ?>
                            </a>
                        <? else: ?>
                            <?= $item['value'] ?>
                        <? endif; ?>
                    </td>
                </tr>
            <? endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <td>Форма заполнена:</td>
                    <td>
                        <?= date($model::OUT_DATE_FORMAT, $model->date_timestamp) ?>
                    </td>
                </tr>
                <tr>
                    <td> Получатель(и):</td>
                    <td><?= $model->to_email ?></td>
                </tr>
            </tfoot>
        </table>
    </div>
    <hr>
    <div class="text-right m-20">
        <? if(!$model->isNewRecord): ?>
        <?= Html::a('<i class="icon-bin"></i>Удалить', [
            Yii::$app->controller->id .'/remove-result?id=' .  $model->id
        ], [
            'onclick' => "return confirm('Вы уверены что хотите удалить данные?');",
            'class' => 'btn btn-danger btn-sm'
        ]) ?>
        <? endif; ?>
    </div>
</div>
<? endif; ?>