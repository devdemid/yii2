<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\Form;

if($model->isNewRecord)
    $this->context->view->title = 'Добавить форму';
else
    $this->context->view->title = 'Настроить форму';

$this->context->smallTitle = 'Этот модуль позволяет гибко настраивать отображаемые поля, поведения формы, шаблоны сообщений...';

$this->context->breadcrumbs[] = [
    'label' => 'Генератор форм',
    'url'   => '/'. $this->context->id .'/index'
];

$pageName = $this->context->view->title;
$this->context->breadcrumbs[] = $pageName;

?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title"><?= $pageName ?></h5>
    </div>
    <div class="panel-body">
    <? $form = ActiveForm::begin([
        'id' => $this->context->id . '-form',
        'options' => [
            'role'    => 'form',
            'class' => 'form-horizontal'
        ]
    ]); ?>
        <div class="row">
            <div class="col-sm-12">
                <div class="tabbable">
                    <ul class="nav nav-tabs nav-tabs-bottom">
                        <li class="active">
                            <a href="#<?= Html::getInputId($model, 'basic') ?>" data-toggle="tab">
                                Основные поля
                            </a>
                        </li>
                        <li>
                            <a href="#<?= Html::getInputId($model, 'model_form') ?>" data-toggle="tab">
                                Модель формы
                            </a>
                        </li>
                        <li>
                            <a href="#<?= Html::getInputId($model, 'buttons') ?>" data-toggle="tab">
                                Кнопки
                            </a>
                        </li>
                        <li>
                            <a href="#<?= Html::getInputId($model, 'actions') ?>" data-toggle="tab">
                                Действия формы
                            </a>
                        </li>
                        <li>
                            <a href="#<?= Html::getInputId($model, 'template') ?>" data-toggle="tab">
                                Шаблоны
                            </a>
                        </li>
                        <li>
                            <a href="#<?= Html::getInputId($model, 'help') ?>" data-toggle="tab">
                                Переменные
                            </a>
                        </li>
                        <? if(!$model->isNewRecord): ?>
                        <li>
                            <a href="#<?= Html::getInputId($model, 'shortcode') ?>" data-toggle="tab">
                                <i class=" icon-embed mr-5 text-size-small"></i> Короткий код
                            </a>
                        </li>
                        <? endif; ?>
                    </ul>
                    <div class="tab-content pt-5">
                        <div class="tab-pane active" id="<?= Html::getInputId($model, 'basic') ?>">
                            <?= $form->field($model, 'name', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint($model->getAttributeLabel('name') . ' которое будет использоваться в заголовке блока формы на страницах ресурса.')->label(
                                $model->getAttributeLabelRequired('name'),
                                ['class' => 'control-label col-lg-2']
                            ) ?>

                            <?= $form->field($model, 'email', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint('На указаный e-mail (можно перечислить несколько через запятую) будет выслано уведомление о заполнении формы на сайте.')->label(
                                $model->getAttributeLabelRequired('email'),
                                ['class' => 'control-label col-lg-2']
                            ) ?>

                            <hr>
                            <div class="col-lg-10 col-lg-offset-2">
                                <div class="form-group">
                                    <?= $form->field($model, 'is_save_result', [
                                        'template' => '<label class="checkbox-inline">{input}'. $model->getAttributeLabel('is_save_result') .'{hint}</label>',
                                    ])->label(false)->checkbox(['class' => 'styled'], false)->hint('Сохранения принятых данных в базу.<br> Если в шаблонах используется переменная <strong>{id}</strong> при выключеном сохранении то она не будет иметь идентификатора.') ?>
                                </div>
                            </div>
                            <div class="col-lg-10 col-lg-offset-2">
                                <div class="form-group">
                                    <?= $form->field($model, 'captcha', [
                                        'template' => '<label class="checkbox-inline">{input}'. $model->getAttributeLabel('captcha') .'{hint}</label>',
                                    ])->label(false)->checkbox(['class' => 'styled'], false)->hint('Позволит предотвратить множественные автоматические отправления сообщений программами-роботами.') ?>
                                </div>
                            </div>
                            <div class="col-lg-10 col-lg-offset-2">
                                <div class="form-group">
                                    <?= $form->field($model, 'published', [
                                        'template' => '<label class="checkbox-inline">{input}'. $model->getAttributeLabel('published') .'{hint}</label>',
                                    ])->label(false)->checkbox(['class' => 'styled'], false)->hint('Включения функционала формы для приема данных.') ?>
                                </div>
                            </div>
                        </div><!-- /.tab-pane -->
                        <div class="tab-pane" id="<?= Html::getInputId($model, 'model_form') ?>">
                            <?= $this->context->renderPartial('field-model', [
                                'model' => $model
                            ])?>
                        </div><!-- /.tab-pane -->
                        <div class="tab-pane" id="<?= Html::getInputId($model, 'buttons') ?>">
                            <?= $form->field($model, 'btn_name', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control',
                                    'placeholder' => Form::DEFAULT_BTN_NAME
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint('Текст внутри кнопки отправки данных, по умолчанию "'. Form::DEFAULT_BTN_NAME .'"')->label(
                                $model->getAttributeLabelRequired('btn_name'),
                                ['class' => 'control-label col-lg-2']
                            ) ?>

                            <div class="col-lg-10 col-lg-offset-2">
                                <div class="form-group">
                                    <?= $form->field($model, 'btn_reset', [
                                        'template' => '<label class="checkbox-inline">{input}'. $model->getAttributeLabel('btn_reset') .'{hint}</label>',
                                    ])->label(false)->checkbox(['class' => 'styled'], false)->hint('Кнопка для очистки введенных данных формы и возвращения значений в первоначальное состояние.') ?>
                                </div>
                            </div>
                        </div><!-- /.tab-pane -->
                        <div class="tab-pane" id="<?= Html::getInputId($model, 'actions') ?>">
                            <?= $form->field($model, 'return_type', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'select'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->dropDownList($model::getReturnTypes())->label(
                                $model->getAttributeLabelRequired('return_type'),
                                ['class' => 'control-label col-lg-2']
                            )->hint('После проверки формы скрипт выводит текстовое сообщения об успешной обработке данных.') ?>

                            <?= $form->field($model, 'run_method', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10"><div class="input-group"><span class="input-group-addon">FormRunMethod::</span>{input}</div>{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control',
                                    'placeholder' => '\common\components\FormRunMethod run static method'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint("Будет выполнен указанный метод, результаты которого выводится в форме.")->label(
                                $model->getAttributeLabelRequired('run_method'),
                                ['class' => 'control-label col-lg-2']
                            ) ?>

                            <?= $form->field($model, 'success_position', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'select'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->dropDownList($model::getSuccesPosition())->label(
                                $model->getAttributeLabelRequired('success_position'),
                                ['class' => 'control-label col-lg-2']
                            )->hint('После проверки формы скрипт выводит текстовое сообщения об успешной обработке данных.') ?>

                            <?= $form->field($model, 'success_text', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control',
                                    'rows' => '10',
                                    'placeholder' => 'Спасибо. Ваши данные успешно отправлены!'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint('Текстовый или html ответ после успешной проверки данных формы.')->label(
                                $model->getAttributeLabelRequired('success_text'),
                                ['class' => 'control-label col-lg-2']
                            )->textarea() ?>
                        </div><!-- /.tab-pane -->
                        <div class="tab-pane" id="<?= Html::getInputId($model, 'template') ?>">
                            <?= $form->field($model, 'format_mail', [
                                // Begin input theme
                                'template' => '<div class="col-lg-12">{label}{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'select'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->dropDownList($model::getFormatMessage())->hint('Все сообщения этой формы будут отправлены в выбранном формате plain/text или text/html.')->label(
                                $model->getAttributeLabelRequired('format_mail'),
                                ['class' => 'control-label']
                            ) ?>

                            <div class="col-lg-12">
                                <div class="form-group">
                                    <?= $form->field($model, 'is_to_message', [
                                        'template' => '<label class="checkbox-inline">{input}'. $model->getAttributeLabel('is_to_message') .'{hint}</label>',
                                    ])->label(false)->checkbox(['class' => 'styled'], false)->hint('Отправить данные формы на email адрес получателя.') ?>
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-group">
                                    <?= $form->field($model, 'is_add_attachments', [
                                        'template' => '<label class="checkbox-inline">{input}'. $model->getAttributeLabel('is_add_attachments') .'{hint}</label>',
                                    ])->label(false)->checkbox(['class' => 'styled'], false)->hint('Если форма содержит поля для загрузки файлов то эти файлы могут быть прикреплены к письму получателя.') ?>
                                </div>
                            </div>

                            <?= $form->field($model, 'to_view', [
                                // Begin input theme
                                'template' => '<div class="col-lg-12">{label}{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control',
                                    'rows' => '20'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint('Содержимое e-mail сообщения которое будет отправлено в качестве уведомления на "'. $model->getAttributeLabel('email') .'".')->label(
                                $model->getAttributeLabelRequired('to_view'),
                                ['class' => 'control-label']
                            )->textarea() ?>

                            <div class="col-lg-12">
                                <div class="form-group">
                                    <?= $form->field($model, 'is_reply_message', [
                                        'template' => '<label class="checkbox-inline">{input}'. $model->getAttributeLabel('is_reply_message') .'{hint}</label>',
                                    ])->label(false)->checkbox(['class' => 'styled'], false)->hint('Система отправит ответное сообщение на email адресата.') ?>
                                </div>
                            </div>

                            <?= $form->field($model, 'from_view', [
                                // Begin input theme
                                'template' => '<div class="col-lg-12">{label}{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control',
                                    'rows' => '20'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint('Содержимое e-mail сообщения которое будет отправлено в качестве ответа системы на e-mail отправителя.')->label(
                                $model->getAttributeLabelRequired('from_view'),
                                ['class' => 'control-label']
                            )->textarea() ?>

                        </div><!-- /.tab-pane -->
                        <div class="tab-pane" id="<?= Html::getInputId($model, 'help') ?>">
                            <? if(count($model->fields)): ?>
                             <table class="table table-sm table-hover">
                                <thead>
                                    <tr>
                                        <th colspan="2">Переменные формы:</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <? foreach($model->fields as $item): ?>
                                    <tr id="variable-<?= $item->id ?>">
                                        <td style="width: 20%"><code>{<?= $item->attribute ?>}</code></td>
                                        <td style="width: 80%">
                                            <?= $item->label ?>
                                            <? if($item->required): ?>
                                                <span class="text-danger">*</span>
                                            <? endif ?>
                                            <? if($item->input_mask): ?>
                                                <span class="label label-success ml-5" data-popup="tooltip" title="Включена маска ввода">
                                                    Input Mask
                                                </span>
                                            <? endif; ?>
                                        </td>
                                    </tr>
                                    <? endforeach; ?>
                                </tbody>
                            </table>
                            <br>
                            <? endif; ?>
                            <table class="table table-sm table-hover">
                                <thead>
                                    <tr>
                                        <th colspan="2">Системные переменные:</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <? if($model->is_save_result): ?>
                                    <tr>
                                        <td style="width: 20%"><code>{id}</code></td>
                                        <td style="width: 80%">
                                            Идентификатор записи в базе данных
                                        </td>
                                    </tr>
                                    <? endif ?>
                                    <? foreach($model->systemVariables as $variable => $name): ?>
                                    <tr>
                                        <td style="width: 20%"><code>{<?= $variable ?>}</code></td>
                                        <td style="width: 80%"><?= $name ?></td>
                                    </tr>
                                    <? endforeach ?>
                                </tbody>
                            </table>
                            <br>
                            <table class="table table-sm table-hover">
                                <thead>
                                    <tr>
                                        <th colspan="2">Управляющие символы:</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="width: 20%"><code>\r\n</code></td>
                                        <td style="width: 80%">Символы конца строки</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%"><code>&lt;br&gt;</code></td>
                                        <td style="width: 80%">Устанавливает перевод строки в том месте, где этот тег находится.</td>
                                    </tr>
                                </tbody>
                            </table>
                            <br>
                            <blockquote class="no-margin">
                                <em>Переменные доступны для использования в шаблонах и сообщение ответа, все переменные будут заменены на соответствующие данные.</em>
                            </blockquote>
                        </div><!-- /.tab-pane -->
                        <? if(!$model->isNewRecord): ?>
                        <div class="tab-pane" id="<?= Html::getInputId($model, 'shortcode') ?>">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Короткий код формы:</label>
                                    <div class="col-lg-10">
                                        <div class="form-control-static">
                                            <code><?= $model->shortcode ?></code>
                                            <p class="help-block">Короткий код который можно установить в редакторе или в шаблон, в место которого будет выведена форма.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <? endif; ?>
                    </div><!-- /.tab-content -->
                </div>
            </div>
        </div><!-- /.row -->
        <hr>
        <div class="text-right">
            <button type="submit" name="btnAction" value="save" class="btn bg-teal-400 btn-sm">
                <i class="icon-checkmark3 position-left"></i> Сохранить
            </button>
            <button type="submit" name="btnAction" value="save-exit" class="btn btn-primary btn-sm">
                <i class="icon-undo2 mr-5"></i> Сохр. и закрыть
            </button>
            <button type="submit" name="btnAction" value="save-new" class="btn btn-primary btn-sm">
                <i class="icon-new-tab mr-5"></i> Сохр. и создать новую запись
            </button>
            <? if(!$model->isNewRecord): ?>
            <?= Html::a('<i class="icon-bin"></i>Удалить', [
                Yii::$app->controller->id . '/'. $model->id .'/remove'
            ], [
                'onclick' => "return confirm('Вы уверены что хотите удалить данные?');",
                'class' => 'btn btn-danger btn-sm'
            ]) ?>
            <? endif; ?>
        </div>
    <? ActiveForm::end(); ?>
    </div><!-- /.panel-body -->
</div>
<!-- Begin field modal -->
<div id="ajax-modal-container" class="hidden"></div>
<!-- End field modal -->