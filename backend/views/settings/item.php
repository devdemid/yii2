<?php

use yii\bootstrap\ActiveForm;
use common\models\Settings;

// Set the component description
$this->context->view->title = 'Добавить новый параметр';
$this->context->breadcrumbs[] = ['label' => 'Настройка системы', 'url' => '/settings/index'];
$this->context->breadcrumbs[] = $this->context->view->title;
$this->context->smallTitle = 'Добавление параметра в раздел `Настройка системы`';

?>
<div class="panel panel-body">
    <?php $form = ActiveForm::begin([
        'id' => 'settings-form',
        'options' => [
            'class' => 'form-horizontal'
        ]
    ]); ?>
    <fieldset class="content-group">
        <legend class="text-bold">Новый параметр (Dev)</legend>
        <?= $form->field($settings, 'group', [
            'template' => '{label}<div class="col-lg-9">{input}{error}</div>',
            'inputOptions' => [
                'class'        => 'form-control',
                'autocomplete' => 'off',
                'placeholder'  => 'Введите имя группы параметров. Например: Основные',
            ],
            'options' => [
                'class' => 'form-group'
            ],
            'labelOptions' => [
                'class' => 'control-label col-lg-3'
            ]
        ])->label(
            $settings->getAttributeLabel('group') .
            ($settings->isAttributeRequired('group') ? ' <span class="text-danger">*</span>:' : ':')
        )->hint(false) ?>

        <?= $form->field($settings, 'param', [
            'template' => '{label}<div class="col-lg-9">{input}{error}</div>',
            'inputOptions' => [
                'class'        => 'form-control',
                'autocomplete' => 'off',
                'placeholder'  => 'Введите уникальный ключ параметра. Например: email',
            ],
            'options' => [
                'class' => 'form-group'
            ],
            'labelOptions' => [
                'class' => 'control-label col-lg-3'
            ]
        ])->label(
            $settings->getAttributeLabel('param') .
            ($settings->isAttributeRequired('param') ? ' <span class="text-danger">*</span>:' : ':')
        )->hint(false) ?>

        <?= $form->field($settings, 'defaultValue', [
            'template' => '{label}<div class="col-lg-9">{input}{error}</div>',
            'labelOptions' => [
                'class' => 'control-label col-lg-3'
            ]
        ])->textArea([
            'class' => 'form-control',
            'rows' => 5,
            'cols' => 5
        ])->label($settings->getAttributeLabel('defaultValue') . ':')->hint(false) ?>

        <?= $form->field($settings, 'mask', [
            'template' => '{label}<div class="col-lg-9"><div class="input-group">{input}<span class="input-group-addon"><a href="https://igorescobar.github.io/jQuery-Mask-Plugin/" target="_blank" class="icon-info22" data-popup="tooltip" data-placement="left" title="Документация jQuery Mask"></a></span></div>{error}</div>',
            'inputOptions' => [
                'class'        => 'form-control',
                'autocomplete' => 'off',
                'placeholder'  => '99/99/9999',
            ],
            'options' => [
                'class' => 'form-group'
            ],
            'labelOptions' => [
                'class' => 'control-label col-lg-3'
            ]
        ])->label(
            $settings->getAttributeLabel('mask') .
            ($settings->isAttributeRequired('mask') ? ' <span class="text-danger">*</span>:' : ':')
        )->hint(false) ?>


        <?= $form->field($settings, 'label', [
            'template' => '{label}<div class="col-lg-9">{input}{error}</div>',
            'inputOptions' => [
                'class'        => 'form-control',
                'autocomplete' => 'off',
                'placeholder'  => '',
            ],
            'options' => [
                'class' => 'form-group'
            ],
            'labelOptions' => [
                'class' => 'control-label col-lg-3'
            ]
        ])->label(
            $settings->getAttributeLabel('label') .
            ($settings->isAttributeRequired('label') ? ' <span class="text-danger">*</span>:' : ':')
        )->hint(false) ?>

        <?= $form->field($settings, 'description', [
            'template' => '{label}<div class="col-lg-9">{input}{error}</div>',
            'labelOptions' => [
                'class' => 'control-label col-lg-3'
            ]
        ])->textArea([
            'class' => 'form-control',
            'rows' => 5,
            'cols' => 5
        ])->label(
            $settings->getAttributeLabel('description') .
            ($settings->isAttributeRequired('description') ? ' <span class="text-danger">*</span>:' : ':')
        )->hint(false) ?>

        <?= $form->field($settings, 'placeholder', [
            'template' => '{label}<div class="col-lg-9">{input}{error}</div>',
            'inputOptions' => [
                'class'        => 'form-control',
                'autocomplete' => 'off',
                'placeholder'  => '',
            ],
            'options' => [
                'class' => 'form-group'
            ],
            'labelOptions' => [
                'class' => 'control-label col-lg-3'
            ]
        ])->label(
            $settings->getAttributeLabel('placeholder') .
            ($settings->isAttributeRequired('placeholder') ? ' <span class="text-danger">*</span>:' : ':')
        )->hint(false) ?>

        <?= $form->field($settings, 'type', [
            'template' => '{label}<div class="col-lg-9">{input}{error}</div>',
            'labelOptions' => [
                'class' => 'control-label col-lg-3'
            ]
            ])->dropDownList(Settings::getTypes(), [
                'class' => 'select',
                'prompt' => '',
                'data-placeholder' => '-- Выберите тип параметра --'
        ])->label(
            $settings->getAttributeLabel('type') .
            ($settings->isAttributeRequired('type') ? ' <span class="text-danger">*</span>:' : ':')
        ) ?>

            <?= $form->field($settings, 'run_method', [
            'template' => '{label}<div class="col-lg-9">{input}<p class="help-block">\common\components\ParamHelper run static method for return array to settings</p>{error}</div>',
            'inputOptions' => [
                'class'        => 'form-control',
                'autocomplete' => 'off',
                'placeholder'  => 'test',
            ],
            'options' => [
                'class' => 'form-group'
            ],
            'labelOptions' => [
                'class' => 'control-label col-lg-3'
            ]
        ])->label(
            $settings->getAttributeLabel('run_method') .
            ($settings->isAttributeRequired('run_method') ? ' <span class="text-danger">*</span>:' : ':')
        )->hint(false) ?>

        <?= $form->field($settings, 'disabled', [
            'template' => '<div class="col-lg-offset-3 col-lg-9"><label class="checkbox-inline">{input}'. $settings->getAttributeLabel('disabled') .'? </label><p class="help-block">Блокирует доступ и изменение поля формы. Оно в таком случае отображается серым и недоступным для активации пользователем</p></div>',
            'options' => [
                'class' => 'form-group'
            ],
        ])->label(false)->checkbox(['class' => 'styled'], false) ?>

        <?= $form->field($settings, 'fixed', [
            'template' => '<div class="col-lg-offset-3 col-lg-9"> <label class="checkbox-inline">{input}'. $settings->getAttributeLabel('fixed') .'? </label></div>',
            'options' => [
                'class' => 'form-group'
            ],
        ])->label(false)->checkbox(['class' => 'styled'], false) ?>

        <div class="text-right">
            <button type="submit" name="btnAction" value="save" class="btn bg-teal-400 btn-sm">
                <i class="icon-checkmark3 position-left"></i> Сохранить
            </button>
            <button type="submit" name="btnAction" value="save-exit" class="btn btn-primary btn-sm">
                <i class="icon-undo2 mr-5"></i> Сохр. и закрыть
            </button>
            <button type="reset" class="btn btn-default">
                <i class="icon-reload-alt mr-5"></i> Отмена изменений
            </button>
        </div>
    </fieldset>
    <?php ActiveForm::end(); ?>
</div>