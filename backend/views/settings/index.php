<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\Settings;
use common\models\Files;
use yii\helpers\ArrayHelper;
use \yii\helpers\StringHelper;
use yii\bootstrap\Tabs;
use backend\widgets\FilesWidget;

// Set the component description
$this->context->view->title = 'Настройка системы';
$this->context->breadcrumbs[] = $this->context->view->title;
$this->context->smallTitle = 'Настройка параметров скрипта (используйте навигацию для доступа к разделам)';

// Private variable
$_counter = 0;

?>
<div class="panel panel-body">
<?php if(count($settings)): ?>
    <?php $form = ActiveForm::begin([
        'id' => 'settings-form',
        'options' => [
            'enctype' => 'multipart/form-data'
        ]
    ]); ?>
        <div class="tabbable">
            <ul class="nav nav-tabs nav-tabs-bottom">
            <?php foreach($settings as $name => $item): ?><!-- class="active" -->
                <?php if($_counter++ < 5): ?>
                <li class="<?= !$_counter ? 'active' : false ?>">
                    <a href="#setting-<?= md5($name) ?>" data-toggle="tab">
                        <?= Html::encode($name) ?>
                    </a>
                </li>
                <?php else: ?>
                    <?php if($_counter == 6): ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            Еще... <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                    <?php endif; ?>
                            <li>
                                <a href="#setting-<?= md5($name) ?>" data-toggle="tab">
                                    <?= Html::encode($name) ?>
                                </a>
                            </li>
                    <?php if($_counter == count($settings)): ?>
                        </ul>
                    </li>
                    <?php endif; ?>
                <?php endif; ?>
            <?php endforeach; ?>
            </ul>

            <div class="tab-content pt-5">
            <?php foreach($settings as $name => $setting): ?>
                <div class="tab-pane" id="setting-<?= md5($name) ?>">
                    <?php foreach($setting as $item): ?>

                        <?php if($item->type == Settings::TYPE_STRING): ?>
                            <div class="form-group mt-20 mb-20">
                                <label for="Settings_<?= $item->type ?>_<?= $item->id ?>">
                                    <?= $item->label ?>:
                                </label>
                                <?= Html::textInput('Settings['. $item->id .']', $item->value, ArrayHelper::merge(
                                    [
                                        'id' => 'Settings_' . $item->type . '_' . $item->id,
                                        'class' => 'form-control',
                                        'placeholder' => $item->placeholder,
                                    ], ($item->disabled ? ['disabled' => 'disabled'] : []), ($item->mask ? ['data-mask' => $item->mask] : [])
                                )); ?>
                                <p class="help-block"><?= $item->description ?></p>
                            </div><!-- /.form-group -->
                        <?php endif; ?>

                        <?php if($item->type == Settings::TYPE_TEXTAREA): ?>
                            <div class="form-group mt-20 mb-20">
                                <label for="Settings_<?= $item->type ?>_<?= $item->id ?>">
                                    <?= $item->label ?>:
                                </label>
                                <?= Html::textarea('Settings['. $item->id .']', $item->value, ArrayHelper::merge(
                                    [
                                        'id' => 'Settings_' . $item->type . '_' . $item->id,
                                        'class' => 'form-control',
                                        'placeholder' => $item->placeholder,
                                        'rows' => 5
                                    ], ($item->disabled ? ['disabled' => 'disabled'] : [])
                                )); ?>
                                <p class="help-block"><?= $item->description ?></p>
                            </div><!-- /.form-group -->
                        <?php endif; ?>

                        <?php if($item->type == Settings::TYPE_CHECKBOX): ?>
                            <div class="form-group mt-20 mb-20">
                                <?= Html::hiddenInput('Settings['. $item->id .']', 0); ?>
                                <div class="checkbox-inline">
                                    <label>
                                        <?= Html::checkbox('Settings['. $item->id .']', $item->value, ArrayHelper::merge(
                                            ['class' => 'styled'],
                                            ($item->disabled ? ['disabled' => 'disabled'] : [])
                                        )); ?>
                                        <?= $item->label ?>
                                    </label>
                                </div>
                                <p class="help-block"><?= $item->description ?></p>
                            </div><!-- /.form-group -->
                        <?php endif; ?>

                        <?php if($item->type == Settings::TYPE_SWITCHER): ?>
                            <div class="form-group mt-20 mb-20">
                                <div class="checkbox checkbox-switchery switchery-xs">
                                    <label>
                                        <?= Html::hiddenInput('Settings['. $item->id .']', 0); ?>
                                        <?= Html::checkbox('Settings['. $item->id .']', $item->value, ArrayHelper::merge(
                                            ['class' => 'switchery'],
                                            ($item->disabled ? ['disabled' => 'disabled'] : [])
                                        )); ?>
                                        <?= $item->label ?>
                                    </label>
                                </div>
                                <p class="help-block"><?= $item->description ?></p>
                            </div><!-- /.form-group -->
                        <?php endif; ?>

                        <?php if($item->type == Settings::TYPE_RADIO_GROUP): ?>
                            <div class="form-group mt-20 mb-20">
                                <label for="Settings_<?= $item->type ?>_<?= $item->id ?>">
                                    <?= $item->label ?>:
                                </label>
                                <?= Html::radioList('Settings['. $item->id .']', $item->value, $item->multiSelection,
                                    [
                                        'tag' => 'div',
                                        'item' => function($index, $label, $name, $checked, $value) {
                                            return '<label class="radio-inline">
                                                <input type="radio" name="'. $name .'" class="styled" value="'. $value .'" '. ($checked ? 'checked="checked" ' : false) . ($item->disabled ? 'disabled="disabled"' : false) .'>
                                                '. $label .'
                                            </label>';
                                        },
                                    ]
                                ); ?>
                                <p class="help-block"><?= $item->description ?></p>
                            </div><!-- /.form-group -->
                        <?php endif; ?>

                        <?php if($item->type == Settings::TYPE_EDITOR): ?>
                            <div class="form-group mt-20 mb-20">
                                <label for="Settings_<?= $item->type ?>_<?= $item->id ?>">
                                    <?= $item->label ?>:
                                </label>
                                <?= yii\imperavi\Widget::widget(ArrayHelper::merge(
                                    [
                                        'attribute' => 'Settings['. $item->id .']',
                                        'value' => $item->value
                                    ],
                                    Yii::$app->params['imperavi'], [
                                        'options' => [
                                            'minHeight' => '250px',
                                            'placeholder' => $item->placeholder,
                                            'uploadImageFields' => [
                                                Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
                                                'Files[item_id]' => $item->id,
                                                'Files[module]' => StringHelper::basename(Settings::className()),
                                                'Files[type]' => Files::TYPE_IMAGE
                                            ],
                                            'uploadFileFields' => [
                                                Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
                                                'Files[item_id]' => $item->id,
                                                'Files[module]' => StringHelper::basename(Settings::className()),
                                                'Files[type]' => Files::TYPE_FILE
                                            ]
                                        ]
                                    ]
                                )); ?>
                                <p class="help-block"><?= $item->description ?></p>
                            </div><!-- /.form-group -->
                        <?php endif; ?>

                        <?php if($item->type == Settings::TYPE_SELECT || $item->type == Settings::TYPE_SELECT_MULTIPLE): ?>
                            <div class="form-group mt-20 mb-20">
                                <label for="Settings_<?= $item->type ?>_<?= $item->id ?>">
                                    <?= $item->label ?>:
                                </label>
                                <div>
                                <?= Html::dropDownList('Settings['. $item->id .']', $item->value, $item->multiSelection, ArrayHelper::merge([
                                        'class' => 'select',
                                        'id' => 'Settings_' . $item->type . '_' . $item->id,
                                    ],
                                    ($item->type == Settings::TYPE_SELECT ? [
                                        'data-placeholder' => $item->placeholder,
                                        'prompt' => ''
                                    ] : ['multiple' => 'multiple']),
                                    ($item->disabled ? ['disabled' => 'disabled'] : [])
                                )); ?>
                                </div>
                                <p class="help-block"><?= $item->description ?></p>
                            </div><!-- /.form-group -->
                        <?php endif; ?>

                        <?php if($item->type == Settings::TYPE_FILE || $item->type == Settings::TYPE_FILES): ?>
                            <div class="form-group">
                                <label for="Settings_<?= $item->type ?>_<?= $item->id ?>">
                                    <?= $item->label ?>:
                                </label>
                                <?= FilesWidget::widget([
                                    'id' => $item->id,
                                    'type' => Files::TYPE_FILE,
                                    'files' => $item->prewiewsWidget,
                                    'module' => Settings::className()
                                ]) ?>
                                <p class="help-block"><?= $item->description ?></p>
                            </div>
                        <?php endif; ?>

                    <?php endforeach; ?>
                </div>
            <?php endforeach; ?>
            </div>
        </div><!--/.tabbable -->
        <div class="text-right">
            <button type="submit" name="btnAction" value="save" class="btn bg-teal-400 btn-sm">
                <i class="icon-checkmark3 position-left"></i> Сохранить
            </button>
            <button type="submit" name="btnAction" value="save-exit" class="btn btn-primary btn-sm">
                <i class="icon-undo2 mr-5"></i> Сохр. и закрыть
            </button>
            <button type="reset" class="btn btn-default btn-sm">
                <i class="icon-reload-alt mr-5"></i> Отмена изменений
            </button>
        </div>
    <?php ActiveForm::end(); ?>
<?php else: ?>
    <div class="alert bg-info alert-styled-left">
        В данном компоненте раздела нет данных для вывода.
    </div>
<?php endif; ?>
</div><!--/.end panel -->