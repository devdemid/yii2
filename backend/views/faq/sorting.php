<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

// Set the component description
$this->context->smallTitle = 'Здесь список всех ответов этой ветки faq, тут можно отредактировать, удалить или создать новую запись.';
$this->context->breadcrumbs[] = [
    'label' => 'FAQ Вопросы и ответы',
    'url'   => '/'. $this->context->id .'/index'
];

$this->context->breadcrumbs[] = [
    'label' => $faqModel->name,
    'url'   => '/'. $this->context->id .'/branch?item_id=' . $faqModel->id
];

$pageName = $this->context->view->title;
$this->context->breadcrumbs[] = $this->context->view->title;

?>
<? if(count($models)): ?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title"><?= $pageName ?></h5>
        <small class="display-block text-muted">
            Всего ответов: <?= $total ?>
        </small>
    </div>
    <div class="panel-body">
        <table class="table table-togglable table-hover">
            <thead>
                <tr>
                    <th data-toggle="true" style="width: 30px;">#ID</th>
                    <th data-toggle="true" width="70%">Вопрос</th>
                    <th class="text-center" data-toggle="true">Публикация</th>
                    <th class="text-center" width="10%">
                        <a href="#" onclick="return Sorting('/<?= Yii::$app->controller->id ?>/sorting?item_id=<?= $faqModel->id ?>', 'Sorting');" title="Сохранить позиции" data-popup="tooltip">
                            <i class="icon-sort text-size-mini text-default"></i>
                        </a>
                    </th>
                </tr>
            </thead>
            <tbody>
            <? foreach($models as $item): ?>
                <tr>
                    <td><?= $item->id ?></td>
                    <td>
                        <?= Html::a($item->question, [
                            Yii::$app->controller->id . '/edit-answer?id=' . $item->id
                        ]) ?>
                    </td>
                    <td class="text-center">
                        <div class="checkbox checkbox-switchery switchery-xs" title="Вкл/Выкл">
                            <label>
                                <?= Html::checkbox('Published['. $item->id .']', $item->published, [
                                    'class' => 'switchery',
                                    'onclick' => "ChangeRequest('" . Url::to([
                                        Yii::$app->controller->id . '/published-answer',
                                        'id' => $item->id
                                    ]) . "', {published: this.checked});"
                                ]); ?>
                            </label>
                        </div>
                    </td>
                    <td>
                        <?= Html::input('text', 'Sorting[' . $item->id . ']', $item->sorting, [
                            'class' => 'form-control input-xs text-center',
                            'style' => 'width: 50px; margin: 0 auto'
                        ]) ?>
                    </td>
                </tr>
            <? endforeach; ?>
            </tbody>
        </table>
    </div><!-- /.panel-body -->
</div>
<div class="mb-20">
    <?= LinkPager::widget([
        'pagination' => $pages,
        'options'    => [
            'class' => 'pagination pagination-xs'
        ]
    ]); ?>
</div>
<? else: Alert::begin(['options' => ['class' => 'bg-info alert-styled-left']]); ?>
    Не удается найти данные для отображения
<? Alert::end(); endif;?>
<!-- /table with togglable columns -->