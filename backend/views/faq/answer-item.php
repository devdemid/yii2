<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \yii\helpers\StringHelper;
use yii\helpers\ArrayHelper;
use \common\models\Files;

if($model->isNewRecord)
    $this->context->view->title = 'Добавить ответ';
else
    $this->context->view->title = 'Редактировать ответ';

$this->context->breadcrumbs[] = [
    'label' => 'FAQ Вопросы и ответы',
    'url'   => '/'. $this->context->id .'/index'
];

$this->context->breadcrumbs[] = [
    'label' => $faqModel->name,
    'url'   => '/'. $this->context->id .'/branch?item_id=' . $faqModel->id
];

$pageName = $this->context->view->title;
$this->context->breadcrumbs[] = $pageName;

$className = StringHelper::basename($model::className());

?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title"><?= $pageName ?></h5>
    </div>
    <div class="panel-body">
    <? $form = ActiveForm::begin([
        'id' => $this->context->id . '-form',
        'options' => [
            'role'    => 'form',
            'class' => 'form-horizontal'
        ]
    ]); ?>
        <div class="row">
            <div class="col-sm-12">
                <?= $form->field($model, 'question', [
                    // Begin input theme
                    'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                    // End input theme
                    'inputOptions' => [
                        'class' => 'form-control'
                    ],
                    'options' => [
                        'class' => 'form-group'
                    ]
                ])->hint(false)->label(
                    $model->getAttributeLabelRequired('question'),
                    ['class' => 'control-label col-lg-2']
                ) ?>

                <!-- Begin editor -->
                <div class="form-group">
                    <label class="control-label col-lg-2" for="<?= Html::getInputId($model, 'answer') ?>">
                        <?= $model->getAttributeLabelRequired('answer') ?>
                    </label>
                    <div class="col-lg-10 clearfix">
                        <?= \yii\imperavi\Widget::widget(ArrayHelper::merge(
                            [
                                'attribute' => Html::getInputName($model, 'answer'),
                                'value' => $model->answer
                            ],
                            Yii::$app->params['imperavi'], [
                                'id' => Html::getInputId($model, 'answer'),
                                'options' => [
                                    'minHeight' => '250px',
                                    'uploadImageFields' => [
                                        Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
                                        'Files[item_id]' => $model->id,
                                        'Files[module]' => $className,
                                        'Files[type]' => Files::TYPE_IMAGE
                                    ],
                                    'uploadFileFields' => [
                                        Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
                                        'Files[item_id]' => $model->id,
                                        'Files[module]' => $className,
                                        'Files[type]' => Files::TYPE_FILE
                                    ]
                                ]
                            ]
                        )); ?>
                        <? if($model->hasErrors('answer')): ?>
                        <p class="help-block help-block-error text-danger">
                            <?= $model->getFirstError('answer') ?>
                        </p>
                        <? endif; ?>
                        <p class="help-block">
                            Полное описание ответа по данному вопросу.
                        </p>
                    </div>
                </div>
                <!-- End editor -->

                <hr>
                <div class="col-lg-10 col-lg-offset-2">
                    <div class="form-group">
                        <?= $form->field($model, 'published', [
                            'template' => '<label class="checkbox-inline">{input}'. $model->getAttributeLabel('published') .'{hint}</label>',
                        ])->label(false)->checkbox(['class' => 'styled'], false)->hint('Вопрос будет доступен для просмотра посетителями ресурса') ?>
                    </div>
                </div>
            </div>
        </div><!-- /.row -->
        <hr>
        <div class="text-right">
            <button type="submit" name="btnAction" value="save" class="btn bg-teal-400 btn-sm">
                <i class="icon-checkmark3 position-left"></i> Сохранить
            </button>
            <button type="submit" name="btnAction" value="save-exit" class="btn btn-primary btn-sm">
                <i class="icon-undo2 mr-5"></i> Сохр. и закрыть
            </button>
            <button type="submit" name="btnAction" value="save-new" class="btn btn-primary btn-sm">
                <i class="icon-new-tab mr-5"></i> Сохр. и создать новую запись
            </button>
            <? if(!$model->isNewRecord): ?>
            <?= Html::a('<i class="icon-bin"></i>Удалить', [
                Yii::$app->controller->id . '/'. $model->id .'/remove'
            ], [
                'onclick' => "return confirm('Вы уверены что хотите удалить данные?');",
                'class' => 'btn btn-danger btn-sm'
            ]) ?>
            <? endif; ?>
        </div>
    <? ActiveForm::end(); ?>
    </div><!-- /.panel-body -->
</div>