<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

// Set the component description
$this->context->view->title = $this->context->view->title;
$this->context->smallTitle = 'Редактирование файла robots.txt';

$this->context->breadcrumbs[] = $this->context->view->title;

?>

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title"><?= $this->context->view->title ?></h5>
        <small class="display-block text-muted">
             Файл robots.txt предоставляет важную информацию для поисковых роботов, которые сканируют интернет. Перед тем как пройтись по страницам вашего сайта, поисковые роботы проверяют данный файл.
        </small>
    </div>
    <div class="panel-body">
        <? $form = ActiveForm::begin([
            'id' => $this->context->id . '-form',
            'options' => [
                'role'    => 'form',
                'class' => 'form-horizontal'
            ]
        ]); ?>
        <?= Html::textarea('robots', $robots_text, [
            'class' => 'form-control',
            'rows' => 15,
            'placeholder' => "User-agent: *\rSitemap: /sitemap.xml\r"
        ]); ?>
        <? if($robots_filemtime): ?>
        <div class="mt-10">
            <span class="mr-5 text-muted">Дата последнего изменения файла:</span> <?= date('d.m.Y H:i:s', $robots_filemtime) ?>
        </div>
        <? endif; ?>
        <br>
        <div class="text-right">
            <button type="submit" name="btnAction" value="save" class="btn bg-teal-400 btn-sm">
                <i class="icon-checkmark3 position-left"></i> Сохранить
            </button>
            <button type="submit" name="btnAction" value="save-exit" class="btn btn-primary btn-sm">
                <i class="icon-undo2 mr-5"></i> Сохр. и закрыть
            </button>
        </div>
        <? ActiveForm::end(); ?>
    </div><!-- /.panel-body -->
</div>
<!-- /table with togglable columns -->

<?/*<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Спецификации файла robots.txt</h5>
    </div>
    <div class="panel-body">
        <p><strong>User-Agent:</strong> робот для которого будут применяться следующие правила (например, «Googlebot»)</p>
        <p><strong>Disallow:</strong> страницы, к которым вы хотите закрыть доступ (можно указать большой список таких директив с каждой новой строки)</p>
        <p>Каждая группа <strong>User-Agent / Disallow</strong> должны быть разделены пустой строкой. Но, не пустые строки не должны существовать в рамках группы (между User-Agent и последней директивой Disallow).</p>
        <p><strong>Символ хэш (#)</strong> может быть использован для комментариев в файле robots.txt: для текущей строки всё что после # будет игнорироваться. Данные комментарий может быть использован как для всей строки, так в конце строки после директив.</p>
        <p>Каталоги и имена файлов <strong>чувствительны к регистру</strong>: «catalog», «Catalog» и «CATALOG» – это всё разные директории для поисковых систем.</p>
        <p><strong>Host:</strong> применяется для указание Яндексу основного зеркала сайта. Поэтому, если вы хотите склеить 2 сайта и делаете постраничный 301 редирект, то для файла robots.txt (на дублирующем сайте) НЕ надо делать редирект, чтобы Яндекс мог видеть данную директиву именно на сайте, который необходимо склеить.</p>
        <p><strong>Crawl-delay:</strong> можно ограничить скорость обхода вашего сайта, так как если у вашего сайта очень большая посещаемость, то, нагрузка на сервер от различных поисковых роботов может приводить к дополнительным проблемам.</p>
        <p><strong>Регулярные выражения:</strong> для более гибкой настройки своих директив вы можете использовать 2 символа</p>
        <ul>
            <li><strong>*</strong> (звездочка) – означает любую последовательность символов</li>
            <li><strong>$</strong> (знак доллара) – обозначает конец строки</li>
        </ul>
    </div><!-- /.panel-body -->
</div>*/?>
<!-- /table with togglable columns -->