<?php
/*
 * Левый блок меню
 */

use yii\helpers\Html;
use yii\widgets\Menu;
use \common\components\Navigation;

// Get navigation from a file
$items = (new Navigation('@backend'))->getItems();

?>
<div class="sidebar sidebar-main">
    <div class="sidebar-content">
        <!-- User menu -->
        <div class="sidebar-user">
            <div class="category-content">
                <div class="media">
                    <?= Html::a('<img src="/files/image.png" class="img-circle img-sm" alt="'.Yii::$app->user->identity->first_name .'">', [
                        'users/profile/' . Yii::$app->user->identity->id
                    ], ['class' => 'media-left']) ?>
                    <div class="media-body">
                        <span class="media-heading text-semibold"><?= Yii::$app->user->identity->first_name ?></span>
                        <div class="text-size-mini text-muted">
                            <i class="icon-puzzle3 text-size-small"></i> &nbsp; <?= Yii::$app->user->identity->groupName ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /user menu -->
        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <?= Menu::widget([
                    'items' => $items,
                    'options' => [
                        'class' => 'navigation navigation-main navigation-accordion'
                    ],
                    'activeCssClass' => 'active'
                ]); ?>
            </div>
        </div>
        <!-- /main navigation -->
    </div>
</div>