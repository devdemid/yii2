<?php
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

use backend\components\MenuEvent;

?>
<?php if($this->context->smallTitle || count($this->context->breadcrumbs)): ?>
<div class="page-header page-header-default">

    <div class="page-header-content">
        <div class="page-title">
            <h5>
                <span class="text-semibold">
                    <?= Html::encode($this->context->view->title) ?>
                </span>
                <?php if($this->context->smallTitle): ?>
                <small class="display-block">
                        <?= Html::encode($this->context->smallTitle) ?>
                </small>
                <?php endif; ?>
            </h5>
        </div>
        <?php /*<div class="heading-elements">
            <form class="heading-form" action="#">
                <div class="form-group">
                    <div class="has-feedback">
                        <input type="search" class="form-control" placeholder="Search">
                        <div class="form-control-feedback">
                            <i class="icon-search4 text-size-small text-muted"></i>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <a class="heading-elements-toggle"><i class="icon-more"></i></a>*/?>
    </div><!-- /.page-header-content -->

    <div class="breadcrumb-line">
        <?= Breadcrumbs::widget([
            'homeLink' => [
                'label' => '<i class="icon-home2 position-left"></i> Главная',
                'url' => '/'
            ],
            'links' => isset($this->context->breadcrumbs) ? $this->context->breadcrumbs : [],
            'encodeLabels' => false
        ]) ?>

        <ul class="breadcrumb-elements">
            <? foreach($this->context->headMenu as $item): ?>
            <li>
                <?= Html::a(($item->icon ? '<i class="'. $item->icon .'"></i> ': '') . $item->label, [
                    $item->url
                ], [
                    'target' => $item->target ? '_blank' : false
                ]) ?>
            </li>
            <? endforeach; ?>
            <?/*<li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-gear position-left"></i>
                    Действия
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <?php foreach( $this->context->actionsLink as $itemAction ) : ?>
                        <?php if( $itemAction['class'] == 'divider' ): ?>
                            <li class="divider"></li>
                        <?php else: ?>
                            <li><a href="<?=$itemAction['link']?>"><i class="icon-<?=$itemAction['class']?> position-left"></i> <?=$itemAction['name']?></a></li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            </li>*/?>
        </ul>
        <a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
    </div><!-- /.breadcrumb-line -->
</div>
<?php endif; ?>