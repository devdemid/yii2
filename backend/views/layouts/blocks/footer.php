<?php
/*
* Подвал панели управления
*/
use yii\helpers\Html;
?>
<footer class="footer">
	<?= date('Y') ?> © <?= \Yii::$app->id ?>.
</footer>