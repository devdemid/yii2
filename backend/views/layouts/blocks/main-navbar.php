<?php
/*
* Верхний блок навигации
*/

use yii\helpers\Html;

?>
<!-- Main navbar -->
<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-header">
        <?= Html::a(Html::encode(\Yii::$app->id), [Yii::$app->homeUrl], ['class' => 'navbar-brand']) ?>
        <ul class="nav navbar-nav pull-right visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>
    <?php //var_dump(Yii::$app->user->identity); ?>
    <div class="navbar-collapse collapse" id="navbar-mobile">
    <?php if(Yii::$app->user->isGuest): ?>
        <ul class="nav navbar-nav navbar-right">
            <li>
                <?= Html::a('<i class="icon-comment-discussion"></i><span class="visible-xs-inline-block position-right">Поддержка</span>', ['#'], ['onclick' => 'return false']) ?>
            </li>
            <?/*<li>
                <?= Html::a('<i class="icon-user-plus"></i><span class="visible-xs-inline-block position-right">Регистрация</span>', ['users/signup']) ?>
            </li>*/?>
        </ul>
    <?php else: ?>
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
        <!-- <p class="navbar-text">
            <span class="label bg-success">874 Онлайн</span>
        </p> -->
        <ul class="nav navbar-nav navbar-right">
            <li>
                <?= Html::a('<i class="icon-help"></i><span class="visible-xs-inline-block position-right">Помощник</span>', ['help/index'], ['aria-hidden' => 'true', 'data-popup' => 'tooltip', 'title' => 'Помощник', 'data-placement' => 'bottom']) ?>
            </li>
            <? if(\Yii::$app->user->can('editSettings')) :?>
            <li>
                <?= Html::a('<i class="icon-gear"></i><span class="visible-xs-inline-block position-right">Основные настройки</span>', ['/settings'], ['aria-hidden' => 'true', 'data-popup' => 'tooltip', 'title' => 'Основные настройки', 'data-placement' => 'bottom']) ?>
            </li>
            <li>
                <?= Html::a('<i class="icon-folder-remove"></i><span class="visible-xs-inline-block position-right">Очистить кэш</span>', ['cache/clearing'], ['aria-hidden' => 'true', 'data-popup' => 'tooltip', 'title' => 'Очистить кэш', 'data-placement' => 'bottom', 'onclick' => 'return confirm(\'Вы уверены что хотите удалить данные кэша?\');']) ?>
            </li>
            <? endif; ?>
            <li>
                <?= Html::a('<i class="icon-square-right"></i><span class="visible-xs-inline-block position-right">Просмотр сайта</span>', Yii::$app->params['frontentUrl'], ['aria-hidden' => 'true', 'target' => '_blank', 'data-popup' => 'tooltip', 'title' => 'Просмотр сайта', 'data-placement' => 'bottom']) ?>
            </li>
            <li class="dropdown dropdown-user">
                <a class="dropdown-toggle" data-toggle="dropdown">
                    <img src="/files/image.png" alt="<?= Yii::$app->user->identity->first_name ?>">
                    <span><?= Yii::$app->user->identity->first_name ?></span>
                    <i class="caret"></i>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li>
                        <?= Html::a('<i class="icon-user"></i>Мой профиль', [
                            'users/'. Yii::$app->user->identity->id . '/profile'
                        ]) ?>
                    </li>
                    <li>
                        <?= Html::a('<i class="icon-enter2"></i>Выход', ['users/logout']) ?>
                    </li>
                </ul>
            </li>
        </ul>
    <?php endif; ?>
    </div>
</div>
<!-- /main navbar -->