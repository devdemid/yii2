<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\BackendAsset;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

BackendAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex, nofollow">
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="<?= Html::encode($this->params['bodyClasses']) ?>">
    <?= $this->context->renderPartial('@layoutsBlocks/main-navbar'); ?>
    <?php $this->beginBody() ?>
    <!-- Page container -->
    <div class="page-container">
        <!-- Page content -->
        <div class="page-content"><?= $content ?></div><!-- /page content -->
    </div>
    <!-- /page container -->
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>