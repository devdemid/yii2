<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use common\widgets\Alert;
use backend\assets\BackendAsset;
use yii\helpers\ArrayHelper;

BackendAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <title><?= Html::encode($this->title) ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="robots" content="noindex, nofollow">
        <meta name="description" content="">
        <!-- <link rel="shortcut icon" href="/images/favicon_1.ico"> -->
        <?= Html::csrfMetaTags() ?>
        <?php $this->head() ?>
        <script>
            var resizefunc = [];
        </script>
    </head>
    <body class="navbar-top">
        <?php $this->beginBody() ?>
        <!-- Begin page -->
        <div id="wrapper">
            <?= $this->context->renderPartial('@layoutsBlocks/main-navbar'); ?>
            <div class="page-container">
                <div class="page-content">
                    <?= $this->context->renderPartial('@layoutsBlocks/left-sidebar'); ?>
                    <div class="page-container">
                        <!-- Start content -->
                        <div class="content-wrapper">
                            <?= $this->context->renderPartial('@layoutsBlocks/breadcrumbs'); ?>
                            <div class="content">
                                <? //Alert::widget() ?>
                                <?= $content ?>
                                <?= $this->context->renderPartial('@layoutsBlocks/footer'); ?>
                            </div><!-- /.content -->
                        </div> <!-- content-wrapper -->
                    </div><!-- /.page-container -->
                </div><!-- /.page-content -->
            </div><!-- /.page-container -->
            <?//= $this->context->renderPartial('@layoutsBlocks/right-sidebar'); ?>
        </div>
        <!-- END wrapper -->
        <?php $this->endBody() ?>
    </body>
    <?php foreach (Yii::$app->session->allFlashes as $type => $message): ?>
        <script>
            new PNotify({
                //title: '<?= $type ?> notice',
                text: '<?= $message ?>',
                icon: '<?= ArrayHelper::getValue([
                    'error' => 'icon-blocked',
                    'success' => 'icon-checkmark3',
                    'info' => 'icon-info22'
                ], $type)?>',
                type: '<?= $type ?>'
            });
        </script>
    <?php endforeach; ?>
</html>
<?php $this->endPage() ?>