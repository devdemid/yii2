<?php
use yii\helpers\Html;
$this->title = $name;
?>
<!-- Main content -->
<div class="content-wrapper">
	<!-- Content area -->
	<div class="content">
		<!-- Error title -->
		<div class="text-center content-group">
			<h1 class="error-title"><?= Yii::$app->errorHandler->exception->statusCode ?></h1>
			<h5><?= nl2br(Html::encode($message)) ?></h5>
		</div>
		<!-- /error title -->
		<!-- Error content -->
		<div class="row">
			<div class="col-lg-4 col-lg-offset-4 col-sm-6 col-sm-offset-3">
				<form action="" class="main-search" onsubmin="return false">
					<div class="input-group content-group">
						<input type="text" class="form-control input-lg" placeholder="Поиск">
						<div class="input-group-btn">
							<button type="submit" class="btn bg-slate-600 btn-icon btn-lg">
								<i class="icon-search4"></i>
							</button>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<?= Html::a('<i class="icon-circle-left2 position-left"></i> Перейти в панель', ['/'], [
								'class' => 'btn btn-primary btn-block content-group'
							]) ?>
						</div>
						<div class="col-sm-6">
							<?= Html::a('<i class="icon-menu7 position-left"></i> Расширенный поиск', ['/'], [
								'class' => 'btn btn-default btn-block content-group',
								'onclick' => 'return false'
							]) ?>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- /error wrapper -->
		<!-- Footer -->
		<div class="footer text-muted text-center">
			&copy; <?= date('Y') ?>. <?= \Yii::$app->id ?>
		</div>
		<!-- /footer -->
	</div>
	<!-- /content area -->
</div>
<!-- /main content -->