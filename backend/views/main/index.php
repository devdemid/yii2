<?php
    $this->title = 'Панель управления';
?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h6 class="no-margin text-semibold">Системная информация</h6>
    </div>
    <div class="panel-body">
        <? if(count($server_info)): ?>
        <div class="well">
            <ul class="list-unstyled">
                <? foreach ($server_info as $name => $value): ?>
                <li><span class="text-muted mr-5"><?= $name ?>:</span><?= $value ?></li>
                <? endforeach; ?>
            </ul>
        </div>
        <? endif; ?>
    </div>
</div><!-- /.panel -->