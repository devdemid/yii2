<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use common\models\Page;

// Set the component description
$this->context->smallTitle = 'История всех заказов, тут Вы можите изменить статус, отредактировать или удалить заказ';
$this->context->breadcrumbs[] = $this->context->view->title;

?>

<? if(count($models)): ?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Все заказы</h5>
        <small class="display-block text-muted">
            <?= $search ? 'Всего найдено' :  'Всего заказов' ?>: <?= $pages->totalCount ?>
        </small>
        <div class="heading-elements">
            <?= Html::beginForm(Url::to([Yii::$app->controller->id . '/index']), 'get', ['class' => 'heading-form']) ?>
                <div class="form-group has-feedback">
                    <?= Html::textInput('search', $search, ['class' => 'form-control input-xs', 'placeholder' => 'Поиск...']) ?>
                    <div class="form-control-feedback">
                        <i class="icon-search4 text-size-base text-muted"></i>
                    </div>
                </div>
            <?= Html::endForm() ?>
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>
    <div class="panel-body">
        <table class="table table-togglable table-hover">
            <thead>
                <tr>
                    <th data-toggle="true" width="10%" class="text-center">#ID</th>
                    <th data-toggle="true" width="15%" class="text-center">Дата заказа</th>
                    <th data-toggle="true" width="15%" class="text-center">Метод оплаты</th>
                    <th data-toggle="true" class="text-center">Стоимость заказа</th>
                    <th data-toggle="true" width="15%" class="text-center">Статус</th>
                    <th class="text-center" width="10%">
                        <i class="icon-menu-open2" data-popup="tooltip" title="Действие" data-placement="left"></i>
                    </th>
                </tr>
            </thead>
            <tbody>
            <? foreach($models as $item): ?>
                <tr>
                    <td class="text-center"><?= $item->id ?></td>
                    <th class="text-center"><?= date($item::OUT_DATE_FORMAT, $item->create_timestamp) ?></th>
                    <td class="text-center">
                        <?= $item::getPaymentName($item->payment_type) ?>
                    </td>
                    <td class="text-center"><?= $item->totalPriceFormat ?></td>
                    <td class="text-center">
                        <span class="label <?= $item::getStatusColor($item->status) ?>">
                            <?= $item::getStatusName($item->status) ?>
                        </span>
                    </td>
                    <td class="text-center">
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Действия">
                                    <i class="icon-menu7"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <?= Html::a('<i class="icon-pencil5"></i>Редактировать', [
                                            Yii::$app->controller->id . '/edit?id=' . $item->id
                                        ]) ?>
                                    </li>
                                    <li>
                                        <?= Html::a('<i class="icon-bin"></i>Удалить', [
                                            Yii::$app->controller->id . '/remove?id=' . $item->id
                                        ], [
                                            'onclick' => "return confirm('Вы уверены что хотите удалить заказ?');"
                                        ]) ?>
                                    </li>
                                </ul>
                            </li>
                        </ul><!-- /.icons-list -->
                    </td>
                </tr>
            <? endforeach; ?>
            </tbody>
        </table>
    </div><!-- /.panel-body -->
</div>
<div class="mb-20">
    <?= LinkPager::widget([
        'pagination' => $pages,
        'options'    => [
            'class' => 'pagination pagination-xs'
        ]
    ]); ?>
</div>
<? else: Alert::begin(['options' => ['class' => 'bg-info alert-styled-left']]); ?>
    Не удалось найти заказаные туры для отображения
<? Alert::end(); endif;?>
<!-- /table with togglable columns -->