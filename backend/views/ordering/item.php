<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\Groupmodels;
use backend\widgets\FilesWidget;

if($model->isNewRecord)
    $this->context->view->title = 'Добавить заказ';
else
    $this->context->view->title = 'Редактировать заказ';

$this->context->breadcrumbs[] = [
    'label' => 'Все заказы',
    'url'   => '/'. $this->context->id .'/index'
];

if($model->isNewRecord)
    $pageName = 'Добавить заказ';
else
    $pageName = 'Редактировать заказ';

$this->context->breadcrumbs[] = $pageName;

?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title"><?= $pageName ?> - #ID<?= $model->id ?></h5>
    </div>
    <div class="panel-body">
    <? $form = ActiveForm::begin([
        'id' => $this->context->id . '-form',
        'options' => [
            'role'    => 'form',
            'class' => 'form-horizontal'
        ]
    ]); ?>
        <div class="row">
            <div class="col-sm-12">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <th width="25%">Параметр</th>
                                <th>Значение параметра</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Дата заказа:</td>
                                <td><?= date($model::OUT_DATE_FORMAT, $model->create_timestamp) ?></td>
                            </tr>
                            <? if($model->paid_timestamp): ?>
                            <tr>
                                <td>Дата оплаты:</td>
                                <td><?= date($model::OUT_DATE_FORMAT, $model->paid_timestamp) ?></td>
                            </tr>
                            <? endif; ?>
                            <tr>
                                <td>Метод оплаты:</td>
                                <td><?= $model::getPaymentName($model->payment_type) ?></td>
                            </tr>
                            <tr>
                                <td>Стоимость заказа:</td>
                                <td><?= $model->totalPriceFormat ?></td>
                            </tr>
                            <tr>
                                <td>Пользователь:</td>
                                <td>
                                    <a href="/users/<?= $model->user_id ?>/edit" target="_blank">
                                        <?= $model->user->nameTogether ?>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>E-Mail:</td>
                                <td>
                                    <a href="mailto:<?= $model->user->email ?>">
                                        <?= $model->user->email ?>
                                    </a>
                                </td>
                            </tr>
                            <? if($model->user->phone): ?>
                            <tr>
                                <td>Номер телефона:</td>
                                <td>
                                    <?= $model->user->phone ?>
                                </td>
                            </tr>
                            <? endif; ?>
                         </tbody>
                    </table>
                </div>
                <? if(count($model->orderData)): ?>
                <? foreach($model->orderData as $data): ?>
                <div class="table-responsive mt-20">
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <th width="25%">Параметр</th>
                                <th>Значение параметра</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Название тура:</td>
                                <td><?= $data->name ?></td>
                            </tr>
                            <tr>
                                <td>Взрослых:</td>
                                <td><?= $data->adults ?></td>
                            </tr>
                            <? if($data->children): ?>
                            <tr>
                                <td>Детей:</td>
                                <td><?= $data->children ?></td>
                            </tr>
                            <? endif; ?>
                            <tr>
                                <td>Начало - окончание:</td>
                                <td>
                                    <?= date($data::OUT_DATE_FORMAT, $data->start_date_timestamp) ?>
                                    -
                                    <?= date($data::OUT_DATE_FORMAT, $data->end_date_timestamp) ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Цена:</td>
                                <td><?= $data->price ?> руб.</td>
                            </tr>
                            <tr>
                                <td>Цена для ребенка:</td>
                                <td><?= $data->children_price ?> руб.</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <? endforeach; ?>
                <? endif; ?>
                <hr>
                <?= $form->field($model, 'status', [
                    // Begin input theme
                    'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                    // End input theme
                    'inputOptions' => [
                        'class' => 'select'
                    ],
                    'options' => [
                        'class' => 'form-group'
                    ]
                ])->dropDownList($model::getStatusesList())->label(
                    $model->getAttributeLabelRequired('status'),
                    ['class' => 'control-label col-lg-2']
                )->hint('Статус заказа - это ключевая информация, которая сообщает вам, как покупателю, текущее состояние заказа.') ?>
            </div>
        </div><!-- /.row -->
        <hr>
        <div class="text-right">
            <button type="submit" name="btnAction" value="save" class="btn bg-teal-400 btn-sm">
                <i class="icon-checkmark3 position-left"></i> Сохранить
            </button>
            <button type="submit" name="btnAction" value="save-exit" class="btn btn-primary btn-sm">
                <i class="icon-undo2 mr-5"></i> Сохр. и закрыть
            </button>
            <? if(!$model->isNewRecord): ?>
            <?= Html::a('<i class="icon-bin"></i>Удалить', [
                Yii::$app->controller->id . '/'. $model->id .'/remove'
            ], [
                'onclick' => "return confirm('Вы уверены что хотите удалить заказ?');",
                'class' => 'btn btn-danger btn-sm'
            ]) ?>
            <? endif; ?>
        </div>
    <? ActiveForm::end(); ?>
    </div><!-- /.panel-body -->
</div>