<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

// Set the component description
$this->context->view->title = 'Добавить новые сообщения';
$this->context->breadcrumbs[] = ['label' => 'Переводы (локализация)', 'url' => Url::toRoute('translations/index') ];
$this->context->breadcrumbs[] = $this->context->view->title;
$this->context->smallTitle = 'Добавление новых сообщения для перевода';

?>
<div class="panel panel-body">
    <?php $form = ActiveForm::begin([
        'id' => 'translate-form',
        'action' => [ Url::toRoute('translations/save-new') ],
        'options' => [
            'class' => 'form-horizontal'
        ]
    ]); ?>
        <?= Html::hiddenInput('curLang', $lang, [
            'id' => 'category_'. 0,
            'class' => 'form-control',
        ] ); ?>
        <div class="form-group mt-20 mb-20">
            <div class="row">
                <div class="col-sm-3 col-md-2">
                    <?= Html::textInput('category['. 0 .']', '', [
                        'id' => 'category_'. 0,
                        'class' => 'form-control',
                        'placeholder' => 'введите категорию сообщений'
                    ] ); ?>
                </div>
                <div class="col-sm-3 col-md-2">
                    <?= Html::textInput('str_id['. 0 .']', '', [
                        'id' => 'str_id_'. 0,
                        'class' => 'form-control',
                        'placeholder' => 'введите строковый идентификатор'
                    ] ); ?>
                </div>
                <div class="col-sm-6 col-md-8">
                    <?= Html::textInput('message['. 0 .']', '', [
                        'id' => 'message_'. 0,
                        'class' => 'form-control',
                        'placeholder' => 'введите сообщение для перевода'
                    ] ); ?>
                </div>
            </div>
        </div>

        <div class="text-right">
            <div class="btn-group">
                <button type="submit" name="btnAction" value="save" class="btn btn-primary btn-xs">
                    <i class="icon-checkmark3 position-left"></i> Сохранить
                </button>
                <button type="button" class="btn btn-primary dropdown-toggle btn-xs" data-toggle="dropdown" aria-expanded="false">
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li>
                        <button type="submit" name="btnAction" value="save-exit" class="btn btn-link">
                            <i class="icon-undo2 mr-5"></i> Сохр. и закрыть
                        </button>
                    </li>
                    <li>
                        <button type="reset" class="btn btn-link">
                            <i class="icon-reload-alt mr-5"></i> Отмена изменений
                        </button>
                    </li>
                </ul>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>