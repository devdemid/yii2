<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

// Set the component description
$this->context->view->title = 'Переводы (локализация)';
$this->context->breadcrumbs[] = $this->context->view->title;
$this->context->smallTitle = 'Перевод текста на иностранные языки, для текстового наполнения компонентов';

$intNumTab = 0;
?>
<div class="panel panel-body">
    <div class="dropdown pull-right">
        <?php                 
            $objWidget = new frontend\components\LangPicker( $lang, false );
            echo $objWidget->run();
        ?>
    </div>
    <div class="clearfix"></div>
    <?php if(count($msgs)): ?>
    	<?php $form = ActiveForm::begin(['id' => 'settings-form','action' => [ Url::toRoute('translations/save'), 'curLang'=> $lang ], ]); ?>
		<div class="tabbable">
                    <ul class="nav nav-tabs nav-tabs-bottom">
                        <?php foreach($msgs as $category => $arrMsgs): ?><!-- class="active" -->
                            <?php if($intNumTab++ < 5): ?>
                            <li class="<?= !$intNumTab ? 'active' : false ?>">
                                <a href="#category-<?= $category ?>" data-toggle="tab">
                                    <?= Html::encode($category).'(todo описание)' ?>
                                </a>
                            </li>
                            <?php else: ?>
                                <?php if($intNumTab == 6): ?>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        Еще... <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                <?php endif; ?>
                                    <li>
                                        <a href="#category-<?= $category ?>" data-toggle="tab">
                                                <?= Html::encode($category).'(todo описание)' ?>
                                        </a>
                                    </li>
                                <?php if($intNumTab == count($settings)): ?>
                                    </ul>
                                </li>
                                <?php endif; ?>
                            <?php endif; ?>
			<?php endforeach; ?>
                    </ul>
                    <div class="tab-content pt-5">
                        <?php foreach($msgs as $category => $arrMsgs): ?>
                            <div class="tab-pane active" id="category-<?= $category ?>">
                                <?php foreach($arrMsgs as $strRuMsg => $arrMsg): ?>
                                    <div class="form-group mt-20 mb-20">
                                        <label for="msg_<?= $arrMsg['str_id'] ?>">
                                            <?= '<b>#'.$arrMsg['str_id'].'</b>. '.$strRuMsg ?>:
                                        </label>
                                        <?= Html::textInput('msgs['. $category .']['. $arrMsg['str_id'] .']', $arrMsg['translation'], [
                                                'id' => 'msgs_'. $category .'_'. $arrMsg['str_id'],
                                                'class' => 'form-control',
                                                'placeholder' => 'добавьте перевод фразы'
                                        ] ); ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>    
			<?php endforeach; ?>
                    </div>
                </div>
		<div class="text-right">
                    <div class="btn-group">
                        <button type="submit" name="btnAction" value="save" class="btn btn-primary btn-xs">
                            <i class="icon-checkmark3 position-left"></i> Сохранить
                        </button>
                        <button type="button" class="btn btn-primary dropdown-toggle btn-xs" data-toggle="dropdown" aria-expanded="false">
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <button type="submit" name="btnAction" value="save-exit" class="btn btn-link">
                                    <i class="icon-undo2 mr-5"></i> Сохр. и закрыть
                                </button>
                            </li>
                            <li>
                                <button type="reset" class="btn btn-link">
                                    <i class="icon-reload-alt mr-5"></i> Отмена изменений
                                </button>
                            </li>
                        </ul>
                    </div>
		</div>
        <?php ActiveForm::end(); ?>
    <?php endif; ?>
</div>