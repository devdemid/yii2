<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use backend\widgets\FilesWidget;
use common\models\User;

$this->context->breadcrumbs[] = [
    'label' => 'Все аккаунты',
    'url'   => '/'. Yii::$app->controller->id .'/index'
];

if($user->isNewRecord) {
    $this->context->view->title = 'Добавить новый аккаунт';
    $this->context->smallTitle = 'Добавления нового аккаунта пользователя';
} else {
    $this->context->view->title = 'Редактировать аккаунт';
    $this->context->smallTitle = 'Редактирование и настройка аккаунта пользователя';
}


$this->context->breadcrumbs[] = $this->context->view->title;

?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title"><?= $this->context->view->title ?></h5>
    </div>
    <div class="panel-body">
    <? $form = ActiveForm::begin([
        'id' => $this->context->id . '-form',
        'options' => [
            'role'    => 'form',
            'enctype' => 'multipart/form-data',
            'class' => 'form-horizontal'
        ]
    ]); ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="tabbable">
                <ul class="nav nav-tabs nav-tabs-bottom">
                    <li class="active">
                        <a href="#<?= Html::getInputId($user, 'basic') ?>" data-toggle="tab">
                            Основные поля
                        </a>
                    </li>
                    <? if(!$user->isNewRecord): ?>
                    <li>
                        <a href="#<?= Html::getInputId($user, 'additionally') ?>" data-toggle="tab">
                            Дополнительная информация
                        </a>
                    </li>
                    <? endif; ?>
                </ul>
                <div class="tab-content pt-5">
                    <div class="tab-pane active" id="<?= Html::getInputId($user, 'basic') ?>">
                        <?= $form->field($user, 'username', [
                            // Begin input theme
                            'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                            // End input theme
                            'inputOptions' => [
                                'class' => 'form-control'
                            ],
                            'options' => [
                                'class' => 'form-group'
                            ]
                        ])->hint(false)->label(
                            $user->getAttributeLabel('username') .
                            ($user->isAttributeRequired('username')? ' : <span class="text-danger">*</span>' : ' :'),
                            ['class' => 'control-label col-lg-2']
                        ) ?>
                        <?= $form->field($user, 'email', [
                            // Begin input theme
                            'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                            // End input theme
                            'inputOptions' => [
                                'class' => 'form-control'
                            ],
                            'options' => [
                                'class' => 'form-group'
                            ]
                        ])->hint(false)->label(
                            $user->getAttributeLabel('email') .
                            ($user->isAttributeRequired('email')? ' : <span class="text-danger">*</span>' : ' :'),
                            ['class' => 'control-label col-lg-2']
                        ) ?>
                        <?= $form->field($user, 'phone', [
                            // Begin input theme
                            'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                            // End input theme
                            'inputOptions' => [
                                'class' => 'form-control'
                            ],
                            'options' => [
                                'class' => 'form-group'
                            ]
                        ])->hint(false)->label(
                            $user->getAttributeLabel('phone') .
                            ($user->isAttributeRequired('phone')? ' : <span class="text-danger">*</span>' : ' :'),
                            ['class' => 'control-label col-lg-2']
                        ) ?>
                        <?= $form->field($user, 'first_name', [
                            // Begin input theme
                            'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                            // End input theme
                            'inputOptions' => [
                                'class' => 'form-control'
                            ],
                            'options' => [
                                'class' => 'form-group'
                            ]
                        ])->hint(false)->label(
                            $user->getAttributeLabel('first_name') .
                            ($user->isAttributeRequired('first_name')? ' : <span class="text-danger">*</span>' : ' :'),
                            ['class' => 'control-label col-lg-2']
                        ) ?>
                        <?= $form->field($user, 'second_name', [
                            // Begin input theme
                            'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                            // End input theme
                            'inputOptions' => [
                                'class' => 'form-control'
                            ],
                            'options' => [
                                'class' => 'form-group'
                            ]
                        ])->hint(false)->label(
                            $user->getAttributeLabel('second_name') .
                            ($user->isAttributeRequired('second_name')? ' : <span class="text-danger">*</span>' : ' :'),
                            ['class' => 'control-label col-lg-2']
                        ) ?>
                        <? if(Yii::$app->user->can('changeProfileGroup')): ?>
                            <?= $form->field($user, 'group', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'select'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->dropDownList($user->groupList, [
                                'prompt' => 'Выберите группу...'
                            ])->hint(false)->label(
                                $user->getAttributeLabel('group') .
                                ($user->isAttributeRequired('group')? ' : <span class="text-danger">*</span>' : ' :'),
                                ['class' => 'control-label col-lg-2']
                            ) ?>
                        <? endif; ?>

                        <? if(Yii::$app->user->can('lockingProfiles')): ?>
                            <?= $form->field($user, 'status', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'select'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->dropDownList($user->statusesList, [
                                'prompt' => 'Выберите группу...'
                            ])->hint(false)->label(
                                $user->getAttributeLabel('status') .
                                ($user->isAttributeRequired('status')? ' : <span class="text-danger">*</span>' : ' :'),
                                ['class' => 'control-label col-lg-2']
                            ) ?>
                        <? endif; ?>

                        <?= $form->field($user, 'sex', [
                            'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                            'options' => [
                                'class' => 'form-group'
                            ]
                        ])->hint('Пол — Биологическая принадлежность к сообществу мужчин или женщин')->label($user->getAttributeLabel('sex') . (
                            $user->isAttributeRequired('sex')? ' : <span class="text-danger">*</span>' : ' : '
                        ), [
                            'class' => 'control-label col-lg-2'
                        ])->radioList(User::getSexList(), [
                            'item' => function($index, $label, $name, $checked, $value) {
                                $_checked = $checked ? 'checked' : '';
                                return '<label class="radio-inline" for="">' .
                                    '<input type="radio" name="'. $name .'" value="' . $value . '" ' . $_checked . ' class="styled">' .
                                    $label .
                                '</label>';
                            }
                        ]) ?>
                        <hr>
                        <!-- Begin files module -->
                        <div class="form-group">
                            <label for="" class="control-label col-lg-2">Фото или аватар:</label>
                            <div class="col-lg-10">
                                <?= FilesWidget::widget([
                                    'id' => $user->id,
                                    'type' => \common\models\Files::TYPE_IMAGE,
                                    'files' => $user->prewiewsWidget,
                                    'module' => User::className(),
                                    'sizes' => ['300x300'],
                                    'hint' => 'Каждый из пользователей хочет и имеет возможность проявить свою индивидуальность, например, через установку аватара или фото.',
                                    'labels' => [
                                        'Размер изображения' => '300x300px',
                                        'Допустимое расширение файла' => 'jpg, jpeg, png, gif'
                                    ]
                                ]) ?>
                            </div>
                        </div>
                        <!-- End files module -->
                    </div>
                    <div class="tab-pane" id="<?= Html::getInputId($user, 'additionally') ?>">
                        <?= $form->field($user, 'country', [
                            // Begin input theme
                            'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                            // End input theme
                            'inputOptions' => [
                                'class' => 'form-control'
                            ],
                            'options' => [
                                'class' => 'form-group'
                            ]
                        ])->hint(false)->label(
                            $user->getAttributeLabel('country') .
                            ($user->isAttributeRequired('country')? ' : <span class="text-danger">*</span>' : ' :'),
                            ['class' => 'control-label col-lg-2']
                        ) ?>
                        <?= $form->field($user, 'city', [
                            // Begin input theme
                            'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                            // End input theme
                            'inputOptions' => [
                                'class' => 'form-control'
                            ],
                            'options' => [
                                'class' => 'form-group'
                            ]
                        ])->hint(false)->label(
                            $user->getAttributeLabel('city') .
                            ($user->isAttributeRequired('city')? ' : <span class="text-danger">*</span>' : ' :'),
                            ['class' => 'control-label col-lg-2']
                        ) ?>
                        <?= $form->field($user, 'address', [
                            // Begin input theme
                            'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                            // End input theme
                            'inputOptions' => [
                                'class' => 'form-control'
                            ],
                            'options' => [
                                'class' => 'form-group'
                            ]
                        ])->hint(false)->label(
                            $user->getAttributeLabel('address') .
                            ($user->isAttributeRequired('address')? ' : <span class="text-danger">*</span>' : ' :'),
                            ['class' => 'control-label col-lg-2']
                        ) ?>
                        <?= $form->field($user, 'index', [
                            // Begin input theme
                            'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                            // End input theme
                            'inputOptions' => [
                                'class' => 'form-control'
                            ],
                            'options' => [
                                'class' => 'form-group'
                            ]
                        ])->hint(false)->label(
                            $user->getAttributeLabel('index') .
                            ($user->isAttributeRequired('index')? ' : <span class="text-danger">*</span>' : ' :'),
                            ['class' => 'control-label col-lg-2']
                        ) ?>
                        <div class="col-lg-10 col-lg-offset-2">
                            <div class="form-group">
                                <?= $form->field($user, 'notification_new_tours', [
                                    'template' => '<label class="checkbox-inline">{input}'. $user->getAttributeLabel('notification_new_tours') .'{hint}</label>',
                                    ])->label(false)->checkbox(['class' => 'styled'], false)->hint(false) ?>
                            </div>
                        </div>
                        <div class="col-lg-10 col-lg-offset-2">
                            <div class="form-group">
                                <?= $form->field($user, 'notification_comp_news', [
                                    'template' => '<label class="checkbox-inline">{input}'. $user->getAttributeLabel('notification_comp_news') .'{hint}</label>',
                                    ])->label(false)->checkbox(['class' => 'styled'], false)->hint(false) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="text-right">
        <button type="submit" name="btnAction" value="save" class="btn bg-teal-400 btn-sm">
            <i class="icon-checkmark3 position-left"></i> Сохранить
        </button>
        <button type="submit" name="btnAction" value="save-exit" class="btn btn-primary btn-sm">
            <i class="icon-undo2 mr-5"></i> Сохр. и закрыть
        </button>
        <button type="submit" name="btnAction" value="save-new" class="btn btn-primary btn-sm">
            <i class="icon-new-tab mr-5"></i> Сохр. и создать новую запись
        </button>
        <?= !$user->isNewRecord ? Html::a('<i class="icon-bin"></i>Удалить', [
            Yii::$app->controller->id . '/'. $user->id . '/remove'
        ], [
            'onclick' => "return confirm('Вы уверены что хотите удалить аккаунт?');",
            'class' => 'btn btn-danger btn-sm'
        ]) : false ?>
    </div>
    <? ActiveForm::end(); ?>
    </div><!-- /.panel-body -->
</div>