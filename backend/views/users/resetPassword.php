<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Востановления пароля';
$this->params['bodyClasses'] = 'login-container';

?>
<!-- Main content -->
<div class="content-wrapper">
	<!-- Content area -->
	<div class="content">
		<!-- Password recovery -->
		<? $form = ActiveForm::begin(['id' => 'password-reset-form']); ?>
			<div class="login-form">
				<div class="text-center">
					<div class="icon-object border-success text-success">
						<i class="icon-user-check"></i>
					</div>
					<h5 class="content-group">
						<?= $this->title ?>
						<small class="display-block">Пожалуйста, введите ваш новый пароль.</small>
					</h5>
				</div>
				<?= $form->field($ResetPassword, 'password',  [
					'template' => '{input}<div class="form-control-feedback"><i class="icon-user-lock text-muted"></i></div>{error}',
					'inputOptions' => [
						'class'        => 'form-control',
						'autocomplete' => 'off',
						'placeholder'  => $ResetPassword->getAttributeLabel('password')
					],
					'options' => [
						'class' => 'form-group has-feedback has-feedback-left'
					]
				])->passwordInput()->label(false)->hint(false) ?>
				<?= $form->field($ResetPassword, 'password_repeat',  [
					'template' => '{input}<div class="form-control-feedback"><i class="icon-user-lock text-muted"></i></div>{error}',
					'inputOptions' => [
						'class'        => 'form-control',
						'autocomplete' => 'off',
						'placeholder'  => $ResetPassword->getAttributeLabel('password_repeat')
					],
					'options' => [
						'class' => 'form-group has-feedback has-feedback-left'
					]
				])->passwordInput()->label(false)->hint(false) ?>
				<?= Html::submitButton('Сохранить пароль <i class="icon-arrow-right14 position-right"></i>', [
					'class' => 'btn btn-primary btn-block',
					'name'  => 'login-button'
				]) ?>
			</div>
		<?php ActiveForm::end(); ?>
		<!-- /password recovery -->
		<!-- Footer -->
		<div class="footer text-muted text-center">
			&copy; <?= date('Y') ?>. <?= \Yii::$app->id ?>
		</div>
		<!-- /footer -->
	</div>
	<!-- /content area -->
</div>
<!-- /main content -->