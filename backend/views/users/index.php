<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\bootstrap\ActiveForm;
use backend\models\Users;
use yii\bootstrap\Alert;

//$this->params['breadcrumbs'][] = $this->context->pageTitle;
$this->context->smallTitle = 'Управление зарегистрированными на сайте пользователями (создание, редактирование, блокировка аккаунта)';
?>

<!-- Table with togglable columns -->
<? if(count($users)): ?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Список всех аккаунтов</h5>
        <small class="display-block text-muted">
            Зарегистрировано: <?= $total ?> чел.
        </small>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>
    <div class="panel-body">
        <table class="table table-togglable table-hover">
            <thead>
                <tr>
                    <th data-toggle="true">#UID</th>
                    <th data-toggle="true">Имя/Фамилия</th>
                    <th data-hide="phone">Группа</th>
                    <th data-hide="phone,tablet">E-Mail</th>
                    <th data-hide="phone,tablet">Дата регистрации</th>
                    <th data-hide="phone,tablet">Дата редактирования</th>
                    <th data-hide="phone,tablet">Последный IP авторизации</th>
                    <th data-hide="phone">Последний визит</th>
                    <th data-hide="phone" class="text-center">Статус аккаунта</th>
                    <th class="text-center" style="width: 30px;">
                        <i class="icon-menu-open2" data-popup="tooltip" title="Действие" data-placement="left"></i>
                    </th>
                </tr>
            </thead>
            <tbody>
            <? foreach($users as $item): ?>
                <tr>
                    <td><?= $item->id ?></td>
                    <td><?= $item->nameTogether ?></td>
                    <td><?= $item->groupName ?></td>
                    <td><?= $item->email ?></td>
                    <td><?= (new DateTime())->setTimestamp($item->created_timestamp)->format('d.m.Y H:i:s') ?></td>
                    <td><?= (new DateTime())->setTimestamp($item->updated_timestamp)->format('d.m.Y H:i:s') ?></td>
                    <td><?= $item->last_auth_ip ?></td>
                    <td><?= (new DateTime())->setTimestamp($item->auth_timestamp)->format('d.m.Y H:i:s') ?></td>
                    <td class="text-center">
                        <span class="label <?= $item->getStatusLabel() ?>">
                            <?= $item->getStatusName() ?>
                        </span>
                    </td>
                    <td class="text-center">
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Действия">
                                    <i class="icon-menu7"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <? if(Yii::$app->user->can('accessProfile') || Yii::$app->user->identity->id == $item->id): ?>
                                    <li>
                                        <?= Html::a('<i class="icon-user"></i> Профиль', [
                                            Yii::$app->controller->id . '/'. $item->id . '/profile'
                                        ]) ?>
                                    </li>
                                    <? endif; ?>
                                    <li>
                                        <?= Html::a('<i class="icon-user-plus"></i> Редактировать', [
                                            Yii::$app->controller->id . '/' . $item->id . '/edit'
                                        ]) ?>
                                    </li>
                                    <? if($item->id != Yii::$app->user->identity->id && $item->status != $item::STATUS_BLOCKED): ?>
                                    <li>
                                        <?= Html::a('<i class="icon-user-block"></i> Заблокировать', [
                                            Yii::$app->controller->id . '/'. $item->id .'/blocked'
                                        ], [
                                            'onclick' => "return confirm('Вы уверены что хотите заблокировать аккаунт?');"
                                        ]) ?>
                                    </li>
                                    <? endif; ?>
                                    <? if(Yii::$app->user->can('changePassword')): ?>
                                    <li>
                                        <?= Html::a('<i class="icon-user-lock"></i> Изменить пароль', [
                                            Yii::$app->controller->id . '/' . $item->id . '/passwd'
                                        ], [
                                            'onclick' => "return confirm('Вы уверены что хотите изменить пароль?');"
                                        ]) ?>
                                    </li>
                                    <? endif; ?>
                                    <li>
                                        <?= Html::a('<i class="icon-user-cancel"></i> Удалить', [
                                            Yii::$app->controller->id . '/'. $item->id .'/remove'
                                        ], [
                                            'onclick' => "return confirm('Вы уверены что хотите удалить данные?');"
                                        ]) ?>
                                    </li>
                                </ul>
                            </li>
                        </ul><!-- /.icons-list -->
                    </td>
                </tr>
            <? endforeach; ?>
            </tbody>
        </table>
    </div><!-- /.panel-body -->
</div>
<? else: Alert::begin(['options' => ['class' => 'bg-info alert-styled-left']]); ?>
    Не удается найти пользователей для отображения
<? Alert::end(); endif;?>
<!-- /table with togglable columns -->