<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Войдите в свой аккаунт';
$this->params['bodyClasses'] = 'login-container';

?>
<!-- Main content -->
<div class="content-wrapper">
    <!-- Content area -->
    <div class="content">
        <!-- Simple login form -->
        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
            <div class="login-form">
                <div class="text-center">
                    <div class="icon-object border-slate-300 text-slate-300">
                        <i class="icon-reading"></i>
                    </div>
                    <h5 class="content-group">
                        <?= $this->title ?>
                        <small class="display-block">Введите учетные данные ниже</small>
                    </h5>
                </div>
                <?= $form->field($Signin, 'email',  [
                    'template' => '{input}<div class="form-control-feedback"><i class="icon-mention text-muted"></i></div>{error}',
                    'inputOptions' => [
                        'class'        => 'form-control',
                        'autocomplete' => 'off',
                        'placeholder'  => $Signin->getAttributeLabel('email'),
                    ],
                    'options' => [
                        'class' => 'form-group has-feedback has-feedback-left'
                    ]
                ])->label(false)->hint(false) ?>
                <?= $form->field($Signin, 'password',  [
                    'template' => '{input}<div class="form-control-feedback"><i class="icon-lock2 text-muted"></i></div>{error}',
                    'inputOptions' => [
                        'class'        => 'form-control',
                        'autocomplete' => 'off',
                        'placeholder'  => $Signin->getAttributeLabel('password')
                    ],
                    'options' => [
                        'class' => 'form-group has-feedback has-feedback-left'
                    ]
                ])->passwordInput()->label(false)->hint(false) ?>
                <div class="form-group">
                    <?= $form->field($Signin, 'rememberMe', [
                        'template' => '<label class="checkbox-inline">{input}'. $Signin->getAttributeLabel('rememberMe') .'? </label>',
                    ])->label(false)->checkbox(['class' => 'styled'], false) ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Войти <i class="icon-circle-right2 position-right"></i>', [
                        'class' => 'btn btn-primary btn-block',
                        'name'  => 'login-button'
                    ]) ?>
                </div>
                <div class="text-center">
                    <?= Html::a('Забыли пароль?', ['users/request-password-reset']) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
        <!-- /simple login form -->
        <!-- Footer -->
        <div class="footer text-muted text-center">
            &copy; <?= date('Y') ?>. <?= \Yii::$app->id ?>
        </div>
        <!-- /footer -->
    </div>
    <!-- /content area -->
</div>
<!-- /main content -->