<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Забыли пароль?';
$this->params['bodyClasses'] = 'login-container';

?>
<!-- Main content -->
<div class="content-wrapper">
	<!-- Content area -->
	<div class="content">
		<!-- Password recovery -->
		<? $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
			<div class="login-form">
				<div class="text-center">
					<div class="icon-object border-warning text-warning">
						<i class="icon-spinner11"></i>
					</div>
					<h5 class="content-group">
						<?= $this->title ?>
						<small class="display-block">Мы вышлем вам инструкции по электронной почте</small>
					</h5>
				</div>
				<?= $form->field($PasswordReset, 'email',  [
					'template' => '{input}<div class="form-control-feedback"><i class="icon-mail5 text-muted"></i></div>{error}',
					'inputOptions' => [
						'class'        => 'form-control',
						'autocomplete' => 'off',
						'placeholder'  => $PasswordReset->getAttributeLabel('email'),
					],
					'options' => [
						'class' => 'form-group has-feedback'
					]
				])->label(false)->hint(false) ?>
				<?= Html::submitButton('Сброс пароля <i class="icon-arrow-right14 position-right"></i>', [
					'class' => 'btn btn-primary btn-block',
					'name'  => 'login-button'
				]) ?>
			</div>
			<div class="text-center">
				<?= Html::a('<i class="icon-arrow-left13 position-left"></i> Вернуться к форме входа', ['users/signin'], [
					'class' => 'btn btn-link'
				]) ?>
			</div>
		<?php ActiveForm::end(); ?>
		<!-- /password recovery -->
		<!-- Footer -->
		<div class="footer text-muted text-center">
			&copy; <?= date('Y') ?>. <?= \Yii::$app->id ?>
		</div>
		<!-- /footer -->
	</div>
	<!-- /content area -->
</div>
<!-- /main content -->