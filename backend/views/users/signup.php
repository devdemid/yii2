<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Регистрация';
$this->params['bodyClasses'] = 'login-container';

?>
<!-- Main content -->
<div class="content-wrapper">
    <!-- Content area -->
    <div class="content">
        <!-- Registration form -->
        <?php $form = ActiveForm::begin(['id' => 'signup-form']); ?>
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                    <div class="registration-form">
                        <div class="panel-body">
                            <div class="text-center">
                                <div class="icon-object border-success text-success">
                                    <i class="icon-plus3"></i>
                                </div>
                                <h5 class="content-group-lg">
                                    <?= $this->title ?>
                                    <small class="display-block">Все поля обязательны для заполнения</small>
                                </h5>
                            </div>
                            <?= $form->field($signup, 'phone',  [
                                'template' => '{input}<div class="form-control-feedback"><i class="icon-mobile text-muted"></i></div>{error}',
                                'inputOptions' => [
                                    'class'        => 'form-control',
                                    'autocomplete' => 'off',
                                    'placeholder'  => $signup->getAttributeLabel('phone'),
                                ],
                                'options' => [
                                    'class' => 'form-group has-feedback'
                                ]
                            ])->label(false)->hint(false) ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <?= $form->field($signup, 'first_name',  [
                                        'template' => '{input}<div class="form-control-feedback"><i class="icon-user-check text-muted"></i></div>{error}',
                                        'inputOptions' => [
                                            'class'        => 'form-control',
                                            'autocomplete' => 'off',
                                            'placeholder'  => $signup->getAttributeLabel('first_name'),
                                        ],
                                        'options' => [
                                            'class' => 'form-group has-feedback'
                                        ]
                                    ])->label(false)->hint(false) ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($signup, 'second_name',  [
                                        'template' => '{input}<div class="form-control-feedback"><i class="icon-user-check text-muted"></i></div>{error}',
                                        'inputOptions' => [
                                            'class'        => 'form-control',
                                            'autocomplete' => 'off',
                                            'placeholder'  => $signup->getAttributeLabel('second_name'),
                                        ],
                                        'options' => [
                                            'class' => 'form-group has-feedback'
                                        ]
                                    ])->label(false)->hint(false) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <?= $form->field($signup, 'password',  [
                                        'template' => '{input}<div class="form-control-feedback"><i class="icon-user-lock text-muted"></i></div>{error}',
                                        'inputOptions' => [
                                            'class'        => 'form-control',
                                            'autocomplete' => 'off',
                                            'placeholder'  => $signup->getAttributeLabel('password'),
                                        ],
                                        'options' => [
                                            'class' => 'form-group has-feedback'
                                        ]
                                    ])->passwordInput()->label(false)->hint(false) ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($signup, 'password_repeat',  [
                                        'template' => '{input}<div class="form-control-feedback"><i class="icon-user-lock text-muted"></i></div>{error}',
                                        'inputOptions' => [
                                            'class'        => 'form-control',
                                            'autocomplete' => 'off',
                                            'placeholder'  => $signup->getAttributeLabel('password_repeat'),
                                        ],
                                        'options' => [
                                            'class' => 'form-group has-feedback'
                                        ]
                                    ])->passwordInput()->label(false)->hint(false) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <?= $form->field($signup, 'email',  [
                                        'template' => '{input}<div class="form-control-feedback"><i class="icon-mention text-muted"></i></div>{error}',
                                        'inputOptions' => [
                                            'class'        => 'form-control',
                                            'autocomplete' => 'off',
                                            'placeholder'  => $signup->getAttributeLabel('email'),
                                        ],
                                        'options' => [
                                            'class' => 'form-group has-feedback'
                                        ]
                                    ])->label(false)->hint(false) ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($signup, 'email_repeat',  [
                                        'template' => '{input}<div class="form-control-feedback"><i class="icon-mention text-muted"></i></div>{error}',
                                        'inputOptions' => [
                                            'class'        => 'form-control',
                                            'autocomplete' => 'off',
                                            'placeholder'  => $signup->getAttributeLabel('email_repeat'),
                                        ],
                                        'options' => [
                                            'class' => 'form-group has-feedback'
                                        ]
                                    ])->label(false)->hint(false) ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="styled" checked="checked">
                                        Send me <a href="#">test account settings</a>
                                    </label>
                                </div>
                            </div>
                            <div class="text-right">
                                <?= Html::a('<i class="icon-arrow-left13 position-left"></i> Вернуться к форме входа', ['users/signin'], [
                                    'class' => 'btn btn-link'
                                ]) ?>
                                <?= Html::submitButton('<b><i class="icon-plus3"></i></b> Регистрация', [
                                    'class' => 'btn bg-teal-400 btn-labeled btn-labeled-right ml-10',
                                    'name'  => 'signup-button'
                                ]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
        <!-- /registration form -->
        <!-- Footer -->
        <div class="footer text-muted text-center">
            &copy; <?= date('Y') ?>. <?= \Yii::$app->id ?>
        </div>
        <!-- /footer -->
    </div>
    <!-- /content area -->
</div>
<!-- /main content -->