<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\User;
use common\models\ResetPasswordForm;

$this->context->breadcrumbs[] = [
    'label' => 'Все аккаунты',
    'url'   => '/'. Yii::$app->controller->id .'/index'
];

$this->context->breadcrumbs[] = [
    'label' => 'Редактировать аккаунт',
    'url'   => '/users/'. $user->id .'/edit'
];

$this->context->view->title = 'Изменить пароль';
$this->context->smallTitle = 'Вы можете изменить пароль для защиты своего аккаунта. Если же вы забыли пароль, его можно сбросить.';

$this->context->breadcrumbs[] = $this->context->view->title;

?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title"><?= $user->nameTogether ?> </h5>
    </div>
    <div class="panel-body">
    <? $form = ActiveForm::begin([
        'id' => $this->context->id . '-form',
        'options' => [
            'role'    => 'form',
            'class' => 'form-horizontal'
        ]
    ]); ?>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($ResetPassword, 'password', [
                // Begin input theme
                'template' => '{label}<div class="col-lg-10">{input}<div class="form-control-feedback"><i class="icon-user-lock text-muted"></i></div>{hint}{error}</div>',
                // End input theme
                'inputOptions' => [
                    'class' => 'form-control',
                    'autocomplete' => 'off',
                ],
                'options' => [
                    'class' => 'form-group has-feedback'
                ]
            ])->hint(false)->label(
                $ResetPassword->getAttributeLabel('password') .
                ($ResetPassword->isAttributeRequired('password')? ' : <span class="text-danger">*</span>' : ' :'),
                ['class' => 'control-label col-lg-2']
            ) ?>

            <?= $form->field($ResetPassword, 'password_repeat', [
                // Begin input theme
                'template' => '{label}<div class="col-lg-10">{input}<div class="form-control-feedback"><i class="icon-user-lock text-muted"></i></div>{hint}{error}</div>',
                // End input theme
                'inputOptions' => [
                    'class' => 'form-control',
                    'autocomplete' => 'off',
                ],
                'options' => [
                    'class' => 'form-group has-feedback'
                ]
            ])->hint(false)->label(
                $ResetPassword->getAttributeLabel('password_repeat') .
                ($ResetPassword->isAttributeRequired('password_repeat')? ' : <span class="text-danger">*</span>' : ' :'),
                ['class' => 'control-label col-lg-2']
            )->passwordInput() ?>
            <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                    <label class="display-block checkbox-inline" for="send_email">
                        <?= Html::checkbox('send_email', $send_email, [
                            'id' => 'send_email',
                            'class' => 'styled'
                        ]) ?>
                        Отправить пароль на почту аккаунта
                    </label>
                </div>
            </div>
        </div>
        <div class="col-lg-offset-2 col-lg-10">
            <?= Html::button('<b><i class="icon-dice"></i></b>Сгенерировать пароль', [
                'class' => 'btn bg-slate-400 btn-labeled btn-xs',
                'onclick' => "generatePasswd(); return false;"
            ]) ?>
        </div>
    </div>
    <hr>
    <div class="text-right">
        <button type="submit" name="btnAction" value="save" class="btn bg-teal-400 btn-sm">
            <i class="icon-checkmark3 position-left"></i> Сохранить
        </button>
        <button type="submit" name="btnAction" value="save-exit" class="btn btn-primary btn-sm">
            <i class="icon-undo2 mr-5"></i> Сохр. и закрыть
        </button>
    </div>
    <? ActiveForm::end(); ?>
    </div><!-- /.panel-body -->
</div>
<script>
    function generatePasswd() {
        $('input#<?= Html::getInputId($ResetPassword, 'password'); ?>, input#<?= Html::getInputId($ResetPassword, 'password_repeat'); ?>').val(randomPassword(<?= ResetPasswordForm::MIN_PASSWORD_STRING ?>,<?= ResetPasswordForm::MAX_PASSWORD_STRING ?>));
    }
</script>