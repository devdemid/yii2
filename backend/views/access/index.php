<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

// Set the component description
$this->context->view->title = 'Группы пользователей';
$this->context->breadcrumbs[] = $this->context->view->title;
$this->context->smallTitle = 'Создание и управление различными группами пользователей на сайте, назначение прав доступа для этих групп';

?>

<div class="panel panel-body">
<?php $form = ActiveForm::begin(['id' => 'access-form']); ?>
    <table class="table table-togglable table-hover mb-20">
            <thead>
                <tr>
                    <th data-toggle="true">Разрешение</th>
                    <?php if(count($roles)): ?>
                    <?php foreach($roles as $role): ?>
                        <th data-hide="phone" class="text-center"><?= $role->description ?></th>
                    <?php endforeach; ?>
                    <?php endif;?>
                </tr>
            </thead>
            <tbody>
            <?php if(count($rolePermissions)): ?>
                <?php foreach($rolePermissions as $permission => $item): ?>
                <tr>
                    <td><?= $item['description'] ?></td>
                    <?php  foreach($item['roles'] as $role => $value): ?>
                    <td class="text-center">
                        <div class="checkbox-inline">
                            <label>
                                <?= Html::checkbox('Access['. $role .']['. $permission .']', $value, ArrayHelper::merge(
                                    ['class' => $value ? 'control-success' : 'styled'],
                                    ($role == 'root' ? ['disabled' => 'disabled'] : [])
                                )); ?>
                            </label>
                        </div>
                    </td>
                    <?php endforeach; ?>
                </tr>
                <?php endforeach;?>
            <?php endif;?>
            </tbody>
        </table><!-- /table with togglable columns -->
        <hr>
        <div class="text-right">
            <button type="submit" name="btnAction" value="save" class="btn bg-teal-400 btn-sm">
                <i class="icon-checkmark3 position-left"></i> Сохранить
            </button>
            <button type="submit" name="btnAction" value="save-exit" class="btn btn-primary btn-sm">
                <i class="icon-undo2 mr-5"></i> Сохр. и закрыть
            </button>
            <button type="reset" class="btn btn-default btn-sm">
                <i class="icon-reload-alt mr-5"></i> Отмена изменений
            </button>
        </div>
<? ActiveForm::end(); ?>
</div>