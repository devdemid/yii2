<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use common\models\Slider;
use yii\helpers\Url;

// Set the component description
$this->context->view->title = $this->context->view->title;
$this->context->smallTitle = 'Все слайды которые относятся к этому слайдеру их можно заменить, удалить или добавить новый';

$this->context->breadcrumbs[] = [
    'label' => 'Генератор слайдеров',
    'url'   => '/slider/index'
];

$this->context->breadcrumbs[] = $this->context->view->title;

?>

<? if(count($slides)): ?>
<!-- Media library -->
<div class="panel panel-white">
    <div class="panel-heading">
        <h5 class="panel-title">
            Галерея слайдов
        </h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>
    <table class="table table-striped media-library table-lg">
        <thead>
            <tr>
                <th width="5%">#ID</th>
                <th>Изображение</th>
                <th>Ссылка</th>
                <th class="text-center">Открыть в новом окне</th>
                <th class="text-center" data-toggle="true">Публикация</th>
                <th class="text-center" style="width: 30px;">
                    <i class="icon-menu-open2" data-popup="tooltip" title="Действие" data-placement="left"></i>
                </th>
            </tr>
        </thead>
        <tbody>
        <? foreach($slides as $item): ?>
            <tr>
                <td><?= $item->id ?></td>
                <td>
                    <?= Html::a('<img src="'. $item->getPathImage(Slider::IMAGE_SMALL_SIZE) . '" alt="" class="img-rounded img-preview">', [Yii::$app->controller->id . '/' . $slider->id . '/item?slide=' .$item->id]) ?>
                </td>
                <td>
                    <? if($item->url): ?>
                        <?= Html::a($item->url, $item->url, ['target' => '_blank']) ?>
                    <? else: ?>
                        <span class="text-muted">Нет</span>
                    <? endif;  ?>
                </td>
                <td class="text-center">
                    <? if($item->target): ?>
                        <i class="icon-checkmark3 text-success"></i>
                    <? else: ?>
                        <i class="icon-cross2 text-danger"></i>
                    <? endif; ?>
                </td>
                <td class="text-center">
                    <div class="checkbox checkbox-switchery switchery-xs" title="Вкл/Выкл">
                        <label>
                            <?= Html::checkbox('Published['. $item->id .']', $item->published, [
                                'class' => 'switchery',
                                'onclick' => "ChangeRequest('" . Url::to([
                                    Yii::$app->controller->id . '/published-slide',
                                    'id' => $item->id
                                ]) . "', {published: this.checked});"
                            ]); ?>
                        </label>
                    </div>
                </td>
                <td class="text-center">
                    <ul class="icons-list">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Действия">
                                <i class="icon-menu9"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li>
                                    <?= Html::a('<i class="icon-pencil5"></i>Редактировать', [
                                        Yii::$app->controller->id . '/' . $slider->id . '/item?slide=' .$item->id
                                    ]) ?>
                                </li>
                                <li>
                                    <?= Html::a('<i class="icon-bin"></i>Удалить', [
                                        Yii::$app->controller->id . '/item-remove?id=' . $item->id
                                    ], [
                                        'onclick' => "return confirm('Вы уверены что хотите удалить данные?');"
                                    ]) ?>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </td>
            </tr>
        <? endforeach; ?>
        </tbody>
    </table>
    <div class="panel-footer">
        <div class="heading-elements text-right">
            <?= Html::a('<i class="icon-sort mr-5"></i> Сортировать', [
                Yii::$app->controller->id . '/'. $slider->id .'/sorting'
            ], [
                'class' => 'btn btn-default btn-xs'
            ]) ?>
        </div>
    </div>
</div>
<!-- /media library -->
<div class="mb-20">
    <?= LinkPager::widget([
        'pagination' => $pages,
        'options'    => [
            'class' => 'pagination pagination-xs'
        ]
    ]); ?>
</div>
<? else: Alert::begin(['options' => ['class' => 'bg-info alert-styled-left']]); ?>
    Не удается найти слайды для отображения
<? Alert::end(); endif;?>
<!-- /table with togglable columns -->