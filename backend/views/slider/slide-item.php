<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use backend\widgets\FilesWidget;
use common\models\Slide;
use common\models\Slider;
use \backend\widgets\ExtendFieldForm;

$this->context->breadcrumbs[] = [
    'label' => 'Генератор слайдеров',
    'url'   => '/slider/index'
];

$this->context->breadcrumbs[] = [
    'label' => $slider->name,
    'url'   => '/slider/'. $slider->id .'/items'
];

$this->context->breadcrumbs[] = $this->context->view->title;

if($model->isNewRecord) {
    $this->context->smallTitle = 'Создание и настройка нового слайда';
} else {
    $this->context->smallTitle = 'Редактирование и настройка слайда';
}

?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title"><?= $this->context->view->title ?></h5>
    </div>
    <div class="panel-body">
    <? $form = ActiveForm::begin([
        'id' => $this->context->id . '-form',
        'options' => [
            'role'    => 'form',
            'enctype' => 'multipart/form-data',
            'class' => 'form-horizontal'
        ]
    ]); ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="tabbable">
                    <ul class="nav nav-tabs nav-tabs-bottom">
                        <li class="active">
                            <a href="#<?= Html::getInputId($model, 'basic') ?>" data-toggle="tab">
                                Основные поля
                            </a>
                        </li>
                        <? if($model->slider->isFields): ?>
                        <li>
                            <a href="#<?= Html::getInputId($model, 'additional_fields') ?>" data-toggle="tab">
                                Дополнительные поля
                            </a>
                        </li>
                        <? endif; ?>
                    </ul>
                    <div class="tab-content pt-5">
                        <div class="tab-pane active" id="<?= Html::getInputId($model, 'basic') ?>">
                            <!-- Begin files module -->
                            <div class="form-group">
                                <label for="" class="control-label col-lg-2">Изображение:</label>
                                <div class="col-lg-10">
                                    <?= FilesWidget::widget([
                                        'id' => $model->id,
                                        'type' => \common\models\Files::TYPE_IMAGE,
                                        'files' => $model->prewiewsWidget,
                                        'module' => Slide::className(),
                                        'sizes' => [$slider->size, Slider::IMAGE_SMALL_SIZE],
                                        'hint' => 'Изображение слайда, который будет отображаться в слайдере'
                                    ]) ?>
                                </div>
                            </div>
                            <!-- End files module -->
                           <?= $form->field($model, 'url', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control',
                                    'placeholder' => 'Например: http(s)://'. Yii::$app->request->serverName .'/page.html'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint(false)->label(
                                $model->getAttributeLabelRequired('url'),
                                ['class' => 'control-label col-lg-2']
                            ) ?>

                            <?= $form->field($model, 'video', [
                                // Begin input theme
                                'template' => '{label}<div class="col-lg-10">{input}{hint}{error}</div>',
                                // End input theme
                                'inputOptions' => [
                                    'class' => 'form-control',
                                    'placeholder' => 'Например: https://www.youtube.com/watch?v=9bZkp7q19f0'
                                ],
                                'options' => [
                                    'class' => 'form-group'
                                ]
                            ])->hint('Просто добавьте URL своего видео на слайд YouTube или Vimeo, а модуль добавит его в ваше слайд-шоу.')->label(
                                $model->getAttributeLabelRequired('video'),
                                ['class' => 'control-label col-lg-2']
                            ) ?>
                            <div class="col-sm-12">
                                <div class="col-lg-10 col-lg-offset-2">
                                    <div class="form-group">
                                        <?= $form->field($model, 'target', [
                                            'template' => '<label class="checkbox-inline">{input}'. $model->getAttributeLabel('target') .'{hint}</label>',
                                        ])->label(false)->checkbox(['class' => 'styled'], false)->hint('По умолчанию, при переходе по ссылке документ открывается в текущем окне или фрейме. При необходимости, это условие может быть изменено этим параметром ссылка будет открына в новом окне или фрейме') ?>
                                    </div>
                                </div>
                                <div class="col-lg-10 col-lg-offset-2">
                                    <div class="form-group">
                                        <?= $form->field($model, 'published', [
                                            'template' => '<label class="checkbox-inline">{input}'. $model->getAttributeLabel('published') .'{hint}</label>',
                                        ])->label(false)->checkbox(['class' => 'styled'], false)->hint('Включает отображения слайда на страницах сайта') ?>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.tab-pane -->
                        <? if($model->slider->isFields): ?>
                        <div class="tab-pane" id="<?= Html::getInputId($model, 'additional_fields') ?>">
                            <?= ExtendFieldForm::widget([
                                'id' => $model->id,
                                'model' => $field_model,
                                'options' => $model->slider->field_options
                            ]) ?>
                        </div><!-- /.tab-pane -->
                        <? endif; ?>
                    </div><!-- /.tab-content -->
            </div>
        </div>
    </div>
    <hr>
    <div class="text-right">
        <button type="submit" name="btnAction" value="save" class="btn bg-teal-400 btn-sm">
            <i class="icon-checkmark3 position-left"></i> Сохранить
        </button>
        <button type="submit" name="btnAction" value="save-exit" class="btn btn-primary btn-sm">
            <i class="icon-undo2 mr-5"></i> Сохр. и закрыть
        </button>
        <button type="submit" name="btnAction" value="save-new" class="btn btn-primary btn-sm">
            <i class="icon-new-tab mr-5"></i> Сохр. и создать новую запись
        </button>
        <?= !$model->isNewRecord ? Html::a('<i class="icon-bin"></i>Удалить', [
            Yii::$app->controller->id . '/item-remove?id='. $model->id
        ], [
            'onclick' => "return confirm('Вы уверены что хотите удалить данные?');",
            'class' => 'btn btn-danger btn-sm'
        ]) : false ?>
    </div>
    <? ActiveForm::end(); ?>
    </div><!-- /.panel-body -->
</div>