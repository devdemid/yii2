<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use common\models\Slider;
use yii\helpers\Url;

// Set the component description
$this->context->view->title = $this->context->view->title;
$this->context->smallTitle = 'Все слайды которые относятся к этому слайдеру будут отсортированы в указаном порядке';

$this->context->breadcrumbs[] = [
    'label' => 'Генератор слайдеров',
    'url'   => '/slider/index'
];

$this->context->breadcrumbs[] = [
    'label' => $sliderModel->name,
    'url'   => '/slider/' . $sliderModel->id . '/items'
];

$this->context->breadcrumbs[] = $this->context->view->title;

?>

<? if(count($slidesModel)): ?>
<? $form = ActiveForm::begin([
    'id' => $this->context->id . '-form',
    'options' => [
        'role'    => 'form'
    ]
]); ?>
<!-- Media library -->
<div class="panel panel-white">
    <div class="panel-heading">
        <h5 class="panel-title">
            <?= $this->context->view->title ?> <small><?= $sliderModel->name ?></small>
        </h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>
    <table class="table table-striped media-library table-lg">
        <thead>
            <tr>
                <th width="5%">#ID</th>
                <th>Предпросмотр</th>
                <th>Ссылка</th>
                <th class="text-center">Открыть в новом окне</th>
                <th class="text-center" data-toggle="true">Включение</th>
                <th class="text-center" data-toggle="true" style="width: 150px">Позиция</th>
            </tr>
        </thead>
        <tbody>
        <? foreach($slidesModel as $item): ?>
            <tr>
                <td><?= $item->id ?></td>
                <td>
                    <?= Html::a('<img src="'. $item->getPathImage(Slider::IMAGE_SMALL_SIZE) . '" alt="" class="img-rounded img-preview">', $item->pathImage, ['target' => '_blank']) ?>
                </td>
                <td>
                    <? if($item->url): ?>
                        <?= Html::a($item->url, $item->url, ['target' => '_blank']) ?>
                    <? else: ?>
                        <span class="text-muted">Нет</span>
                    <? endif;  ?>
                </td>
                <td class="text-center">
                    <? if($item->target): ?>
                        <i class="icon-checkmark3 text-success"></i>
                    <? else: ?>
                        <i class="icon-cross2 text-danger"></i>
                    <? endif; ?>
                </td>
                <td class="text-center">
                    <div class="checkbox checkbox-switchery switchery-xs" title="Вкл/Выкл">
                        <label>
                            <?= Html::checkbox('Published['. $item->id .']', $item->published, [
                                'class' => 'switchery',
                                'onclick' => "ChangeRequest('" . Url::to([
                                    Yii::$app->controller->id . '/published-slide',
                                    'id' => $item->id
                                ]) . "', {published: this.checked});"
                            ]); ?>
                        </label>
                    </div>
                </td>
                <td class="text-center">
                    <?= Html::textInput('Sorting[' . $item->id . ']', $item->sorting, [
                        'class' => 'form-control text-center input-xs',
                        'placeholder' => '0'
                    ]) ?>
                </td>
            </tr>
        <? endforeach; ?>
        </tbody>
    </table>
    <div class="panel-footer">
        <div class="heading-elements">
            <span class="heading-text text-semibold">Результат сортировки:</span>
            <button type="submit" name="btnAction" value="save-exit" class="btn btn-primary heading-btn pull-right btn-sm">
                <i class="icon-undo2 mr-5"></i> Сохр. и закрыть
            </button>
            <button type="submit" class="btn bg-teal-400 heading-btn pull-right btn-xs">
                <i class="icon-checkmark3 position-left"></i> Сохранить
            </button>
        </div>
        <a class="heading-elements-toggle"><i class="icon-more"></i></a>
    </div>
</div>
<? ActiveForm::end(); ?>
<!-- /media library -->
<? else: Alert::begin(['options' => ['class' => 'bg-info alert-styled-left']]); ?>
    Не удается найти слайды для отображения
<? Alert::end(); endif;?>
<!-- /table with togglable columns -->