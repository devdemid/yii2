<?php
/**
 *
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace backend\widgets;

use Yii;
use yii\base\Widget;
use yii\web\Response;
use yii\helpers\ArrayHelper;

class Notify extends Widget {

    /**
     * [set description]
     * @param [type] $type   [description]
     * @param [type] $text   [description]
     * @param array
     */
    public function set($type, $text, $params = []) {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $Return = [
            'Notify' => [
                'text' => $text,
                'type' => $type,
            ]
        ];

        if(is_array($params) && count($params)) {
            $Return = ArrayHelper::merge($Return, $params);
        }

        return $Return;
    }

    /**
     * [danger description]
     * @param  [type] $text   [description]
     * @param  array  $params [description]
     * @param array
     */
    public function danger($text, $params = []) {
        return self::set('danger', $text, $params);
    }

    /**
     * [success description]
     * @param  [type] $text   [description]
     * @param  array  $params [description]
     * @param array
     */
    public function success($text, $params = []) {
        return self::set('success', $text, $params);
    }

}