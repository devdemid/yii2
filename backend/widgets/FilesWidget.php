<?php
/**
 * Widget for download in the admin panel files
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.1
 */

namespace backend\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use \common\models\Files;
use \yii\helpers\StringHelper;

class FilesWidget extends Widget {

    /**
     * A unique record ID for which the file will be assigned
     * @var intval
     */
    public $id;

    /**
     * The module to which the file
     * @var string
     */
    public $module;

    /**
     * A list of downloaded file...
     * @var array
     */
    public $files;

    /**
     * File type [Files::TYPE_IMAGE, Files::TYPE_FILE]
     * @var intval
     */
    public $type;

    /**
     * Description for downloading files
     * @var string
     */
    public $hint;

    /**
     * If the file is an image, and then you need to cut the size of cutting
     * @var array
     */
    public $sizes = [];

    /**
     * Scenario name
     * @var string
     */
    private $scenario;

    /**
     * Disable file downloading
     * @var boolean
     */
    public $disabled = false;

    /**
     * Various additional information
     * @var array
     */
    public $labels = [];


    public function init() {

        // File Download Script
        if($this->type == Files::TYPE_IMAGE) {
            $this->scenario = Files::SCENARIO_IMAGE;
        } else {
            $this->scenario = Files::SCENARIO_FILE;
        }

        parent::init();
    }

    public function run() {
        $returnHtml = Html::fileInput('Files[uploadedFile]', '', ArrayHelper::merge([
            'class' => 'file-input-ajax',
            'id' => StringHelper::basename($this->module) . '_' . $this->type . '_' . $this->id,
            'data-lang' => 'ru', // TODO: Temporarily!
            'data-upload' => '/upload/' . $this->scenario,
            'data-params' => [
                Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
                'Files[item_id]' => $this->id ? $this->id : 0,
                'Files[module]' => StringHelper::basename($this->module),
                'Files[type]' => $this->type,
                'Files[sizes]' => $this->type === Files::TYPE_IMAGE ? $this->sizes : [],
            ],
            'data-preview' => $this->files,
            'data-maxFileCount' => Yii::$app->params['upload'][$this->scenario]['maxFiles'],
            'data-show-remove' => 'true',
            'data-show-caption' => 'true',
        ],
            ($this->disabled ? ['disabled' => 'disabled'] : [])
        ));

        // If there is a description of his show
        if ($this->hint) {
            $returnHtml .= Html::tag('div', Html::encode($this->hint), [
                'class' => 'help-block',
            ]);
        }

        // Various additional information
        if(count($this->labels)) {
            $label = '';
            foreach ($this->labels as $key => $value) {
                $label .= "<li><span class='text-muted'>{$key}</span>: {$value}</li>\n";
            }

            // Info template
            $returnHtml .= '<ul class="list-square mt-20">';
            $returnHtml .=     $label;
            $returnHtml .= '</ul>';
        }

        return $returnHtml;
    }
}