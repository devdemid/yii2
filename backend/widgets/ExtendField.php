<?php
/**
 * Output widget for additional fields
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace backend\widgets;

use backend\models\NewExtendField;
use backend\models\Field;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class ExtendField extends Widget {

    /**
     * Additional field settings
     * @var string
     */
    public $options;

    /**
     * Additional field settings
     * @var array
     */
    private $fields = [];

    /**
     * [$id description]
     * @var [type]
     */
    public $id;

    /**
     * [$controller description]
     * @var [type]
     */
    public $controller;

    /**
     * Fields available for this module
     * @var array
     */
    public $types = [];

    /**
     * @inheritdoc
     * @return array
     */
    public function init() {
        parent::init();

        if (!empty($this->options)) {
            $this->fields = Json::decode($this->options);
        }

        // Begin Controlling Available Field Types
        if (count($this->types)) {
            $_types = [];
            foreach ($this->types as $type) {
                $_types[$type] = Field::getTypeName($type);
            }
            $this->types = $_types;
            unset($_types);
        } else {
            $this->types = Field::getTypes();
        }
        // End Controlling Available Field Types
    }

    public function run() {
        return $this->render('extend-field', [
            'model' => new NewExtendField,
            'fields' => ArrayHelper::index($this->fields, function ($element) {
                return md5($element['param']);
            }),
        ]);
    }
}