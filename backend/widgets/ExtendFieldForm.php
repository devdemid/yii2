<?php
/**
 * Output widget for additional fields
 * @author Demid [Alexander Pilipenko] <webdemid@gmail.com>
 * @since 1.0
 */

namespace backend\widgets;

use backend\models\Fields;
use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use backend\models\Field;

class ExtendFieldForm extends Widget {

    /**
     * Additional field settings
     * @var string
     */
    public $options;

    /**
     * Model of dynamic fields
     * @var backend\models\Field;
     */
    public $model;

    /**
     * Records ID
     * @var integer
     */
    public $id;

    /**
     * @inheritdoc
     * @return array
     */
    public function init() {
        if (!empty($this->options)) {
            $this->options = Json::decode($this->options);

            foreach ($this->options as $i => $item) {
                $list = [];
                if($item['type'] == Field::TYPE_SELECT) {
                    foreach (explode(Field::LIST_SEPARATOR, $item['list']) as $value) {
                        $list[$value] = $value;
                    }
                }

                $this->options[$i]['list'] = $list;
                unset($list);
            }
        }

        parent::init();
    }

    public function run() {
        return $this->render('extend-field-form', [
            'id' => $this->id,
            'model' => $this->model,
            'options' => $this->options,
        ]);
    }

}