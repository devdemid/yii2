<?php

use yii\bootstrap\Alert;
use yii\helpers\Html;
use backend\models\Field;
use \yii\helpers\StringHelper;
use yii\helpers\ArrayHelper;
use \common\models\Files;

?>
<? if(count($options)): ?>
    <? foreach($options as $item): ?>
    <? $item_prop_name = $item['param']; ?>

    <? if($item['type'] == Field::TYPE_CHECKBOX): ?>
        <div class="col-lg-10 col-lg-offset-2">
            <div class="form-group">
                <label class="checkbox-inline" style="margin-bottom: 0">
                    <?= Html::activeCheckbox($model, $item_prop_name, [
                        'class' => 'styled'
                    ]) ?>

                    <!-- Begin error -->
                    <? if($model->hasErrors($item_prop_name)): ?>
                    <p class="help-block help-block-error text-danger">
                        <?= $model->getFirstError($item_prop_name) ?>
                    </p>
                    <? endif; ?>
                    <!-- End error  -->

                    <!-- Begin hint -->
                    <? if($item['hint']): ?>
                    <p class="help-block" style="margin-top: 0"><?= $item['hint'] ?></p>
                    <? endif; ?>
                    <!-- End hint -->
                </label>
            </div>
        </div>
    <? else: ?>
        <div class="form-group">
            <label class="control-label col-lg-2" for="<?= Html::getInputId($model, $item_prop_name) ?>">
                <?= $model->getAttributeLabel($item_prop_name) ?>
                <? if($model->isAttributeRequired($item_prop_name)): ?>
                    <span class="text-danger">*</span>
                <? endif; ?>
            </label>
            <div class="col-lg-10 clearfix">
                <? if($item['type'] == Field::TYPE_STRING): ?>
                    <?= Html::activeInput('text', $model, $item_prop_name, [
                        'class' => 'form-control'
                    ]); ?>
                <? endif; ?>

                <? if($item['type'] == Field::TYPE_TEXTAREA): ?>
                    <?= Html::activeTextarea($model, $item_prop_name, [
                        'class' => 'form-control',
                        'rows' => Field::ROWS_TEXTAREA
                    ]); ?>
                <? endif; ?>

                <? if($item['type'] == Field::TYPE_EDITOR): ?>
                    <!-- Begin editor -->
                    <?= \yii\imperavi\Widget::widget(ArrayHelper::merge(
                        [
                            'attribute' => Html::getInputName($model, $item_prop_name),
                            'value' => $model->$item_prop_name
                        ],
                        Yii::$app->params['imperavi'], [
                            'id' => Html::getInputId($model, $item_prop_name),
                            'options' => [
                                'minHeight' => '150px',
                                'uploadImageFields' => [
                                    Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
                                    'Files[item_id]' => $id,
                                    'Files[module]' => StringHelper::basename($model::className()),
                                    'Files[type]' => Files::TYPE_IMAGE
                                ],
                                'uploadFileFields' => [
                                    Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
                                    'Files[item_id]' => $id,
                                    'Files[module]' => StringHelper::basename($model::className()),
                                    'Files[type]' => Files::TYPE_FILE
                                ]
                            ]
                        ]
                    )); ?>
                    <!-- End editor -->
                <? endif; ?>

                <? if($item['type'] == Field::TYPE_FILE): ?>
                    <?= Html::hiddenInput(Html::getInputName($model, $item_prop_name), $model->$item_prop_name) ?>
                    <?= Html::fileInput(Html::getInputName($model, $item_prop_name), null, [
                        'class' => 'file-styled'
                    ]) ?>
                    <? if($model->$item_prop_name): ?>
                        <div class="mt-5" id="buttons-<?= $item_prop_name ?>">
                            <a href="<?= Field::getFileLink($model->$item_prop_name)?>" class="btn bg-teal btn-xs" target="_blank">
                                <i class="icon-file-download2 mr-5"></i>
                                <?= $model->getAttributeLabel($item_prop_name) ?>
                            </a>
                            <a href="#" class="btn btn-danger btn-xs" onclick="removeFieldFile('<?= Html::getInputName($model, $item['param']) ?>', '<?= $item['param'] ?>'); return false;">
                                <i class="icon-bin"></i> Удалить файл
                            </a>
                        </div>
                    <? endif; ?>
                <? endif; ?>

                <? if($item['type'] == Field::TYPE_SELECT): ?>
                    <?= Html::activeDropDownList($model, $item_prop_name, $item['list'], [
                        'prompt' => '-- Выберите значение --',
                        'class' => 'select',
                    ]); ?>
                <? endif; ?>

                <?/* if($item['type'] == Field::TYPE_IMAGE): ?>
                    <?= FilesWidget::widget([
                        'id' => $id,
                        'type' => \common\models\Files::TYPE_IMAGE,
                        'files' => [],
                        'module' => $model::className(),
                        'sizes' => ['100x100'],
                        'hint' => false
                    ]) ?>
                <? endif; */?>

                <!-- Begin error -->
                <? if($model->hasErrors($item_prop_name)): ?>
                <p class="help-block help-block-error text-danger">
                    <?= $model->getFirstError($item_prop_name) ?>
                </p>
                <? endif; ?>
                <!-- End error  -->

                <!-- Begin hint -->
                <? if($item['hint']): ?>
                <p class="help-block"><?= $item['hint'] ?></p>
                <? endif; ?>
                <!-- End hint -->
            </div>
        </div>
    <? endif; ?>
    <? endforeach ?>
<? else: ?>
    <? Alert::begin([
        'options' => ['class' => 'bg-info alert-styled-left'],
        'closeButton' => false
    ]); ?>
        Дополнительных полей для этой записи нет.
    <? Alert::end(); ?>
<? endif ?>
<script>
    /**
     * Delete file name
     * @param  string hiddenName
     * @param  string param
     * @return boolean
     */
    function removeFieldFile(hiddenName, param) {
        if(confirm('Вы уверены что хотите удалить файл?') && hiddenName) {
            $('input[name="' + hiddenName + '"]').val('');
            $('#buttons-' + param).hide();
        }
        return false;
    }
</script>