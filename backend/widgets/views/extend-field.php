<?php

use yii\helpers\Html;
use yii\bootstrap\Alert;
use backend\models\Field;
use \yii\helpers\StringHelper;

$className = StringHelper::basename($model::className());

?>
<div class="table-responsive mb-20">
    <table class="table table-striped">
        <thead>
            <tr>
                <th><?= $model->getAttributeLabel('label') ?></th>
                <th><?= $model->getAttributeLabel('type') ?></th>
                <th class="text-center"><?= $model->getAttributeLabel('param') ?></th>
                <th class="text-center"><?= $model->getAttributeLabel('required') ?></th>
                <th></th>
            </tr>
        </thead>
        <tbody id="field-list">
        <? if(count($fields)): ?>
            <? foreach($fields as $key => $item): ?>
                <tr id="field-<?= $key ?>">
                    <td>
                        <span data-popup="tooltip" title="<?= $item['hint'] ?>">
                            <?= $item['label'] ?>
                        </span>
                        <? if($item['required']): ?>
                            <span class="text-danger">*</span>
                        <? endif; ?>
                    </td>
                    <td><?= Field::getTypeName($item['type']) ?></td>
                    <td class="text-center">
                        <code><?= $item['param'] ?></code>
                    </td>
                    <td class="text-center">
                    <? if($item['required']): ?>
                        <i class="icon-checkmark3 text-success" title="Да"></i>
                    <? else: ?>
                        <i class="icon-cross2 text-danger" title="Нет"></i>
                    <? endif; ?>
                    </td>
                    <td class="text-center">
                        <a href="#" onclick="removeField('<?= $key ?>'); return false;" data-popup="tooltip" title="Удалить параметр">
                            <i class="icon-trash text-danger"></i>
                        </a>
                    </td>
                </tr>
            <? endforeach; ?>
        <? else: ?>
            <tr>
                <td colspan="5">
                    <? Alert::begin([
                        'options' => ['class' => 'bg-info alert-styled-left'],
                        'closeButton' => false
                    ]); ?>
                        Дополнительных полей в этой новостной ленте нет.
                    <? Alert::end(); ?>
                </td>
            </tr>
        <? endif;?>
        </tbody>
    </table>
</div>
<hr>
<div class="col-sm-12">
    <div class="form-horizontal">
        <div class="form-group">
            <label class="control-label col-lg-12 text-bold">
                Создать новое дополнительное поле:
            </label>
            <div class="col-lg-12" id="<?= Html::getInputId($model, 'form') ?>">
                <div class="row">
                    <div class="col-md-3">
                        <?= Html::activeDropDownList($model, 'type', $this->context->types, [
                            'class' => 'select',
                            'prompt' => 'Выберите тип поля *',
                            'onchange' => 'return hiddenList(this.value)'
                        ]) ?>
                    </div>
                    <div class="col-md-3">
                        <?= Html::activeInput('text', $model, 'label', [
                            'class' => 'form-control',
                            'placeholder' => $model->getAttributeLabel('label') . ' *'
                        ]); ?>
                    </div>
                    <div class="col-md-3">
                        <?= Html::activeInput('text', $model, 'param', [
                            'class' => 'form-control',
                            'placeholder' => $model->getAttributeLabel('param') . ' *'
                        ]); ?>
                    </div>
                    <div class="col-md-3">
                        <div class="checkbox">
                            <label>
                                <?= Html::checkbox(Html::getInputName($model, 'required'), $model->required, [
                                    'class' => 'styled',
                                    'id' => Html::getInputId($model, 'required')
                                ]) ?>
                                <?= $model->getAttributeLabel('required') ?>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row mt-10 hidden" id="input-list">
                    <div class="col-sm-9">
                        <?= Html::activeInput('text', $model, 'list', [
                            'class' => 'form-control',
                            'placeholder' => 'Список значений (значения через запятую)'
                        ]); ?>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-9">
                        <?= Html::activeInput('text', $model, 'hint', [
                            'class' => 'form-control',
                            'placeholder' => $model->getAttributeLabel('hint')
                        ]); ?>
                    </div>
                    <div class="col-md-3">
                        <button type="button" onclick="addField(); return false;" class="btn btn-primary btn-xs">
                            <i class="icon-plus3 position-left"></i> Создать новое поле
                        </button>
                    </div>
                </div>
                <div class="row mt-15">
                    <div class="col-md-12">
                        <small class="help-block">Дополнительные поля значительно упрощают кастомизацию модулей. К тому же благодаря дополнительным полям можно управлять содержимым модуля: выводить нужного размера картинку или ссылку на файл в нужном Вам месте, оформить оригинально блок ссылок, упорядочить описание к той или иной записи.</small>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    /**
     * Remove an additional field
     * @param  string key
     */
    function removeField(key) {
        if(!confirm('Вы уверены что хотите удалить поле?\nПосле удаления поля все данные и файлы будут так же удалены.')) {
            return;
        }

        $.get('/<?= $this->context->controller ?>/removefield', {
                id: <?= $this->context->id ?>,
                key: key
        }, function(Response) {
            if(Response.Notify) {
                new PNotify(Response.Notify);
            }

            if(typeof Response.key != 'undefined') {
                $('#field-' + Response.key).remove();
            }
        }, 'json');
    }

    /**
     * Adding an additional field
     */
    function addField() {
        var options = {};
        $('[id^="<?= mb_strtolower($className) ?>-"]').each(function(_, input) {
            var $input = $(input);

            if($input.prop('type') == 'checkbox') {
                options[$input.attr('name')] = $input.is(':checked') ? 1 : 0;
            } else {
                options[$input.attr('name')] = $input.val();
            }
        });

        $.get('/<?= $this->context->controller ?>/addfield', $.extend({
            id: <?= $this->context->id ?>
        }, options), function(Response) {
            if(Response.Notify) {
                new PNotify(Response.Notify);
            }

            if(Response.reload == true) {
                location.reload();
            }
        }, 'json');
    }

    /**
     * Field list, input control
     * @param  String value
     * @return Boolean
     */
    function hiddenList(value) {
        $inputList = $('#input-list');
        if(value == '<?= Field::TYPE_SELECT ?>') {
            $inputList.removeClass('hidden');
        } else {
            $inputList.addClass('hidden');
        }
        return false;
    }
</script>